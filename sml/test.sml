
(* This is a comment, this is our first program *)
(* dynamically enviroment *)

val x = 1 + 2;
val y = x + 3;

val x = 1;

val z = 0 - ((x + y) + (y + 2));

val q = 1;

val abs_of_z = if z < 0 then 0 - z else z;

val abs_of_z_simper = abs z;

val error = ~5;


val a = 1;
val a = 2;

fun pow(x : int, y : int) =
  if y = 0 then 1
  else x * pow(x, y-1)

fun cube(x : int) =
  pow(x, 3)



