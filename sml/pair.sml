
fun swap(pr : int * bool) =
  (#2 pr, #1 pr)

fun div_mod (x : int, y : int) =
  (x div y, x mod y)

fun sort_int (pr : int * int) =
  if (#2 pr) > (#1 pr) then
      pr
  else
      (#2 pr, #1 pr)


fun count_down(x : int) =
  if x = 0 then []
  else x :: count_down(x -1);


fun countup_from1(x : int) =
    let
        fun count(from : int, to : int) =
          if from = to then from::[]
          else from::count(from + 1, to)
    in count(1, x)
    end;

fun countup_from1_better(to : int) =
  let
      fun count(from :int ) =
        if from = to then from::[]
        else from::count(from+1)
  in count 1 end;

fun bad_max(xs : int list) =
  if null xs then 0
  else if null (tl xs) then hd xs
  else if (hd xs) > bad_max(tl xs) then (hd xs)
  else bad_max(tl xs);

fun good_max(xs : int list) =
  if null xs then 0
  else if null (tl xs) then
      hd xs
  else
      let
          val left_max =  good_max(tl xs)
      in
          if (hd xs) > left_max then (hd xs)
          else left_max
      end;

fun good_max(xs : int list) =
  if null xs then NONE
  else if null (tl xs) then
      SOME(hd xs)
  else
      let
          val left_max =  good_max(tl xs)
      in
          if isSome left_max andalso (hd xs) > valOf left_max then SOME(hd xs)
          else left_max
      end;










