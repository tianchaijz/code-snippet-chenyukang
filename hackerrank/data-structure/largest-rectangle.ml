open Core.Std
open Str

let next_int() =
  Scanf.bscanf Scanf.Scanning.stdin "%d " (fun x -> x)

let read_ints n =
  let res = ref [] in
  for _ = 1 to n do
    let n = next_int() in
    res := !res @ [n]
  done;
  !res


let () =
  let n = read_int() in
  let hs = read_ints n in
  let stack = Stack.create() in
  let max_area = ref 0 in
  let i = ref 0 in
  while !i < List.length hs do
    let h = List.nth_exn hs !i in
    let len = Stack.length stack in
    if (len = 0 || (List.nth_exn hs (Stack.top_exn stack)) <= h) then (
      Stack.push stack !i;
      incr i
    )
    else (
      let tp = Stack.pop_exn stack in
      let pi = if (Stack.length stack = 0) then (-1) else Stack.top_exn stack in
      let cur_area = (List.nth_exn hs tp) * (!i - pi - 1) in
      max_area := Int.max !max_area cur_area;
    )
  done;

  while (Stack.length stack > 0) do
    let tp = Stack.pop_exn stack in
    let len = List.length hs in
    let pi = if (Stack.length stack = 0) then (-1) else Stack.top_exn stack in
    let cur_area = (List.nth_exn hs tp) * (len - pi - 1) in
    max_area := Int.max !max_area cur_area
  done;
  Printf.printf "%d" !max_area
