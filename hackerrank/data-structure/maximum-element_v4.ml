open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

type stack = {
  mutable max_stack : int Stack.t;
  mutable val_stack : int Stack.t;
}


let make_stack() =
  let s =
  {
    max_stack = Stack.create();
    val_stack = Stack.create();
  } in
  Stack.push s.max_stack Int.min_value;
  s

let push stack e =
  Stack.push stack.val_stack e;
  match Stack.top stack.max_stack with
  | Some(x) when x <= e ->
    Stack.push stack.max_stack e
  | _ -> ()

let pop stack =
  let res = Stack.pop stack.val_stack in
  match res with
  | Some(x) -> (
      match Stack.top stack.max_stack with
      | Some(m) when m = x ->
        ignore(Stack.pop stack.max_stack)
      | _ -> ()
    )
  | _ -> ()


let query stack =
  match Stack.top stack.max_stack with
  | Some(x) -> x
  | _ -> failwith "error for query"

let () =
  let s = make_stack() in
  let n = read_int() in
  for i = 1 to n do
    let ints = read_ints() in
    match ints with
        | [1; x] -> ignore(push s x)
        | [2] -> ignore(pop s)
        | _ -> Printf.printf "%d\n" (query s)
  done

