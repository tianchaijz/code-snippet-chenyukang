open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)


let () =
  let t = read_int() in
  for _ = 1 to t do
    let _ = read_int() in
    let piles = read_ints() in
    let res = ref (List.nth_exn piles 0) in
    List.iteri piles ~f:(fun i x ->
                         if i >= 1 then
                           res := Int.bit_xor !res x
      );
    let all_less_than_1 = (List.find piles ~f:(fun x -> x > 1 )) = None in
    if (all_less_than_1 && !res = 1) || ((all_less_than_1 <> true) && !res = 0) then
      Printf.printf "Second\n"
    else
      Printf.printf "First\n"
  done

