open Core.Std
open Str


type node = {
  mutable count : int;
  mutable children : (node option) array;
}

let make_node() =
  {
    count = 0;
    children = Array.create ~len:(Char.to_int 'j' - Char.to_int 'a' + 1) None;
  }

let index str =
  let c = String.get str 0 in
  (Char.to_int c) - (Char.to_int 'a')


let rec add_str node str =
  if str <> "" then
    let i = index str in
    let next = Array.get node.children i in
    let left = String.sub str 1 (String.length str - 1) in
    match next with
    | None -> (
        let new_node = make_node() in
        Array.set node.children i (Some new_node);
        add_str new_node left
      )
    | Some(next_node) -> (
        if next_node.count > 0 then false
        else add_str next_node left
      )
  else (
    if node.count = 0 then (
      if Array.exists node.children ~f:(fun x -> x <> None) then false
      else (node.count <- 1; true)
    )
    else false
  )

let () =
  let root = make_node() in
  let n = read_int() in
  let res = ref "" in
  for _ = 1 to n do
    let str = read_line() in
    if (!res = "") && (add_str root str = false) then
      res := str
  done;
  if !res <> "" then
    Printf.printf "BAD SET\n%s" !res
  else
    Printf.printf "GOOD SET"
