open Core.Std
open Str
open Hash_heap

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let () =
  match read_ints() with
  | [_; k] -> (
      let cookies = read_ints() in
      let heap = Heap.of_list cookies ~cmp:Int.compare in
      let finished = ref false in
      let res = ref 0 in
      if Heap.top_exn heap >= k then
        finished := true
      else (
        while (Heap.length heap >= 2) && !finished = false do
          if Heap.top_exn heap >= k then
            finished := true
          else (
            let e1 = Heap.pop_exn heap in
            let e2 = Heap.pop_exn heap in
            incr res;
            Heap.add heap (e1 + e2 * 2)
          )
        done;
      );
      if !finished = false then
        Printf.printf "-1"
      else
        Printf.printf "%d" !res
    )
  | _ -> ()


