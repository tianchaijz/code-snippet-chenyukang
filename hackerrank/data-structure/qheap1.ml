open Core.Std
open Str

module PrioQueue =
    struct
      type elem = int
      type 'a queue = Empty | Node of elem * 'a queue * 'a queue
      let empty = Empty
      let rec insert queue elt =
        match queue with
          Empty -> Node(elt, Empty, Empty)
        | Node(e, left, right) ->
            if elt <= e
            then Node(elt, insert right e, left)
            else Node(e, insert right elt, left)
      exception Queue_is_empty
      let rec remove_top = function
          Empty -> raise Queue_is_empty
        | Node(elt, left, Empty) -> left
        | Node(elt, Empty, right) -> right
        | Node(elt, (Node(lelt, _, _) as left),
               (Node(relt, _, _) as right)) ->
            if lelt <= relt
            then Node(lelt, remove_top left, right)
            else Node(relt, left, remove_top right)
      let extract = function
          Empty -> raise Queue_is_empty
        | Node(elt, _, _) as queue -> (elt, remove_top queue)

      let query_top = function
        | Empty -> raise Queue_is_empty
        | Node(elt, _, _) -> elt

      let rec remove_elem queue elem =
        match queue with
        | Empty -> Empty
        | Node(elt, left, right) -> (
            match elt with
            | _ when elt = elem -> remove_top queue
            | _ when elt > elem -> queue
            | _ -> (
                Node(elt, remove_elem left elem, remove_elem right elem)
              )
          )
    end;;

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)


let () =
  let n = read_int() in
  let heap = ref PrioQueue.empty in
  for _ = 1 to n do
    let ns = read_ints() in
    match ns with
    | [1; v] -> (
        heap := PrioQueue.insert !heap v
      )
    | [3] -> Printf.printf "%d\n" (PrioQueue.query_top !heap)
    | [2; v] -> heap := PrioQueue.remove_elem !heap v
    | _ -> failwith "error input"
  done;

