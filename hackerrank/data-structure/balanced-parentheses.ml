open Core.Std
open Str

let solve str =
  let s = Stack.create() in
  match String.find str ~f:(fun c ->
      match c with
      | '{' | '[' | '(' -> (Stack.push s c; false)
      | '}' | ']' | ')' -> (
          match Stack.pop s with
          | None -> true
          | Some(x) -> (
              match x with
              | _ when x = '{' && c = '}' -> false
              | _ when x = '(' && c = ')' -> false
              | _ when x = '[' && c = ']' -> false
              | _ -> true
            )
        )
      | _ -> failwith "error") with
  | None -> (if Stack.length s = 0 then "YES" else "NO")
  | _ -> "NO"

let () =
  let n = read_int() in
  for _ = 1 to n do
    let str = read_line() in
    Printf.printf "%s\n" (solve str);
  done
