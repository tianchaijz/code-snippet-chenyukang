open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

type stack = {
  mutable keys : int array;
  mutable _stack : int Stack.t;
}

let make_stack() =
  {
    keys = Array.create ~len:0 0;
    _stack = Stack.create();
  }

let rec find_post keys key head tail =
  (* Printf.printf "head: %d tail: %d\n" head tail; *)
  if head > tail then
    head
  else (
    let mid = (head + tail) / 2 in
    let kv = Array.nget keys mid in
    match kv with
    | _ when kv > key -> find_post keys key head (mid-1)
    | _ when kv < key -> find_post keys key (mid+1) tail
    | _ -> mid
  )

let insert_key keys key =
  let pos = find_post keys key 0 (Array.length keys - 1) in
  (* Printf.printf "pos:%d len:%d\n" pos (Array.length keys); *)
  match pos with
  | _ when pos <= 0 -> Array.append [|key|] keys
  | _ when pos >= Array.length keys -> Array.append keys [|key|]
  | _ -> (
      let head = Array.slice keys 0 pos in
      let tail = Array.slice keys pos (Array.length keys) in
      Array.append head (Array.append [|key|] tail))
and
  remove_key keys key =
  let pos = find_post keys key 0 (Array.length keys - 1) in
  (* Printf.printf "pos: %d\n" pos; *)
  let head =
    if pos <= 0 then [||]
    else Array.slice keys 0 pos in
  let tail = Array.slice keys (pos+1) (Array.length keys) in
  Array.append head tail

let push stack elem =
  Stack.push stack._stack elem;
  stack.keys <- insert_key stack.keys elem;
  stack


let pop stack =
  let res = Stack.pop stack._stack in
  match res with
  | Some(elem) ->
    stack.keys <- remove_key stack.keys elem
  | _ -> ()


let () =
  let s = make_stack() in
  let n = read_int() in
  for _ = 1 to n do
    let ints = read_ints() in
    match ints with
        | [1; x] -> ignore(push s x)
        | [2] -> ignore(pop s)
        | _ -> Printf.printf "%d\n" (Array.last s.keys)
  done


