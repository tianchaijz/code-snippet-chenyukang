open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

type elem = {
  value : int;
  index : int;
}

type stack = {
  mutable max_stack : elem Stack.t;
  mutable val_stack : elem Stack.t;
}


let make_stack() =
  {
    max_stack = Stack.create();
    val_stack = Stack.create();
  }

let push stack e i =
  Stack.push stack.val_stack { value = e; index = i};
  match Stack.top stack.max_stack with
  | Some(x) when x.value < e ->
    Stack.push stack.max_stack { value = e; index = i}
  | None ->
    Stack.push stack.max_stack { value = e; index = i}
  | _ -> ()


let pop stack =
  let res = Stack.pop stack.val_stack in
  match res with
  | Some(x) -> (
      match Stack.top stack.max_stack with
      | Some(m) when m.value = x.value && m.index = x.index ->
        ignore(Stack.pop stack.max_stack)
      | _ -> ()
    )
  | _ -> ()


let query stack =
  match Stack.top stack.max_stack with
  | Some(x) -> x.value
  | _ -> failwith "error for query"

let () =
  let s = make_stack() in
  let n = read_int() in
  for i = 1 to n do
    let ints = read_ints() in
    match ints with
        | [1; x] -> ignore(push s x i)
        | [2] -> ignore(pop s)
        | _ -> Printf.printf "%d\n" (query s)
  done

