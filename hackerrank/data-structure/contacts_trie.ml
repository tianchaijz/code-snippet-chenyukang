open Core.Std
open Str

type node = {
  mutable count : int;
  mutable children: (node option) array;
}

let make_node() =
  {
    count = 1;
    children = Array.create ~len:26 None;
  }

let index str =
  let c = String.get str 0 in
  (Char.to_int c) - (Char.to_int 'a')

let rec add_str node str =
  if String.length str > 0 then
    let i = index str in
    let next = Array.get node.children i in
    let left = String.sub str 1 (String.length str - 1) in
    match next with
    | None -> (
        let new_node = make_node() in
        Array.set node.children i (Some new_node);
        add_str new_node left
      )
    | Some(next_node) -> (
        next_node.count <- next_node.count + 1;
        add_str next_node left
      )
  else
    node

let rec find_str node str cur =
  if String.length str > 0 then
    let i = index str in
    let next = Array.get node.children i in
    let left = String.sub str 1 (String.length str - 1) in
    match next with
    | None -> 0
    | Some(next_node) -> (
        find_str next_node left next_node.count
      )
  else
    cur

let () =
  let root = make_node() in
  let n = read_int() in
  for _ = 1 to n do
    let words = Str.split (Str.regexp "[ \n]+") (read_line()) in
    let key = List.nth_exn words 0 in
    let word = List.nth_exn words 1 in
    if String.equal key "add" then
      ignore(add_str root word)
    else (
      let count = find_str root word 0 in
      Printf.printf "%d\n" count
    )
  done;;

