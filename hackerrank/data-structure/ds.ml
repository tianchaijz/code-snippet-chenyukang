open Core.Std
open Str

let solve() =
  let _ = read_int() in
  let vals = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.iter  (List.rev vals)
    ~f:(fun x -> Printf.printf "%d " (int_of_string x));;

let () =
  solve();;
