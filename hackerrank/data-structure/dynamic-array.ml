open Core.Std
open Str

let read_ints() =
  let vals = Str.split (Str.regexp "[ \n]+") (read_line()) in
  Array.of_list (List.map vals ~f:(fun x -> int_of_string x))

let () =
  let nq = read_ints() in
  let n = nq.(0) in
  let query_num = nq.(1) in
  let seq_list = Array.create ~len:n [] in
  let last_ans = ref 0 in
  for i = 1 to query_num do
    let ints  = read_ints() in
    let q, x, y = ints.(0), ints.(1), ints.(2) in
    let index = (Int.bit_xor x !last_ans) mod n in
    let list = Array.nget seq_list index in
    match q with
    | 1 -> (
        Array.set seq_list index (List.append list [y]);
      )
    | _ -> (
        last_ans := List.nth_exn list (y mod (List.length list));
        Printf.printf "%d\n" !last_ans;
      );
  done;;
