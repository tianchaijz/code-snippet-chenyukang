open Core.Std
open Str

module PrioQueue =
    struct
      type elem = int
      type 'a queue = Empty | Node of elem * 'a queue * 'a queue
      let empty = Empty
      let rec insert queue elt =
        match queue with
          Empty -> Node(elt, Empty, Empty)
        | Node(e, left, right) ->
            if elt <= e
            then Node(elt, insert right e, left)
            else Node(e, insert right elt, left)
      exception Queue_is_empty
      let rec remove_top = function
          Empty -> raise Queue_is_empty
        | Node(elt, left, Empty) -> left
        | Node(elt, Empty, right) -> right
        | Node(elt, (Node(lelt, _, _) as left),
               (Node(relt, _, _) as right)) ->
            if lelt <= relt
            then Node(lelt, remove_top left, right)
            else Node(relt, left, remove_top right)
      let extract = function
          Empty -> raise Queue_is_empty
        | Node(elt, _, _) as queue -> (elt, remove_top queue)

      let query_top = function
        | Empty -> raise Queue_is_empty
        | Node(elt, _, _) -> elt

    end;;

type queue_ty = {
  mutable queue : int PrioQueue.queue;
  mutable count : int
}

let push queue elem =
  queue.queue <- PrioQueue.insert queue.queue elem;
  queue.count <- queue.count + 1

let pop queue =
  if queue.count <= 0 then
    failwith "empty queue"
  else (
    let e, left = PrioQueue.extract queue.queue in
    queue.queue <- left;
    queue.count <- queue.count - 1;
    e
  )

let count queue =
  queue.count

let top queue =
  PrioQueue.query_top queue.queue

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)


let () =
  match read_ints() with
  | [_; k] -> (
      let cookies = read_ints() in
      let queue = { queue = PrioQueue.empty; count = 0 } in
      List.iter cookies ~f:(fun x -> push queue x);
      let finished = ref false in
      let res = ref 0 in
      while (count queue >= 2) && !finished = false do
        let e1 = pop queue in
        let e2 = pop queue in
        incr res;
        push queue (e1 + e2 * 2);
        if top queue >= k then (
          finished := true;
          Printf.printf "%d" !res
        )
      done;
      if !finished = false then
        Printf.printf "-1"
    )
  | _ -> ()

