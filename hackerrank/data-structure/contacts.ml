open Core.Std
open Str


let () =
  let dict = ref [] in
  let n = read_int() in
  for i = 1 to n do
    let words = Str.split (Str.regexp "[ \n]+") (read_line()) in
    let key = List.nth_exn words 0 in
    let word = List.nth_exn words 1 in
    if String.equal key "add" then
      dict := !dict @ [word]
    else (
      let count = List.length (List.filter !dict ~f:(fun x -> String.is_prefix x ~prefix: word)) in
      Printf.printf "%d\n" count
    )
  done;;

