open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let () =
  let n = read_int() in
  let hash = Hash_heap.Heap.create ~min_size: 100000 ~cmp:Int.compare () in
  for _ = 1 to n do
    let ns = read_ints() in
    match ns with
    | [1; v] -> (
        Hash_heap.Heap.add hash v
      )
    | [3] -> Printf.printf "%d\n" (Hash_heap.Heap.top_exn hash)
    | [2; v] -> (
        let _v = Hash_heap.Heap.find_elt hash ~f:(fun x -> v = x ) in
        match _v with
        | Some(m) ->
          Hash_heap.Heap.remove hash m
        | _ -> failwith "cannot found"
      )
    | _ -> failwith "error input"
  done;

