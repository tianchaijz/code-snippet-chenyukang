open Core.Std
open Str

let read_ints() =
  let vals = Str.split (Str.regexp "[ \n]+") (read_line()) in
  Array.of_list (List.map vals ~f:(fun x -> int_of_string x))

let solve() =
  let rec loop i res =
    if i = 6 then res
    else(
      let ints = read_ints() in
      loop (i+1) (Array.append res [|ints|])
    ) in
  let matrix = loop 0 [||] in
  let max = ref Int.min_value in
  for i = 1 to 4 do
    for j = 1 to 4 do
      let v = matrix.(i-1).(j) + matrix.(i+1).(j) +
              matrix.(i-1).(j+1) + matrix.(i+1).(j-1) +
              matrix.(i-1).(j-1) + matrix.(i+1).(j+1) +
              matrix.(i).(j)
      in
      max := Int.max !max v;
    done
  done;
  !max;;

let () =
  Printf.printf "%d\n" (solve());;

