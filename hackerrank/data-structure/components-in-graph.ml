open Core.Std
open Str

type node = {
  mutable parent : int;
  mutable rank : int;
  mutable count : int;
}

type union_ty = node array

let create_union size =
  Array.init size (fun i -> { parent = i; rank = 0; count = 1 })

let rec find u e =
  let n = u.(e) in
  if n.parent = e then (e, u.(e))
  else (
    let p, pn = find u n.parent in
    n.parent <- p;
    (p, pn)
  )

let set_count n1 n2 =
  let n = n1.count + n2.count in
  n1.count <- n;
  n2.count <- n

let union u e1 e2 =
  let r1, n1 = find u e1
  and r2, n2 = find u e2 in
  if (compare r1 r2) <> 0 then
    if n1.rank < n2.rank then (
      n1.parent <- e2;
      set_count n1 n2
    )
    else
      (n2.parent <- e1;
       set_count n1 n2;
       if n1.rank = n2.rank then
         n1.rank <- n1.rank + 1);
  ()


let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let () =
  let size = 15001 * 2 + 1 in
  let m = read_int() in
  let u = create_union size in
  let vs = Array.init size ~f:(fun _ -> 0) in
  for _ = 1 to m do
    match read_ints() with
    | [a; b] -> (
        union u a b;
        Array.set vs a 1;
        Array.set vs b 1
      )
    | _ -> failwith "error"
  done;
  let max = ref Int.min_value in
  let min = ref Int.max_value in
  Array.iteri u ~f:(fun i e ->
      if Array.nget vs i > 0 then (
        let _, pn = find u i in
        max := Int.max pn.count !max;
        min := Int.min pn.count !min;
      )
    );
  Printf.printf "%d %d\n" !min !max

