open Core.Std
open Str


let read_input s =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  match elems with
  | ["1"; w] -> (
      match Stack.top s with
      | None -> Stack.push s w
      | Some(t) -> Stack.push s (t ^ w)
    )
  | ["2"; k] -> (
      let k = Int.of_string k in
      match Stack.top s with
      | None -> failwith "error delete"
      | Some(t) -> (
          let l = String.length t - k in
          let v = if l = 0 then "" else String.slice t 0 l in
          Stack.push s v
        )
    )
  | ["3"; k] -> (
      let k = Int.of_string k in
      match Stack.top s with
      | None -> failwith "error display"
      | Some(t) -> (
          Printf.printf "%c\n" (String.nget t (k-1))
        )
    )
  | ["4"] -> (
      let _ = Stack.pop s in ()
    )
  | _ -> ()

let () =
  let s = Stack.create() in
  let n = read_int() in
  for _ = 1 to n do
    read_input s
  done

