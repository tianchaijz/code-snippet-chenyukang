open Core.Std
open Str

let () =
  let hash = Hashtbl.Poly.create() in
  let n = read_int() in
  for i = 1 to n do
    let str = read_line() in
    let n = (match Hashtbl.find hash str with
      | Some(n) -> n + 1
      | None -> 1) in
    Hashtbl.set hash ~key:str ~data:n;
  done;

  let q = read_int() in
  for i = 1 to q do
    let key = read_line() in
    let n = match Hashtbl.find hash key with
      | Some(n) -> n
      | None -> 0 in
    Printf.printf "%d\n" n;
  done;;







