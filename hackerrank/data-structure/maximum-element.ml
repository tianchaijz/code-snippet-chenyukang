open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

type stack = {
  mutable count : int;
  mutable max : int;
  mutable elems : int array;
}

let make_stack() =
  {
    count = 0;
    max = Int.min_value;
    elems = Array.create ~len:0  0;
  }

let push_stack stack elem =
  stack.elems <- Array.append stack.elems [|elem|];
  let _ = match elem with
    | _ when elem = stack.max ->
      stack.count <- stack.count + 1
    | _ when elem > stack.max -> (
        stack.max <- elem;
        stack.count <- 1
      )
    | _ -> ()
  in stack

let pop_stack stack =
  let last = Array.last stack.elems in
  let len = Array.length stack.elems in
  stack.elems <- (
    if len = 1 then [||]
    else Array.slice stack.elems 0 (len - 1));
  let _ = match last with
  | _ when last = stack.max -> (
      stack.count <- stack.count - 1;
      if stack.count = 0 then (
        let count = ref 0 in
        (* this is O(N) for one query *)
        let max = Array.fold stack.elems ~init:Int.min_value
            ~f:(fun m x ->
                match x with
                | _ when x = m -> incr count; m
                | _ when x > m -> count := 1; x
                | _ -> m) in
        stack.count <- !count;
        stack.max <- max;
      )
    )
  | _ -> () in
  stack

let () =
  let s = make_stack() in
  let n = read_int() in
  for _ = 1 to n do
    let ints = read_ints() in
    match ints with
        | [1; x] -> ignore(push_stack s x)
        | [2] -> ignore(pop_stack s)
        | _ -> Printf.printf "%d\n" s.max
  done

