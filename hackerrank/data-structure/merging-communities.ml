open Core.Std
open Str


type node = {
  mutable parent : int;
  mutable rank : int;
  mutable count : int;
}

type union_ty = node array

let create_union size =
  Array.init size (fun i -> { parent = i; rank = 0; count = 1})

let print_uion u =
  Array.iteri u ~f:(fun i e ->
      Printf.printf "%d:(p:%d r:%d c:%d) " i e.parent e.rank e.count
    );
  Printf.printf "\n"

let rec find u e =
  let n = u.(e) in
  if n.parent = e then (e, u.(e))
  else (
    let p, pn = find u n.parent in
    n.parent <- p;
    (p, pn)
  )

let union u e1 e2 =
  let r1, n1 = find u e1
  and r2, n2 = find u e2 in
  if (compare r1 r2) <> 0 then
    if n1.rank < n2.rank then (
      n1.parent <- e2;
      n2.count <- n2.count + n1.count
    )
    else
      (n2.parent <- e1;
       n1.count <- n1.count + n2.count;
       if n1.rank = n2.rank then
         n1.rank <- n1.rank + 1);
  (* print_uion u; *)
  ()

let query u v =
  let _, e = find u v in
  e.count

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let read_elems() =
  Str.split (Str.regexp "[ \n]+") (read_line())

let () =
  let n = ref 0 in
  let q = ref 0 in
  let _ = match read_ints() with
  | [_n; _q] -> (n := _n ; q := _q)
  | _ -> failwith "input error" in
  let u = create_union (!n + 1) in
  for _ = 1 to !q do
    match read_elems() with
      | ["Q"; v] -> (
          let v = Int.of_string v in
          Printf.printf "%d\n" (query u v)
        )
      | ["M"; a; b] -> (
          let a = Int.of_string a in
          let b = Int.of_string b in
          union u a b
        )
      | _ -> ()
  done;
  ()

