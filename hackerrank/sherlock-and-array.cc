#include <iostream>
#include <vector>
#include <string.h>
#include <stdio.h>
using namespace std;

int main() {
    int T, N, t;
    vector<int> arr;

    cin >> T;
    while(T--) {
        cin >> N;
        arr.clear();
        for(int i=0; i<N; i++) {
            cin >> t;
            arr.push_back(t);
        }
        int left = 0;
        int right = 0;
        int rr;
        for(int i=0; i<arr.size(); i++) {
            right += arr[i];
        }
        bool ans = false;
        for(int i=0; i<arr.size(); i++) {
            rr = (i == arr.size() - 1) ? 0 : right - arr[i];
            if(left == rr) {
                ans = true;
                break;
            }
            left += arr[i];
            right -= arr[i];
        }
        if(ans) {
            std::cout << "YES" << std::endl;
        } else {
            std::cout << "NO" << std::endl;
        }
    }
    return 0;
}
