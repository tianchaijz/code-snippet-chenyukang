#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int minDelete(string s) {
    int maxLen = 1;
    char now = s[0];
    for(int i=1; i<s.size(); i++) {
        if(now != s[i]) {
            maxLen++;
            now = s[i];
        }
    }
    return s.size() - maxLen;
}

int main() {
    int T;
    string s;
    cin >> T;
    while(T--) {
        cin >> s;
        std::cout << minDelete(s) << std::endl;
    }
    return 0;
}
