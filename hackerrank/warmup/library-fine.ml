open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  let lst = List.map elems ~f:(fun x -> Int.of_string x) in
  (List.nth_exn lst 0, List.nth_exn lst 1, List.nth_exn lst 2)

let () =
  let d1, m1, y1 = read_ints() in
  let d2, m2, y2 = read_ints() in
  let res = match y1, y2 with
    | _ when y1 < y2 -> 0
    | _ when y1 > y2 -> 10000
    | _ -> (
        match m1, m2 with
        | _ when m1 < m2 -> 0
        | _ when m1 > m2 -> (m1 - m2) * 500
        | _ -> (
            match d1, d2 with
            | _ when d1 < d2 -> 0
            | _ when d1 > d2 -> (d1 - d2) * 15
            | _ -> 0
          )
      ) in
  Printf.printf "%d\n" res
