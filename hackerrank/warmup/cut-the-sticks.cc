#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <math.h>
using namespace std;


int main() {
    int N, v;
    int lowest = numeric_limits<int>::max();
    vector<int> vec;
    cin >> N;
    for(int i=0; i<N; i++) {
        cin >> v;
        lowest = min(lowest, v);
        vec.push_back(v);
    }
    while(1) {
        vector<int> buf;
        int cut = 0;
        int next_lowest = numeric_limits<int>::max();
        for(int i=0; i<vec.size(); i++) {
            if(vec[i] - lowest >= 0) {
                cut += 1;
                v = vec[i] - lowest;
                if(v > 0) {
                    buf.push_back(vec[i] - lowest);
                    next_lowest = min(vec[i] - lowest, next_lowest);
                }
            }
        }
        std::cout << cut << std::endl;
        vec.swap(buf);
        lowest = next_lowest;
        if(lowest == numeric_limits<int>::max()) {
            break;
        }
    }
    return 0;
}
