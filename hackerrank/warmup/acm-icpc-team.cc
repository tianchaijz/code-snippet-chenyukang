#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int eval(string& a, string& b) {
    int ans = 0;
    for(int i=0; i<a.size(); i++) {
        if(a[i] == '1' || b[i] == '1')
            ans++;
    }
    return ans;
}

int main() {
    int N, M, cnt, val, max;
    vector<string> vec;
    string s;
    max = numeric_limits<int>::min();
    cnt = 0;
    cin >> N >> M;
    for(int i=0; i<N; i++) {
        cin >> s;
        vec.push_back(s);
    }
    for(int i=0; i<N-1; i++) {
        for(int j=i+1; j<N; j++) {
            val = eval(vec[i], vec[j]);
            if(val > max) {
                max = val;
                cnt = 1;
            } else if(val == max) {
                cnt++;
            }
        }
    }
    std::cout << max << std::endl;
    std::cout << cnt << std::endl;
    return 0;
}
