open Core.Std
open Str

let () =
  let str = read_line() in
  let last_2 = String.slice str 8 10 in
  let first_2 = String.slice str 0 2 in
  let middle = String.slice str 2 8 in
  let res = match last_2 with
  | "AM" -> (
      match first_2 with
      | "12" -> "00" ^ middle
      | _ -> first_2 ^ middle
    )
  | "PM" -> (
      match first_2 with
      | "12" -> "12" ^ middle
      | _ -> (string_of_int (int_of_string first_2 + 12)) ^ middle
    )
  | _ -> failwith "error"
  in
  Printf.printf "%s" res
