#include <iostream>
using namespace std;

int height(int n) {
    int ans = 1;
    for(int i=1; i<=n; i++) {
        if(i%2 == 1) {
            ans *= 2;
        } else {
            ans++;
        }
    }
    return ans;
}

int main() {
    int T;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cout << height(n) << endl;
    }
}
