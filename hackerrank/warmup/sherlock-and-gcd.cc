#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int gcd(int a, int b) {
    if(b == 0) return a;
    return gcd(b, a%b);
}

int main() {
    int T, n, v, cur;
    bool yes = false;
    cin >> T;
    while(T--) {
        cin >> n;
        yes = false;
        cur = 0;
        while(n--) {
            cin >> v;
            if(!yes){
                cur = gcd(v, cur);
                if(cur == 1)
                    yes = true;
            }
        }
        if(yes) std::cout << "YES" << std::endl;
        else std::cout << "NO" << std::endl;
    }
    return 0;
}
