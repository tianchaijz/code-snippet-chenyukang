open Core.Std
open Str

let () =
  let n = read_int() in
  let v = ref (Big_int.big_int_of_int 1) in
  for i = 1 to n do
    v := Big_int.mult_big_int !v (Big_int.big_int_of_int i)
  done;
  Printf.printf "%s\n" (Big_int.string_of_big_int !v)
