open Core.Std
open Str

let () =
  let n = read_int() in
  for _ = 1 to n do
    let b, w = Scanf.sscanf (read_line()) "%d %d" (fun b w -> b, w) in
    let x, y, z = Scanf.sscanf (read_line()) "%d %d %d" (fun x y z -> x, y, z) in
    let res = (b * (Int.min x (y + z))) + (w * (Int.min y (x + z))) in
    Printf.printf "%d\n" res
  done

