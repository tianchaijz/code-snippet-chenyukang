#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int findDigits(int N) {
    int ans = 0;
    int t = N;
    while(t > 0) {
        int bit = t%10;
        if( bit > 0 && (bit * (N/bit)) == N) {
            ans++;
        }
        t /= 10;
    }
    return ans;
}


int main() {
    int T, N;
    cin >> T;
    while(T--) {
        cin >> N;
        std::cout << findDigits(N) << std::endl;
    }

    return 0;
}
