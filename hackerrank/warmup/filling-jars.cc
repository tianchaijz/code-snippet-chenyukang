#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

typedef unsigned long long LL;

int main() {
    LL N, M;
    cin >> N >> M;
    LL a, b, k;
    LL sum = 0;
    while(M--) {
        cin >> a >> b >> k;
        sum += (b + 1 - a) * k;
    }
    LL ans = sum / N;
    std::cout << ans << std::endl;
    return 0;
}
