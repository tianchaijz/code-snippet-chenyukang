#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <string.h>
using namespace std;

#define idx(c) ((c)-'a')
int buf[30];

bool testValid(string s) {
    int cnt = 0;
    memset(buf, 0, sizeof(buf));
    for(int i=0; i<s.size(); i++) {
        buf[idx(s[i])]++;
    }
    for(int i=0; i<26; i++) {
        if(buf[i]%2 != 0) {
            cnt++;
        }
    }
    return (cnt == 1 || cnt == 0);
}

int main() {
    string s;
    cin>>s;

    bool flag = testValid(s);

    if(flag)
        cout<<"YES";
    else
        cout<<"NO";
    return 0;
}
