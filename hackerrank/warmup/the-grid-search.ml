open Core.Std
open Str


let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let read_map() =
  match read_ints() with
  | [r; c] -> (
      let map = Array.init r (fun _ -> String.to_array (read_line())) in
      (r, c, map)
    )
  | _ -> failwith "error"

exception EndLoop
let solve() =
  let r, c, map = read_map() in
  let _r, _c, pat = read_map() in
  let good x y =
    if x + _r - 1 >= r || y + _c - 1 >= c then false
    else
      try
        for _x = 0 to _r - 1 do
          for _y = 0 to _c - 1 do
            if pat.(_x).(_y) <> map.(x + _x).(y + _y) then raise EndLoop
          done
        done;
        true
       with EndLoop -> false
  in
  try
    for x1 = 0 to r - 1 do
      for y1 = 0 to c - 1 do
        if good x1 y1 then raise EndLoop
      done;
    done;
    Printf.printf "NO\n"
   with EndLoop -> Printf.printf "YES\n"


let () =
  let t = read_int() in
  for _ = 1 to t do
    ignore(solve())
  done

