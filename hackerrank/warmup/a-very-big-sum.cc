#include <stdio.h>
#include <iostream>
using namespace std;;

int main() {
    int num = 0;
    unsigned long long res = 0;
    unsigned long long x = 0;
    cin >> num;
    while(num--) {
        cin >> x;
        res += x;
    }
    std::cout << res;
    return 0;
}
