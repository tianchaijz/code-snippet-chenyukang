open Core.Std
open Str


let get_rc len =
  let sq = sqrt (Float.of_int len) in
  let l = Int.of_float sq in
  let r = (l + 1) in
  if l * l >= len then (l, l)
    else (l, r)

let strip str =
  String.filter str ~f:(fun c -> c <> ' ')

let () =
  let str = strip (read_line()) in
  let len = String.length str in
  let r, c = get_rc len in
  let arr = ref [] in
  let pos = ref 0 in
  while !pos < len do
    let t = Int.min c (len - !pos) in
    let s = String.sub str !pos t in
    arr := !arr @ [s];
    pos := !pos + c
  done;
  let res = Array.init c ~f:(fun c ->
      List.fold !arr ~init:"" ~f:(fun r s ->
          let x = if String.length s > c then
              String.of_char (String.nget s c)
            else "" in
          r ^ x
        )
    ) in
  Array.iter res ~f:(fun x -> Printf.printf "%s " x)

