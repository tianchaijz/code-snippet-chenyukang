#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string.h>
using namespace std;

#define NUM 26
int main() {
    int N, idx;
    int cnt[NUM];
    string str;
    memset(cnt, 1, sizeof(cnt));
    cin >> N;
    while(N--) {
        cin >> str;
        int buf[NUM];
        memset(buf, 0, sizeof(buf));
        for(int i=0; i<str.size(); i++) {
            buf[str[i]-'a'] = 1;
        }
        for(int i=0; i<NUM; i++) {
            cnt[i] &= buf[i];
        }
    }
    int ans = 0;
    for(int i=0; i<26; i++) {
        if(cnt[i] == 1) {
            ans++;
        }
    }
    std::cout << ans << std::endl;
    return 0;
}
