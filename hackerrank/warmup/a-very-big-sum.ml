open Core.Std
open Str

let solve() =
  let _ = read_int() in
  let vals = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.fold_left vals ~init:0 ~f:(fun sum x ->
      (int_of_string x + sum));;

let () =
  print_int(solve());;
