#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;



int cmp(const void* a, const void* b) {
    const int* aa = (const int*)a;
    const int* bb = (const int*)b;
    return *aa - *bb;
}

int main() {
    int N, K, unfairness;
    cin >> N >> K;
    int candies[N];
    for (int i=0; i<N; i++)
        cin >> candies[i];
    unfairness = std::numeric_limits<int>::max();

    qsort(candies, N, sizeof(int), cmp);
    for(int i=0; i<N-K; i++) {
        if(candies[i+K-1] - candies[i] < unfairness) {
            unfairness = candies[i+K-1] - candies[i];
        }
    }
    cout << unfairness << "\n";
    return 0;
}
