#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int T;
    int n, a, b, now;
    int l, r;
    vector<int> ans;
    int last;
    cin >> T;
    while(T--) {
        cin >> n >> a >> b;
        ans.clear();
        l = min(a, b);
        r = max(a, b);
        last = -1;
        for(int i=0; i<n; i++) {
            now = r * i + l *(n-i-1);
            if(now > last) {
                ans.push_back(now);
                last = now;
            }
        }
        for(int i=0; i<ans.size(); i++) {
            if(i != 0) printf(" ");
            printf("%d", ans[i]);
        }
        printf("\n");
    }
    return 0;
}
