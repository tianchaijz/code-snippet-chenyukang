#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <vector>
#include <string.h>
using namespace std;


int main() {
    int N, T, v, l, r, ans;
    vector<int> vec;
    cin >> N >> T;
    while(N--) {
        cin >> v;
        vec.push_back(v);
    }
    while(T--) {
        cin >> l >> r;
        ans = 3;
        for(int i=l; i <= r; i++) {
            ans = min(ans, vec[i]);
        }
        std::cout << ans << std::endl;
    }
    return 0;
}
