open Core.Std
open Str

let int_of_string str =
  if String.length str = 0 then 0 else Int.of_string str

let is_kap i =
  let sq = i * i in
  let str = Int.to_string sq in
  let len = String.length str in
  let mid = (if len % 2 = 1 then (len/2 + 1) else len/2 ) in
  let fst = String.sub str 0 (len - mid) in
  let snd = String.sub str (len - mid) mid in
  i = (int_of_string fst + int_of_string snd)

let () =
  let p = read_int() in
  let q = read_int() in
  let f = ref false in
  for i = p to q do
    if is_kap i then (
      Printf.printf "%d " i;
      f := true
    )
  done;
  if !f = false then
    Printf.printf "INVALID RANGE"

