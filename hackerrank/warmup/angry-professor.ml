open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)


let () =
  let n = read_int() in
  for _ = 1 to n do
    match read_ints() with
    | [n; k] -> (
        if (List.count (read_ints()) ~f:(fun x -> x <= 0)) < k then
          Printf.printf "YES\n"
        else
          Printf.printf "NO\n"
      )
    | _ -> ()
  done
