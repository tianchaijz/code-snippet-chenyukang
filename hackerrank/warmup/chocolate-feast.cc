#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int t,n,c,m;
    cin>>t;
    while(t--){
        cin>>n>>c>>m;
        int ans=0;
        ans = n / c;
        int wrapped = 0;
        while((ans - wrapped*m)/m > 0) {
            int k = (ans - wrapped*m) / m;
            wrapped += k;
            ans += k;
        }
        cout<<ans<<endl;
    }
    return 0;
}
