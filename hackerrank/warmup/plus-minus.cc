#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int N, val;
    int positive_num, negitive_num, zero_num, count;
    positive_num = negitive_num = zero_num = 0;
    count = 0;
    cin >> N;
    count = N;
    while(N--) {
        cin >> val;
        if (val > 0)
            positive_num++;
        else if (val < 0)
            negitive_num++;
        else
            zero_num++;
    }
    printf("%.3f\n", ((float)positive_num / count));
    printf("%.3f\n", ((float)negitive_num / count));
    printf("%.3f\n", ((float)zero_num / count));
    return 0;
}
