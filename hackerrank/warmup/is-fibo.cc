
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

typedef unsigned long long LL;

#if 0
LL PerfectSquare(LL n) {
    LL h = n & 0xF; // last hexidecimal "digit"
    if (h > 9)
        return 0; // return immediately in 6 cases out of 16.

    // Take advantage of Boolean short-circuit evaluation
    if ( h != 2 && h != 3 && h != 5 && h != 6 && h != 7 && h != 8 ) {
        // take square root if you must
        LL t = (LL) floor( sqrt((double) n) + 0.5 );
        return t*t == n;
    }
    return 0;
}
#endif

bool PerfectSquare(LL n) {
    long tst = (long)(sqrt(n));
    return tst * tst == n;
}

LL isFibo(LL N) {
    return PerfectSquare(5*N*N+4) || PerfectSquare(5*N*N-4);
}

int isFibo2(LL N) {
    LL a, b, t;
    a = 0;
    b = 1;
    while(true) {
        if(b == N)
            return true;
        if(b > N)
            return false;
        t = b;
        b = a + b;
        a = t;
    }
}

int main() {
    LL T, N;
    cin >> T;
    while(T--) {
        cin >> N;
        if(isFibo2(N)) {
            std::cout << "IsFibo" << std::endl;
        } else {
            std::cout << "IsNotFibo" << std::endl;
        }
    }
    return 0;
}
