#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int T, a, b, l, r;
    cin >> T;
    while(T--) {
        cin >> a >> b;
        l = (int)sqrt((double)a);
        if(l*l < a) l++;
        r = (int)sqrt(b);
        std::cout << r + 1 - l << std::endl;
    }
    return 0;
}
