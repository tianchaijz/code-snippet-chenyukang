#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 101

int arr[MAX][MAX];

#define abs(a) (((a) >= 0) ? (a) : (-(a)))

int main() {
    int N;
    int res1, res2;
    std::cin >> N;
    memset(arr, 0, sizeof(arr));

    for(int i= 0; i < N; i++) {
        for(int k = 0; k < N; k++) {
            std::cin >> arr[i][k];
        }
    }
    res1 = res2 = 0;
    for(int i=0; i<N; i++)
        res1 += (arr[i][i]);
    for(int i=0; i<N; i++)
        res2 += (arr[i][N-i-1]);

    std::cout << abs(res1 - res2) << std::endl;
    return 0;
}
