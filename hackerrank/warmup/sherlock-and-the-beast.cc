#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;



int main() {
    string ans;
    int T, n, dig5, dig3, left;
    cin >> T;
    while(T--) {
        cin >> n;
        ans = "";
        dig3 = n/3;
        while(dig3>0) {
            left = n - 3*dig3;
            if(left%5 == 0 || left%3 == 0) {
                break;
            }
            dig3--;
        }
        for(int i=0; i<dig3; i++) {
            ans += "555";
        }
        left = n - ans.size();
        if(left%3 == 0) {
            for(int i=0; i<left/3; i++)
                ans += "555";
        } else if (left%5 == 0) {
            for(int i=0; i<left/5; i++)
                ans += "33333";
        }

        if(ans.size() == 0) {
            std::cout << -1 << std::endl;
        } else {
            std::cout << ans << std::endl;
        }
    }
    return 0;
}
