open Core.Std
open Str


let read_ints() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)


let solve n k =
  let chapters = read_ints() in
  let page = ref 0 in
  let res = ref 0 in
  List.iter chapters ~f:(fun pr_count ->
      let idx = ref 1 in
      while !idx <= pr_count do
        incr page;
        let tail = Int.min (!idx + k - 1) pr_count in
        (* Printf.printf "page: %d (%d-%d)\n" !page !idx tail; *)
        (if !page >= !idx && !page <= tail then
           incr res
        );
        idx := !idx + k
      done
    );
  Printf.printf "%d" !res

let () =
  match read_ints() with
  | [n; k] -> solve n k
  | _ -> ()
