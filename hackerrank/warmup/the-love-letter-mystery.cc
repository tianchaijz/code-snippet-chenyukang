#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int actionNum(string str) {
    int res = 0;
    for(int i=0; i<str.size() / 2; i++) {
        res += abs(str[i] - str[str.size() - 1 - i]);
    }
    return res;
}

int main() {
    int N;
    string str;
    cin >> N;
    while(N--) {
        cin >> str;
        std::cout << actionNum(str) << std::endl;
    }
    return 0;
}
