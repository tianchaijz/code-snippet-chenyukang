#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int N;
    cin >> N;
    for(int i=0; i<N; i++) {
        for(int j=1; j<=N; j++) {
            if ((N-i) > j) printf(" ");
            else printf("#");
        }
        printf("\n");
    }
    return 0;
}
