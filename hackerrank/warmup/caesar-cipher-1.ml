open Core.Std
open Str

let solve str diff =
  let trans c =
    let need_trans = ref true in
    let head = match c with
      | _ when c >= 'a' && c <= 'z' -> 'a'
      | _ when c >= 'A' && c <= 'Z' -> 'A'
      | _ -> (need_trans := false; 'a') in
    if !need_trans then (
      let h = Char.to_int head in
      let v = h + (Char.to_int c + diff - h) mod 26 in
      Char.of_int_exn v
    )
    else c in
  let res = String.map str ~f:(fun x -> trans x) in
  Printf.printf "%s\n" res

let () =
  let _ = read_int() in
  let str = read_line() in
  let diff = read_int() in
  solve str diff

