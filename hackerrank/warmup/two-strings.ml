open Core.Std
open Str

let to_idx c =
  Char.to_int c - Char.to_int 'a'

let () =
  let n = read_int() in
  for _ = 1 to n do
    let a = read_line() in
    let b = read_line() in
    let ah = Array.init 26 ~f:(fun _ -> 0) in
    String.iter a ~f:(fun c -> Array.set ah (to_idx c) 1);
    if String.exists b ~f:(fun c -> Array.get ah (to_idx c) > 0) then
      Printf.printf "YES\n"
    else
      Printf.printf "NO\n"
  done

