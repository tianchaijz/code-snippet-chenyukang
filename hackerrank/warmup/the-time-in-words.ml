open Core.Std
open Str

let table = [|"one";"two";"three";"four";"five";"six";"seven";"eight";
              "nine";"ten";"eleven";"twelve";"thirteen";"fourteen";
              "quarter";"sixteen";"seventeen";"eighteen";"nineteen";
              "twenty";"twenty one";"twenty two";"twenty three";"twenty four";
              "twenty five";"twenty six";"twenty seven";"twenty eight";
              "twenty nine";"half";|]


let hour h =
  Array.get table (h - 1)

let minute m =
  let r = Array.get table (m - 1) in
  match m with
  | 1 -> r ^ " minute"
  | 15 -> r
  | 30 -> r
  | _ -> r ^ " minutes"

let () =
  let h = read_int() in
  let m = read_int() in
  let res = match m with
    | 0 -> (hour h) ^ " o' clock"
    | _ when m <= 30 -> (
        (minute m) ^ " past " ^ (hour h)
      )
    | _ -> (minute (60 - m)) ^ " to " ^ (hour (h + 1)) in
  Printf.printf "%s\n" res
