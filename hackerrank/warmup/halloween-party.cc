#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    unsigned long long T, v;
    cin >> T;
    while(T--) {
        cin >> v;
        if((v%2) == 0) {
            std::cout << (v*v/4) << std::endl;
        } else {
            std::cout << (v-(v+1)/2) * (v+1)/2 << std::endl;
        }
    }
    return 0;
}
