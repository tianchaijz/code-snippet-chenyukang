use std::io::stdin;

fn read_int() -> int {
    let input = stdin().read_line().unwrap();
    let words = input.words().take(1)
        .map(|s| s.parse()).collect::<Vec<Option<int>>>();
    words[0].unwrap()
}


fn main() {
    let a = read_int();
    let b = read_int();
    println!("{}", a+b);
}
