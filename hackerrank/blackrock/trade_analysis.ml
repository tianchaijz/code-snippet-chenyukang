open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let read_int() =
  List.nth_exn (read_ints()) 0

let rec choose k l =
  if k = 0 then [ [] ]
  else (
    let len = List.length l in
    if len < k
    then []
    else if k = len
    then [l]
    else
      match l with
        h :: t ->
        let starting_with_h =
          (List.map ~f:(fun sublist -> [h] @ sublist) (choose (pred k) t))
        in
        let not_starting_with_h = choose k t in
        starting_with_h @ not_starting_with_h
      | [] -> assert false
  )


let () =
  let n = read_int() in
  let ns = read_ints() in
  let base = (Int.pow 10 9) + 7 in
  let res = ref 0 in
  for i = 1 to n do
    let permut = choose i ns in
    let values = List.map ~f:(fun l -> List.fold ~init:1.0 ~f:(fun s e -> s *. (Float.of_int e)) l) permut in
    let v = List.fold_left ~init:0.0 ~f:(fun s e -> Float.mod_float (s +. (Float.of_int i) *. e) (Float.of_int base)) values in
    res := (!res + Int.of_float v) % base;
  done;
  Printf.printf "%d\n" !res




