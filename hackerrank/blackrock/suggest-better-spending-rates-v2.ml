open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let read_int() =
  List.nth_exn (read_ints()) 0

let read_strs() =
  Str.split (Str.regexp "[ \n\r]+") (read_line())


module ListSet : sig
  type t = float List.t
  include Comparable.S with type t := t
end = struct
  module T = struct
    type t = float List.t with sexp
    let compare t1 t2 =
      if List.length t1 = List.length t2 then
        let res = ref 0 in
        let i = ref 0 in
        while(!i < (List.length t1) && !res = 0) do
          if (List.nth_exn t1 !i) <> (List.nth_exn t2 !i) then
            res := 1;
          incr i
        done;
        !res
      else
        -1
  end
  include T
  include Comparable.Make(T)
end

let value p r t ts =
    let _r = (1.0 +. r) in
  let first = List.nth_exn ts 0 in
  let res = ref ((Float.of_int p *. _r *. first) /. 100.0) in
  let values = List.mapi ts ~f:(fun i x ->
      if i = 0 then !res
      else (
        let prev = List.nth_exn ts (i -1) in
        res := !res *. x *. (((100.0 -. prev) *. _r) /. (100.0 *. prev));
        !res
      )) in
    List.fold_left values ~init:0.0 ~f:(fun s x -> s +. x)

let permutations ts sum s =
  let candidate = List.map ts ~f:(fun x ->
      let r = ref [] in
      for i = -(s) to s do
        r := !r @ [x +. (Float.of_int i)]
      done;
      !r
    ) in
  let res = ref ListSet.Set.empty in
  let len = List.length candidate in
  let rec gen cur list _sum diff =
    if (cur = len && _sum = sum) then (
      res := ListSet.Set.add !res list
    ) else (
      let left_max = (Float.of_int (len - cur)) *. (Float.of_int s) in
      let left_min = (Float.of_int (len - cur)) *. (0.0 -. (Float.of_int s)) in
      if cur < len && (diff +. left_max >= 0.0) && (diff +. left_min <= 0.0) then (
        let elems = List.nth_exn candidate cur in
        List.iter elems ~f:(fun x ->
            if x >= 0.0 && _sum +. x <= sum then
              gen (cur + 1) (list @ [x]) (_sum +. x) diff
        )
      )
    ) in
  gen 0 [] 0.0 0.0;
  !res

let () =
  match read_strs() with
  | [_p; _r; _t; _s] -> (
      let p = Int.of_string _p in
      let r = (Float.of_string _r) /. 100.0 in
      let t = Int.of_string _t in
      let s = Int.of_string _s in
      let ts = List.map (read_ints()) ~f:Float.of_int in
      let first = value p r t ts in
      Printf.printf "%.3f\n" first;
      let sum = List.fold_left ts ~init:0.0 ~f:(fun s x -> s +. x) in
      let permuts = permutations ts sum s in
      let permuts_values = ref [] in
      ListSet.Set.iter permuts ~f:(fun x ->
          let v = (value p r t x, x) in
          permuts_values := !permuts_values @ [v]
        );
      (* let permuts_values = (List.map permuts ~f:(fun x -> (value p r t x, x))) in *)
      let res = List.sort ~cmp:(fun a b -> if a > b then -1 else 1) !permuts_values in
      List.iteri res ~f:(fun i (a, b) ->
          if i <= 2 then (
            Printf.printf "%.3f - " a;
            List.iter b ~f:(fun x -> Printf.printf "%d " (Int.of_float x));
            Printf.printf "\n"
          )
      )
    )
  | _ -> ()



