#include <stdio.h>
#include <stdlib.h>

#define MAXN (100000+1)
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))

struct employee{
    int id;
    int rating;
};

int comp( const void*p1, const void*p2){
    return ((struct employee*)p1)->rating - ((struct employee*)p2)->rating;
}

struct employee employees[MAXN];
int candies[MAXN];
int ratings[MAXN];
int minimal[MAXN];

int main(int argc, char *argv[])
{
    int N;
    int i,j;
    int candy;
    int id;
    long long sumcandies = 0;

    scanf("%d",&N);
    for(i = 0; i < N; i++){
        scanf("%d",&employees[i].rating);
        employees[i].id = i;
        ratings[i] = employees[i].rating;
    }

    for(i=0; i < N; i++) {
      scanf("%d", &minimal[i]);
    }

    qsort(employees, N, sizeof(struct employee), comp);

    for(i = 0; i < N; i++){
        id = employees[i].id;
        candy = minimal[id];

        for(j = max(0, id-10); j <= min(N-1, id+10); j++) {
          if(candies[j] != 0 && ratings[id] > ratings[j] && candy <= candies[j]) {
            candy = candies[j]+1;
          }
        }
        sumcandies += candy;
        candies[id] = candy;
    }

    printf("%lld\n",sumcandies);
    return 0;
}


