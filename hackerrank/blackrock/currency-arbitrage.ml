open Core.Std
open Str

let read_floats() =
  let elems = Str.split (Str.regexp "[ \n]+") (read_line()) in
  List.map elems ~f:(fun x -> Float.of_string x)

let my_read_int() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  let res = List.map elems ~f:(fun x -> Int.of_string x) in
  List.nth_exn res 0


let ins_all_positions x l =
  let rec aux prev acc = function
    | [] -> (prev @ [x]) :: acc |> List.rev
    | hd::tl as l -> aux (prev @ [hd]) ((prev @ [x] @ l) :: acc) tl
  in
  aux [] [] l

let rec permutations = function
  | [] -> []
  | x::[] -> [[x]] (* we must specify this edge case *)
  | x::xs -> List.fold_left ~init:[] ~f:(fun acc p -> acc @ ins_all_positions x p )
               (permutations xs)


let () =
  let n = my_read_int() in
  for _ = 1 to n do
    let arr = read_floats() in
    let n = ref (Float.of_int 100000) in
    List.iter arr ~f:(fun x -> n := (!n /. x));
    if !n > 100000.0 then
      Printf.printf "%d\n" (Int.of_float (!n -. 100000.0))
    else
      Printf.printf "0\n"
  done




