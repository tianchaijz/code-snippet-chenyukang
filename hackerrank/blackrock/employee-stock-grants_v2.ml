open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)


let read_int() =
  List.nth_exn (read_ints()) 0


let print_arr arr msg =
  Printf.printf "%s: " msg;
  Array.iter arr ~f:(fun x -> Printf.printf "%d " x);
  Printf.printf "\n"

let () =
  let n = read_int() in
  let ratings = read_ints() in
  let minimal = read_ints() in
  let dp = Array.init n ~f:(fun _ -> 0) in
  let res = ref 0 in
  List.iteri minimal ~f:(fun i m ->
      let cur = ref m in
      let cur_rate = List.nth_exn ratings i in
      for _i = (Int.max (i - 10) 0) to i do
        let s = Array.nget dp _i in
        let rate = List.nth_exn ratings _i in
        if rate < cur_rate then (cur := Int.max !cur (s + 1))
      done;
      for _i = (Int.max (i-10) 0) to i do
        let s = Array.nget dp _i in
        let rate = List.nth_exn ratings _i in
        if rate > cur_rate && s <= !cur then (
          res := !res - s;
          Array.set dp _i (Int.max !cur (s + 1));
          res := !res + (Array.nget dp _i);
        )
      done;
      Array.set dp i !cur;
      res := !res + (Array.nget dp i);
      (* print_arr dp "now" *)
    );
  Printf.printf "%d\n" !res
  (* Array.iter dp ~f:(fun x -> Printf.printf "%d " x); *)
  (* Printf.printf "\n"; *)
  (* Printf.printf "%d\n" (Array.fold dp ~init:0 ~f:(fun s x -> s + x)) *)

