open Core.Std;;

let () =
  let x, y = Scanf.sscanf (read_line()) "%d %d" (fun a b -> a, b) in
  let n = 5 in
  let map = Array.init n ~f:(fun _i -> read_line()) in
  let min = ref (Int.max_value) in
  let tx, ty = ref 0, ref 0 in
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      if (String.nget map.(i) j) = 'd' then (
        let cur = Int.abs(i - x) + Int.abs(j - y) in
        if cur < !min then (tx := i; ty := j);
        min := Int.min cur !min;
      )
    done;
  done;
  match x, y, tx, ty with
  | _ when (x < !tx) -> Printf.printf "DOWN\n"
  | _ when (x > !tx) -> Printf.printf "UP\n"
  | _ when (y < !ty) -> Printf.printf "RIGHT\n"
  | _ when (y > !ty) -> Printf.printf "LEFT\n"
  | _ -> Printf.printf "CLEAN\n"



