open Core.Std;;

let solve() =
  let n = read_int() in
  let mx, my = ref 0, ref 0 in
  let px, py = ref 0, ref 0 in
  let map = Array.init n ~f:(fun _ -> "") in
  for i = 0 to n-1 do
    map.(i) <- read_line()
  done;

  for i = 0 to n-1 do
    for j = 0 to n-1 do
      match (String.nget map.(i) j) with
      | 'm' -> ( mx := i; my := j)
      | 'p' -> ( px := i; py := j)
      | _ -> ()
    done
  done;
  while !px < !mx do
    Printf.printf "UP\n";
    decr mx;
  done;
  while !px > !mx do
    Printf.printf "DOWN\n";
    incr mx;
  done;
  while !py < !my do
    Printf.printf "LEFT\n";
    decr my;
  done;
  while !py > !my do
    Printf.printf "RIGHT\n";
    incr my;
  done;;

let () =
  solve();;
