open Core.Std
open Sys

let update_map map =
  let filename = "cache.txt" in
  (match Sys.file_exists filename with
   | `Yes ->(
       let str = In_channel.read_all filename in
       let old = List.to_array (Str.split (Str.regexp "[ \n\r]+") str) in
       Array.iteri map ~f:(fun i s ->
           Array.iteri s ~f:(fun j e ->
               if e = 'o' then (
                 let c = String.nget old.(i) j in
                 map.(i).(j) <- c
               )));
     )
  | _ -> ());
  Out_channel.write_all filename ~data: (Array.fold map ~init:""
                                           ~f:(fun s e ->
                                               let _s = Array.fold e ~init:""
                                                   ~f:(fun _s _e -> _s ^ (Char.to_string _e)) in
                                               if String.length s > 0 then s ^ "\n" ^ _s else _s))

let () =
  let x, y = Scanf.sscanf (read_line()) "%d %d" (fun a b -> a, b) in
  let map = Array.init 5 ~f:(fun _i -> (String.to_array (read_line()))) in
  update_map map;
  let dx, dy = ref (-1), ref (-1) in
  let ox, oy = ref (-1), ref (-1) in
  let min_d = ref Int.max_value in
  let min_o = ref Int.max_value in
  for i = 0 to 4 do
    for j = 0 to 4 do
      let dist = Int.abs(i - x) + Int.abs(j - y) in
      if dist < !min_d && map.(i).(j) = 'd' then ( min_d := dist; dx := i; dy := j);
      if dist < !min_o && map.(i).(j) = 'o' then ( min_o := dist; ox := i; oy := j);
    done;
  done;
  let tx = if !dx <> (-1) then !dx else !ox in
  let ty = if !dy <> (-1) then !dy else !oy in
  match x, y, tx, ty with
  | _ when (x < tx) -> Printf.printf "DOWN\n"
  | _ when (x > tx) -> Printf.printf "UP\n"
  | _ when (y < ty) -> Printf.printf "RIGHT\n"
  | _ when (y > ty) -> Printf.printf "LEFT\n"
  | _ -> Printf.printf "CLEAN\n"

