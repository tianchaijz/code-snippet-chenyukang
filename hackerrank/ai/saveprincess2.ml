open Core.Std;;

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  let res = List.map elems ~f:(fun x -> Int.of_string x) in
  List.to_array res

let solve() =
  let n = read_int() in
  let _ = read_ints() in
  let mx, my = ref 0, ref 0 in
  let px, py = ref 0, ref 0 in
  let map = Array.init n ~f:(fun _ -> "") in
  for i = 0 to n-1 do
    map.(i) <- read_line()
  done;

  for i = 0 to n-1 do
    for j = 0 to n-1 do
      match (String.nget map.(i) j) with
      | 'm' -> ( mx := i; my := j)
      | 'p' -> ( px := i; py := j)
      | _ -> ()
    done
  done;
  match (!px, !mx, !py, !my) with
  | _ when (!px < !mx) -> Printf.printf "UP\n"
  | _ when (!px > !mx) -> Printf.printf "DOWN\n"
  | _ when (!py < !my) -> Printf.printf "LEFT\n"
  | _ when (!py > !my) -> Printf.printf "RIGHT\n"
  | _ -> ();;

let () =
  solve();;
