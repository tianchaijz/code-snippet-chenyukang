open Core.Std
open Str


let () =
  let str = read_line() in
  let new_line = ref false in
  let in_quato = ref false in
  String.iter str ~f:(fun c ->
      match c with
      | '.' | '!' | '?' -> (
          if !in_quato = false then (
            Printf.printf "%c\n" c;
            new_line := true
          ) else
            Printf.printf "%c" c
        )
      | ' ' | '\t' -> (
          if !new_line = false then
            Printf.printf "%c" c;
        )
      | '"' -> (
          if !in_quato = false then
            in_quato := true
          else
            in_quato := false;
          Printf.printf "%c" c;
        )
      | _ -> (
          new_line := false;
          Printf.printf "%c" c;
        )
    )

