open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)

let () =
  let n = read_int() in
  let ints = read_ints() in
  let count = Array.init 105 ~f:(fun _ -> 0) in
  let res = ref 1 in
  for i = 1 to n do
    res := !res * i
  done;
  List.iter ints ~f:(fun e -> Array.set count e (count.(e) + 1));
  Array.iteri count ~f:(fun i c ->
      for x = c downto 2 do
        res := !res / x;
      done
    );
  Printf.printf "%.6f\n" (Float.of_int !res)
