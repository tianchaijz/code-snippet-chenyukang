open Core.Std
open Str

let () =
  let x1, v1, x2, v2 = Scanf.sscanf (read_line()) "%d %d %d %d" (fun x1 v1 x2 v2 -> x1,v1, x2, v2) in
  let res = match v1, v2 with
  | _ when v1 > v2 -> (
      x2 > x1 && ((x2 - x1) % (v1 - v2) = 0)
    )
  | _ when v1 < v2 -> (
      x1 > x2 && ((x1 - x2) % (v2 - v1) = 0)
    )
  | _ -> (
      x1 = x2
    )
  in
  if res then
    Printf.printf "YES"
  else
    Printf.printf "NO"

