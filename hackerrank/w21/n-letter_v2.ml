open Core.Std
open Str

let graphic (x1, y1) (x2, y2) (x3, y3) =
  let pi =  acos (-. 1.) in
  let a = Float.of_int ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) in
  let b = Float.of_int ((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3)) in
  let c = Float.of_int ((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1)) in
  let angle = (acos ((a +. b -. c) /. sqrt(4.0 *. a *. b))) *. 180.0 /. pi in
  let side = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1) in
  (angle, side)


let () =
  try (
    while true do
      let n = read_int() in
      let points = Array.init n
          ~f:(fun _ -> Scanf.sscanf (read_line()) "%d %d" (fun x y -> x, y)) in
      let dp = ref [] in
      let ans = ref 0 in
      for i = 0 to n - 1 do
        for j = 0 to n - 1 do
          for x = 0 to n - 1 do
            for y = 0 to n - 1 do
              if x <> i && x <> j && y <> i && y <> j && x <> y  && i <> j then (
                let elems = [|x; i; j; y|] in
                let rev_elems = Array.copy elems in
                Array.rev_inplace rev_elems;
                let angle_x, side_x = graphic points.(i) points.(j) points.(x) in
                let angle_y, side_y = graphic points.(i) points.(j) points.(y) in
                if (side_x * side_y) < 0 && angle_x <= 90.0  && angle_y <= 90.0 &&
                   (List.exists !dp ~f:(fun a -> a = elems || a = rev_elems)) = false then (
                  Printf.printf "add: ";
                  Array.iter elems ~f:(fun x -> Printf.printf "%d " x);
                  Printf.printf "\n";
                  dp := !dp @ [elems];
                  incr ans
                );
              )
            done;
          done;
        done;
      done;
      Printf.printf "%d\n" !ans
    done
  )
  with _ -> ()


