open Core.Std
open Str

let () =
  let important = ref [] in
  let unimportant = ref [] in
  let n, k = Scanf.sscanf (read_line()) "%d %d" (fun n k -> n, k) in
  for _ = 1 to n do
    let luck, impt = Scanf.sscanf (read_line()) "%d %d" (fun a b -> a, b) in
    if impt = 1 then
      important := !important @ [luck]
    else
      unimportant := !unimportant @ [luck]
  done;
  important := List.sort ~cmp:(fun a b -> b - a) !important;
  let imp = List.foldi !important ~f:(fun i s e -> if i < k then s + e else s - e) ~init: 0 in
  let res = List.fold !unimportant ~f:(fun s e -> s + e) ~init:imp in 
  Printf.printf "%d\n" res

