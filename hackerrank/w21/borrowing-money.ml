open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  let res = List.map elems ~f:(fun x -> Int.of_string x) in
  List.to_array ([0] @ res)

let use_it map moneys idx used =
  used.(idx) <- 1;
  let subed = ref 0 in
  for j = 1 to (Array.length map.(idx)) - 1 do
    if map.(idx).(j) = 1 then (
      if used.(j) = 0 then
        subed := !subed + moneys.(j);
      used.(j) <- 2
    )
  done;
  !subed

let solve n m moneys map =
  let used = Array.init (n+1) ~f:(fun _ -> false) in
  let sub_dp() =
    let next = ref [] in
    for i = 1 to n do
      if used.(i) = false then (
        let sub = ref 0 in
        for j = 1 to n do
          if map.(i).(j) = 1 && used.(j) = false then
            sub := !sub + moneys.(j)
        done;
        next := !next @ [(i, moneys.(i) - !sub)];
      );
    done;
    let res = List.to_array !next in
    Array.sort res ~cmp:(fun (a1, m1) (a2, m2) -> m2 - m1);
    (* Array.iter res ~f:(fun (a, m) -> Printf.printf "(%d %d)" a m); *)
    res in
  (* Printf.printf "\n"; *)
  let max_money = ref 0 in
  let loop = ref true in
  while !loop do
    let next = sub_dp() in
    if Array.length next > 0 then (
        let idx, money = next.(0) in
        if used.(idx) = false then (
          max_money := !max_money + moneys.(idx);
          used.(idx) <- true;
          for j = 1 to n do
            if map.(idx).(j) = 1 then
              used.(j) <- true;
          done;
        )
      )
    else loop := false
  done;
  let used = ref (Array.init (n+1) ~f:(fun _ -> 0)) in
  let sub_limit = (Array.fold ~init: 0 ~f:(fun s e -> s + e) moneys) - !max_money in
  (* Printf.printf "sub_limit: %d\n" sub_limit; *)
  let count = ref 0 in
  let rec iter cur idx subed =
    match idx, n with
    | _ when idx = (n+1) -> (
        if cur = !max_money then (
          (* Array.iteri !used ~f:(fun i x -> Printf.printf "(%d %d) " i x); *)
          (* Printf.printf "\n"; *)
          incr count
        )
      )
    | _ when idx <= n -> (
        if !used.(idx) = 0 && subed <= sub_limit then (
          let back = Array.copy !used in
          let cur_sub = use_it map moneys idx !used in
          iter (cur + moneys.(idx)) (idx+1) (subed + cur_sub);
          used := back;
          iter cur (idx+1) subed
        )
        else iter cur (idx+1) subed
      )
    | _ -> ()
  in
  iter 0 1 0;
  (* Printf.printf "max: %d cnt: %d\n" !max_money !count; *)
  (!max_money, !count)

let () =
  let n, m = Scanf.sscanf (read_line()) "%d %d" (fun a b -> a, b) in
  let moneys = read_ints() in
  let map = Array.init (n+1) ~f:(fun _ -> Array.init (n+1) ~f:(fun _ -> 0)) in
  for _ = 1 to m do
    let a, b = Scanf.sscanf (read_line()) "%d %d" (fun a b -> a, b) in
    map.(a).(b) <- 1;
    map.(b).(a) <- 1
  done;

  let value, count = solve n m moneys map in
  Printf.printf "%d %d\n" value count
