open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  let res = List.map elems ~f:(fun x -> Int.of_string x) in
  List.to_array res



let () =
  let n = read_int() in
  let ws = read_ints() in
  let b, e = Scanf.sscanf (read_line()) "%d %d" (fun a b -> a, b) in
  let dp = Array.init (e+1) ~f:(fun _ -> Array.init (n+1) ~f:(fun _ -> 0)) in
  let base = (Int.pow 10 9) + 7 in
  for i = 0 to n do
    dp.(0).(i) <- 1
  done;
  for i = 1 to e do
    for j = 0 to n-1 do
      let x = if i - ws.(j) >= 0 then (
          dp.(i - ws.(j)).(j)
        )
        else
          0 in
      let y = if j >= 1 then dp.(i).(j-1) else 0 in
      dp.(i).(j) <- (x + y) mod base
    done;
  done;
  let ans = ref 0 in
  for i = b to e do
    ans := (!ans + dp.(i).(n-1)) mod base
  done;
  Printf.printf "%d\n" !ans

