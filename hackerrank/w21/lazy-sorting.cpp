#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;
typedef unsigned long long LL;

int main() {
/* Enter your code here. Read input from STDIN. Print output to STDOUT */ 

    map <int, int> m; //create a map to store the number of repetitions of each number
    int N;  //number of elements in list
    //calculate the number of permutations
    cin >> N;
    int j;
    LL total_perm = 1;
    LL temp;
    for (int i = 0; i < N; i++){
        cin >> temp;
        //if temp exists, add one to the value of m[temp], else initialize a new key value pair
        if (m.find(temp) == m.end()){
            m[temp] = 1;
        }else{
            m[temp] += 1;
        }
        total_perm *= i+1;
    }
    //calculate permutations taking into account of repetitions
    for (map<int,int>::iterator iter = m.begin(); iter != m.end(); ++iter)
    {
        if (iter -> second > 1){
            temp  = iter -> second;
            while (temp > 1){
                total_perm = total_perm / temp;
                temp -= 1;
            }
        }
    }

    printf("%6f", float(total_perm));
    return 0;
}
