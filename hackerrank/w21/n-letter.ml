open Core.Std
open Str

let () =
  try (
    while true do
      let n = read_int() in
      let points = Array.init n
          ~f:(fun _ -> Scanf.sscanf (read_line()) "%d %d" (fun x y -> x, y)) in
      let arr = Array.init n ~f:(fun _ ->
          Array.init n ~f:(fun _ -> Array.init 2 ~f:(fun _ -> 0))) in
      let pi =  acos (-. 1.) in
      for i = 0 to n - 2 do
        for j = i+1 to n - 1 do
          for x = 0 to n - 1 do
            if x <> i && x <> j then (
              let x1, y1 = points.(i) in
              let x2, y2 = points.(j) in
              let x3, y3 = points.(x) in
              let a = Float.of_int ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) in
              let b = Float.of_int ((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3)) in
              let c = Float.of_int ((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1)) in
              let angle = (acos ((a +. b -. c) /. sqrt(4.0 *. a *. b))) *. 180.0 /. pi in
              let side = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1) in
              (* Printf.printf "now: (%d %d) (%d %d) (%d %d) -> %.2f %d\n" x1 y1 x2 y2 x3 y3 angle side; *)
              if side < 0 && angle <= 90.0 then (
                Printf.printf "1: (%d %d %d) -> %.2f %d\n" i j x angle side;
                arr.(i).(j).(0) <- arr.(i).(j).(0) + 1
              );
              if side > 0 && angle <= 90.0 then (
                Printf.printf "2: (%d %d %d) -> %.2f %d\n" i j x angle side;
                arr.(i).(j).(1) <- arr.(i).(j).(1) + 1;
              )
            )
          done;
        done;
      done;
      let ans = ref 0 in
      for i = 0 to n - 2 do
        for j = i + 1 to n - 1 do
          ans := !ans + arr.(i).(j).(0) * arr.(i).(j).(1);
        done;
      done;
      Printf.printf "%d\n" !ans
    done
  )
  with _ -> ()


