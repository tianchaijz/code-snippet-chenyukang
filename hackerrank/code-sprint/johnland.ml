open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.to_array (List.map elems ~f:(fun x -> Int.of_string x))
