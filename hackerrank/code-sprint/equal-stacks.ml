open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.to_array (List.map elems ~f:(fun x -> Int.of_string x))


let () =
  let size = read_ints() in
  let stacks = Array.init 3 ~f:(fun i -> Array.init (size.(i)) ~f:(fun _ -> 0)) in
  for i = 0 to 2 do
    let arr = read_ints() in
    Array.rev_inplace arr;
    Array.set stacks i arr
  done;
  let max_height = Array.map ~f:(fun a -> Array.fold ~init:0 ~f:(fun s x -> s + x) a) stacks in
  let loop = ref true in
  while !loop do
    let max, idx = Array.foldi max_height ~init:(Int.min_value, -1) ~f:(
        fun cur (m, i) a -> if a > m then
            (a, cur)
          else
            (m, i)
      ) in
    let ids = Array.filter max_height ~f:(fun a -> a = max) in
    loop := Array.length ids <> Array.length max_height;
    if !loop then(
      let last = Array.nget stacks.(idx) (size.(idx) - 1) in
      Array.set size idx (size.(idx) - 1);
      Array.set max_height idx (max_height.(idx) - last)
    )
  done;
  Printf.printf "%d\n" max_height.(0)




