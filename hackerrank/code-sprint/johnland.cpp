#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

typedef long long LL;
const int MAXN = 100100;
const int MAXM = 200100;
int fa[MAXN];
int n, m;
LL ans[MAXM];
int cnt[MAXN];
vector<pair<int,int> > mp[MAXN];
string final;

struct E {int u, v, w;} e[MAXM];
int cmp_w(E a, E b) {return a.w < b.w;}

int findp(int p) {
    if (fa[p] == p) return p;
    return fa[p] = findp(fa[p]);
}

void solve(int p, int fa) {
    cnt[p] = 1;
    for (int i = 0 ; i < mp[p].size() ; ++i) {
        int id = mp[p][i].first;
        if (id == fa) continue;
        int w = mp[p][i].second;
        solve(id, p);
        cnt[p] += cnt[id];
        ans[w] = (LL)cnt[id] * (n - cnt[id]);
        printf("p: %d id: %d w: %d,  cnt1: %d, cnt2: %d\n", p+1, id+1, w, cnt[id], (n-cnt[id]));
    }
}

int main() {
    scanf("%d%d",&n,&m);
    for (int i = 0 ; i < m ; ++i) {
        scanf("%d%d%d", &e[i].u, &e[i].v, &e[i].w);
        --e[i].u; --e[i].v;
    }
    sort(e, e+m, cmp_w);
    for (int i = 0 ; i < n ; ++i)
        fa[i] = i;
    for (int i = 0 ; i < m ; ++i) {
        int s1 = findp(e[i].u);
        int s2 = findp(e[i].v);
        if (s1 == s2) continue;
        fa[s1] = s2;
        mp[e[i].u].push_back(make_pair(e[i].v, e[i].w));
        mp[e[i].v].push_back(make_pair(e[i].u, e[i].w));
    }

    solve(0, -1);

    for (int i = 0 ; i <= m ; ++i) {
        if (ans[i] % 2) final.append(1, '1');
        else final.append(1, '0');
        ans[i+1] += ans[i] / 2;
    }
    while (ans[m+1]) {
        if (ans[m+1] % 2) final.append(1, '1');
        else final.append(1, '0');
        ans[m+1] /= 2;
    }
    int head = 1;
    for (int i = final.size() - 1 ; i >= 0 ; --i) {
        if (final[i] == '0' && head) continue;
        printf("%c", final[i]);
        head = 0;
    }
    printf("\n");
    return 0;
}
