open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.to_array (List.map elems ~f:(fun x -> Int.of_string x))

let () =
  let n = read_int() in
  let ns = read_ints() in
  let ans = ref Int.max_value in
  let len = Array.length ns in
  for i = 0 to len - 2 do
    for j = i+1 to len -1 do
      if ns.(i) = ns.(j) && Int.abs (i - j) < !ans then
        ans := Int.abs(i - j)
    done;
  done;
  if !ans = Int.max_value then
    Printf.printf "-1"
  else
    Printf.printf "%d\n" !ans
