#include <set>
#include <vector>
#include <map>
#include <iostream>
#include <string.h>

using namespace std;

int main() {
    int T;
    int M, N, t;
    cin >> T;
    while(T--) {
        vector<int> vec;
        map<int, vector<int> > hash;
        cin >> M >> N;
        for(int i=0; i<N; i++) {
            cin >> t;
            vec.push_back(t);
            map<int, vector<int> >::iterator it = hash.find(t);
            if(it != hash.end()) {
                it->second.push_back(i);
            } else {
                vector<int> arr;
                arr.push_back(i);
                hash.insert(make_pair(t, arr));
            }
        }
        bool found = false;
        for(int i=0; i<N && (!found);  i++) {
            int diff = M - vec[i];
            map<int, vector<int> >::iterator it = hash.find(diff);
            if(it != hash.end()) {
                const vector<int>& arr =  it->second;
                for(int k=0; k<arr.size(); k++) {
                    if(arr[k] != i) {
                        printf("%d %d\n", i+1, arr[k]+1);
                        found = true;
                        break;
                    }
                }
            }
        }
    }
    return 0;
}
