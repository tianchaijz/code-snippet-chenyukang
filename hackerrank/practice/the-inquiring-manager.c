#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define maxi(a, b) ((a) > (b) ? (a) : (b))
#define mini(a, b) ((a) < (b) ? (a) : (b))

int main() {

  int i,j,m,n,t=0,max,m1,r, b;

  scanf("%d",&n);
  int pr[n],tm[n];
  r=0;
  b=0;
  for(i=0;i<n;i++){
    max= -1;
    scanf("%d",&m);
    if(m==1){
      scanf("%d%d",&pr[r],&tm[r]);
      r++;
    }
    else{
      scanf("%d",&m1);
      t = maxi(0, m1 - 59);
      int min = 0x7fffffff;
      for(j=b; j<r; j++){
        if(tm[j] >= t && tm[j] <= m1){
          max = maxi(max, pr[j]);
          min = mini(min, j);
        }
      }
      b = min;
      printf("%d\n",max);
    }
  }
  return 0;
}
