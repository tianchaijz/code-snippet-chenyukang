open Core.Std
open Str

let read_ints() =
  let elems = Str.split (Str.regexp "[ \n\r]+") (read_line()) in
  List.map elems ~f:(fun x -> Int.of_string x)


let read_int() =
  List.nth_exn (read_ints()) 0

type shoe = {
  mutable id: int;
  mutable times: int;
}

let () =
  match read_ints() with
  | [k; m; n] -> (
      let arr = Array.init m ~f:(fun i -> { id = i; times = 0}) in
      for _ = 1 to n do
        let idx = read_int() in
        let s = Array.nget arr idx in
        s.times <- s.times + 1
      done;

      Array.sort arr ~pos:0 ~len:m ~cmp:(fun a b ->
          if a.times > b.times then (-1)
          else (
            if a.times = b.times then (
              if a.id < b.id then (-1) else 1
            ) else
              1
          )
        );
      (* Array.iter arr ~f:(fun x -> *)
      (*     Printf.printf "id:%d times:%d\n" x.id x.times *)
      (*   ); *)
      for i = 0 to k-1 do
        Printf.printf "%d\n" (Array.nget arr i).id
      done
    )
  | _ -> ()

