open Core.Std;;

let read_int () = Scanf.bscanf Scanf.Scanning.stdib "%d " (fun x -> x)

let power a k limit =
  try
    let v = (Int.pow a k) in
    if v <= limit then Some(v)
    else None
  with
  | _ -> None;;

let solve n k =
  let hash = Hashtbl.Poly.create() in
  let res = ref 0 in
  let rec loop i now =
    if now = n then ignore(incr res)
    else
      if now < n && i < n then
        match Hashtbl.find hash i with
        | Some(x) -> (begin loop (i+1) (now + x);
                            loop (i+1) now;
                      end)
        | _ -> loop (i + 1) now in
  for a = 1 to n do
    match power a k n with
      Some(x) -> ignore(Hashtbl.add hash a x);
    | _ -> ();
  done;
  ignore(loop 0 0);
  !res;;

let () =
  let n = read_int() and
      k = read_int() in
  Printf.printf "%d\n" (solve n k);;
