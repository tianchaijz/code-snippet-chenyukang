open Core.Std;;

let () =
  let s = read_line() |> String.strip in
  let arr = Array.create 26 0 in
  let res = ref "" in
  String.iter s (fun x ->
                 let v = (Char.to_int x) - 97 in
                 if arr.(v) = 0 then begin
                     res := !res ^ (Char.to_string x);
                     arr.(v) <- 1;
                   end);
  Printf.printf "%s\n" (String.to_string !res);;
