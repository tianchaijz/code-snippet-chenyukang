open Core.Std;;

let solve str =
  let len = String.length str in
  for i = 0 to len - 2 do
    if i mod 2 = 0 then begin
        Printf.printf "%c" str.[i+1];
        Printf.printf "%c" str.[i];
      end
  done;
  Printf.printf "\n";;

let () =
  let n = read_int() in
  for _ = 1 to n do
    let s = read_line() in
    solve s;
  done;;
