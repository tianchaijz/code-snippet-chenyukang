open Core.Std;;

let primes va =
  let rec it v m =
    match v, m with
    | v, m when (m * m) > v -> if v = 1 then [] else [v]
    | v, m when v mod m = 0 -> [m] @ (it (v / m) m)
    | _, _ -> it v (m + 1) in
  (it va 2);;

let is_prime v =
  List.length (primes v) = 1;;

let shifts v direction =
  let str = Int.to_string v in
  let len = String.length str in
  let res = ref [] in
  let f = (if direction = 1 then String.drop_suffix else String.drop_prefix) in
  for i = 1 to len - 1 do
    let s = f str i in
    res := (Int.of_string s) :: !res;
  done;
  !res;;

let contain_zero v =
  match (String.index (Int.to_string v) '0') with
    Some(_) -> true
  | None -> false;;

let test_number v =
  if (is_prime v) = false ||  contain_zero v then
    "DEAD"
  else (
    let rsfs = (List.map (shifts v (1)) ~f:(fun x -> is_prime x)) and
        lsfs = (List.map (shifts v (-1)) ~f:(fun x -> is_prime x))  in
    let rs = (List.length (List.filter rsfs ~f:(fun x -> x = false))) in
    let ls = (List.length (List.filter lsfs ~f:(fun x -> x = false))) in
    match rs, ls with
      0, 0 -> "CENTRAL"
    | rs, ls when rs > 0 && ls > 0 -> "DEAD"
    | rs, ls when rs > 0 -> "LEFT"
    | rs, ls when ls > 0 -> "RIGHT"
    | _, _ -> assert false);;

let () =
  let n = read_int() in
  for _ = 1 to n do
    let v = read_int() in
    Printf.printf "%s\n" (test_number v);
  done;;
