open Core.Std;;

let solve a b =
  let hash = Hashtbl.Poly.create() in
  let res = ref [] in
  List.iter b ~f:(fun x ->
                  match Hashtbl.find hash x with
                  | None -> ignore(Hashtbl.add hash x 1)
                  | Some(c) -> Hashtbl.set hash x (c + 1));
  List.iter a ~f:(fun x ->
                  match Hashtbl.find hash x with
                  | None -> assert false;
                  | Some(c) -> Hashtbl.set hash x (c - 1));
  Hashtbl.iter hash ~f:(fun ~key:k ~data:v -> if v > 0 then res := !res @ [k] );
  List.sort ~cmp:Int.compare !res;;

let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let () =
  let la = read_int() in
  let a = read_nums() in
  let lb = read_int() in
  let b = read_nums() in
  let res = solve a b in
  List.iter res ~f:(fun x -> Printf.printf "%d " x);
  print_newline();
