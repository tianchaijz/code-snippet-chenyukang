open Core.Std;;

let blank = (Str.regexp_string " ");;
let t = int_of_string (input_line stdin);;

let test s t =
  let ls = String.length s in
  let lt = String.length t in
  Printf.printf "test: %s %s\n" (String.to_string s) (String.to_string t);
  try
    let hd = String.sub t 0 ls in
    if hd = s then (true, String.sub t ls (lt - ls))
    else (false, "")
  with
    Invalid_argument _ -> (false, "")
;;

(* wrong for case: 
   wedow because can do must we what
   wedowhatwemustbecausewecan *)
let round () =
  let n = int_of_string (input_line stdin) in
  let ws = Str.split blank (input_line stdin) in
  let l = input_line stdin in
  let rec dfs state =
    let next (flag, ans, rest) w = match flag with
      | true -> (true, ans, rest)
      | false ->
        let (fit, last) = test w rest in
        if fit then if last = ""
          then (true, w :: ans, "")
          else dfs (false, w :: ans, last)
        else (false, ans, rest) in
    List.fold ws ~init:state ~f:next in 
  let (result, answer, _) = dfs (false, [], l) in
  let rec print s = match s with
    | [] -> print_string "\n"
    | h :: s -> print_string (h ^ " "); print s in
  if result then print (List.rev answer) else print_string "WRONG PASSWORD\n"
;;

for _t = 1 to t do
  round ()
done;;
