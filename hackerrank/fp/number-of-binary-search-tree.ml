open Core.Std;;


let rec solve n dp =
  let base = Int.pow 10 8 + 7 in
  if n = 1 || n = 0 then 1 else (
    match Hashtbl.find dp n with
    | Some(x) -> x
    | None -> (
        let sum = ref 0 in
        for k = 1 to n do
          sum := !sum + (solve (k - 1) dp) * (solve (n - k) dp);
        done;
        sum := !sum mod base;
        ignore(Hashtbl.add dp n !sum);
        !sum));;

let () =
  let dp = Hashtbl.Poly.create() in
  let t = read_int() in
  for _t = 1 to t do
    let n = read_int() in
    Printf.printf "%d\n" (solve n dp);
  done;;
