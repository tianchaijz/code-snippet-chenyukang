open Core.Std;;

let rec solve n dp =
  if n <= 1 then 1
  else (
    match Hashtbl.find dp n with
      Some(x) -> x
    | None -> (
      let p = solve (n - 1) dp in
      let v = p + 3 * n - 2 in
      ignore(Hashtbl.add dp n v);
      v));;

let () =
  let t = read_int() in
  let dp = Hashtbl.Poly.create() in
  for _t = 1 to t do
    let n = read_int() in
    Printf.printf "%d\n" (solve n dp);
  done;;
