open Core.Std;;

let res = ref [];;

let rec solve list curi cur cur_list =
  if cur = 1 then (
    res := cur_list;
    true
  ) else (
    if cur < 1 || curi >= List.length list then
      false
    else (
      let v = List.nth_exn list curi in
      if (cur mod v) = 0 &&
           (solve list curi (cur/v) (cur_list@[v])) then
        true
      else solve list (curi + 1) cur cur_list));;

let cmp (a :int) (b :int) =
  match a - b with
  | v when v < 0 -> 1
  | v when v > 0 -> -1
  | _ -> 0;;

let read_nums () = read_line () |> String.strip |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let _ =
  let nums = read_nums() in
  let n = List.nth_exn nums 0 in
  let k = List.nth_exn nums 1 in
  let l = List.filter (read_nums()) ~f:(fun x -> n mod x = 0) in
  let l' = List.sort l ~cmp:cmp in
  if solve l' 0 n [] then (
    res := List.rev (!res @ [1]);
    let r = ref 1 in
    List.iter !res (fun x ->
                    r := !r * x;
                    Printf.printf "%d " !r;
                   );
    Printf.printf "\n";
  ) else
    Printf.printf "-1\n";;
