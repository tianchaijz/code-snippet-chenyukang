open Core.Std;;

let read_words() = read_line() |> String.strip |> String.split ~on:' ';;

let pre_check arr dp =
  let rec it dp =
    match dp with
      [] -> ()
    | a::left -> (
        let i, w = (fst a), (snd a) in
        for t = i to i + (String.length w) - 1 do
          arr.(t) <- 1;
        done;
        it left) in
  it dp;
  match Array.find arr (fun x -> x = 0) with
    Some(_) -> false | _ -> true;;

let solve password words =
  let len = List.length words in
  let rec it words res =
    match words with
      [] -> res
    | a::left -> (
        let es = String.substr_index_all password ~may_overlap:true ~pattern: a in
        it left (List.append (List.map es (fun x -> (x, a))) res)) in 
  let dp = it words [] in
  let rec _solve cur index res =
    if cur = "" then Some(res) else
      List.find_map dp (fun e ->
           let i, w = (fst e), (snd e) in
           if i = index then (
             let l = String.length w in
             _solve (String.drop_prefix cur l) (index+l) (w::res))
           else None) in
  if pre_check (Array.create (String.length password) 0) dp then (
    match _solve password 0 [] with
      Some(r) -> List.rev r
    | _ -> [])
  else [];;
         
let () =
  let num = read_int() in
  for _ = 1 to num do
    let _ = read_int() in
    let words = read_words() in
    let password = read_line() in
    let res = solve password words in
    match res with
      [] -> Printf.printf "WRONG PASSWORD\n"
    | _ -> (List.iter res (fun x -> Printf.printf "%s " (String.to_string x)); Printf.printf "\n")
  done;;
