open Core.Std;;

let get list nth =
  match List.nth list nth with
    Some(x) -> x | None -> assert false;;

let trans list =
  let rec it cur now =
    if cur >= (List.length list) then now
    else
      let base = get list cur in
      let x = get list (cur + 1) in 
      it (cur + 2) ([base; x]:: now) in
  List.rev (it 0 []);;

let gcd la lb =
  let find base pow =
    let res = ref [] in 
    List.iter lb (fun x ->
        match x with
          b::(p::_) -> (if base = b then res := [base; Int.min p pow])
        | _ -> assert false);
    !res in
  let res = ref [] in
  List.iter la (fun x ->
      let r = find (get x 0) (get x 1) in
      if List.length r <> 0 then res := r :: !res);
  List.rev !res;;


let solve list =
  let res = ref (get list 0) in
  List.iter list (fun x -> res := gcd !res x);
  List.iter !res (fun x ->
      match x with
        a::(b::_) -> Printf.printf "%d %d " a b
      | _ -> assert false);
  Printf.printf "\n";;
  
let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string

let () =
  let n = read_int() in
  let nums = ref [] in
  for _ = 1 to n do
    let vs = read_nums() in
    let v = trans vs in
    nums := v :: !nums
  done;
  solve !nums;;

