(* https://www.hackerrank.com/challenges/dice-path *)

open Core.Std;;

let size = 61;;

let rotate_right cur =
  let next = [|0; 3; 3; 6; 1; 3; 3|] in
  next.(cur);;

let rotate_down cur =
  let next = [|0; 5; 1; 5; 5; 6; 5|] in
  next.(cur);;

let make_dp() =
  let dp = Array.make_matrix size size [||] in
  for i = 0 to size - 1 do
    for j = 0 to size - 1 do
      dp.(i).(j) <- Array.create 7 0;
    done;
  done;

  dp.(1).(1).(1) <- 1;
  dp.(1).(1).(0) <- 1;

  for j = 2 to size - 1 do
    let m = ref 0 in
    for k = 1 to 6 do
      if dp.(1).(j-1).(k) > 0 then
      let x = dp.(1).(j-1).(k) + (rotate_right k) in (
        dp.(1).(j).(rotate_right k) <- x;
        if x > !m then m := x)
    done;
    dp.(1).(j).(0) <- !m;
  done;

  for i = 2 to size - 1 do
    let m = ref 0 in
    for k = 1 to 6 do
      if dp.(i-1).(1).(k) > 0 then
        let x = dp.(i-1).(1).(k) + (rotate_down k) in (
          dp.(i).(1).(rotate_down k) <- x;
          if x > !m then m := x)
    done;
    dp.(i).(1).(0) <- !m;
  done;

  for i = 2 to size - 1 do
    for j = 2 to size - 1 do
      let u = dp.(i-1).(j) in
      let l = dp.(i).(j-1) in
      let mu = ref 0 in
      let ml = ref 0 in
      Array.iteri (fun k v ->
                   if k > 0 && v > 0 then
                     let k' =  rotate_down k in
                     let n = v + k' in (
                       mu := max !mu n;
                       dp.(i).(j).(k') <- max dp.(i).(j).(k') n;)) u;
      Array.iteri (fun k v ->
                   if k > 0 && v > 0 then
                    let k' = rotate_right k in
                    let n = v + k' in (
                      ml := max !ml n;
                      dp.(i).(j).(k') <- max dp.(i).(j).(k') n;)) l;
      let res = max !mu !ml in
      dp.(i).(j).(0) <- res;
    done;
  done;
  dp;;

let () =
  let dp = make_dp() in
  let t = read_int() in
  for _ = 1 to t do
    let i, j = Scanf.scanf " %d %d " (fun i j -> i, j) in
    Printf.printf "%d\n" dp.(i).(j).(0);
  done;;
