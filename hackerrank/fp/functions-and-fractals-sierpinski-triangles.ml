open Core.Std;;

let map =
  let s = "_______________________________1_______________________________
           ______________________________111______________________________
           _____________________________11111_____________________________
           ____________________________1111111____________________________
           ___________________________111111111___________________________
           __________________________11111111111__________________________
           _________________________1111111111111_________________________
           ________________________111111111111111________________________
           _______________________11111111111111111_______________________
           ______________________1111111111111111111______________________
           _____________________111111111111111111111_____________________
           ____________________11111111111111111111111____________________
           ___________________1111111111111111111111111___________________
           __________________111111111111111111111111111__________________
           _________________11111111111111111111111111111_________________
           ________________1111111111111111111111111111111________________
           _______________111111111111111111111111111111111_______________
           ______________11111111111111111111111111111111111______________
           _____________1111111111111111111111111111111111111_____________
           ____________111111111111111111111111111111111111111____________
           ___________11111111111111111111111111111111111111111___________
           __________1111111111111111111111111111111111111111111__________
           _________111111111111111111111111111111111111111111111_________
           ________11111111111111111111111111111111111111111111111________
           _______1111111111111111111111111111111111111111111111111_______
           ______111111111111111111111111111111111111111111111111111______
           _____11111111111111111111111111111111111111111111111111111_____
           ____1111111111111111111111111111111111111111111111111111111____
           ___111111111111111111111111111111111111111111111111111111111___
           __11111111111111111111111111111111111111111111111111111111111__
           _1111111111111111111111111111111111111111111111111111111111111_
           111111111111111111111111111111111111111111111111111111111111111" in
  String.split ~on: '\n' s |> List.map ~f:String.strip |> List.to_array;;


let rec solve (x, y) width height depth max =
  if depth = max then ()
  else (
    let mid_x =  x  in
    let mid_y = y + (width / 2) in
    String.set map.(mid_x) mid_y '_';
    let l = ref mid_y in
    let r = ref mid_y in
    for i = 0 to (height / 2) - 1 do
      for j = !l to !r do
        String.set map.(mid_x - i) j '_';
      done;
      l := !l - 1;
      r := !r + 1;
    done;
    solve (x, y) (width/2) (height/2) (depth+1) max;
    solve (x, y + width/2 + 1) (width/2) (height/2) (depth+1) max;
    solve ((x - (height/2)), (y + width/4 + 1)) (width/2) (height/2) (depth+1) max;
  );;


let _ =
  let n = read_int() in
  solve (31, 0) 63 32 0 n;
  Array.iter map ~f:(fun x -> Printf.printf "%s\n" x);;
