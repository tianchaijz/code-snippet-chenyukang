(* https://www.hackerrank.com/challenges/different-ways-fp *)
open Core.Std;;

let base = Int.pow 10 8 + 7;;

let rec solve n k dp =
  if dp.(n).(k) > 0 then dp.(n).(k)
  else
    match n, k with
    | _, 0 -> 1
    | n', k' when n' = k' -> 1
    | n', k' -> (
      let r = (solve (n - 1) (k - 1) dp + solve (n - 1) k dp) mod base in
      dp.(n).(k) <- r;
      r);;

let () =
  let dp = Array.make_matrix 1001 1001 (-1) in
  let t = read_int() in
  for _ = 1 to t do
    let n, k = Scanf.scanf " %d %d " (fun n k -> n, k) in
    Printf.printf "%d\n" (solve n k dp);
  done;;
