open Core.Std;;
open Str;;


(* let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string *)

(* let filter xs k = *)
(*   let update m e = Map.change m e (fun x -> match x with | None -> Some 1 | Some n -> Some (n + 1)) in *)
(*   let counts = List.fold xs ~init:Int.Map.empty ~f:update in *)
(*   let update (seen, items) e = *)
(*     if (Map.find_exn counts e >= k) && (not (Set.mem seen e)) *)
(*     then (Set.add seen e, e :: items) *)
(*     else (seen, items) *)
(*   in *)
(*   match List.fold xs ~init:(Int.Set.empty, []) ~f:update |> snd with *)
(*   | [] -> [-1] *)
(*   | _ as l -> List.rev l *)

(* let () = *)
(*   let num_tests = read_line () |> Int.of_string in *)
(*   for i = 1 to num_tests do *)
(*     let [_; k] = read_nums () in *)
(*     let nums = read_nums () in *)
(*     filter nums k |> List.iter ~f:(fun x -> printf "%d " x); *)
(*     print_newline () *)
(*   done *)


let solve k list =
  let hash = Hashtbl.Poly.create() in
  let found = ref false in
  List.iter (List.rev list)
            (fun cur ->
             match Hashtbl.find hash cur with
               None -> ignore(Hashtbl.add hash cur 1); if k = 1 then found := true;
               | Some(x) -> ignore(Hashtbl.set hash cur (x + 1)); if x + 1 = k then found := true);
  if !found = false  then
    Printf.printf "-1"
  else
    List.iter list (fun x ->
                    match Hashtbl.find hash x with
                    | None -> ()
                    | Some(c) -> (
                      if c >= k then begin
                          Printf.printf "%d " x;
                          ignore(Hashtbl.remove hash x);
                        end));
  Printf.printf "\n";;


let read_list() =
  let line = read_line() in
  let list = List.map (Str.split (Str.regexp_string " ") line) int_of_string in
  list;;

let list_get list k =
  match List.nth list k with
  | Some(x) -> x
  | None -> 0;;

let process_case() =
  let l = read_list() in
  let _ = list_get l 0 in
  let k = list_get l 1 in
  let list = read_list() in
  solve k list;;

let () =
  let case_number = read_int() in
  for _ = 1 to case_number do
    process_case();
  done;
