open Core.Std;;

let rotate_left v k =
  let len = List.length v in
  let d = len - (k mod len) in
  let res = Array.create len 0 in
  for i = 0 to len - 1 do
    res.(i) <- List.nth_exn v ((i + d) mod len);
  done;
  res;;

let solve matrix m n k =
  let step = (Int.min m n) / 2  - 1 in
  for s = 0 to step do
    let v = ref [] in
    let (i, j) = (s, s) in
    let (i', j') = ((m - 1 - s), (n - 1 - s)) in
    for x = i to i' do
      v := matrix.(x).(j) :: !v;
    done;
    for y = j + 1 to j' do
      v := matrix.(i').(y) :: !v;
    done;
    for x = i + 1 to i' do
      v := matrix.(m - 1 - x).(j') :: !v;
    done;
    for y = j + 1 to j' - 1 do
      v := matrix.(i).(n - 1 - y) :: !v;
    done;
    v := List.rev !v;
    v := Array.to_list (rotate_left !v k);
    let d = ref 0 in
    for x = i to i' do
      matrix.(x).(j) <- List.nth_exn !v !d; incr d;
    done;
    for y = j + 1 to j' do
      matrix.(i').(y) <- List.nth_exn !v !d; incr d;
    done;
    for x = i + 1 to i' do
      matrix.(m - 1 - x).(j') <- List.nth_exn !v !d; incr d;
    done;
    for y = j + 1 to j' - 1 do
      matrix.(i).(n - 1 - y) <- List.nth_exn !v !d; incr d;
    done;
  done;;

let read_nums() = read_line() |> String.strip |> String.split ~on:' '
                  |> List.map ~f:Int.of_string;;

let print matrix m n =
  for i = 0 to m - 1 do
    for j = 0 to n - 1 do
      Printf.printf "%d " matrix.(i).(j);
    done;
    Printf.printf "\n";
  done;;

let _ =
  let l = read_nums() in
  let m = List.nth_exn l 0 in
  let n = List.nth_exn l 1 in
  let r = List.nth_exn l 2 in
  let matrix = Array.make_matrix m n 0 in
  for i = 0 to m - 1 do
    let l = read_nums() in
    for j = 0 to n - 1 do
      matrix.(i).(j) <- List.nth_exn l j;
    done;
  done;
  ignore(solve matrix m n r);
  print matrix m n;;
