open Core

let rec read_lines () =
  try let line = read_line () in
      int_of_string (line) :: read_lines()
  with
    End_of_file -> []

let f n arr =
  let rec make v n =
    match n with
      0 -> []
    | _ -> v::(make v (n - 1)) in
  let res = List.map (fun v -> make v n)
                     arr in
  List.fold_left List.append [] res

let () =
  let n::arr = read_lines() in
  let ans = f n arr in
  List.iter (fun x -> print_int x; print_newline ()) ans;;
