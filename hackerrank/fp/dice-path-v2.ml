(* https://www.hackerrank.com/challenges/dice-path *)

open Core.Std;;

let size = 61;;

(* rotate down *)
let rd (up, fr, ri, sum) = (7 - fr, up, ri, sum + 7 - fr);;
(* rotate right *)
let rr (up, fr, ri, sum) = (7 - ri, fr, up, sum + 7 - ri);;

let better a b =
  match a, b with
    (u1, _, _, s1), (u2, _, _, s2) -> u1 = u2 && s1 >= s2
  | _, _ -> false;;

let filter ss =
  let h = ref [] in
  let rec need l x =
    match l with
      [] -> true
    | n::left -> (if better n x then false else (need left x)) in
  List.iter ss (fun x -> if need !h x then h := x :: !h);
  !h;;

let make_dp() =
  let dp = Array.make_matrix size size [] in
  dp.(1).(1) <- [(1, 2, 4, 1)];

  for j = 2 to size - 1 do
    let p = dp.(1).(j-1) in
    let rec it p now =
      match p with
        [] -> dp.(1).(j) <- filter now
      | s::n -> it n ((rr s)::now) in
    it p [];
  done;

  for i = 2 to size - 1 do
    let p = dp.(i-1).(1) in
    let rec it p now =
      match p with
        [] -> dp.(i).(1) <- filter now
      | s::n -> it n ((rd s)::now) in
    it p [];
  done;

  for i = 2 to size - 1 do
    for j = 2 to size - 1 do
      let u = dp.(i-1).(j) in
      let l = dp.(i).(j-1) in
      let u' = List.map u (fun x -> rd x) in
      let l' = List.map l (fun x -> rr x) in
      dp.(i).(j) <- filter (List.append u' l');
    done;
  done;
  dp;;

let solve dp i j =
  let h = dp.(i).(j) in
  List.fold_left h ~init:0 ~f:(fun now s ->
                               match s with
                                 (_, _, _, sum) -> max sum now);;

let () =
  let dp = make_dp() in
  let t = read_int() in
  for _ = 1 to t do
    let i, j = Scanf.scanf " %d %d " (fun i j -> i, j) in
    Printf.printf "%d\n" (solve dp i j);
  done;
