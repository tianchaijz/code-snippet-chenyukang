open Core.Std;;

let solve str =
  let len = String.length str in
  let s = ref str in 
  for i = 0 to len - 1 do
    let cur = (!s).[0] in
    let left = String.drop_prefix (!s) 1 in begin
      s :=  left ^ (String.of_char cur);
      Printf.printf "%s " (String.to_string !s);
    end;
  done;;
  

let () =
  let n = read_int() in
  for _ = 1 to n do
    let s = read_line() in
    solve s;
    Printf.printf "\n";
  done;;

    
