open Core.Std;;


let rec solve str =
  (* Printf.printf "now: %s\n" (String.to_string str); *)
  if String.length str <= 1 then
    Int.of_string str
  else (
    let next = ref 0 in
    String.iter str ~f:(fun x ->
                     let x' = Int.of_string (String.of_char x) in
                     next := !next + x');
    solve (Printf.sprintf "%d" !next)
  );;

let read_nums() = read_line() |> String.strip |> String.split ~on:' ';;

let _ =
  let nums = read_nums() in
  match nums with
    a::(b::_) -> (
    let v = String.fold a ~init:0  ~f:(fun sum c -> sum + (Int.of_string (String.of_char c))) in
    let s = Printf.sprintf "%d" (v * (Int.of_string b)) in
    Printf.printf "%d\n" (solve s)
  )
  | _ -> assert false ;;
