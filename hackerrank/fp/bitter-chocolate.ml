open Core.Std;;

let max = 30;;

let make_dp () =
  let res = Array.create max [||] in
  for i = 0 to max - 1 do
    res.(i) <- Array.make_matrix max max (-1);
  done;
  res.(1).(0).(0) <- 0; (* 0 means player lose *)
  res;;

let rec solve r1 r2 r3 dp =
  if dp.(r1).(r2).(r3) <> (-1) then
    dp.(r1).(r2).(r3)
  else
    let next = ref false in 
    for i = 1 to (r1 - 1) do
      if solve i (Int.min i r2) (Int.min i r3) dp = 0 then next := true
    done;
    for i = 0 to (r2 - 1) do
      if solve r1 i (Int.min i r3) dp = 0 then next := true
    done;
    for i = 0 to (r3 - 1) do
      if solve r1 r2 i dp = 0 then next := true
    done;
    if !next then (
      dp.(r1).(r2).(r3) <- 1;
      1
    ) else (
      dp.(r1).(r2).(r3) <- 0;
      0
    );;
      

let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string;;
                     
let _ =
  let t = read_int() in
  for _ = 1 to t do
    let dp = make_dp() in 
    let nums = read_nums() in
    let (r1, r2, r3) = (List.nth_exn nums 0, List.nth_exn nums 1, List.nth_exn nums 2) in
    if solve r1 r2 r3 dp  = 0 then
      Printf.printf "LOSE\n"
    else
      Printf.printf "WIN\n"
  done;
  

