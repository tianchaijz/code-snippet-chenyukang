open Core.Std;;

let solve sa sb =
  match String.substr_index sa sb with
    Some(idx) -> true
  | _ -> false;;


let () =
  let num = read_int() in
  for _ = 1 to num do
    let a = read_line() in
    let b = read_line() in
    if solve a b then
      Printf.printf "YES\n"
    else
      Printf.printf "NO\n"
  done;;
  
