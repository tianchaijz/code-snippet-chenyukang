open Core.Std;;

let process str =
  let compress cur count =
    match count with
      0 -> ""
    | 1 -> (String.of_char cur)
    | _ -> (String.of_char cur) ^ (Int.to_string count) in
  let rec it now res prev count =
    (* Printf.printf "now: %s res: %s\n" (String.to_string now) (String.to_string res); *)
    if String.length now = 0 then
      res ^ (compress prev count)
    else
      let first = String.nget now 0 in
      let rest = String.drop_prefix now 1 in
      if first = prev then
        it rest res prev (count+1)
      else
        let next = res ^ (compress prev count) in
        it rest next first 1 in
  it str "" '_' 0;;

let compress cur count =
  match count with
    0 -> ""
  | 1 -> (String.of_char cur)
  | _ -> (String.of_char cur) ^ (Int.to_string count);;

let process_v str =
  let res = ref "" in
  let count = ref 0 in
  let prev = ref '_' in
  for i = 0 to (String.length str)  - 1 do
    let cur = String.nget str i in
    (* Printf.printf "now cur: %c\n" cur; *)
    if cur = (!prev) then
      incr count
    else begin
      res := !res ^ (compress !prev !count);
      count := 1;
      prev := cur;
    end
  done;
  res := !res ^ (compress !prev !count);
  !res;;


let process_v3 str =
  let len = String.length str in
  let rec loop cur index list =
    match list with
    | _ when index >= len -> list
    | ((ch, count) :: tl) when str.[index] = ch ->
       loop ch (index+1) ((ch, count + 1) :: tl)
    | _ -> loop str.[index] (index+1) ((str.[index], 1)::list) in
  let l = loop str.[0] 1 [(str.[0], 1)] |> List.rev in
  let rec print_list = function
    | [] -> ()
    | (ch, count)::tl when count = 1 -> (print_char ch; print_list tl)
    | (ch, count)::tl -> (print_char ch; print_int count; print_list tl) in
  print_list l;;

let () =
  let str = read_line() in
  process_v3 str;;
