open Core.Std;;

let query arr l r =
  let x = if l - 2 < 0 then 0 else arr.(l - 2) in 
  let res = arr.(r-1) - x in
  if res mod 2 = 0 then Printf.printf "Even\n"
  else Printf.printf "Odd\n";;

let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let read_two () =
  let nums = read_nums() in
  match nums with
    a::(b::_) -> a, b
  | _ -> assert false;;

let read_arr() =
  let numbers = read_nums() in
  let arr = List.to_array numbers in
  for i = 1 to Array.length arr - 1 do
    arr.(i) <- (arr.(i-1) + arr.(i)) mod 2;
  done;
  arr;;
                             

let () =
  let n, q = read_two() in
  let arr = read_arr() in
  for _ = 1 to q do
    let l, r = read_two() in
    query arr l r;
  done;;
