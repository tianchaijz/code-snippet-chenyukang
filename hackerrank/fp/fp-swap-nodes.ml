open Core.Std;;

type tree = Empty | Leaf of int | Node of tree * int * tree

let rec inorder root =
  match root with
    Empty -> ()
  | Leaf(v) -> Printf.printf "%d " v
  | Node(l, v, r) -> (inorder l; Printf.printf "%d " v; inorder r);;

let make_node v l r =
  match l, r with
    -1, -1 -> Leaf v
  | -1, x -> Node(Empty, v, Leaf x)
  | x, -1 -> Node(Leaf x, v, Empty)
  | x, y -> Node(Leaf x, v, Leaf y);;

let rec find root v =
  match root with
    Empty -> None
  | Node(l, _v, r) -> (
    if _v = v then Some(root)
    else
      match (find l v), (find r v) with
      | Some(_), None -> Some(l)
      | None, Some(_) -> Some(r)
      | _, _ -> None)
  | Leaf(_v) -> (if _v = v then Some(root) else None);;

let rec insert root value left right =
  match root with
    Empty -> make_node value left right
  | Node(l, v, r) -> (
    match (find l value), (find r value) with
      Some(_), None -> Node((insert l value left right), v, r)
    | None, Some(_) -> Node(l, v, (insert r value left right))
    | _, _ -> assert false)
  | Leaf(v) -> ( if v = value then
                   (make_node value left right)
                 else root);;

let rec swap root kth cur =
  match root with
    Node(l, v, r) -> (
    let next = cur + 1 in
    if cur mod kth = 0  then
      Node((swap r kth next), v, (swap l kth next))
    else
      Node((swap l kth next), v, (swap r kth next)))
  | _ -> root;;

let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string

let () =
  let n = read_int() in
  let tree = ref Empty in
  for i = 1 to n do
    match read_nums() with
      a::(b::_) -> ( tree := insert !tree i a b )
    | _ -> assert false;
  done;
  let swap_num = read_int() in
  for _ = 1 to swap_num do
    let kth = read_int() in
    tree := swap !tree kth 1;
    inorder !tree;
    Printf.printf "\n";
  done
