open Core.Std;;

let rec gcd a b =
  if a = b then a
  else (if a > b then gcd (a - b) b
        else gcd a (b - a));;

let primes va =
  let rec it v m =
    match v, m with
    | v, m when (m * m) > v -> if v = 1 then [] else [v]
    | v, m when v mod m = 0 -> [m] @ (it (v / m) m)
    | _, _ -> it v (m + 1) in
  (it va 2);;

let solve a b =
  let ps = primes (gcd a b) in
  let g = List.group ps ~break:(fun a b -> a <> b) in
  List.fold g ~init:1  ~f:(fun ac x -> ac * ((List.length x) + 1));;

let () =
  let n = read_int() in
  for _ = 1 to n do
    let a, b = Scanf.scanf " %d %d" (fun i j -> i, j) in
    Printf.printf "%d\n" (solve a b);
  done;;
