open Core.Std;;

let try_this nums k =
  if k > 0 && k <= Array.length nums then
    nums.(k-1)
  else
    0;;

let solve nums sum =
  let l = 0 in
  let r = Array.length nums in
  let rec loop l r =
    if l <= r then (
      let mid = l + (r - l) / 2 in
      let max = try_this nums mid in
      if max >= sum then loop l (mid - 1)
      else loop (mid + 1) r)
    else
      l in
  let res = loop l r in
  if res > Array.length nums then -1
  else res;;

let read_nums() = read_line() |> String.split ~on:' '
                  |> List.map ~f:Int.of_string |> List.to_array;;

let () =
  let n = read_int() in
  let nums = read_nums() in
  Array.stable_sort nums (fun a b -> -(Int.compare a b));
  let res = ref 0 in
  Array.iteri ~f:(fun i v ->
                  res := !res + v;
                  nums.(i) <- !res) nums;
  let k = read_int() in
  for _ = 1 to k do
    let s = read_int() in
    Printf.printf "%d\n" (solve nums s);
  done;;
