open Core.Std;;

let res = ref 0 ;;

let conflict cur x =
  let (a, b) = x in
  let (a', b') = cur in
  let diff_a = a - a' in
  let diff_b = b - b' in
  let abs_a = Int.abs diff_a in
  let abs_b = Int.abs diff_b in
  let t = ((a = a') || (b = b') ||
           (abs_a = 1 && abs_b = 2) ||
           (abs_b = 1 && abs_a = 2)  ||
           (abs_a = abs_b))
  in t;;

let rec solve row cur max =
  let can_put x =
    match List.find cur ~f:(fun n -> conflict n x) with
      Some(_x) -> false | None -> true in 
  if row = max then ( incr res; )
  else (
    for col = 1 to (max - 1) do
      if can_put (row, col) then
        solve (row + 1) ((row, col)::cur) max;
    done;
  );;
    
let _ =
  let n = read_int() in
  if n > 1 then 
    solve 1 [] (n+1);
  Printf.printf "%d\n" !res;;
    
