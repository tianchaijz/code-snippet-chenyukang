open Core.Std;;

(* https://www.hackerrank.com/challenges/pangrams  *)

let solve line =
  let l = String.to_list line in
  let hash = Array.create 26 0 in
  let to_int c =
    let v = int_of_char c in
    match v with
      x when x >= 97 && x <= 122 -> (v - 97)
    | x when x >= 65 && x <= 90 -> (v - 65)
    | _ -> -1 in
  List.iter l (fun x ->
               match to_int x with
                 v when v >= 0 -> hash.(v) <- hash.(v) + 1
               | _ -> ());
  let r = Array.filter (fun x -> x = 0) hash in
  Array.length r = 0;;

let () =
  let s = read_line() in
  if solve s then print_endline "pangram"
  else print_endline "not pangram";;
