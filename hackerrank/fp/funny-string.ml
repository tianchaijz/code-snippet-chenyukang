(* https://www.hackerrank.com/challenges/funny-string *)
open Core.Std;;

let solve str =
  let rev = String.to_array (String.rev str) in
  let pre = String.to_array str in
  let len = Array.length pre in
  let res = Array.filteri (fun i c ->
                 if i < 1 then true
                 else (
                   let x1 = int_of_char pre.(i-1) in
                   let x2 = int_of_char c in
                   let y1 = int_of_char rev.(i-1) in
                   let y2 = int_of_char rev.(i) in
                   let d1 = x1 - x2 in
                   let d2 = y1 - y2 in
                   Int.abs d1 = Int.abs d2)) pre in
  Array.length res = len;;


let () =
  let t = read_int() in
  for _ = 1 to t do
    let s = read_line() in
    match solve s with
    | true -> print_endline "Funny"
    | _ -> print_endline "Not Funny"
  done;;
