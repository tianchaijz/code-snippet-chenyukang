open Core.Std;;

let read_input() =
  let line = read_line() in
  let a = String.split_on_chars line ~on: [' '; '\t'; '\r'] in
  List.map ~f:int_of_string a;;


let rec fib n =
  match n with
    1 -> 0
  | 2 -> 1
  | _ -> fib (n - 1) + fib (n - 2);;


let () =
  let input = read_input() in
  match input with
    a::rest -> Printf.printf "%d\n" (fib a)
  | _ -> Printf.printf "error\n";;
