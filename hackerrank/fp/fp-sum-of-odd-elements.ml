let rec read_lines() =
  try let line = read_line() in
      int_of_string (line) :: read_lines()
  with
    End_of_file -> [];;
  
let () =
  let arr = read_lines() in
  let arr = List.filter (fun x -> (abs (x mod 2)) = 1) arr in
  let res = List.fold_left (+) 0 arr in
  Printf.printf "%d\n" res;;
  
