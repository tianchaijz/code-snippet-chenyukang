open Core.Std;;

let direction = [|(0, 1); (1, 0)|];;

let valid_direction map current =
  let len = String.length current in
  let res = ref [] in
  let valid map x y =
    x >= 0 && x < (Array.length map) && y >= 0 && y < (Array.length map.(0)) in 
  let can_put x y (dx, dy) =
    let elems = ref [] in 
    for i = 0 to len - 1 do
      let nx, ny = x + i * dx , y + i * dy in
      if (valid map nx ny) &&
         (map.(nx).(ny) = '-' || map.(nx).(ny) = current.[i]) then
        elems := (dx, dy)::!elems;
    done;
    List.length !elems = len in
  for i = 0 to Array.length map - 1 do
    for j = 0 to Array.length map - 1 do
      for k = 0 to Array.length direction - 1 do
        if can_put i j direction.(k) then
          res := (i, j, k)::!res;
      done;
    done;
  done;
  !res;;

let apply_on_map map current (x, y, d) =
  let prev = ref "" in
  let (dx, dy) = direction.(d) in
  for k = 0 to String.length current - 1 do
    let nx, ny = x + k * dx, y + k * dy in
    prev := !prev ^ (String.of_char map.(nx).(ny));
    map.(nx).(ny) <- current.[k];
  done;
  !prev;;

let rec solve map words ~depth =
  if depth >= List.length words then (
    for i = 0 to 9 do
      Array.iter map.(i) ~f:(fun x -> Printf.printf "%c" x);
      print_newline();
    done;
    true
  )
  else (let current = List.nth_exn words depth in
        let candidates = valid_direction map current in
        let next = List.find candidates ~f:(fun e ->
            let prev = apply_on_map map current e in
            if solve map words (depth+1) then true
            else ( ignore(apply_on_map map prev e); false)) in
        match next with
          Some(_) -> true | None -> false);;

let read_words () = read_line () |> String.strip |> String.split ~on:';';;

let _ =
  let map = Array.create 10 [||] in
  for i = 1 to 10 do
    let l = read_line() in
    map.(i-1) <- String.to_array l;
  done;
  let words = read_words() in
  solve map words 0;;

