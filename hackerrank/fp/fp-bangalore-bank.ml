open Core.Std;;


let size = 11;;

let diff a b =
  Int.abs (a - b) + 1;;

let make_dp n vec =
  let dp = Array.make_matrix (n+1) size [||] in
  for i = 0 to n do
    for j = 0 to 10 do
      dp.(i).(j) <- Array.create size 0;
    done;
  done;

  (* base case *)
  for i = 1 to 10 do
    for j = 1 to 10 do
      dp.(1).(i).(j) <- 1;
    done;
  done;

  (* inductive case *)
  for i = 2 to n do
    let last = List.nth_exn vec (i-2) in
    let now = List.nth_exn vec (i-1) in
    for k = 1 to 10 do
      let last_to_now = diff last now in
      let last_to_k = diff last k in
      dp.(i).(now).(k) <- min (dp.(i-1).(last).(k) + last_to_now) (dp.(i-1).(now).(last) + last_to_k);
      dp.(i).(k).(now) <- min (dp.(i-1).(k).(last) + last_to_now) (dp.(i-1).(last).(now) + last_to_k);
    done;
  done;
  dp;;

let solve n dp vec =
  let res = ref Int.max_value in
  let last = List.last_exn vec in
  for i = 1 to 10 do
    res := min !res dp.(n).(last).(i);
    res := min !res dp.(n).(i).(last);
  done;
  !res;;

let read_nums() = read_line() |> String.split ~on:' ' |> List.map ~f:(fun x ->
                                                                      (let v = Int.of_string x in
                                                                       if v > 0 then v else 10));;

let () =
  let n = read_int() in
  let vec = read_nums() in
  let dp = make_dp n vec in
  let res = solve n dp vec in
  Printf.printf "%d\n" res;;
