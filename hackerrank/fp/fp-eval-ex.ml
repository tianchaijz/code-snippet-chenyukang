
let read_int() =
  let line = read_line() in
  int_of_string line;;

let rec read_floats need now =
  if now = need then []
  else
    try let line = read_line () in
        float_of_string (line) :: read_floats need (now + 1)
    with
      End_of_file -> [];;
  
                     

let rec eval_ex value now cur res =
  if now == 10 then res
  else
    let next = cur *. (value) /. (float_of_int now) in 
    eval_ex value (now + 1) next (res +. next);;
    
let () =
  let n = read_int() in
  let values = read_floats n 0 in
  List.iter (fun x -> Printf.printf "%.4f\n" (eval_ex x 1 1.0 1.0)) values;;

  
          
  
