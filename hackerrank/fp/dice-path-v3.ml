
let faces = [
    (1, 2, 4); (1, 3, 2); (1, 4, 5); (1, 5, 3);
    (2, 1, 3); (2, 3, 6); (2, 4, 1); (2, 6, 4);
    (3, 1, 5); (3, 2, 1); (3, 5, 6); (3, 6, 2);
    (4, 1, 2); (4, 2, 6); (4, 5, 1); (4, 6, 5);
    (5, 1, 4); (5, 3, 1); (5, 4, 6); (5, 6, 3);
    (6, 2, 3); (6, 3, 5); (6, 4, 2); (6, 5, 4)]

let up ((x, y), (t, f, r)) = (x-1, y), (f, 7-t, r)
let left ((x, y), (t, f, r)) = (x, y-1), (r, f, 7-t)
let top (t, _, _) = t
let tbl = Hashtbl.create 1181

let rec search ((pos, face) as state)=
  try Hashtbl.find tbl state
  with Not_found ->
    let new_entry =
      if fst pos = 0 || snd pos = 0 then 0
      else
        let from_up = search (up state) in
        let from_left = search (left state) in
        if (max from_up from_left) = 0 then 0
        else (max from_up from_left) + top face in
    Hashtbl.replace tbl state new_entry;
    new_entry

let solve pos =
  let results = List.map (fun face -> search (pos, face)) faces in
  List.fold_left max 0 results

let _ =
  let t = read_int () in
  Hashtbl.replace tbl ((1, 1), (1, 2, 4)) 1;
  let rec test i =
    if i = 0 then ()
    else
      let pos = Scanf.scanf " %d %d" (fun x y -> x, y) in
      let max = solve pos in
      print_endline (string_of_int max);
      test (i-1)
  in test t
