open Core.Std;;


let construct dp nums =
  let len = List.length nums in
  let k = ref (len - 1) in 
  let elm = ref (List.find dp.(!k) (fun ((x, _, _)) -> x mod 101 = 0)) in
  let res = ref [] in
  while !k >= 1 do 
    match !elm with
      Some((_, op, last)) -> (
        res := !res @ [op];
        decr k;
        elm := List.nth dp.(!k) last;
      )
    | None -> ();
  done;
  res := List.rev !res;
  let output = ref "" in 
  for i = 0 to len - 2 do
    output := !output ^ (Printf.sprintf " %d %c" (List.nth_exn nums i) (List.nth_exn !res i));
  done;
  output := !output ^ (Printf.sprintf " %d" (List.nth_exn nums (len - 1)));
  String.strip !output;;

let solve dp nums =
  let first = List.nth_exn nums 0 in 
  dp.(0) <- [(first, '.', -1)];
  let len = List.length nums in
  for i = 1 to len - 1 do
    let prev = dp.(i - 1) in
    let next = ref [] in
    let value = List.nth_exn nums i in
    let seen = Hashtbl.Poly.create() in
    List.iteri prev (fun idx (x, _, _) ->
        List.iter [((x + value) mod 101, '+', idx);
                   ((x - value + 101) mod 101, '-', idx);
                   ((x * value) mod 101, '*', idx)]
          (fun (x, op, idx) ->
             if not (Hashtbl.mem seen x) then (
               next := !next @ [(x, op, idx)];
               Hashtbl.replace seen x ();
             )));
    dp.(i) <- !next;
  done;
  construct dp nums;;

let read_nums () = read_line() |> String.strip |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let _ =
  let n = read_int() in
  let nums = read_nums() in
  let dp = Array.create n [] in
  let res = solve dp nums in
  Printf.printf "%s\n" (String.to_string res);;

  
