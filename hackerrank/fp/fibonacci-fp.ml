open Core.Std;;

let hash = Hashtbl.Poly.create();;
let base = Int.pow 10 8 + 7;;

let rec kth_fib k =
  match k with
    0 | 1 -> k
  | _ -> (
      match Hashtbl.find hash k with
        Some(x) -> x
      | _ -> (let x = ((kth_fib (k - 1)) + (kth_fib (k - 2))) mod base in
              ignore(Hashtbl.add hash k x);
              x));;

let () =
  let num = read_int() in
  for _ = 1 to num do
    let k = read_int() in
    Printf.printf "%d\n" (kth_fib k);
  done;;
