open Core.Std;;

let read_int () = Scanf.bscanf Scanf.Scanning.stdib " %d " (fun x -> x)
let read_float () = Scanf.bscanf Scanf.Scanning.stdib "%f " (fun x -> x)
let read_string () = Scanf.bscanf Scanf.Scanning.stdib "%s " (fun x -> x)

let rec compress a b =
  match String.length a , String.length b with
    0, _  | _, 0-> ""
    | _, _ -> (
    let fa = String.nget a 0 in
    let fb = String.nget b 0 in
    if fa = fb then
      (String.of_char fa) ^ (compress (String.drop_prefix a 1)
                                      (String.drop_prefix b 1))
    else
      ""
  );;

let solve a b =
  let res = compress (String.strip a) (String.strip b) in
  let len = String.length res in
  let left_a = String.drop_prefix a len in
  let left_b = String.drop_prefix b len in
  Printf.printf "%d %s\n" len (String.to_string res);
  Printf.printf "%d %s\n" (String.length left_a) (String.to_string left_a);
  Printf.printf "%d %s\n" (String.length left_b) (String.to_string left_b);;

let solve_v2 x y =
  let pos = ref 0 in
  let nx = String.length x in
  let ny = String.length y in
  let n = min nx ny in
  while !pos < n && x.[!pos] = y.[!pos] do
    incr pos
  done;
  Printf.printf "%d %s\n" !pos (String.sub x 0 !pos);
  Printf.printf "%d %s\n" (nx - !pos) (String.drop_prefix x !pos);
  Printf.printf "%d %s\n" (ny - !pos) (String.drop_prefix y !pos);;

let () =
  let a = read_line() and
      b = read_line() in
  solve_v2 a b;;
