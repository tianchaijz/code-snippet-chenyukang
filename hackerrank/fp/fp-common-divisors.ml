open Core.Std;;

let rec gcd a b =
  if a = b then a
  else begin
      if a > b then gcd (a - b) b
      else gcd a (b - a)
    end;;

let solve a b =
  let min = Int.min a b and
      max = Int.max a b in
  let hash_max = Hashtbl.Poly.create() in
  let hash_min = Hashtbl.Poly.create() in
  let set_hash hash v g =
    for i = 1 to g do
      if v mod i = 0 then
        Hashtbl.set hash i 0;
    done in
  let g = gcd a b in
  set_hash hash_max max g;
  set_hash hash_min min g;
  let res = ref 0 in
  Hashtbl.iter hash_min ~f:(fun ~key:x ~data: _ -> if Hashtbl.mem hash_max x then
                                                     incr res);
  !res;;

let () =
  let n = read_int() in
  for _ = 1 to n do
    let a, b = Scanf.scanf " %d %d" (fun i j -> i, j) in
    Printf.printf "%d\n" (solve a b);
  done;;
