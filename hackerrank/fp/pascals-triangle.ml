open Core.Std;;

let print l =
  List.iter l (fun x -> Printf.printf "%d " x);
  print_newline();;

let read_input() =
  let line = read_line() in
  let a = String.split_on_chars line ~on: [' '; '\t'; '\r'] in
  let l = List.map ~f:int_of_string a in
  match l with
    v::rest -> v
  | _ -> 0;;

let mult_to k =
  let r = ref 1 in
  for i = 1 to k do
    r := i * !r;
  done;
  !r

let value r c =
  (* Printf.printf "now: %d %d\n" r c; *)
  (mult_to r) / ((mult_to c) * (mult_to (r - c)));;

let next r =
  let rec it c =
    if c > r then []
    else
      (value r c)::(it (c + 1)) in
  it 0;;


let rec process n k =
  let res = next n in
  print res;
  if n < k - 1 then
    process (n + 1) k;;


let () =
  let k = read_input() in
  process 0 k;;
