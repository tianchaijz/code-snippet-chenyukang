open Core.Std;;

let rec better cur pre =
  match List.length cur, List.length pre with
  | l1, 0 -> true
  | l1, l2 when l1 < l2 -> true
  | l1, l2 when l1 > l2 -> false
  | l1, l2 ->  (
      if l1 = 0 then false else
        match cur, pre with
        | v1::left1, v2::left2 -> ( if v1 < v2 then true
                                    else (if v2 < v1 then false 
                                          else better left1 left2))
        | _, _ -> assert false);;


let print_list l =
  if List.length l = 0 then
    Printf.printf "-1\n"
  else (
    List.iter l (fun x -> Printf.printf "%d " x);
    Printf.printf "\n");;

let res = ref [];;

let rec solve depth numbers target cur_val cur_list =
  print_list cur_list;
  match target, cur_val with
  | t, cur when cur = t -> (
      print_list cur_list;
      if better cur_list !res then
        res := cur_list;
    )
  | t, cur when cur > t -> ()
  | t, cur -> (
      if depth = (List.length numbers) then ()
      else (
        let next = List.nth_exn numbers depth in 
        let next_cur = cur_val * next in
        ignore(solve (depth+1) numbers target next_cur (cur_list@[next_cur]));
        ignore(solve (depth+1) numbers target cur_val cur_list))
  );;


let read_nums () = read_line () |> String.strip |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let _ =
  let nums = read_nums() in
  let n = List.nth_exn nums 0 in
  let k = List.nth_exn nums 1 in
  let l = read_nums() in
  solve 0 l n 1 [1];
  print_list !res;;


