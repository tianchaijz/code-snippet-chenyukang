open Core.Std;;

let res = ref 0;;
let base = Int.pow 10 9 + 7;;
let bits = Array.init 3 (fun i -> i + 4);;

let rec dfs arr limit now depth max_depth =
  if depth = max_depth then ()
  else
    for i = 0 to 2 do
      if arr.(i) < limit.(i) then (
        let next = (now * 10 + bits.(i)) mod base in
        Printf.printf "add: %d\n" next;
        flush stdout;
        res := (!res + next) mod base;
        arr.(i) <- arr.(i) + 1;
        dfs arr limit next (depth + 1) max_depth;
        arr.(i) <- arr.(i) - 1;
      )
    done;;

let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let read_limits () =
  let nums = read_nums() in
  match nums with
    a::(b::(c::_)) -> [|a; b; c|]
  | _ -> assert false;;

let () =
  let limits = read_limits() in
  let max_depth = Array.fold_right limits ~init:0 ~f:(+) in
  dfs (Array.create 3 0) limits 0 0 max_depth;
  Printf.printf "%d\n" !res;;
