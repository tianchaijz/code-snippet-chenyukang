open Core.Std;;

let merge a b =
  let res = List.mapi a ~f:(fun i v -> (v, List.nth_exn b i)) in
  List.to_array res;;

let try_this ab k =
  let res = Array.map ab ~f:(fun (a, b) -> a + (k-1) * b) in
  Array.stable_sort res Int.compare;
  let rec fold_k sum i =
    if i >= k then sum
    else
      fold_k (sum + res.(i)) (i+1) in
  fold_k 0 0;;

let solve ab m =
  let l = 0 in
  let r = Array.length ab in
  let rec loop l r =
    if l <= r then (
      let mid = l + (r - l) / 2 in
      let min = try_this ab mid in
      if min > m then loop l (mid - 1)
      else loop (mid + 1) r)
    else
      l - 1 in
  loop l r;;

let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let () =
  let nums = read_nums() in
  let n, m = (List.hd_exn nums), (List.last_exn nums) in
  let a = read_nums() in
  let b = read_nums() in
  let ab = merge a b in
  Printf.printf "%d\n" (solve ab m);;
