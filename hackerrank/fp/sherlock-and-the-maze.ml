open Core.Std;;

let max = 101;;
let base = 1000000000 + 7;;

let make_tbl() =
  let dp = Array.make_matrix max max [|[||]|] in
  for i = 1 to max - 1 do
    for j = 1 to max - 1 do
      (* 0 face down, 1 face right *)
       dp.(i).(j) <- Array.make_matrix 2 max 0;
    done;
  done;
  for i = 1 to max - 1 do
    dp.(i).(1).(0).(0) <- 1;
    dp.(1).(i).(1).(0) <- 1;
  done;
  dp;;

let solve n m k dp =
  for i = 2 to n do
    for j = 2 to m do
      for k' = 1 to k do
        dp.(i).(j).(0).(k') <-
          (dp.(i-1).(j).(0).(k') + dp.(i-1).(j).(1).(k'-1)) mod base;
        dp.(i).(j).(1).(k') <-
          (dp.(i).(j-1).(1).(k') + dp.(i).(j-1).(0).(k'-1)) mod base;
      done;
    done;
  done;;

let () =
  let dp = make_tbl() in
  let t = read_int() in
  for _ = 1 to t do
    let n, m, k = Scanf.scanf " %d %d %d" (fun n m k -> n, m, k) in
    let ans = ref 0 in
    if n = 1 && m = 1 then
      ans := 1
    else (
      solve n m k dp;
      for k' = 0 to k do
        let v1 = dp.(n).(m).(0).(k') in
        let v2 = dp.(n).(m).(1).(k') in
        ans := (!ans + v1 + v2) mod base;
      done;);
    Printf.printf "%d\n" !ans;
  done;;
