open Core.Std
       
let rec print_res hash v =
  match Hashtbl.find hash v with
    None -> (Printf.printf "%d " v)
  | Some(x) -> (print_res hash x; Printf.printf "%d " v;)
    

let bfs queue numbers target =
  let hash = Hashtbl.Poly.create() in 
  let continue = ref true in
  let found = ref false in 
  while !continue do
    let top = Heap.pop queue in
    match top with
      None -> ( continue := false )
    | Some(v) -> (
        let count = Heap.count queue (fun _ -> true) in
        (* Printf.printf "now: %d %d\n" v count; *)
        if v = target then (
          found := true;
          print_res hash target;
          Printf.printf "\n";
        )
        else (
          if v > target then (
            Printf.printf "-1\n";
            continue := false;
          )
          else (
            (* if !found then continue := false; *)
            List.iter numbers (fun x ->
                let next = x * v in
                if next <= target && (target mod next = 0) then (
                    Heap.add queue next;
                    ignore(Hashtbl.set hash next v);)
              ))))
  done;;



let read_nums () = read_line () |> String.strip |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let cmp (a :int) (b :int) =
  if a > b then -1
  else (
    if a < b then 1
    else 0);;
  
let _ =
  let nums = read_nums() in
  let n = List.nth_exn nums 0 in
  let k = List.nth_exn nums 1 in
  let l = read_nums() in
  let l' = (List.filter l (fun x -> n mod x = 0)) in
  let l'' = (List.sort ~cmp:cmp l') in 
  let pq = Heap.create ~min_size: 1 ~cmp:cmp () in
  Heap.add pq 1;
  bfs pq l'' n;
  

