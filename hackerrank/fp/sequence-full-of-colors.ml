open Core.Std;;

let solve seq =
  let num = Array.create 4 0 in
  match String.find seq ~f:(fun x -> 
      ignore(match x with
        'R' -> num.(0) <- num.(0) + 1;
      | 'G' -> num.(1) <- num.(1) + 1;
      | 'Y' -> num.(2) <- num.(2) + 1;
      | _ -> num.(3) <- num.(3) + 1);
        Int.abs (num.(0) - num.(1)) > 1 || Int.abs (num.(2) - num.(3)) > 1 ) with
    Some(_) -> false | None -> ( num.(0) = num.(1) && num.(2) = num.(3));;

let _ =
  let n = read_int() in
  for _ = 1 to n do
    if read_line() |> solve then
      Printf.printf "True\n"
    else
      Printf.printf "False\n"
  done;;
  
  
      
      

  
