open Core.Std;;

let rec string_mingling a b =
  if String.length a <= 1 then
    a ^ b
  else
    String.of_char (String.nget a 0) ^ String.of_char (String.nget b 0) ^
      string_mingling (String.drop_prefix a 1) (String.drop_prefix b 1);;

let read() =
  let s = read_line() in
  String.strip s;;

let rec process a b i =
  if i < String.length a then begin
      print_char (a.[i]);
      print_char (b.[i]);
      process a b (i + 1);
    end;;

let () =
  let a = read() in
  let b = read() in
  process a b 0;;
