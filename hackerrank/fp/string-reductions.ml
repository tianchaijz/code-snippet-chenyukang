open Core.Std;;

let solve str =
  let hash = Hashtbl.Poly.create() in
  let res = ref "" in
  let prev = ref ' ' in
  for i = 0 to String.length str - 1 do
    let cur = str.[i] in
    match Hashtbl.find hash cur with
    | None -> (
      ignore(Hashtbl.add hash cur 1);
      res := !res ^ (String.of_char cur);
    )
    | _ -> ()
  done;
  Printf.printf "%s\n" !res;;

let () =
  let s = read_line() in
  solve s;;
