open Core.Std;;

let get list nth =
  match List.nth list nth with
  | Some(x) -> x
  | None -> assert false;;

let rec solve list l r =
  let rec find cur func =
    if cur > r then (-1) else
      match func (get list cur) with
        true -> cur
      | _ -> find (cur + 1) func in  
  let rec valid l r func =
    if l > r then true
    else
      match func (get list l) with 
        false -> false
      | true -> valid (l + 1) r func in
  if l > r then false else
    let cur = get list l in
    let min = find (l + 1) (fun a -> a < cur) in
    let max = find (l + 1) (fun a -> a > cur) in
    match min, max with
    -1, -1 -> true
    | -1, _ -> solve list max  r
    | _, -1 -> solve list min r
    | _, _ ->(
        if (valid min (max - 1) (fun a -> a < cur)) && (valid max r (fun a -> a > cur)) then
          (solve list min (max - 1)) && (solve list max r)
        else false);;

let read_nums () = read_line () |> String.split ~on:' ' |> List.map ~f:Int.of_string
  
let () =
  let case = read_int() in
  for _ = 1 to case do
    let n = read_int() in
    let list = read_nums() in
    if solve list 0 (n - 1) then
      Printf.printf "YES\n"
    else
      Printf.printf "NO\n"
  done;
  
    
  
    
