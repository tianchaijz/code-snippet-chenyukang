open Core.Std;;

let read_input() =
  let line = read_line() in
  let a = String.split_on_chars line ~on: [' '; '\t'; '\r'] in
  List.map a int_of_string;;

let rec gcd a b =
  (* Printf.printf "now: %i %i\n" a b; *)
  if a = b then a
  else begin
    if a > b then gcd (a - b) b
    else gcd a (b - a)
  end;;

let () =
  let arr = read_input() in
  match arr with
    a::b::rest -> Printf.printf "%d\n" (gcd a b)
  | _ -> Printf.printf "error"
