(* https://www.hackerrank.com/challenges/flipping-bits *)

open Core.Std;;

(* print_int ((lnot value) land 0xFFFFFFFF); *)

let solve v =
  let bits = Array.create 32 0 in
  let va = Int64.of_string v in
  let res = ref (Int64.of_int 0) in
  Array.iteri (fun i x ->
               if Int64.bit_and va (Int64.shift_left (Int64.of_int 1) i) >
                    Int64.of_int 0 then
                 bits.(i) <- 0 else bits.(i) <- 1) bits;
  Array.iteri (fun i x -> if x = 1 then
               res := Int64.bit_or !res (Int64.shift_left (Int64.of_int 1) i);) bits;
  print_endline (Int64.to_string !res);;

let () =
  let t = read_int() in
  for _ = 1 to t do
    let s = read_line() in
    solve s;
  done;;
