let rec read_lines () =
  try let line = read_line () in
      int_of_string (line) :: read_lines()
  with
    End_of_file -> []

let f arr =
  let res = ref [] in 
  List.iteri (fun i x ->
              if (i mod 2) <> 0 then
                res := x::!res) arr;
  List.rev !res
   
let () =
  let arr = read_lines() in
  let ans = f arr in
  List.iter (fun x -> print_int x; print_newline ()) ans;;
