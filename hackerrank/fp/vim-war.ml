open Core.Std;;

let to_int s =
  let s = String.strip s in
  let res = ref 0 in
  for i = 0 to (String.length s) - 1 do
    res := !res * 2;
    if s.[i] = '1' then incr res;
  done;
  !res;;

let res = ref 0;;
let base = Int.pow 10 9 + 1;;

let rec dfs set cur_set depth cur target =
  if depth = Array.length set then (
    if Int.bit_and target cur = target then
      incr res;
  )
  else (
    if cur_set.(depth) = 0 then (
      cur_set.(depth) <- 1;
      let next_cur = Int.bit_or cur set.(depth) in
      if Int.bit_and next_cur target = target then (
        let k = ref 1 in
        for i = 1 to (Array.length set) - depth - 1 do
          k := (!k * 2) mod base;
        done;
        res := !res + !k;
      )
      else (
        dfs set cur_set (depth + 1) next_cur target;
        cur_set.(depth) <- 0;
      )
    );
    dfs set cur_set (depth + 1) cur target);
  ;;

let read_nums() = read_line() |> String.strip |> String.split ~on:' ' |> List.map ~f:Int.of_string;;

let read_two() =
  let nums = read_nums() in
  match nums with
    a::(b::_) -> a, b
  | _ -> assert false;;

let () =
  let n, m = read_two() in
  let rec read cur res =
    if cur = n  then res
    else
      let s = read_line() in
      let v = to_int s in
      read (cur + 1) (v::res) in
  let set = read 0 [] in
  let l = read_line() in
  let target = to_int l in
  dfs (List.to_array set) (Array.create (List.length set) 0) 0 0 target;
  Printf.printf "%d\n" !res;;
