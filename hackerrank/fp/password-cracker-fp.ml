open Core.Std;;

let read_words () = read_line () |> String.split ~on:' '

(* very slowly for the banding case *)
let solve password words =
  let rec _solve cur now =
    if cur = password then Some(now)
    else
      let candidate = List.filter words (fun x ->
          match String.substr_index password (cur ^ x) with
          | Some(0) -> true
          | _ -> false) in
      let res = List.find_map candidate (fun x ->
          let cur = cur ^ x in
          match _solve cur (x::now) with
          | Some(x) -> Some(x) 
          | None -> None) in
      res in 
  match _solve "" [] with
    Some(x) -> List.rev x
  | None -> [];;

(* this version is more fast, and we need to sort the words for these kind of case *)
(* wedow because can do must we what
   wedowhatwemustbecausewecan *)
let solve_v2 password words =
  let words = List.sort (fun a b -> (String.length a) - (String.length b)) words in
  let cur = ref "" in 
  let rec _solve now =
    if !cur = password then Some(now)
    else
      let res = List.find_map words (fun x ->
          match String.substr_index password (!cur ^ x) with
          Some(0) -> (
              cur := !cur ^ x;
              match _solve (x::now) with
              | Some(x) -> Some(x) 
              | _ -> None)
          | _ -> None) in
      res in 
  match _solve [] with
    Some(x) -> List.rev x
  | None -> [];;

      
let () =
  let num = read_int() in
  for _ = 1 to num do
    let _ = read_int() in 
    let words = read_words() in
    let password = read_line() in
    let res = solve_v2 password words in
    (* List.iter words (fun x -> Printf.printf "now: %s\n" (String.to_string x)); *)
    match res with
      [] -> Printf.printf "WRONG PASSWORD\n"
    | _ -> (List.iter res (fun x -> Printf.printf "%s " (String.to_string x)); Printf.printf "\n")
  done;;



(* let words = ["because"; "can"; "do"; "must"; "we"; "what"];; *)
(* let password = "wedowhatwemustbecausewecan";; *)
(* let res = solve password words;; *)


(* let words = ["hello"; "planet"];; *)
(* let password = "helloworld";; *)
(* let res = solve password words;; *)


(* let words = ["ab"; "abcd"; "cd"];; *)
(* let password = "abcd";; *)
(* let res = solve password words;; *)
