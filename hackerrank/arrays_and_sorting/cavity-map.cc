#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

int main() {
    vector<vector<int> > map;
    int N;
    string t;
    cin >> N;
    for(int i=0; i<N; i++) {
        vector<int> v;
        cin >> t;
        for(int j=0; j<N;j ++) {
            v.push_back(t[j]-'0');
        }
        map.push_back(v);
    }

    int tested[N][N];
    memset(tested, 0, sizeof(tested));
    for(int i=1; i<N-1; i++) {
        for(int j=1; j<N-1; j++) {
            if(tested[i][j] == 0) {
                if(map[i-1][j] < map[i][j] &&
                   map[i][j-1] < map[i][j] &&
                   map[i+1][j] < map[i][j] &&
                   map[i][j+1] < map[i][j]) {
                    tested[i][j] = 2; //hole
                    tested[i+1][j] = 1;
                    tested[i][j+1] = 1;
                }
            }
        }
    }
    for(int i=0; i<N; i++) {
        for(int j=0; j<N; j++) {
            if(tested[i][j] == 2) {
                std::cout << "X";
            } else {
                std::cout << map[i][j];
            }
        }
        std::cout << std::endl;
    }
    return 0;
}
