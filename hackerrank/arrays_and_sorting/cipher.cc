#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

int main() {
    int N, K;
    string s;
    vector<int> vec;
    cin >> N >> K;
    cin >> s;
    for(int i=0; i<N; i++) {
        vec.push_back(s[i] - '0');
    }

    vector<int> ans;
    for(int i=0; i<N; i++) {
        int v = vec[i];
        for(int j=i-1; j>i-K && j >=0; j--) {
            v = v ^ ans[j];
        }
        ans.push_back(v);
    }
    for(int i=0; i<ans.size(); i++) {
        std::cout << ans[i];
    }

    std::cout << std::endl;
    return 0;
}
