#include <iostream>
#include <string.h>
#include <vector>
#include <algorithm>
using namespace std;


int main() {
    vector<int> vec;
    int N, K, t;
    cin >> N >> K;
    for(int i=0; i<N; i++) {
        cin >> t;
        vec.push_back(t);
    }

    sort(vec.begin(), vec.end());
    int ans = 0;
    int sum = 0;
    for(int i=0; i<vec.size(); i++) {
        if(sum + vec[i] <= K) {
            sum += vec[i];
            ans++;
        } else {
            break;
        }
    }
    std::cout << ans << std::endl;
    return 0;
}
