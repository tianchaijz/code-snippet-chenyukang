#include <iostream>
#include <string.h>
#include <vector>
using namespace std;

void swap(int& a, int& b) {
    int c = b;
    b = a;
    a = c;
}

void print(const vector<int>& vec) {
    for(int i=0; i<vec.size(); i++) {
        printf("%d ", vec[i]);
    }
    printf("\n");
}

void partition(vector<int>& vec) {
    int value = vec[0];
    int index = 0;
    swap(vec[vec.size()-1], vec[0]);
    print(vec);
    for(int i=0; i<vec.size() - 1; i++) {
        if(vec[i] < value) {
            swap(vec[i], vec[index]);
            index++;
        }
    }
    swap(vec[index], vec[vec.size()-1]);
    print(vec);
}

void partition_stable(vector<int>& vec) {
    vector<int> buf;
    int value = vec[0];
    int index = 0;
    buf.push_back(value);
    for(int i=1; i<vec.size(); i++) {
        buf.push_back(vec[i]);
        if(vec[i] < value) {
            int k;
            for(k=buf.size() - 2; k >= 0; k--) {
                if(buf[k] >= value) {
                    buf[k+1] = buf[k];
                } else {
                    break;
                }
            }
            buf[k+1] = vec[i];
        }
    }
    print(buf);
}


int main() {
    int N, t;
    vector<int> vec;
    cin >> N;
    while(N--) {
        cin >> t;
        vec.push_back(t);
    }
    partition_stable(vec);
    return 0;
}
