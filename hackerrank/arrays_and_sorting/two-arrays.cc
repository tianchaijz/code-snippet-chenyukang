#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <string.h>
using namespace std;


// O^2 , should have simpler solution

bool run_slow(vector<int>& va, vector<int>& vb, int K) {
    bool used[va.size()];
    int i, j;
    memset(used, false, sizeof(used));
    sort(va.begin(), va.end());
    sort(vb.begin(), vb.end());
    for(i=0; i<va.size(); i++) {
        for(j=0; j<vb.size(); j++) {
            if(used[j]) continue;
            if(vb[j] + va[i] >= K) {
                used[j] = true;
                break;
            }
        }
        if(j == vb.size()) {
            return false;
        }
    }
    return true;
}

int main() {
    int T, N, K, t;
    vector<int> va;
    vector<int> vb;
    cin >> T;
    while(T--) {
        cin >> N >> K;
        va.clear();
        vb.clear();
        for(int i=0; i<N; i++) {
            cin >> t;
            va.push_back(t);
        }
        for(int i=0; i<N; i++) {
            cin >> t;
            vb.push_back(t);
        }
        bool res = run_slow(va, vb, K);
        if(res)
            std::cout << "YES" << std::endl;
        else
            std::cout << "NO" << std::endl;
    }
    return 0;
}
