#include <iostream>
#include <string.h>
#include <stdio.h>
using namespace std;


#define MAX 27

int main() {
    string StrA, StrB;
    int CountA[MAX];
    int CountB[MAX];
    int ans = 0;

    memset(CountA, 0, sizeof(CountA));
    memset(CountB, 0, sizeof(CountB));

    cin >>  StrA >> StrB;

    for(int i=0; i<StrA.size(); i++) {
        CountA[StrA[i] - 'a']++;
    }
    for(int i=0; i<StrB.size(); i++) {
        CountB[StrB[i] - 'a']++;
    }
    for(int i=0; i<MAX; i++) {
        ans += abs(CountA[i] - CountB[i]);
    }
    std::cout << ans << std::endl;
    return 0;
}
