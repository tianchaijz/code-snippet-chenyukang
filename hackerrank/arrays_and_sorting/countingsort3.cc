#include <iostream>
#include <vector>
#include <string.h>
#include <stdio.h>
using namespace std;

#define MAX 100

int main() {
    int N, t;
    int now = 0;
    int vec[MAX];
    string str;

    memset(vec, 0, sizeof(vec));

    cin >> N;
    for(int i=0; i<N; i++) {
        cin >> t >> str;
        vec[t]++;
    }

    for(int i=0; i<=99; i++) {
        now += vec[i];
        printf("%d ", now);
    }
    return 0;
}
