#include <iostream>
#include <string.h>
#include <vector>
#include <algorithm>
#include <limits>
using namespace std;

typedef unsigned long long LL;

int main() {
    int T, N, t;
    cin >> T;
    while(T--) {
        vector<int> vec;
        cin >> N;
        for(int i=0; i<N; i++) {
            cin >> t;
            vec.push_back(t);
        }
        sort(vec.begin(), vec.end());
        LL ans = 0;
        int index = 0;
        while(index < vec.size()) {
            int k = index;
            int count = 0;
            while(k < vec.size() && vec[k] == vec[index]) {
                count++;
                k++;
            }
            ans += ((LL)count * (LL)(count - 1));
            index = (k != index) ? k : index+1;
        }
        std::cout << ans << std::endl;
    }
    return 0;
}
