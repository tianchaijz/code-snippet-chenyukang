#include <iostream>
#include <vector>
#include <string.h>
using namespace std;


int main() {
    int v, t, N, ans;
    cin >> v >> N;
    for(int i=0; i<N; i++) {
        cin >> t;
        if(t == v) {
            ans = i;
        }
    }
    std::cout << ans << std::endl;
    return 0;
}
