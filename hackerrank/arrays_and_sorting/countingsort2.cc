#include <iostream>
#include <vector>
#include <string.h>
#include <stdio.h>
using namespace std;

#define MAX 100

int main() {
    int N, t;
    int vec[MAX];
    memset(vec, 0, sizeof(vec));

    cin >> N;
    for(int i=0; i<N; i++) {
        cin >> t;
        vec[t]++;
    }

    for(int i=0; i<=99; i++) {
        if(vec[i] != 0) {
            for(int k=0; k<vec[i]; k++) {
                printf("%d ", i);
            }
        }
    }
    return 0;
}
