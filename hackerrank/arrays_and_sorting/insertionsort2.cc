#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

void print(const vector<int>& ar) {
    for(int i=0; i<ar.size(); i++) {
        if(i != 0) {
            printf(" ");
        }
        printf("%d", ar[i]);
    }
    printf("\n");
}

void iter(vector<int>&  ar, int index) {
    int t = ar[index];
    int i;
    for(i=index - 1; i >= 0; i--) {
        if(ar[i] > t) {
            ar[i+1] = ar[i];
        } else {
            break;
        }
    }
    ar[i+1] = t;
    print(ar);
}

void insertionSort(vector<int>& ar) {
    for(int i=1; i<ar.size(); i++) {
        iter(ar, i);
    }
}


int main(void) {
    int _ar_size;
    cin >> _ar_size;
    vector<int> vec;
    int t, _ar_i;
    for(_ar_i = 0; _ar_i < _ar_size; _ar_i++) {
        cin >> t;
        vec.push_back(t);
    }

    insertionSort(vec);

    return 0;
}
