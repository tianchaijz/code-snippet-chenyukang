
def rot13(secret_messages)
  def conv_string str
    key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    val = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm"
    str.chars.map{|x|
      if (index = key.index x) then val[index] else x end
    }.join
  end
  secret_messages.map{|x| conv_string x}
end
