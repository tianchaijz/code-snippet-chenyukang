#include <iostream>
using namespace std;

#if 0
struct Node {
  int data;
  struct Node *next;
};
#endif

int CompareLists(Node *headA, Node* headB) {
    Node* a = headA;
    Node* b = headB;
    while( a != NULL  && b != NULL) {
        if(a->data != b-> data) {
            return 0;
        }
        a = a->next, b = b->next;
    }
    return (a == NULL && b == NULL) ?  1 : 0;
}


#if 0
int main() {
    return 0;
}
#endif
