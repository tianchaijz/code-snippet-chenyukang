#!/usr/bin/env python
import multiprocessing, os, signal, time, Queue
import commands
import sys
 
class MultiWork:
    def __init__(self, paraNum = 4):
        self.job_queue = multiprocessing.Queue()
        self.result_queue = multiprocessing.Queue()
        self.paraNum = paraNum

    def do_work(self, cmd):
        print 'Work Started: %d' % os.getpid()
        commands.getstatusoutput(cmd)
        return 'Success'

    def manual_function(self, job_queue, result_queue):
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        while not job_queue.empty():
            try:
                job = job_queue.get(block=False)
                result_queue.put(self.do_work(job))
            except Queue.Empty:
                pass
            except KeyboardInterrupt: 
                pass

    def addjob(self, cmd):
        self.job_queue.put(cmd)

    def run(self):
        workers = []
        for i in range(self.paraNum):
            tmp = multiprocessing.Process(target=self.manual_function,
                                          args=(self.job_queue, self.result_queue))
            tmp.start()
            workers.append(tmp)

        try:
            for worker in workers:
                worker.join()
        except KeyboardInterrupt:
            print 'parent received ctrl-c'
            for worker in workers:
                worker.terminate()
                worker.join()

        while not self.result_queue.empty():
            print self.result_queue.get(block=False)
 
if __name__ == "__main__":
    workers = MultiWork(6)
    workers.addjob("sleep 4")
    workers.addjob("sleep 10")
    workers.addjob("sleep 3")
    workers.addjob("sleep 8")
    workers.addjob("sleep 5")
    workers.addjob("sleep 5")
    workers.addjob("sleep 5")
    workers.addjob("sleep 5")
    workers.run()
