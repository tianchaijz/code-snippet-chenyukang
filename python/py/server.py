#!/usr/bin/env python

import os
import sys
import SocketServer 
from time import ctime 

HOST = 'localhost' 
PORT = 21567 
ADDR = (HOST, PORT) 

class MyRequestHandler(SocketServer.BaseRequestHandler): 
    def handle(self): 
        print '...connected from:', self.client_address 
        while True: 
            msg = self.request.recv(1024)
            self.request.sendall('[%s] %s' %(ctime(), msg))


tcpServ = SocketServer.ThreadingTCPServer(ADDR, MyRequestHandler) 
print 'waiting for connection...' 

try:
    tcpServ.serve_forever()
except KeyboardInterrupt:
    print " Quit ... "
    sys.exit(1)

