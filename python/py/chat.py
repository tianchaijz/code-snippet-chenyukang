#!/usr/bin/env python

import curses
from random import randrange

def main(win):
    global stdscr
    stdscr = win

    if curses.has_colors():
        bg = curses.COLOR_BLACK
        curses.init_pair(1, curses.COLOR_BLUE, bg)
        curses.init_pair(2, curses.COLOR_CYAN, bg)
        
    curses.nl()
    curses.noecho()
    stdscr.timeout(0)

    col = curses.COLS - 4
    row = curses.LINES - 4
    xpos = [0] * col
    ypos = [0] * row
    print col, row
    while True:
        x = randrange(0, col) + 2
        y = randrange(0, row) + 2
        stdscr.addch(y, x, ord('k'))

        curses.napms(50)

curses.wrapper(main)
    
