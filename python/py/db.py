#!/usr/bin/env python
import os
import time
import sqlite3
from datetime import datetime

DATABASE="demo.db"
USER_ID=0

def switch_time_zone():
  os.environ["TZ"] = "Asia/Shanghai"
  time.tzset()

def format_datetime(timestamp):
  """Format a timestamp for display."""
  if timestamp == None:
    return None
  return datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d @ %H:%M:%S')

def get_db():
  con = sqlite3.connect("demo.db")
  return con

def init_db():
  con = sqlite3.connect("demo.db")
  cur = con.cursor()
  cur.execute('''create table merges (id integer primary key autoincrement,
  user varchar(200), component varchar(200),
  log varchar(30), timestamp integer)''')
  con.commit

def add_record(user, component, log):
  db = get_db()
  db.execute('''insert into merges (user, component, log, timestamp) values(?, ?, ?, ?)''',
             (user, component, log, int(time.time())))
  db.commit()
  print "add finished "

if __name__ == "__main__":
  switch_time_zone()
  if not os.path.exists(DATABASE):
    init_db()
  print DATABASE
  print USER_ID
  add_record("hongzhi", "core test_branch", "fix a bug")
  add_record("yukang", "infra kang_branch", "fix a bug")
  db = get_db()
  cur = db.cursor()
  cur.execute('''SELECT * FROM merges where user == "yukang" order by timestamp desc''')
  res = cur.fetchall()
  cnt = len(res)
  
  print format_datetime(float(res[0][4]))
  print format_datetime(float(time.time()))
  for x in res:
    for _x in x:
      now = str(_x)
      print now, type(now)
    

  
