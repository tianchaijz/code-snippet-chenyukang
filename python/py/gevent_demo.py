#!/usr/bin/env python

import gevent
import signal
from gevent import Timeout

seconds = 10

def foo():
    print('Running foo')
    gevent.sleep(0)
    print('Transfer into foo again')

def bar():
    print('Running bar')
    gevent.sleep(0)
    print('Transfer into bar again')

def run_forever():
    gevent.sleep(1000)
    
# gevent.joinall([
#     gevent.spawn(foo),
#     gevent.spawn(bar)
#     ])

timeout = Timeout(seconds)
timeout.start()


if __name__ == '__main__':
    # gevent.signal(signal.SIGQUIT, gevent.shutdown)
    # thread = gevent.spawn(run_forever)
    # thread.join()

    try:
        gevent.spawn(run_forever).join()
    except Timeout:
        print("Could not completed")
        
