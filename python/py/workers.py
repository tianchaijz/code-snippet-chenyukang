#!/usr/bin/env python
import subprocess
import time
import commands
from multiprocessing import Process
import sys
import os

def demofunc(cmd):
    try:
        res = commands.getstatusoutput(cmd)
    except KeyboardInterrupt:
        print "yes madma"
        sys.exit()

class WorkManager:
    workers = []
    cmds = []
    def __init__(self, cmds = []):
        self.cmds = cmds
        self.workers = []
        for x in self.cmds:
            print x
            self.workers.append(Process(target=demofunc, args=[x]))
        print "size:", len(self.workers)
        print "fnished"

    def run(self):
        for x in self.workers:
            x.start()
        for x in self.workers:
            try:
                x.join()
            except KeyboardInterrupt:
                print "exting ..."
                sys.exit()

# if __name__ == "__main__":
#     manager = WorkManager(["ls", "sleep 2", "sleep 3", "ls", "gcc"])
#     manager.run()


import multiprocessing
import time

class KeyboardInterruptError(Exception): pass

def func(msg):
    print msg
    try:
        commands.getstatusoutput(msg)
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    pool = multiprocessing.Pool(processes=4)
    cmds = ["ls", "sleep 10"]
    p = pool.map_async(func, cmds)
    try:
        results = p.get(0xFFFF)
    except KeyboardInterrupt:
        sys.exit(0)
    for i in results:
        print i
    print "Sub-process(es) done."    
