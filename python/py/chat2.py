#!/usr/bin/env python

import threading
import curses,time

def start_curses():
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()
    xsstdscr.keypad(1)
    return stdscr

def end_curses(screen):
	curses.nocbreak()
	screen.keypad(0)
	curses.echo()
	curses.endwin()

class Logon_View():
	def draw(self,screen):
		screen.addstr(1,1,"Logon screen")
		screen.addstr(1,20,"Username: ")
		screen.addstr(2,20,"Password: ")

screen = start_curses()
view_stack = []
view_stack.append(Logon_View())


while True:
	view_stack[-1].draw(screen)
	screen.refresh()
	time.sleep(1)
    


end_curses(screen)
