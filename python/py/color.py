#!/usr/bin/env python

def printerror(text):
    print '\033[1;35;40m',
    print text,
    print '\033[0m'

def printpass(text):
    print '\033[1;32;40m',
    print text,
    print '\033[0m'

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[32m'
    WARNING = '\033[35m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    @classmethod
    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

def fail_msg(text):
    print bcolors.WARNING + text + bcolors.ENDC

def pass_msg(text):
    print bcolors.OKBLUE + text + bcolors.ENDC

#bcolors.disable()
pass_msg("pass")
fail_msg("fail")
