#!/usr/bin/env python                                                           
import multiprocessing, os, time
import commands

def do_work(i):
    try:
        print 'Work Started: %d %s' % (os.getpid(), i)
        #commands.getstatusoutput(i)
        
        return 'Success'
    except KeyboardInterrupt, e:
        pass
 
def main():
    pool = multiprocessing.Pool(3)
    cmds=["ls", "sleep 10"]
    p = pool.map_async(do_work, cmds)
    try:
        results = p.get(0xFFFF)
    except KeyboardInterrupt:
        print 'parent received control-c'
        return
 
    for i in results:
        print i
 
if __name__ == "__main__":
    main()
