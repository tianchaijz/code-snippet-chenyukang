#!/usr/bin/python
import subprocess
import datetime, signal, os, sys, time

# when we need to run a command in python with a timeout, this function is usefull#
# also when Python version is higher than 2.6, use proc.terminate() to exit subprocess
# ifproc.poll()==None:
#        iffloat(sys.version[:3])>=2.6:
#            proc.terminate()
#

SIGTERM_TO_SIGKILL = 1
def timeout_command(cmd, timeout):
    start = datetime.datetime.now()
    process = subprocess.Popen(cmd, shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    while process.poll() is None:
        now = datetime.datetime.now()
        if (now - start).seconds >= timeout:
            os.kill(process.pid, signal.SIGTERM)
            sys.stderr.write('TIMEOUT %s second%s, sent sigterm to %s "%s"\n' \
                                 %(str(timeout), '' if timeout==1 else 's', process.pid, cmd))
            time.sleep(SIGTERM_TO_SIGKILL)
            if process.poll() is None:
                os.kill(process.pid, signal.SIGKILL)
                sys.stderr.write('process still running, sent sigkill to %s "%s"\n' %(process.pid, cmd))
                os.waitpid(-1, os.WNOHANG)
            return None
        time.sleep(0.1)
    return [process.stdout.read(), process.stderr.read()]

res = timeout_command("ls -l ../", 1)

print res


