#!/usr/bin/env python

import curses
def file_func():
    screen = curses.initscr()
    s = curses.newwin(5,10,2,1)
    s.box()
    s.addstr(1,2, "I", hotkey_attr)
    s.addstr(1,3, "nput", menu_attr)
    s.addstr(2,2, "O", hotkey_attr)
    s.addstr(2,3, "utput", menu_attr)
    s.addstr(3,2, "T", hotkey_attr)
    s.addstr(3,3, "ype", menu_attr)
    s.addstr(1,2, "", hotkey_attr)
    s.refresh()
    c = s.getch()  
    if c in (ord('I'), ord('i'), curses.KEY_ENTER, 10):
        curses.echo()
        s.erase()
        screen.addstr(5,33, " "*43, curses.A_UNDERLINE)
        cfg_dict['source'] = screen.getstr(5,33)
        curses.noecho()
    else:
        curses.beep()
        s.erase()
    return CONTINUE


file_func()
