#!/usr/bin/env python
import argparse
parser = argparse.ArgumentParser(description="This is the build scripts")

parser.add_argument("version", nargs='?', type=str, default="debug", choices=('debug', 'release', 'coverage', 'all'), help="build version")

parser.add_argument("clean", nargs="?", default=False, help="clean the build")
parser.add_argument("component", nargs="*", help="component list")
args = parser.parse_args()

print args.version
print args.clean
print args.component
