package main

import (
	"fmt"
	"reflect"
)

type GetProp struct {
	Offset      int32             `thrift:"Offset,1,required"`
	Limit       int32             `thrift:"Limit,2,required"`
	SortField   []string          `thrift:"SortField,3,required"`
	Keyword     map[string]string `thrift:"Keyword,4,required"`
	FieldFilter []string          `thrift:"FieldFilter,5,required"`
}

type T struct {
	A int
	B string
	C string
}

func runFilderFilter(prop *GetProp, obj interface{}) interface{} {
	if len(prop.FieldFilter) == 0 {
		return obj
	}

	setField := func(obj interface{}, field string, value string) {
		v := reflect.ValueOf(obj).Elem()
		v.FieldByName(field).SetString(value)
	}

	is_need := func(field string) bool {
		for _, v := range prop.FieldFilter {
			if v == field {
				return true
			}
		}
		return false
	}
	re := reflect.ValueOf(obj).Elem()
	typeOfT := re.Type()
	for i := 0; i < re.NumField(); i++ {
		f := re.Field(i)
		field := typeOfT.Field(i).Name
		if f.Type().Kind() == reflect.String &&
			!is_need(field) {
			setField(obj, field, "")
		}
	}
	return obj
}

func main() {
	t := T{23, "skidoo", "world"}
	s := reflect.ValueOf(&t).Elem()
	typeOfT := s.Type()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		if f.Type().Kind() == reflect.String {
			fmt.Printf("%d: %s %s = %v\n", i,
				typeOfT.Field(i).Name, f.Type(), f.Interface())
		}
	}
	arr := []*T{&T{1, "hello", "hello1"}, &T{2, "world", "world1"}}
	for _, v := range arr {
		fmt.Println(v)
	}

	var res []*T
	prop := &GetProp{0, 0, nil, nil, []string{"B"}}
	fmt.Println(arr)
	// for _, v := range arr {
	// 	in = append(in, v)
	// }

	for _, v := range arr {
		r := runFilderFilter(prop, v)
		res = append(res, r.(*T))
	}
	fmt.Println(res)
	for _, v := range res {
		fmt.Println(v)
	}

}
