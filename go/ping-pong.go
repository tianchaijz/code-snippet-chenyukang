package main

import "fmt"

type Chain struct {
	in  chan string
	out chan string
}

func (this *Chain) Close() {
	fmt.Println("closing channel")
	close(this.in)
	close(this.out)
}

func main() {
	com := Chain{make(chan string), make(chan string)}
	defer com.Close()

	go func() {
		com.in <- "ping"
		for {
			data := <-com.out
			fmt.Println(data)
			com.in <- "ping"
		}
	}()

	go func() {
		for {
			data := <-com.in
			fmt.Println(data)
			com.out <- "pong"
		}
	}()

	var input string
	fmt.Scanln(&input)
}
