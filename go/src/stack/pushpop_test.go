package stack

import "testing"

func TestPushPop(t *testing.T) {
        c := new(Stack)
        c.Push(5)
        if c.Pop() != 5 {
                t.Log("Pop doesn't give 5")
                t.Fail()
        }
}

func TestPush(t *testing.T) {
	c := new(Stack)
	c.Push(6)
	if c.Pop() != 6 {
		t.Log("Pop doesn't give 6")
		t.Fail()
	}
}
