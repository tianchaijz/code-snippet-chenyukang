package main

import (
	"api"
	"fmt"
	"io/ioutil"
	"jingwei"
	"kp/models"
)

func modelTest() {
	jingwei.ReadConfig("./conf/default.json")
	hos := models.NewHospital()
	hos.Name = "xiaokang"
	hos.Save()

	id := hos.ID.Hex()
	fmt.Println("hos.ID:", id)
	hos, _ = models.HospitalFindByID(id)
	fmt.Println(hos)
	fmt.Println("now.ID:", hos.ID.Hex())
}

func picTest() {
	jingwei.ReadConfig("./conf/default.json")
	handler := &jingwei.RpcServiceImpl{}
	var apiErr *api.Exception
	var err error
	apiErr, err = handler.AddUser(&api.UserPost{"user24", "passwd", "user24@gmail.com", "User16", "Intro",
		"Picture", "Birth", "Addr", "PhoneNumber", "Man"})
	if apiErr != nil || err != nil {
		panic(apiErr)
	}
	user, _, _ := handler.GetUserByEmail("user24@gmail.com")
	if user == nil {
		panic("error")
	}
	path := "/Users/kang/Desktop/golang.jpg"
	content, ok := ioutil.ReadFile(path)
	if ok != nil {
		panic(ok)
	}
	apiErr, err = handler.AddUserAvatar(&api.UserAvatarPost{user.ID.Hex(), "jpg", string(content)})
	if !(apiErr == nil && err == nil) {
		panic(err)
	}
	fmt.Println("now ID:", user.ID.Hex())
	user, _, _ = handler.GetUserByEmail("user24@gmail.com")
	fmt.Println("expectURL:", user.Picture)
	fmt.Println("finished")
}

func main() {
	modelTest()
}
