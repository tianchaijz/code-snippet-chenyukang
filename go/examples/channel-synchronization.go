package main
import "fmt"
import "time"

func worker(done chan bool) {
	fmt.Println("working ...")
	time.Sleep(time.Second)
	fmt.Println("done")
	done <- true
}

func done_worker(done chan bool) {
	fmt.Println("now in done_worker")
	res := <- done
	if res {
		fmt.Println("done now")
	} else {
		fmt.Println("waiting ...")
	}
}


func main() {
	done := make(chan bool)
        go worker(done)
        go done_worker(done)

	time.Sleep(time.Second * 3)
        //<- done

}
