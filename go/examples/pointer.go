package main
import "fmt"

func zeroval(val int) {
	val = 0
}

func zeropointer(ptr *int) {
	*ptr = 0
}

func main() {
	i := 1

	fmt.Println(i)
	zeroval(i)
	fmt.Println(i)
	zeropointer(&i)
	fmt.Println(i)
}
