package main

import "fmt"
import "time"
import "sync"
import "sync/atomic"
import "runtime"
import "math/rand"


func main() {
	var state = make(map[int]int)
	var mutex = &sync.Mutex{}
	var ops uint64 = 0

	for r := 0; r < 64; r++ {
		go func () {
			total := 0
			for {
				key := rand.Intn(5)
				mutex.Lock()
				total += state[key]
				mutex.Unlock()
				atomic.AddUint64(&ops, 1)
				runtime.Gosched()
			}
		}()
	}

	for w := 0; w < 100; w++ {
		go func() {
			for {
				key := rand.Intn(50)
				val := rand.Intn(100)
				mutex.Lock()
				state[key] = val
				mutex.Unlock()
				atomic.AddUint64(&ops, 1)
				runtime.Gosched()
			}
		}()
	}

	time.Sleep(time.Second * 4)
	opsFinal := atomic.LoadUint64(&ops)
	fmt.Println("ops:", opsFinal)

	mutex.Lock()
	fmt.Println("fmt:", state)
	mutex.Unlock()
}
