package main

func main() {
	done := make(chan bool, 2)
	done <- true
	done <- true
}
