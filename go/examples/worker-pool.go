package main

import "fmt"
import "time"

func woker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Println("worker", id, "processing job", j)
		time.Sleep(time.Second * 2)
		results <- j * 2
	}
}

func main() {
	jobs := make(chan int, 100)
	results := make(chan int, 100)

	for w := 1; w < 30; w++ {
		go woker(w, jobs, results)
	}

	for job := 1; job <= 90; job++ {
		jobs <- job
	}

	close(jobs)

	for a := 1; a <= 90; a++ {
		ans := <- results
		fmt.Printf("ans: %d\n", ans)
	}

}
