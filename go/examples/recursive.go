package main
import "fmt"

func fact(n int) int {
	fmt.Println(n)
	if n == 0 {
		return 1
	} else {
		return fact(n-1) * n
	}
}

func main() {
	fmt.Println(fact(20))
}
