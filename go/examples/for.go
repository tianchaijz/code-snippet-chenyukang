package main
import "fmt"

func main() {
	i := 1
	for i <= 3 {
		fmt.Println(i)
		i += 1
	}

	for j:= 1; j<=9; j++ {
		fmt.Println("now: %d", j);
	}

	for {
		fmt.Println("haha: %d", i)
		i += 1
		if i >= 20 {
			break
		}

	}

}
