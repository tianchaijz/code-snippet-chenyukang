package main
import "fmt"

func sum(nums ... int) {
	fmt.Println(nums, " ")
	total := 0
	for _, num := range nums {
		total += num
	}
	fmt.Println(" = ", total)
}

func main() {
	sum(1, 2, 3)
	var arr = make([]int, 3)
	idx := 0
	for idx < 100 {
		arr = append(arr, idx)
		idx += 1
	}
	nums := []int{1, 2, 3, 4}
	fmt.Println(len(nums))
	fmt.Println(nums)
	sum(nums...)
	sum(arr...)
	nums = append(nums, 1)

	var test [4]int
	idx = 0
	for idx < 4 {
		test[idx] = idx
		idx += 1
	}
	fmt.Println(test)
	test = append(test, 10)
	//sum(test...)

}
