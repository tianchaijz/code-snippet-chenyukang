package main
import "fmt"

func f(from string) {
	for i := 0; i < 30; i++ {
		fmt.Printf("now: %d %s\n", i, from)
	}
}

func main() {
	f("direct")

	go f("goroutines")
	go f("fuck")

	go func(msg string) {
		fmt.Printf("this is anonymous: %s\n", msg)
	}("yukang")

	var input string
	fmt.Scanln(&input)
	fmt.Printf("got: %s\n", input)
}
