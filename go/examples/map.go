package main

import "fmt"

func main() {
	n := map[string]int{"foo" : 1, "bar": 2}
	fmt.Println("map: ", n)

	for k, v := range n {
		fmt.Printf("k: %s v: %d\n", k, v)
	}
}
