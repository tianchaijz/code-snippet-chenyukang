package main

import (
	"fmt"
	"os"
)

func main() {
	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println("error:", err)
	} else {
		fmt.Println("current dir:", pwd)
	}

	if err := os.Chdir("../"); err != nil {
		fmt.Println("error:", err)
		return
	} else {
		cur, _ := os.Getwd()
		fmt.Println("changdir: ",  cur)
	}

	attr := &os.ProcAttr {
		Files: []*os.File{os.Stdin, os.Stdout},
		Env: os.Environ(),
	}
	p, err := os.StartProcess("/bin/ls", []string{"-l"}, attr)
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	ps, _ := p.Wait()
	fmt.Printf("process stat: %s\n", ps.String())

	os.Chdir(pwd)
	cur, _ := os.Getwd()
	fmt.Println("final dir: ", cur)
	fmt.Println("Environment: ", os.Environ())
	env := os.Environ()
	for _, e := range env {
		fmt.Println(e)
	}
	os.Clearenv()
	env = os.Environ()
	if len(env) == 0 {
		fmt.Println("empty now")
	} else {
		fmt.Println("not empty now")
	}
}
