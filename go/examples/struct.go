package main

import "fmt"

type person struct {
	name string
	age  int
}

func setAge(p person, age int) {
	p.age = age
}

func setNewAge(p *person, age int) {
	p.age = age
}


func main() {
	fmt.Println(person{"Bob", 23})
	fmt.Println(&person{"yukang", 28})
	p := person{"yukang", 28}
	setAge(p, 27)
	fmt.Println(p)

	setNewAge(&p, 27)
        fmt.Println(p)

	p.age = 20
	fmt.Println(p)

}
