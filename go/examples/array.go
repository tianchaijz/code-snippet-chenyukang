package main
import "fmt"

func main() {
	var a [5]int
	fmt.Println(a)

	var d [2][3]int
	for i:=0; i < 2; i++ {
		for j:=0; j < 3; j++ {
			fmt.Println(d[i][j])
		}
	}
	fmt.Println("2d: ", d)

	b := [5]int{1, 2, 3, 4, 5}
	for i := 0; i<5; i++ {
		fmt.Println(b[i])
	}

	fmt.Println("size: ", len(b))

}
