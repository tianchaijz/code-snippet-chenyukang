package main

import "time"
import "fmt"

func main() {
	ticker := time.NewTicker(time.Millisecond * 50)

	go func() {
		for t:= range ticker.C {
			fmt.Println("time:", t)
		}
	}()

	time.Sleep(time.Second * 5)
	ticker.Stop()
	fmt.Println("Ticker stopped")
}
