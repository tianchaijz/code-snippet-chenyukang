package main

import "fmt"

type Request struct {
	a, b   int
	replay chan int
}

type BinOp func(a, b int) int

func run(op BinOp, req *Request) {
	req.replay <- op(req.a, req.b)
}

func server(op BinOp, service chan *Request, quit chan bool) {
	for {
		select {
		case req := <-service:
			go run(op, req)
		case <-quit:
			return
		}
	}
}

func startServer(op BinOp) (service chan *Request, quit chan bool) {
	service = make(chan *Request)
	quit = make(chan bool)
	go server(op, service, quit)
	return service, quit
}

func main() {
	adder, quit := startServer(func(a, b int) int { return a + b })
	const N = 100
	var reqs [N]Request
	for i := 0; i < N; i++ {
		req := &reqs[i]
		req.a = i
		req.b = i + N
		req.replay = make(chan int)
		adder <- req
	}

	// check the result
	for i := 0; i < N; i++ {
		if <-reqs[i].replay != N+2*i {
			fmt.Println("fail at:", i)
			panic("error")
		} else {
			fmt.Println("pass at:", i)
		}
	}

	quit <- true
	fmt.Println("done")
}
