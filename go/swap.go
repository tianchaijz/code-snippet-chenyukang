package main

import "github.com/pmylund/go-cache"
import "fmt"
import "time"

func swap(x, y string) (string, string) {
	return y, x
}

func int_swap(x, y int) (int, int) {
	return x, y
}

func split(sum int) (x, y, s int) {
	x = sum * 4 / 9
	y = sum - x
	s = sum
	return
}

func main() {
	a, b := swap("hello", "world")
	c, d := int_swap(1, 2)
	fmt.Println(a, b)
	fmt.Println(c, d)
	fmt.Println(split(17))


        // Create a cache with a default expiration time of 5 minutes, and which
        // purges expired items every 30 seconds
        a_cache := cache.New(5*time.Minute, 30*time.Second)

	a_cache = 1

        // Set the value of the key "foo" to "bar", with the default expiration time
        a_cache.Set("foo", "bar", 0)

        // Set the value of the key "baz" to 42, with no expiration time
        // (the item won't be removed until it is re-set, or removed using
        // a_cacheDelete("baz")
        a_cache.Set("baz", 42, -1)

        // Get the string associated with the key "foo" from the cache
        foo, found := a_cache.Get("foo")
        if found {
                fmt.Println(foo)
        }

        foo, found = a_cache.Get("foo")
        if found {
                fmt.Println(foo.(string))
	}

	for index := 0; index < 10000; index++ {
		foo, found = a_cache.Get("baz")
		if found {
			fmt.Println(foo)
		}
	}

}
