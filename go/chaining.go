package main

import (
	"flag"
	"fmt"
	"runtime"
)

var ngoroutines = flag.Int("n", 100000, "how many goroutines")

func f(left, right chan int) { left <- 1 + <-right }

func main() {
	flag.Parse()
	runtime.GOMAXPROCS(runtime.NumCPU())
	leftMost := make(chan int)
	var left, right chan int = nil, leftMost
	for i := 0; i < *ngoroutines; i++ {
		left, right = right, make(chan int)
		go f(left, right)
	}
	right <- 2
	x := <-leftMost
	fmt.Println(x)
}
