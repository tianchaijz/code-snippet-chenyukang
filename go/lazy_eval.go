package main

import "fmt"

var resume chan int

func intergers() chan int {
	yield := make(chan int)
	count := 0
	go func() {
		for {
			yield <- count
			count++
		}
	}()
	return yield
}

func generateInt() int {
	return <-resume
}

func main() {
	resume = intergers()
	for i := 0; i < 100; i++ {
		fmt.Println(generateInt())
	}
}
