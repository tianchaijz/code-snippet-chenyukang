package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Hello struct {
	Hello string
	World string
}

func main() {
	var conf map[string]string
	jsbuf, err := ioutil.ReadFile("./hello.json")
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(jsbuf, &conf)
	fmt.Println(conf)
	fmt.Println(conf["hello"])
}
