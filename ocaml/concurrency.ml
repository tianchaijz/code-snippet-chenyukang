open Thread
open Mutex

let proc n =
  let result = ref 0 in
  let f i =
    for j = 1 to n do
      let v = !result in
      Thread.delay (Random.float 1.);
      result := v + i;
      Printf.printf "Value: %d\n" !result;
      flush stdout;
    done in
  ignore (Thread.create f 1);
  ignore (Thread.create f 2)

let proc_mutex n =
  let result = ref 0 in
  let m = Mutex.create() in
  let f i =
    for j = 1 to n do
      Mutex.lock m;
      let v = !result in
      Thread.delay (Random.float 1.);
      result := v + i;
      Printf.printf "Value: %d\n" !result;
      flush stdout;
      Mutex.unlock m;
      Thread.delay (Random.float 20.);
    done in
  ignore (Thread.create f 1);
  ignore (Thread.create f 1);
  ignore (Thread.create f 1);;

(* if false then 4 else List.hd [];; *)

(* type 'a list = Nil | Cons of 'a * 'a list *)
type 'a stream = Nil | Cons of 'a * (unit -> 'a stream)

let rec (ones : int stream) = Cons(1, fun() -> ones)

let rec from (n: int) : int stream =
  Cons(n, fun() -> from(n+1))

let natuals = from 0;;

let hd(s : 'a stream) : 'a =
  match s with
    Nil -> failwith "hd"
  | Cons(x, _) -> x

let tl(s : 'a stream) : 'a stream =
  match s with
    Nil -> failwith "tl"
  | Cons(_, g) -> g()

let rec nth(s : 'a stream) (n : int) : 'a =
  if n = 0 then
    hd s
  else
    nth (tl s) (n - 1)

(* (\* make a stream from a list *\) *)
(* let from_list (l : 'a list) : 'a stream = *)
(*   List.fold_right (fun x s -> Cons (x, fun () -> s)) l Nil *)

(* make a list from the first n elements of a stream *)
let rec take (s : 'a stream) (n : int) : 'a list =
  if n <= 0 then
    []
  else match s with
      Nil -> []
    | _ -> hd s :: take (tl s) (n - 1)

(* let from_list (l : 'a list) : 'a stream = *)
(*   List.fold_right (fun x s -> Cons (x, fun () -> s)) l Nil *)

let rec map (f : 'a -> 'b) (s : 'a stream) : 'b stream =
  match s with Nil -> Nil
             | _ -> Cons (f (hd s), fun () -> map f (tl s))


let rec filter (f : 'a -> bool) (s : 'a stream) : 'a stream =
  match s with Nil -> Nil
             | Cons (x, g) ->
                if f x then Cons (x, fun () -> filter f (g ()))
                else filter f (g ())

let is_prime n =
  if n <= 1 then false else
    let rec test_from k =
      (k = n ) || (( n mod k )  <> 0 &&
                     test_from (k + 1)) in
    test_from 2;;


let even = fun n -> n mod 2 = 0;;

let first_10_even = (take (filter even natuals) 10);;
let primes = (take (filter is_prime natuals) 100);;
