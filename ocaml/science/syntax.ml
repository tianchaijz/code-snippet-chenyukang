let (ipow3, ipow4) =
  let sqr x = x * x in
  ((fun x -> x * (sqr x)), fun x -> (sqr x) * (sqr x));;

List.length ["one"; "two"; "three"];;  
Array.length [|1; 2; 3|];;

let f = function
    [i; j; k] when i - j - k = 0 -> true
  | _ -> false

let is_sign = function
    -1 | 0 | 1 -> true
    | _ -> false

let product a  b = match (a, b) with
    (_, 0.) | (0., _) -> 0.
    | (a, b) -> a *. b

type in_option = None | Some of int;;

let extrace = function
  | Some x -> x
  | None -> 0

let sign = function
    i when i < 0 -> -1
  | 0 -> 0
  | _  -> 1

type number = Integer of int | Real of float | Complex of float * float;;

let is_zero = function
  | Integer x -> x = 0
  | Real x -> x = 0.
  | Complex (x, y) -> x = 0. && y = 0.

let number_equl a b = match a with
    Integer i ->
    begin
      match b with
        Integer j when i = j -> true
      | Complex(x, 0.) | Real x when x = float_of_int i -> true
      | Integer _ | Real _ | Complex _ -> false
    end
   | Real x | Complex (x, 0.) ->
               begin
                 match b with
                 | Integer i when x = float_of_int i -> true
                 | Complex (y, 0.) | Real y when x = y -> true
                 | Integer _ | Real _ | Complex _ -> false
               end
   | Complex(x1, y1) ->
      begin
        match b with
        | Complex(x2, y2)  when x1 = x2 && y1 = y2 -> true
        | Integer _ | Real _ | Complex _ -> false
      end
        
                          

        
