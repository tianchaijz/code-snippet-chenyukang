
open Core.Std;;

type 'a aref = { mutable contents: 'a};;
let ref x = { contents = x};;
let (!) r = r.contents;;
let (:=) r x = r.contents <- x;;

let permute array =
  let length = Array.length array in
  for i = 0 to length - 2 do
    let j = i + Random.int(length - i) in
    let tmp = array.(i) in
    array.(i) <- array.(j);
    array.(j) <- tmp
  done;;

let ar = Array.init 20 ~f:(fun i -> i);;
let result = permute ar;;
