open Core.Std;;

let x = 1;;
let y = 2;;
let x_plus_y = x + y;;

let square x = x * x ;;

let ratioxy=
    Float.of_int x /. Float.of_int y

let sum_if_true test first second =
  (if test first then first else 0)
  + (if test second then second else 0)

let even x =
  x mod 2 = 0;;


let first_if_true test x y =
  if test x then x
  else y;;

let a_tuple = (3, "three");;

let (x, y) = a_tuple;;

let distance(x1, y1) (x2, y2) =
  sqrt((x1 -. x2) ** 2. +. (y1 -. y2) ** 2.);;

let languages = ["C"; "Lisp"; "Ocaml"]
    in List.length languages;;

let languages = ["C"; "Lisp"; "Ocaml"];;

List.map ~f:String.length languages;;

let rec sum l =
  match l with
  | [] -> 0
  | hd::tl -> hd + sum tl;;

sum [1; 2; 3];;

let my_first_language =
  match languages with
  | first::rest -> first
  | [] -> "Ocaml";;
  
let rec destutter list =
  match list with
  | [] -> []
  | [x] -> [x]
  | hd1 :: hd2 :: tl ->
     if hd1 = hd2 then destutter (hd2::tl)
     else hd1:: destutter (hd2::tl);;

destutter [1; 2; 3];;
destutter [1; 1; 2; 2; 3];;

let log_entry maybe_time message =
  let time = match maybe_time with
    | Some x -> x
    | None -> Time.now()
  in Time.to_sec_string time ^ " -- " ^ message;;

log_entry None "Up to time";;
log_entry (Some Time.epoch) "Long long ago";;

type point2d = { x: float; y: float};;
(* type point2x = { x: float; y: float; z: float};; *)

let p = { x = 3.; y = -4. };;

let magnitude { x = x_pos; y = y_pos } =
  sqrt(x_pos ** 2. +. y_pos ** 2.);;

let magnitude {x; y} =
  sqrt(x ** 2. +. y ** 2.);;

type circle_desc = { center: point2d; radius: float }
type rect_desc = { lower_left: point2d; width: float; height: float }
type segment_desc = { endpoint1: point2d; endpoint2: point2d } ;;

type scene_element =
  | Circle of circle_desc
  | Rect of rect_desc
  | Segment of segment_desc
;;

let distance v1 v2 =
  magnitude { x = v1.x -. v2.x; y = v1.y -. v2.y };;

let is_inside_scene_element point scene_element = match scene_element with
  | Circle { center; radius } ->
     distance center point < radius
  | Rect { lower_left; width; height } ->
     point.x    > lower_left.x && point.x < lower_left.x +. width
     && point.y > lower_left.y && point.y < lower_left.y +. height
  | Segment { endpoint1; endpoint2 } -> false
;;

let is_inside_scene point scene =
  List.exists scene
              ~f:(fun el -> is_inside_scene_element point el);;

let numbers = [|1; 2; 3; 4|];;
  
