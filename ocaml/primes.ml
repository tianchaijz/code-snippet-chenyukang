type 'a stream_t = Nil | Cons of 'a * (unit -> 'a stream_t)

let hd = function
  | Nil -> failwith "hd"
  | Cons (v, _) -> v

let tl = function
  | Nil -> failwith "tl"
  | Cons (_, g) -> g()

let rec take n = function
  | Nil -> []
  | Cons (_, _)  when n = 0 -> []
  | Cons (hd, g) -> hd::take (n-1) (g())
                             
let rec filter f = function
  | Nil -> Nil
  | Cons (hd, g) ->
     if f hd then Cons (hd, fun() -> filter f (g()))
     else filter f (g())
                 
let rec from i = Cons (i, fun() -> from (i+1))

(* delete multiples of p from a stream *)
let sift p = filter (fun n -> n mod p <> 0)

(* sieve of Eratosthenes *)
let rec sieve = function
  | Nil -> Nil
  | Cons (p, g) -> 
     let next = sift p (g()) in
     Cons (p, fun () -> sieve next)

(* primes *)
let numbers = from 2;;          
let primes = sieve (from 2);;
                   
