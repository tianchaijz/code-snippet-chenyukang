

let rec fact x =
  if x <= 1 then 1 else x * fact (x - 1)


let rec sort = function
  | [] -> []
  | x :: l -> insert x (sort l)
and insert elem = function
  | [] -> [elem]
  | x :: l -> if elem < x then elem :: x :: l
             else x :: insert elem l

let add_polynom p1 p2 =
  let n1 = Array.length p1
  and n2 = Array.length p2 in
  let result = Array.create (max n1 n2) 0 in
  for i = 0 to n1 - 1 do result.(i) <- p1.(i) done;
  for i = 0 to n2 - 1 do result.(i) <- result.(i) + p2.(i) done;
  result;;


let fact_ref n =
  let result = ref 1 in
  for i = 2 to n do
    result := i * !result
  done;
  !result;;

let rec sigma f = function
  | [] -> 0
  | x :: l -> (f x) + (sigma f l);;

(sigma (fun x -> x * x) [1; 2; 3]);;

let average a b =
  (a +. b) /. 2.0;;

(average 10.0 103.0);;

let rec range a b =
  if a > b then []
  else a :: range (a+1) b;;
