open Core.Std
open Async.Std
open Printf       

let def = In_thread.run(fun() -> List.range 1 10);;

let log_delays thunk =
  let start = Time.now() in
  let print_time() =
    let diff = Time.diff(Time.now()) start in
    printf "%s, " (Time.Span.to_string diff)
  in let d = thunk() in
     Clock.every (sec 0.1) ~stop:d print_time;
     d >>| fun() -> print_time(); printf "\n";;

(* log_delays (fun() -> after (sec 1.0));; *)

let busy_loop() =
  let x = ref None in
  for i = 1 to 100_000_000 do x := Some i done;;
    
  (* log_delays (fun() -> return (busy_loop()));; *)

  (* log_delays (fun() -> In_thread.run busy_loop);; *)
    

    
    

  
