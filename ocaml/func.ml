open Core.Std;;
  
let upcase_first_entry_line =
  match String.split ~on:',' line with
  | [] -> assert false
  | first::rest -> String.concat ~sep::"," (String.uppercase first::rest)
;;

let some_or_zero num_opt =
  match num_opt with
  | Some x -> x
  | None -> 0
;;
