(* Set data abstraction with union and intersection *)

module type SET = sig
    type 'a set
    val empty : 'a set
    val mem : 'a -> 'a set -> bool
    val add : 'a -> 'a set -> 'a set
    val rem : 'a -> 'a set -> 'a set
    val size: 'a set -> int
    val union: 'a set -> 'a set -> 'a set
    val inter: 'a set -> 'a set -> 'a set
  end

(* Implementation of sets as lists with duplicates *)

module Set1 : SET = struct
    type 'a set = 'a list
    let empty = []
    let mem = List.mem
    let add x l = x :: l
    let rem x = List.filter ((<>) x)
    let rec size l =
      match l with
      | [] -> 0
      | h :: t -> size t + (if mem h t then 0 else 1)
    let union l1 l2 = l1 @ l2
    let inter l1 l2 = List.filter (fun h -> mem h l2) l1
  end

(* Implementation of sets as lists without duplicates *)

module Set2 : SET = struct
    type 'a set = 'a list
    let empty = []
    let mem = List.mem
    (* add checks if already a member *)
    let add x l = if mem x l then l else x :: l
    let rem x = List.filter ((<>) x)
    let size = List.length (* size is just length if no duplicates *)
    let union l1 l2 = (* check if already in other set *)
      List.fold_left (fun a x -> if mem x l2 then a else x :: a) l2 l1
    let inter l1 l2 = List.filter (fun h -> mem h l2) l1
  end


module type STACK =
  sig
    (* A stack of elements of type 'a. We write  to
     * denote a stack whose top element is a1, with successive
     * elements a2, a3,...an. *)
    type 'a stack

    exception EmptyStack

    (* The empty stack. *)
    val empty : 'a stack

    (* Whether this stack is empty. *)
    val isEmpty : 'a stack -> bool

    (* Returns a new stack with x pushed onto the top. *)
    val push : ('a * 'a stack) -> 'a stack

    (* Returns a new stack with the top element popped off. *)
    val pop : 'a stack -> 'a stack

    (* The top element of the stack. *)
    val top : 'a stack -> 'a

    (* map(f) maps one stack into a corresponding stack, using f. *)
    val map : ('a -> 'b) -> 'a stack -> 'b stack

    (* app(f) applies f to every element of the stack, from the top down. *)
    val app : ('a -> unit) -> 'a stack -> unit
  end

module Stack : STACK =
  struct
    type 'a stack = 'a list
    exception EmptyStack

    let empty : 'a stack = []

    let isEmpty (l : 'a list) : bool =
      l = []

    let push ((x : 'a), (l : 'a stack)) : 'a stack
      = x :: l

    let pop (l : 'a stack) : 'a stack =
      match l with
        [] -> raise EmptyStack
      | x :: xs -> xs

    let top (l : 'a stack) : 'a =
      match l with
        [] -> raise EmptyStack
      | x :: xs -> x

    let map (f : 'a -> 'b) (l : 'a stack) : 'b stack = List.map f l
    let app (f : 'a -> unit) (l : 'a stack) : unit = List.iter f l
  end

module type QUEUE =
  sig
    type 'a queue
    exception EmptyQueue

    val empty : 'a queue
    val isEmpty : 'a queue -> bool

    val enqueue : ('a * 'a queue) -> 'a queue
    val dequeue : 'a queue -> 'a queue
    val front : 'a queue -> 'a

    val map : ('a -> 'b) -> 'a queue -> 'b queue
    val app : ('a -> unit) -> 'a queue -> unit
  end

module Queue : QUEUE =
  struct
    module S = Stack

    type 'a queue = ('a S.stack * 'a S.stack)
    exception EmptyQueue

    let empty : 'a queue = (S.empty, S.empty)

    let isEmpty ((s1, s2) : 'a queue) =
      S.isEmpty s1 && S.isEmpty s2

    let enqueue ((x : 'a), ((s1, s2) : 'a queue)) : 'a queue =
      (S.push (x, s1), s2)

    let rev (s : 'a S.stack) : 'a S.stack =
      let rec loop ((prev : 'a S.stack), (curr : 'a S.stack)) : 'a S.stack =
        if S.isEmpty prev
        then curr
        else loop (S.pop prev, S.push (S.top prev, curr))
      in
      loop (s, S.empty)

    let dequeue ((s1, s2) : 'a queue) : 'a queue =
      if S.isEmpty s2
      then try (S.empty, S.pop (rev s1))
           with S.EmptyStack -> raise EmptyQueue
      else (s1, S.pop s2)

    let front ((s1, s2) : 'a queue) : 'a =
      if (S.isEmpty s2)
      then try S.top (rev s1)
           with S.EmptyStack -> raise EmptyQueue
      else S.top s2

    let map (f : 'a -> 'b) ((s1, s2) : 'a queue) : 'b queue =
      (S.map f s1, S.map f s2)

    let app (f : 'a -> unit) ((s1, s2) : 'a queue) : unit =
      S.app f s2;
      S.app f (rev s1)
  end

let s = Stack.empty;;
let s = Stack.push (10, s);;
let s = Stack.push (11, s);;
let s = Stack.push (12, s);;
Stack.app (fun x -> Printf.printf "now: %d\n" x) s;;

module type DICTIONARY =
  sig
    type key = string
    type 'a dict
    exception NotFound

    val empty : 'a dict
    val insert : 'a dict -> key -> 'a -> 'a dict
    val lookup : 'a dict -> key -> 'a
    val map: ('a -> 'b) -> 'a dict -> 'b dict
  end

module AssocList : DICTIONARY =
  struct
    type key = string
    type 'a dict = (key * 'a) list

    let empty = []
    let insert (d : 'a dict) (k : key) (x : 'a) : 'a dict =
      (k, x) :: d

    exception NotFound
    let rec lookup (d : 'a dict) (k : key) : 'a =
      match d with
        [] -> raise NotFound
      | (k', x) :: rest ->
         if k' = k then x
         else lookup rest k

    let map (f: 'a -> 'b) (d : 'a dict) =
      List.map (fun (k, a) -> (k, f a)) d
  end

module FunctionDict : DICTIONARY =
  struct
    type key = string
    type 'a dict = string -> 'a
    exception NotFound

    let empty = fun _ -> raise NotFound
    let lookup (d : 'a dict) (k : key) : 'a =
      d k
    let insert (d : 'a dict) (k : key) (x : 'a) : 'a dict =
      fun k' -> if k' = k then x else d k'

    let map (f: 'a -> 'b) (d : 'a dict) : 'b dict =
      fun k -> f (d k)
  end

let dict = FunctionDict.empty;;
let dict = FunctionDict.insert dict "hello" "world";;
let dict = FunctionDict.insert dict "hello" "now";;
let res = FunctionDict.lookup dict "hello" in
    Printf.printf "now res: %s\n" res;;



module SortedAssocList : DICTIONARY =
  struct
    type key = string
    type 'a dict = (key * 'a) list

    exception NotFound

    let empty = []

    let rec insert (d : 'a dict) (k : string) (x : 'a) : 'a dict =
      match d with
        [] -> (k, x) :: []
      | (k', x') :: rest ->
         match String.compare k k' with
           1 -> (k', x') :: (insert rest k x)
         | 0 -> (k , x) :: rest
         | -1 -> (k, x) :: (k', x') :: rest
         | _ -> failwith "Impossile"


    let rec lookup (d : 'a dict) (k : key) : 'a =
      match d with
        [] -> raise NotFound
      | (k', x') :: rest ->
         match String.compare k k' with
           0 -> x'
         | -1 -> raise NotFound
         | 1 -> lookup rest k
         | _ -> failwith "Impossile"

    let map (f : 'a -> 'b) (d: 'a dict) =
      List.map (fun (k, a) -> (k, f a)) d
  end

let dict = SortedAssocList.empty;;
let dict = SortedAssocList.insert dict "hello" "world";;
let res = SortedAssocList.lookup dict "hello" in
    Printf.printf "now res: %s\n" res;;

let dict = SortedAssocList.insert dict "hello" "now";;
let res = SortedAssocList.lookup dict "hello" in
    Printf.printf "now res: %s\n" res;;


  
