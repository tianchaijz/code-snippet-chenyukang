
module type SET = sig
    type 'a set
    val empty : 'a set
    val mem : 'a -> 'a set -> bool
    val add : 'a -> 'a set -> 'a set
    val rem : 'a -> 'a set -> 'a set
    val size: 'a set -> int
    val union: 'a set -> 'a set -> 'a set
    val inter: 'a set -> 'a set -> 'a set
  end

module Set1 : SET = struct
    type 'a set = 'a list
    let empty = []
    let mem = List.mem
    let add x l = x :: l
    let rem x = List.filter ((<>) x)
                            
    let rec size l =
      match l with
        [] -> 0
      | hl :: tl ->
         size tl + (if mem hl tl then 0 else 1)

    let union l1 l2 = l1 @ l2
    let inter l1 l2 =
      List.filter (fun h -> mem h l2) l1
  end

let s = Set1.empty;;
let s = Set1.add 10 s;;
let s = Set1.add 10 s;;
let s = Set1.add 11 s;;  
let size = Set1.size s;;
  
  
module Set2 : SET = struct
    type 'a set = 'a list
    let empty = []
    let mem = List.mem
    let add x l = if mem x l then l else x :: l
    let rem x = List.filter ((<>) x)
    let size = List.length
    let union l1 l2 =
      List.fold_left (fun a x -> if mem x l2 then a else x :: a) l2 l1
                     
    let inter l1 l2 =
      List.filter (fun h -> mem h l2) l1
  end
                      
      
let s = Set2.empty;;
let s = Set2.add 10 s;;
let s = Set2.add 10 s;;
let s = Set2.add 11 s;;
let size = Set2.size s;;
  
                  
                  
