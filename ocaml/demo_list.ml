#require "core_bench";;
open Core_bench.Std;;

let rec drop_value l to_drop =
  match l with
  | [] -> []
  | hd::tl ->
     let new_tl = drop_value tl to_drop in
     if hd = to_drop then new_tl else hd :: new_tl
;;

let rec drop_zero l =
  match l with
  | [] -> []
  | 0 :: tl -> drop_zero tl
  | hd :: tl -> hd :: drop_zero tl ;;

let rec sum_if l =
  if List.is_empty l then 0
  else List.hd_exn l + sum_if (List.tl_exn l)
;;

let rec sum l =
  match l with
  | [] -> 0
  | [v] -> v
  | hd :: tl -> hd + sum tl
;;

let rec length = function
  | [] -> 0
  | _ :: tl -> 1 + length tl
;;

let make_list n = List.init n ~f:(fun x -> x);;

make_list 100;;

let rec length_plus_n l n =
  match l with
  | [] -> n
  | _ :: tl -> length_plus_n tl (n + 1)
;;


let length l = length_plus_n l 0 ;;

let rec destutter = function
    match list with
    | [] -> []
    | [hd] -> [hd]
    | hd :: hd' :: tl ->
       if hd = hd' then destutter (hd' :: tl)
       else hd :: destutter (hd' :: tl)
;;

let rec destutter_faster = function
  | [] | [_] as l -> l
  | hd :: (hd' :: _ as tl) when hd = hd' -> destutter_faster tl
  | hd :: tl -> hd :: destutter_faster tl
;;


let numbers = List.range 0 1000 in
    [ Bench.Test.create ~name:"destutter" (fun () -> ignore (destutter (make_list 10000)))
    ; Bench.Test.create ~name:"destutter_faster"    (fun () -> ignore (destutter_faster (make_list 10000))) ]
    |> run_bench
;;

numbers;;
