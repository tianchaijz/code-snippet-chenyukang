open Core;;

type t = {
    foo: int;
    bar: float;
  } with sexp;;

t_of_sexp (Sexp.of_string "((bar 24) (foo 3))");;
