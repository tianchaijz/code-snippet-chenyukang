open Core.Std

type 'a lazy_state =
  | Delayed of (unit -> 'a)
  | Value of 'a
  | Exn of exn

let create_lazy f = ref (Delayed f)

let force v =
  match !v with
  | Value x -> x
  | Exn e ->
     raise e
  | Delayed f ->
     try
       let x = f() in
       v := Value x;
       x
     with exn ->
       v := Exn exn;
       raise exn
;;

let memoize f =
  let table = Hashtbl.Poly.create() in
  (fun x ->
   match Hashtbl.find table x with
   | Some y -> y
   | None ->
      let y = f x in
      Hashtbl.add_exn table ~key:x ~data:y;
      y)


let rec edit_distance s t =
  match String.length s, String.length t with
  | (0, x) | (x, 0) -> x
  | (len_s, len_t) ->
     let s' = String.drop_suffix s 1 in
     let t' = String.drop_suffix t 1 in
     let cost_to_drop_both =
       if s.[len_s - 1] = t.[len_t - 1] then 0 else 1 in
     List.reduce_exn ~f:Int.min
                     [ edit_distance s' t + 1;
                       edit_distance s t' + 1;
                       edit_distance s' t' + cost_to_drop_both]

let time_it f =
  let start = Time.now() in
  let _ = f() in
  let stop = Time.now() in
  printf "Time: %s\n" (Time.Span.to_string (Time.diff stop start))

let rec fib i =
  if i <= 1 then i else fib (i-1) + fib (i-2);;

let fib = memoize fib

let fib_norec fib i =
  if i <= 1 then i else
    fib (i-1) + fib (i-2)

let rec fib i = fib_norec fib i


let memo_rec f_norec x =
  let fref = ref (fun _ -> assert false) in
  let f = memoize (fun x -> f_norec !fref x) in
  fref := f;
  f x

let fib = memo_rec fib_norec

let fib = memo_rec (fun fib i ->
                    if i <= 1 then 1
                    else fib (i-1) + fib (i-2))

let edit_distance_fast = memo_rec (
  fun edit_distance (s, t) ->
  match String.length s, String.length t with
  | (0, x) | (x, 0) -> x
  | (len_s, len_t) ->
     let s' = String.drop_suffix s 1 in
     let t' = String.drop_suffix t 1 in
     let cost_to_drop_both =
       if s.[len_s - 1] = t.[len_t - 1] then 0 else 1
     in List.reduce_exn ~f:Int.min
                        [ edit_distance (s', t) + 1;
                          edit_distance (s, t') + 1;
                          edit_distance (s', t') + cost_to_drop_both])
