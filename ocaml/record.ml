#require "core_extended";;
open Core_extended.Std;;

type host_info = {
    hostname: string;
    os:       string;
    cpu_arch: string;
    timestamp: Time.t;
  };;

let my_host =
  let sh = Shell.sh_one_exn in
  { hostname   = sh "hostname";
    os_name    = sh "uname -s";
    cpu_arch   = sh "uname -p";
    timestamp  = Time.now ();
  };;
