
type 'a tree = TNode of 'a * 'a tree * 'a tree | TLeaf

let rec contains x = function
    TLeaf -> false
  | TNode(y, l, r) ->
     x = y || (x < y && contains x l) || ( contains x r)

let rec add x = function
    TLeaf -> TNode(x, TLeaf, TLeaf)
  | TNode(y, l, r) as t ->
     if x = y then t
     else if x > y then TNode(y, l, add x r)
     else TNode(y, add x l, r)

let make =
  TLeaf


type color = Red | Black
type 'a rbtree =
  Node of color * 'a * 'a rbtree * 'a rbtree |
  Leaf

let rec mem x = function
    Leaf -> false
  | Node(_, y, l, r) ->
     x = y || (x < y && mem x l) || mem x r
