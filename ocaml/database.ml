
type data_card =  string array;;
type data_base = { card_index: string -> int;
                   data: data_card list};;

let field db n (dc: data_card) =
  dc.(db.card_index n);;

let field base name =
  let i = base.card_index name in
  fun(card: data_card) -> card.(i);;
  
  
let suffix s i =
  try String.sub s i ((String.length s) - i)
                 with Invalid_argument("String.sub") -> "";;

  
  
