open Core.Std

let sqaure_root (x : float) : float =
  let delta = 0.0001 in
  let good_enough (guess : float) : bool =
    Float.abs(guess *. guess -. x) < delta in

  let improve guess =
    (guess +. x /. guess) /. 2.0 in

  let rec try_guess guess =
    if good_enough guess then guess
    else try_guess (improve guess) in
  try_guess 1.0

let swap (x, y) =
  (y, x)
