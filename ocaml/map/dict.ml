
open Core.Std

type ('a, 'b) t = {
    mutable length: int;
    buckets: ('a * 'b) list array;
  }

let num_buckets = 17
                    
let hash_bucket key =
  (Hashtbl.hash key) mod num_buckets
                           
let create () = {
    length = 0;
    buckets = Array.create ~len:num_buckets [];
  }

let length t = t.length
let find t key =
  List.find_map t.buckets.(hash_bucket key)
                ~f:(fun (key', data) -> if key' = key then Some data else None)
                
                
let bucket_has_key t i key =
  List.exists t.buckets.(i) ~f:(fun (key', _) -> key' = key)

let add t ~key ~data =
  
  
              
       
