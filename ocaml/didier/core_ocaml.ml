
let new_count =
  let r = ref 0 in
  let next () = r := !r+1; !r in
  next;;
