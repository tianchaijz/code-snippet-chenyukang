let rec even x = (x != 1) && ((x = 0) || odd(x - 1))
and   odd  x = (x != 0) && ((x == 1) || even(x - 1))


let make_pair a b = (a, b);;

let app = fun f p -> (f p);;

let add (x: int) (y: int) =  x + y;;
let add x y = x + y;;

let null l = (l = []);;

let rec size l =
  if null l then 0
  else 1 + (size (List.tl l));;

let compose f ff x = (f (ff x));;

let rec iterator n f =
  if n = 0 then (fun x -> x)
  else compose f (iterator (n-1) f);;

let rec power i n =
  let i_times = ( * ) i in
  (iterator n i_times) 1;;

let rec apply_fun_list x f_list =
  if null f_list then []
  else ((List.hd f_list) x) :: (apply_fun_list x (List.tl f_list));;

apply_fun_list 1 [( + ) 1; ( + ) 2; ( * ) 100];;

let mk_mult_fun_list n =
  let rec mult_rec p =
    if p = n then [( * ) n]
    else (( * ) p ) :: (mult_rec (p + 1))
  in (mult_rec 1);;

let multab n = apply_fun_list n (mk_mult_fun_list 10);;

let f x = match x with 1 -> 3;;

let rec l = 1 :: l;;

let rec memq a l  = match l with
    [] -> false
  | b::l -> ( a == b ) || (memq a l);;

let head_error l = match l with
    [] -> failwith "Empty List"
  | h::t -> h;;

exception Found_zero;;

let rec mult_rec l = match l with
  | [] -> 1
  | 0 :: _ -> raise Found_zero
  | n :: x -> n * mult_rec(x);;

let mult_list l = try
    mult_rec l with Found_zero -> 0;;

let rec f1 x = f1 x;;
