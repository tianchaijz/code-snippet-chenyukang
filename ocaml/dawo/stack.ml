
type 'a stack = {
    mutable ind: int;
    mutable elts: 'a array;
    size: int;
  };;

let init_stack n = { ind = 0; size = n; elts = [||]};;

exception Stack_empty;;
exception Stack_full;;

let pop p =
  if p.ind = 0 then raise Stack_empty
  else (p.ind <- p.ind - 1; p.elts.(p.ind));;

let push e p =
  if p.elts = [||] then
    (p.elts <- Array.create p.size e;
     p.ind <- 1)
  else if p.ind >= p.size then raise Stack_full
  else  (p.elts.(p.ind) <- e; p.ind <- p.ind + 1);;
