open Random;;
open Int32;;

Random.init(10);;

let x = Random.bits();;
let y = Random.bits();;

Printf.printf "(x = %d, y = %d)" x y;;

let rec fib x = if x <= 1 then 1 else fib (x - 1) + fib (x - 2);;
