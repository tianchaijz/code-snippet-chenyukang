
type key = Plus | Minus | Times | Div | Equals | Digit of int;;

let is_digit = fun x -> (x >= 0) && (x <= 9);;

let valid ky = match ky with
  |Digit(x) -> is_digit x
  |_ -> true

type state = {
    lcd : int; (* last computation done *)
    lka : key; (* last key activated *)
    loa : key; (* last operator activated *)
    vpr : int; (* value printed *)
  };;

let evaluate x y ky = match ky with
  | Plus -> x + y
  | Minus -> x - y
  | Times -> x * y
  | Div -> x / y
  | Equals -> y
  | Digit _ -> failwith "evaluate: no op";;

let transition st ky =
  let digit_transitation n = function
      Digit _ -> { st with lka = ky; vpr = st.vpr * 10 + n }
    | _ ->  { st with lka = ky; vpr = n}
  in
  match ky with
  | Digit p -> digit_transitation p st.lka
  | _ -> let res = evaluate st.lcd st.vpr st.loa
         in { lcd = res; lka = ky; loa = ky; vpr = res};;

let s1 = { lcd = 0; lka = Equals; loa = Equals; vpr = 0};;
let s2 = transition s1 (Digit 3);;
let s3 = transition s2 Plus;;
let s4 = transition s3 (Digit 2);;
let s5 = transition s4 (Digit 1);;
let s6 = transition s5 (Digit 0);;
let s7 = transition s6 Times;;
let s8 = transition s7 (Digit 2);;
let s9 = transition s8 Equals;;
