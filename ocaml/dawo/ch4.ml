
type 'a v =
  | Imm of 'a
  | Def of (unit -> 'a);;

type 'a vm = {mutable c: 'a v};;

let eval e = match e.c with
  | Imm a -> a
  | Def f -> let u = f() in e.c <- Imm u; u;;

let if_deferred c e1 e2 =
  if eval c then eval e1 else eval e2;;

let rec facr n =
  if_deferred
    {c = Def(fun() -> n == 0)}
    {c = Def(fun() -> 1)}
    {c = Def(fun() -> n * (facr(n-1)));};;

let freeze e = { c = Def(fun() -> e)};;

let exp = freeze (print_string "trace"; print_newline(); 4 * 5);;  (*  eval exp;; to execute it*)

type 'a enum = {
    mutable i: 'a;
    f: 'a -> 'a
  };;

let next e =
  let x = e.i in
  begin
    e.i <- (e.f e.i);
    x;
  end;;

let nat = { i = 0; f = fun x -> x + 1;};;

let fib =
  let fx =
    let c = ref 0 in
    fun v -> let r = !c + v in
             c := v; r in
  {i = 1; f = fx};;
