
let add ~first ~second =
  first + second;;

let add_2v ?(first=1) ~second =
  first + second;;

let add_3v ?(first=1) second =
  first + second;;

let concat ?sep x y =
     let sep = match sep with None -> "" | Some x -> x in
     x ^ sep ^ y
  ;;


let concat ?(sep="") x y = x ^ sep ^ y ;;

