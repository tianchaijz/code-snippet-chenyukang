let fetch_content uri =
  try let connection = Curl.init()
      and write_buffer = Buffer.create 1763 in
      Curl.set_writefunction connection
        (fun x -> Buffer.add_string write_buffer x;
                  String.length x);
      Curl.set_url connection uri;
      Curl.perform connection;
      Curl.global_cleanup();
      Buffer.contents write_buffer;
      with _ -> raise (Failure uri)


let parse_html_from_uri uri =
  let ch = new Netchannels.input_string (fetch_content uri) in
  let docs = Nethtml.parse ?return_pis:(Some false) ch in
  ch # close_in ();
  docs
