open Core.Std;;
open Async.Std;;
  
let query_uri query =
  let base_uri = Uri.of_string "http://api.duckduckgo.com/?format=json" in
  Uri.add_query_param base_uri ("q", [query])
                      
let get_definition_from_json json =
  match Yojson.Safe.from_string json with
  | `Assoc kv_list ->
     let find key =
       begin match List.Assoc.find kv_list key with
             | None | Some(`String "") -> None
             | Some s -> Some(Yojson.Safe.to_string s)
       end
     in
     begin match find "Abstract" with
           | Some _ as x -> x
           | None -> find "Definition"
     end
  | _ -> None

(* Execute the DuckDuckGo search *)
let get_definition word =
  Cohttp_async.Client.get (query_uri word)
  >>= fun (_, body) ->
  Pipe.to_list body
  >>| fun strings ->
  (word, get_definition_from_json (String.concat strings))
    
let print_result (word, definition) =
  Printf.printf "%s\n%s\n\n%s\n\n"
                word
                (String.init (String.length word) ~f:(fun _ -> '-'))
                (match definition with
                 | None -> "No definition found"
                 | Some def ->
                    String.concat ~sep:"\n"
                                  (Wrapper.wrap (Wrapper.make 70) def))
                

                
