open Core.Std

let upcase_first_entry line =
  match String.split ~on:',' line with
  | [] -> assert false
  | first::rest ->
     String.concat ~sep:"," (String.uppercase first::rest);;
