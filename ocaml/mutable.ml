
open Core.Std;;

type running_sum =
  { mutable sum: float;
    mutable sum_sq: float;
    mutable samples: int;
  };;

let mean rsum = rsum.sum /. float rsum.samples;;
let stdev rsum =
  sqrt(rsum.sum_sq /. float rsum.samples -.
         (rsum.sum /. float rsum.samples) ** 2.);;

let create () = { sum = 0.; sum_sq = 0.; samples = 0};;

let update rsum x =
  rsum.samples <- rsum.samples + 1;
  rsum.sum <- rsum.sum +. x;
  rsum.sum_sq <- rsum.sum_sq +. x *. x;;
