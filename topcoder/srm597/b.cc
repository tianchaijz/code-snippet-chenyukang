#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <map>
using namespace std;

class LittleElephantAndString {
public:
    int getNumber(string A, string B) {
        map<char, int> am;
        map<char, int> bm;
        int len = A.size();
        for(int i=0; i<len; i++) {
            am[A[i]]++;
            bm[B[i]]++;
        }
        bool ans = true;
        for(int i=0; i<len; i++) {
            if(am[A[i]] == bm[A[i]]) continue;
            else {
                ans = false;
                break;
            }
        }
        if(!ans) {
            return -1;
        }
        int i, j, count;
        count = 0;
        i = j = len - 1;
        while(i>=0 && j>=0) {
            //printf("%d %d -> %c %c\n", i, j, A[i], B[j]);
            if(A[i] == B[j]) {
                j--;
                i--;
            } else {
                i--;
                count++;
            }
        }
        return count;
    }
};

int main(){
    LittleElephantAndString A;

    std::cout<<A.getNumber("A",
                           "B") << std::endl;

    std::cout<<A.getNumber("ABC",
                           "CBA") <<std::endl;

    std::cout<<A.getNumber("AAABBB",
                           "BBBAAA") << std::endl;

    std::cout<<A.getNumber("ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                           "ZYXWVUTSRQPONMLKJIHGFEDCBA") << std::endl;

    return 0;
}
