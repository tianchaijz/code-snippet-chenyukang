#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
using namespace std;


class LittleElephantAndDouble {
public:
    string getAnswer(vector<int> A) {
        sort(A.begin(), A.end());
        int len = A.size();
        bool ans = true;
        int now = A[len-1];
        for(int i=A.size()-2; i>=0; i--) {
            if(A[i] == now) continue;
            if((now % A[i]) == 0 ) {
                int k = now / A[i];
                if(k%2 != 0) {
                    ans = false;
                    break;
                }
            } else {
                ans = false;
                break;
            }
        }
        if(ans)
            return "YES";
        else
            return "NO";
    }
};

int main() {
    LittleElephantAndDouble ans;
    vector<int> a;
    a.push_back(148);
    a.push_back(298);
    a.push_back(1184);

    std::cout<<ans.getAnswer(a) << std::endl;
    return 0;
}
