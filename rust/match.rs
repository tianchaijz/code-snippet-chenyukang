
use std::f64;
fn angle(vector: (f64, f64)) -> f64 {
    let pi = f64::consts::PI;
    match vector {
        (0.0, y) if y < 0.0 => 1.5 * pi,
        (0.0, _) => 0.5 * pi,
        (x, y) => (y/x).atan()
    }
}

fn main() {
    angle(vector<int>(1, 2, 3))
}
