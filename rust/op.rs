
#[deriving(Clone, PartialEq, Show)]
struct Node {
    val: int,
    var: int
}

fn check_optional(optional: &Option<Box<Node>>) {
    match *optional {
        Some(ref p) => println!("have value {}", p),
        None => println!("have no value")
    }
}

fn main() {
    let optional: Option<Box<Node>> = None;
    check_optional(&optional);

    let optional: Option<Box<Node>> = Some(box Node{var: 1, val: 2});
    check_optional(&optional);
}
