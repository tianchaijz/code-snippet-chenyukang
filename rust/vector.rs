use std::ops::{Add, Sub, Mul};

#[derive(Copy, Show)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

impl Add for Vector3 {
    type Output = Vector3;
    fn add(self, other: Vector3) -> Vector3 {
        Vector3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z
        }
    }
}

impl Sub for Vector3 {
    type Output = Vector3;
    fn sub(self, other: Vector3) -> Vector3 {
        Vector3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z
        }
    }
}

impl Mul for Vector3 {
    type Output = Vector3;
    fn mul(self, other: Vector3) -> Vector3 {
        Vector3 {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z
        }
    }
}

impl Mul<f32> for Vector3 {
    type Output = Vector3;
    fn mul(self, f: f32) -> Vector3 {
        Vector3 {
            x: self.x * f,
            y: self.y * f,
            z: self.z * f
        }
    }
}

impl Mul<isize> for Vector3 {
    type Output = Vector3;
    fn mul(self, f: isize) -> Vector3 {
        Vector3 {
            x: self.x * (f as f32),
            y: self.y * (f as f32),
            z: self.z * (f as f32)
        }
    }
}

fn main() {
    let a = Vector3{ x: 1.0f32, y: 1.0f32, z: 1.0f32 };
    let b = Vector3{ x: 2.0f32, y: 2.0f32, z: 2.0f32 };
    let mut res = (a + b + b) * b;
    res = res * 2.0f32;
    res = res * 2is;
    println!("a.x: {}", res.x);
}
