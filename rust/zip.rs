
fn main() {
    let a = vec![1i, 2, 3];
    let b = vec![4i, 5, 6];
    let mut it = a.iter().zip(b.iter());
    for (&x, &y) in it {
        println!("x: {}, y: {}", x as int, y);
    }
}
