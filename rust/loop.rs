

fn test_loop_label() {
    'outer: loop {
        println!("Entered the outer loop");

        'inner: loop {
            println!("Entered the inner loop");

            // This would break only the inner loop
            //break;

            // This breaks the outer loop
            break 'outer;
        }

        println!("This point will never be reached");
    }

    println!("Exited the outer loop");
}


fn main() {
    static MONSTER_FACTOR: f64 = 57.8;
    let monster_size = MONSTER_FACTOR * 10.0;
    //let monster_size: int = 50;

    /* A simple loop */
    let hi = "hi";
    let mut count = 0i;
    while count < 10 {
        println!("count is {} {} {}", hi, count, monster_size)
        count += 1;
    }

    test_loop_label();
}
