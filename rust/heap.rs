

fn main() {
    let a = Box::new(4);
    //let a = 5;
    let b = a;
    println!("value: {} {}", b, b);

    let y: &i32;
    let x = 5;
    y = &x;

    println!("{}", y);

}
