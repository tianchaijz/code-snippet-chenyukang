
fn takes_slice(slice: &str) {
    println!("Got: {}", slice);
}

fn main() {
    let mut s = "Hello".to_string();
    takes_slice(s.as_slice());
    s.push_str(", world.");
    takes_slice(s.as_slice());
}
