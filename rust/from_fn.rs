use std::sync::Future;
use std::rand;

fn partial_sum(start: uint) -> f64 {
    let mut local_sum = 0f64;
    for num in range(start*100000, (start+1)*100000) {
        local_sum += (num as f64 + 1.0) * (-2.0);
    }
    local_sum
}

fn main() {
    let mut futures = Vec::from_fn(200, |ind| Future::spawn(move || partial_sum(ind)));

    let mut final_res = 0f64;
    for ft in futures.iter_mut()  {
        final_res += ft.get();
    }
    println!("π^2/6 is not far from : {}", final_res);

    let mut res = Vec::from_fn(10, |_| rand::random::<uint>());
    for ft in res.iter_mut() {
        println!("res: {}", ft);
    }
}
