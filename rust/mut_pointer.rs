use std::mem;

#[derive(Show)]
struct Demo {
    val: u32,
    key: u32
}

#[derive(Show)]
struct Wrapper {
    pub demo: *mut Demo
}

fn main() {
    let mut demo = Box::new(Demo{ val: 1u32, key: 2u32});
    let mut demo2 = Demo{val: 3u32, key: 4u32};
    println!("demo: {:?}", *demo);
    println!("demo2: {:?}", demo2);
    let pointer: *mut Demo = &mut demo2;
    println!("pointer: {:?}", pointer);
    let mut x = Wrapper { demo: &mut demo2 as *mut Demo};
    println!("now: {:?}", x);
    println!("demo: {:?}", x.demo);
    unsafe {
        println!("demo from pointer: {:?}", *(x.demo));
    }
    let mut modify = x.demo;
    unsafe {
        (*(x.demo)).val = 2;
        (*(x.demo)).val = 10;
        x.demo.val = 11;
    }
    unsafe {
        println!("now: {:?}", *(x.demo));
    }

}
