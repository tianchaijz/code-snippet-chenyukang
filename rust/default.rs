use std::default::Default;


fn main() {
    let i: i8 = Default::default();
    let y: f64 = Default::default();
    let (a, b, (c, d)): (isize, usize, (bool, bool)) = Default::default();

    let mut my = 10i32;
    let val: *const i32 = &my;
    println!("val: {}", unsafe{*val});
    my = 11i32;
    println!("val: {}", unsafe{*val});
    println!("i: {}", i);
    println!("y: {}", y);
    println!("a: {}, b: {}, c: {}, d: {}", a, b, c, d);
}
