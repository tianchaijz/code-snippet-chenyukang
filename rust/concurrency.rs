use std::sync::Arc;

fn test_simple() {
    let numbers = vec![1, 2, 3];
    let (tx, ty) = channel();

    println!("try to get: {}",  *numbers.get(0));
    tx.send(numbers);

    spawn(proc() {
        let numbers = ty.recv();
        println!("{}", *numbers.get(2));
    });

}

fn test() {
    let numbers = vec![1, 2, 3];

    for num in range(0, 3) {
        let (tx, ty) = channel<num>();
        println!("try to get: {}",  *numbers.get(0));
        tx.send(numbers.clone());

        spawn(proc() {
            let numbers = ty.recv();
            println!("{}", *numbers.get(num as uint));
        });
    }
}

fn test_arc() {
    let numbers = vec![1, 2, 3];
    let numbers = Arc::new(numbers);

    for num in range(0, 3) {
        let (tx, ty) = channel<num>();
        println!("try to get: {}",  *numbers.get(0));
        tx.send(numbers);

        spawn(proc() {
            let numbers = ty.recv();
            println!("{}", *numbers.get(num as uint));
        });
    }
}




fn main() {

    test();
}
