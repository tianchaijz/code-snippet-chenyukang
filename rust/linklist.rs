
use std::mem::size_of; // bring `size_of` into the current scope, for convenience


struct Foo {
    a: u32,
    b: u32,
    c: u32,
    d: u32
}

struct Bar {
    a: Foo,
    b: Foo,
    c: Foo,
    d: Foo
}

#[deriving(Clone)]
enum List {
    Cons(u32, Box<List>),
    Nil
}

fn print_list(list: &List) {
    match list {
        &Nil => println!("end"),
        &Cons(v, box ref next) => { println!("v: {}", v); print_list(next); }
    }
}

fn eq(xs: &List, ys: &List) -> bool {
    match(xs, ys) {
        (&Nil, &Nil) => true,
        (&Cons(x, box ref next_x), &Cons(y, box ref next_y))
            if x == y => eq(next_x, next_y),
        _ => false
    }
}

fn main() {
    assert_eq!(size_of::<Foo>(), size_of::<u32>() * 4);
    assert_eq!(size_of::<Bar>(), size_of::<u32>() * 16);
    let list = Cons(1, box Cons(2, box Cons(3, box Nil)));
    print_list(&list);

    let xs = Nil;
    let ys = xs.clone();
    //println!("xs: {}", ys);
    print_list(&ys);
    print_list(&xs);

    let a_list = Cons(5, box Cons(10, box Nil));
    let b_list = Cons(5, box Cons(10, box Nil));
    assert!(eq(&a_list, &b_list));
}
