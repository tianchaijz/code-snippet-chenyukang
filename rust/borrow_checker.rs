use std::collections::HashMap;

#[deriving(Show)]
struct Foo<'a> {
    buf: Vec<String>,
    map: HashMap<uint, &'a str>
}

impl<'a> Foo<'a> {
    fn new() -> Foo<'a> {
        Foo { buf: Vec::new(), map: HashMap::new() }
    }
    fn insert(&'a mut self, s: String) {
        self.buf.push(s);
        match self.buf.last() {
            None => panic!(""),
            Some(x) => { self.map.insert(x.len(), x.as_slice()); }
        }
    }
    fn do_bad_stuff(&mut self) {
        self.buf.clear();
        self.buf.push("test".into_string());
    }
}
fn main() {
    let mut foo = Foo::new();
    foo.insert("bad".into_string());
    foo.insert("stuff".into_string());
    println!("{}", foo);
    foo.do_bad_stuff();
    println!("{}", foo);
}
