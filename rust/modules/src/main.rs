fn main() {
    hello::print_hello();
}

mod hello {
    pub fn print_hello() {
        println!("Hello, world!")
    }
}

#[test]
fn is_one_equal_to_one() {
    assert_eq!(1i, 1i);
}
