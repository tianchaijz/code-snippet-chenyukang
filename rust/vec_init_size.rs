use std::iter::repeat;
use std::vec;

fn main() {
    let mut count: Vec<i32> = repeat(0).take(5us).collect();
    for x in count.iter() {
        println!("{}", x);
    }
    count[0] = 1i32;
    for x in count.into_iter() {
        println!("{}", x);
    }

    let mut count: Vec<i32> = Vec::from_fn(10, |_| 0);
    for x in count.into_iter() {
        println!("{}", x);
    }

}
