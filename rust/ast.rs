
#[deriving(Clone, PartialEq)]
pub enum ExprAst {
    Int(IntNode),
    Pair(PairNode)
}

#[deriving(Clone, PartialEq)]
pub struct IntNode {
    pub value: i32
}

#[deriving(Clone, PartialEq)]
pub struct PairNode{
    pub car: Box<ExprAst>,
    pub cdr: Box<ExprAst>
}

pub trait Print {
    fn print(&self);
}

impl Print for ExprAst {
    fn print(&self) {
        match *self {
            ExprAst::Int(ref ast) => ast.print(),
            ExprAst::Pair(ref ast) => ast.print()
        }
    }
}

impl IntNode {
    pub fn new(val: i32) -> IntNode {
        IntNode{ value: val}
    }
}

impl Print for IntNode {
    fn print(&self) {
        println!("IntNode: {}", self.value);
    }
}

impl PairNode {
    pub fn new(_car: Box<ExprAst>, _cdr: Box<ExprAst>) -> PairNode {
        PairNode {
            car: _car,
            cdr: _cdr
        }
    }
}

impl Print for PairNode {
    fn print(&self) {
        println!("PairNode: ");
        self.car.print();
        self.cdr.print();
    }
}


fn main() {
    let node1 = ExprAst::Int(IntNode::new(1));
    let node2 = ExprAst::Int(IntNode::new(2));
    node1.print();

    let pair_node = PairNode::new(box node1, box node2);
    pair_node.print();

}
