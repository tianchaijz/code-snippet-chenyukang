
#[derive(Clone, Copy)]
struct Task {
    val: i32
}

struct Demo{
    key: i32,
    tasks: &'static [Task]
}

fn main() {
    let a = [1is; 20];
    println!("len: {}, {}", a.len(), a[0]);

    let b = [Task{val: 0i32}; 10us];
    println!("len: {}", b.len());

    let demo = Demo {
        key: 10i32,
        tasks: &[Task{val: 0i32}; 10us]
    };
}
