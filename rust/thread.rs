
use std::rand;
use std::rand::Rng;
use std::io::timer;
use std::time::duration::Duration;
use std::rand::distributions::{IndependentSample, Range};

fn main() {
    fn print_message(val :i32) {
        let between = Range::new(1000i, 10000i);
        let mut rng = rand::thread_rng();
        let rand = between.ind_sample(&mut rng);
        println!("I running in a different thread with value : {} {}", val, rand);
        let interval = Duration::milliseconds(rand as i64);
        timer::sleep(interval);
    }

    let val = 1i32;
    for n in range(1u, 10u) {
        let res = std::thread::Thread::spawn(move || print_message(n as i32));
        res.detach();
    }
}
