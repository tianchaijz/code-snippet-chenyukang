use std::f64;

struct Point {
    x: f64,
    y: f64
}

// let mut mypoint = Point {x: 1.0, y: 2.0};
// let mut origion = Point {x: 0.0, y: 0.0};

// mypoint.x += 1.0;
// origion.x += 1.0;

enum Direction {
    South,
    East,
    West
}

enum Shape {
    Circle(Point, f64),
    Rectangle(Point, Point)
}

fn area(sh: Shape) -> f64 {
    match sh {
        Circle(_, size) => f64::consts::PI * size * size,
        Rectangle(Point{x, y}, Point{x: x2, y: y2}) => (x2 - x) * (y2 - y)
    }
}


fn first((value, _): (int, f64)) ->int  {value}

fn main() {
    let mut mypoint = Point {x: 1.0, y: 2.0};
    let mut origion = Point {x: 0.0, y: 0.0};

    mypoint.x += 1.0;
    origion.x += 1.0;
    mypoint.x -= 2.0;

    match mypoint {
        Point  {x:0.0, y: yy} => println!("{}", yy),
        Point  {x:xx,  y: yy} => println!("xx: {}, yy: {}", xx, yy)
    }

    match mypoint {
        Point { x, ..} => println!("now x:{}", x)
    }

    println!("East: {}", East as int);
    println!("South: {}", South as int);
    println!("West: {}", West as int);

    let rect = Rectangle(Point { x: 0.0, y: 0.0 }, Point { x: 2.0, y: 2.0 });
    println!("area: {}", area(rect));

    let circle = Circle(Point{x: 0.0, y: 0.0}, 10.0);
    println!("area: {}", area(circle));

    println!("first {}", first((1, 1.0)))

}
