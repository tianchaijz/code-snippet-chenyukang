
#[deriving(PartialEq, Show)]
struct Foo {
    f: Box<int>,
}

fn max<'a>(x: &'a Foo, y: &'a Foo) -> &'a Foo {
    if x.f > y.f { x } else { y }
}


fn main() {
    let mut a = Foo { f: box 0 };
    let mut b = Foo { f: box 1};
    // mutable borrow
    let x = &mut a;
    let y = &mut b;
    let res = max(a, b);
    println!("res: {}", res);
    // error: cannot borrow `a.f` as immutable because `a` is also borrowed as mutable

}
