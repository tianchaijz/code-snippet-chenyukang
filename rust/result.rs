#[deriving(Show)]
enum Version { Version1, Version2 }

#[deriving(Show)]
enum ParseError { InvalidHeaderLength, InvalidVersion }

fn parse_version(header: &[u8]) -> Result<Version, ParseError> {
    if header.len() < 1 {
        return Err(InvalidHeaderLength);
    }
    match header[0] {
        1 => Ok(Version1),
        2 => Ok(Version2),
        _ => Err(InvalidVersion)
    }
}

fn main() {
    let version = parse_version(&[3, 2, 3, 4]);
    match version {
        Ok(v) => {
            println!("working with version: {}", v);
        }
        Err(e) => {
            println!("error parsing header: {}", e);
        }
    }
}
