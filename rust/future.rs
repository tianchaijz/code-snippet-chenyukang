use std::sync::Future;

use std::io::Timer;
use std::time::Duration;


fn partial_sum(start: uint) -> f64 {
    let mut local_sum = 0f64;
    for num in range(start*100000, (start+1)*100000) {
        local_sum += (num as f64 + 1.0).powf(-2.0);
    }
    let mut timer = Timer::new().unwrap();
    timer.sleep(Duration::milliseconds(10000)); // block the task for awhile
    local_sum
}

fn main() {
    let mut futures = Vec::from_fn(500, |ind| Future::spawn( proc() { partial_sum(ind) }));

    let mut final_res = 0f64;
    for ft in futures.iter_mut()  {
        final_res += ft.get();
    }
    println!("π^2/6 is not far from : {}", final_res);
}
