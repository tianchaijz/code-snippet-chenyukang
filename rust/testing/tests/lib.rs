
extern crate testing;
use testing::add_three_times_four;

#[test]
fn foo() {
    assert_eq!(16i, add_three_times_four(1i));
}
