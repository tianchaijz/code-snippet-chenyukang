
fn next_two(x: int) -> (int, int) { return (x + 1, x + 2) }

fn main() {
    let (x, y) = next_two(5);
    println!("x, y = {}, {}", x , y);

    let x: Box<int> = box 10;
    let y = x;
    let x: Box<int> = box 1;
    let z = x;
    println!("y: {}", *y);
    println!("z: {}", *z);

}
