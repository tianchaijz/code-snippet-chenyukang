use std::num::Float;
use std::rand;
use std::sync::Arc;

fn pnorm(nums: &[f64], p: uint) -> f64 {
    nums.iter().fold(0.0, |a, b| a + b.powf(p as f64)).powf(1.0 / (p as f64))
}

fn main() {
    let numbers = Vec::from_fn(1000000, |_| rand::random::<f64>());
    let numbers_arc = Arc::new(numbers);

    for num in range(1u, 1000) {
        let task_numbers = numbers_arc.clone();

        spawn(proc() {
            println!("{}-norm = {}", num, pnorm(task_numbers.as_slice(), num));
        });
    }
}
