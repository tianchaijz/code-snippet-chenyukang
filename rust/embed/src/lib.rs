use std::thread;

/// Constructs a new `Rc<T>`.
///
/// # Examples
///
/// ```
/// use std::rc::Rc;
///
/// let five = Rc::new(5);
/// ```
pub fn new(value: i32) -> i32 {
    return 0;
    // implementation goes here
}


#[no_mangle]
pub extern fn process() {
    let handles: Vec<_> = (0..1000000).map(|_| {
        thread::spawn(|| {
            let mut _x = 0;
            for _ in (0..5_000_000) {
                _x += 1
            }
            //println!("done hah!");
        })
    }).collect();

    for h in handles {
        h.join().ok().expect("Could not join a thread!");
    }
}


// fn main() {
//     process();
// }
