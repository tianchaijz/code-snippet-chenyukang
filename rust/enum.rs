
#[deriving(PartialEq)]
enum Ordering {
    Less,
    Equal,
    Greater,
}

fn cmp(a: int, b: int) -> Ordering {
    if a < b { return Less }
    if a == b { return Equal }
    Greater
}

fn test() {
    let x = 11;
    let y = 10;

    let result = match cmp(x, y) {
        Less => "less",
        Greater => "greater",
        Equal => "equal",
    };
    println!("{}", result);
}

fn main() {
    let x = 11;
    let y = 10;

    let ordering = cmp(x, y);
    println!("ordering: {}", ordering as int);

    if ordering == Less {
        println!("less");
    } else if ordering == Equal {
        println!("equal");
    } else if ordering == Greater {
        println!("greater");
    }
    test();
}
