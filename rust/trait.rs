
struct Circle {
    x: f64,
    y: f64,
    radius: f64,
}

trait HasArea {
    fn area(&self) -> f64;
    fn with_x(&self) -> f64;
    fn with_y(&self) -> f64;
}

impl HasArea for Circle {
    fn area(&self) -> f64 {
        std::f64::consts::PI * (self.radius * self.radius)
    }

    fn with_x(&self) -> f64 {
        self.x
    }

    fn with_y(&self) -> f64 {
        self.y
    }
}


impl HasArea for int {
    fn area(&self) -> f64 {
        println!("this is silly");
        *self as f64
    }

    fn with_x(&self) -> f64 {
        *self as f64
    }

    fn with_y(&self) -> f64 {
        *self as f64
    }
}



fn print_area<T: HasArea>(shape: T) {
    println!("This shape has an area of {}", shape.area());
    println!("X: {}", shape.with_x());
    println!("Y: {}", shape.with_y());
}

fn main() {
    let circle = Circle{
        x: 1.0,
        y: 1.0,
        radius: 1.0
    };

    print_area(circle);
    print_area(5i);
}
