

fn test_collect() {
    let greater_than_forty_two = range(0i, 100i).filter(|x| x%2 == 0).collect::<Vec<int>>();
    for num in greater_than_forty_two.iter() {
        println!("{}", num);
    }
}

fn main() {
    let greater_than_forty_two = range(0i, 100i).find(|x| *x >= 42);

    match greater_than_forty_two {
        Some(_) => {
            println!("We got some numbers!");
            for num in greater_than_forty_two.iter() {
                println!("{}", num);
            }
        },
        None    => println!("No numbers found :("),
    }
    test_collect();
}
