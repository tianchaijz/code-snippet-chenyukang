


#[deriving(PartialEq)]
enum List<T> {
    Cons(T, Box<List<T>>),
    Nil
}


fn prepend<T>(xs: List<T>, value: T) -> List<T> {
    Cons(value, box xs)
}

// fn print<T: PartialShow> (list: &List<T>) {
//     match list {
//         &Nil => println!("end"),
//         &Cons(ref v, box ref next) => { println!("v: {}", v); print::<T>(next); }
//     }
// }


impl<T: PartialEq> PartialEq for List<T> {
    fn eq(&self, ys: &List<T>) -> bool {
        // Match on the next node in both lists.
        match (self, ys) {
            // If we have reached the end of both lists, they are equal.
            (&Nil, &Nil) => true,
            // If the current elements of both lists are equal, keep going.
            (&Cons(ref x, box ref next_xs), &Cons(ref y, box ref next_ys))
                if x == y => next_xs == next_ys,
            // If the current elements are not equal, the lists are not equal.
            _ => false
        }
    }
}

fn main() {
    let mut xs: List<int> = Nil::<int>;
    xs = prepend::<int>(xs, 1);
    xs = prepend::<int>(xs, 2);

    let xs = Cons(5i, box Cons(10i, box Nil));
    let ys = Cons(5i, box Cons(10i, box Nil));
    // The methods below are part of the PartialEq trait,
    // which we implemented on our linked list.
    assert!(xs.eq(&ys));
    assert!(!xs.ne(&ys));

    // The PartialEq trait also allows us to use the shorthand infix operators.
    assert!(xs == ys);    // `xs == ys` is short for `xs.eq(&ys)`
    assert!(!(xs != ys)); // `xs != ys` is short for `xs.ne(&ys)`

    //print::<int>(xs);
}
