use std::collections::HashMap;
use std::hash::{hash, Hash, SipHasher};

#[derive(Eq, PartialEq, Hash, Show, Clone)]
struct Person {
    id: usize,
    name: String,
    phone: u64
}

#[derive(Clone, Eq, PartialEq, Hash, Show)]
pub enum Expr {
    Int(IntNode),
    Str(StrNode),
    Nil
}

#[derive(Clone, Eq, PartialEq, Hash, Show)]
pub struct IntNode {
    value: i32
}

impl IntNode {
    pub fn new(val: i32) -> IntNode { IntNode{ value: val}}
}

#[derive(Clone, Eq, PartialEq, Hash, Show)]
pub struct StrNode {
    value: String
}

impl StrNode {
    pub fn new(val: String) -> StrNode { StrNode{ value: val}}
}

fn test_expr() {
    let exp1 = Expr::Int(IntNode::new(1i32));
    let exp2 = Expr::Str(StrNode::new("hello".to_string()));
    println!("{:?}", exp1);
    println!("{:?}", exp2);

    let mut hash = HashMap::new();
    let key = "hello".to_string();
    hash.insert(&key, &exp2);
    match hash.get(&key) {
        Some(&value) => println!("get: {:?}", value),
        _ => println!("none")
    }
}

fn main() {
    test_expr();

    let person1 = Person{id: 5, name: "yukang".to_string(), phone: 12223};
    let person2 = Person{id: 6, name: "chen".to_string(), phone: 332};
    println!("{:?}", person1);
    println!("{:?}", person2);
    println!("{:?}", hash::<_, SipHasher>(&person1));
    println!("{:?}", hash::<_, SipHasher>(&person2));

    println!("integer_hash: {:?}", hash::<_, SipHasher>(&("hello".to_string())));
    println!("integer_hash: {:?}", hash::<_, SipHasher>(&("world".to_string())));


    let mut contacts = HashMap::new();
    // contacts.insert(1is, person1);
    // contacts.insert(2is, person2);
    // match contacts.get(&1is) {
    //     Some(ref person) => println!("get: {:?}", person),
    //     _ => println!("none")
    // }
    // match contacts.get(&is) {
    //     Some(ref person) => println!("get: {:?}", person),
    //     _ => println!("none")
    // }

    contacts.insert(&person1, 1is);
    contacts.insert(&person2, 2is);

    match contacts.get(&person1) {
        Some(&value) => println!("get: {:?}", value),
        _ => println!("none")
    }

    let mut contacts2 = contacts.clone();
    match contacts.get(&person2) {
        Some(&value) => println!("get: {:?}", value),
        _ => println!("none")
    }

}
