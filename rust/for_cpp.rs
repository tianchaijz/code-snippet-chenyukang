

fn foo(x: int) -> &'static str {
    if x < 10 {
        "less than 10"
    } else {
        "10 or more"
    }
}

fn print_all(all: Vec<int>) {
    for a in all.iter() {
        println!("now: {}", a);
    }
}

fn print_all_with_index(all: Vec<int>) {
    for i in range(0, all.len()) {
        println!("now: {} {}", i, all.get(i).unwrap());
    }
}

fn main() {
    println!("res: {}", foo(10));
    print_all(vec![1, 2, 3]);
    print_all_with_index(vec![1, 2, 3]);
}
