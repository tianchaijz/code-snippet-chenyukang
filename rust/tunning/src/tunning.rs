#![allow(unstable)]
#[allow(unused_imports)]

extern crate test;
use std::collections::HashMap;
use test::Bencher;


fn main() {
}


#[bench]
fn test_stack_alloc(b: &mut Bencher) {
    fn test() {
        let b = 5is;
    }
    b.iter(|| test());
}

#[bench]
fn test_heap_alloc(b: &mut Bencher) {
    fn test() {
        let b = Box::new(5is);
    }
    b.iter(|| test());
}


#[bench]
fn test_hash(b: &mut Bencher) {
    fn test() {
        let mut hash = HashMap::new();
        for i in 1..1000 {
            let key = i.to_string();
            hash.insert(key.clone(), i.to_string());
            let val = hash.get(&key).unwrap();
            assert!(*val == i.to_string());
        }
    }
    b.iter(|| test());
}

fn search(vec: &Vec<String>, key: String) -> String {
    for i in range(0us, vec.len()) {
        if vec[i] == key {
            return vec[i].clone();
        }
    }
    return "".to_string();
}

#[bench]
fn test_vec(b: &mut Bencher) {
    fn test() {
        let mut vec = vec![];
        for i in 1..1000 {
            let key = i.to_string();
            vec.push(key);
            let val = search(&vec, i.to_string());
            assert!(val == i.to_string());
        }
    }
    b.iter(|| test());
}
