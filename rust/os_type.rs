
#[cfg(target_os = "linux")]
fn print_os() {
    println!("you are on Linux");
}

#[cfg(not(target_os = "linux"))]
fn print_os() {
    println!("you are not on Linux");
}

fn main() {
    print_os();
}
