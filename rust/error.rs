fn main() {
    let x = 1.0f32;
    let y = 0.0f32;
    let result = x / y;
    println!("x: {}, y: {}, result: {}\n", x, y, result);
}
