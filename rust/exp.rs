use std::collections::HashMap;
use std::cell::RefCell;
use std::rc::Rc;
use std::fmt;

#[derive(Clone, PartialEq)]
pub struct Env {
    pub table: HashMap<String, Expr>,
    pub parent: Option<Rc<RefCell<Env>>>
}

impl fmt::Show for Env {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "env")
    }
}


#[derive(Clone, PartialEq, Show)]
pub enum Expr {
    Int(isize),
    Str(String),
    Sym(String),
    Bool(bool),
    Char(char),
    Pair(Vec<Expr>),
    Proc(ProcFunc),
    CompProc(Vec<Expr>, Rc<RefCell<Env>>),
    Nil
}

#[derive(Clone)]
pub struct ProcFunc(fn(Expr) -> Expr);

impl PartialEq for ProcFunc {
    fn eq(&self, o: &ProcFunc) -> bool {
        let _o: *const() = unsafe { ::std::mem::transmute(o)};
        let _s: *const() = unsafe { ::std::mem::transmute(self)};
        _s == _o
    }
    fn ne(&self, o: &ProcFunc) -> bool {
        !self.eq(o)
    }
}

impl ProcFunc {
    pub fn func(&self) -> (fn(Expr) -> Expr) {
        match *self {
            ProcFunc(fun) => fun
        }
    }
}

impl fmt::Show for ProcFunc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "proc")
    }
}

impl Expr {
    pub fn new_pair(car: Expr, cdr: Expr) -> Expr {
        Expr::Pair(vec![car, cdr])
    }

    pub fn new_str(val: &str) -> Expr {
        Expr::Str(val.to_string())
    }

    pub fn new_sym(val: &str) -> Expr {
        Expr::Sym(val.to_string())
    }

    pub fn new_proc(func: fn(Expr) -> Expr) -> Expr {
        Expr::Proc(ProcFunc(func))
    }

    pub fn as_int(&self) -> isize {
        match *self {
            Expr::Int(ref val) => return *val,
            _ => panic!("expect Int")
        }
    }

    pub fn as_bool(&self) -> bool {
        match *self {
            Expr::Bool(ref val) => return *val,
            _ => panic!("expect Bool")
        }
    }

    pub fn as_str(&self) -> String {
        match *self {
            Expr::Str(ref val) => return val.clone(),
            _ => panic!("expect Str")
        }
    }

    pub fn car(&self) -> Expr {
        match *self {
            Expr::Pair(ref vec) => { return vec[0].clone(); },
            _ => panic!("expect Pair")
        }
    }

    pub fn cdr(&self) -> Expr {
        match *self {
            Expr::Pair(ref vec) => { return vec[1].clone(); },
            _ => panic!("expect Pair")
        }
    }

    pub fn is_cproc(&self) -> bool {
        match *self {
            Expr::CompProc(_, _) => true,
            _ => false
        }
    }

    pub fn is_tagged(&self, tag: Expr) -> bool {
        if self.is_pair() {
            let car = self.car();
            return car.is_sym() && car == tag;
        }
        false
    }
}


macro_rules! is_ast_type {
    ($func_name:ident, $type_name:ident) => (impl Expr {
        pub fn $func_name(&self) -> bool {
            match *self {
                Expr::$type_name(_) => true,
                _ => false
            }
        }})
}

is_ast_type!(is_char, Char);
is_ast_type!(is_int, Int);
is_ast_type!(is_sym, Sym);
is_ast_type!(is_str, Str);
is_ast_type!(is_proc, Proc);
is_ast_type!(is_bool, Bool);
is_ast_type!(is_pair, Pair);

macro_rules! is_type {
    ($func_name:ident, $type_str:expr) => (impl Expr {
        pub fn $func_name(&self) -> bool {
            return self.is_tagged(Expr::Sym($type_str.to_string()))
        }
    })
}

is_type!(is_quote, "quote");
is_type!(is_def, "define");
is_type!(is_and, "and");
is_type!(is_or, "or");
is_type!(is_if, "if");
is_type!(is_assign, "set!");
is_type!(is_lambda, "lambda");
is_type!(is_cond, "cond");
is_type!(is_let, "let");
is_type!(is_begin, "begin");


fn main() {
    let a = Expr::Int(10is);
    println!("a: {:?}", a);
    assert!(a.as_int() == 10is);

    let b = Expr::Str("hello".to_string());
    println!("b: {:?}", b);
    assert!(b.as_str() == "hello".to_string());

    let b_str = Expr::new_str("hello");
    assert!(b == b_str);

    let b_sym = Expr::new_sym("hello");
    assert!(b_str != b_sym);

    let c = Expr::Char('a');
    println!("char: {:?}", c);

    let c = Expr::new_pair(a, b);
    println!("c: {:?}", c);

    let car = c.car();
    let cdr = c.cdr();
    println!("car: {:?}", car);
    println!("cdr: {:?}", cdr);

    fn _proc(obj: Expr) -> Expr {
        println!("obj: {:?}", obj);
        Expr::new_str("OK")
    }

    let proc_node = Expr::new_proc(_proc);
    println!("proc_node: {:?}", proc_node);
    assert!(proc_node.is_proc());
}
