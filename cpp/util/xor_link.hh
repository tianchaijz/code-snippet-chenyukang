#ifndef __DOUBLELINKEDXOR_HEADER__
#define __DOUBLELINKEDXOR_HEADER__

template <class T>
class DoubleLinkedXOR 
{
public:
    DoubleLinkedXOR() { m_head = m_tail = 0;}
    ~DoubleLinkedXOR(){}

    void AddToTail( T entry )
        {
            if( !m_head )//no items
            {
                entry->m_NextPrev = 0;
                m_head = entry;
            }
            else if( !m_tail ) //just head is set, one item in list
            {
                m_tail = entry;
                m_tail->m_NextPrev = m_head;
                m_head->m_NextPrev = m_tail;
            }
            else//two items or more, insert
            {
                entry->m_NextPrev = m_tail;
                m_tail->m_NextPrev = (T)((size_t)(m_tail->m_NextPrev) ^ (size_t)entry);
                m_tail = entry;
            }
        }
    T RemoveFromHead()
        {
            T item = m_head;
            if( !m_head )
            {
                return 0;
            }
            else if( !m_tail )//just one item
            {
                m_head = 0;
                return item;
            }
            else if( m_head->m_NextPrev == m_tail
                     && m_tail->m_NextPrev == m_head ) // only two items
            {
                m_head = m_tail;
                m_tail = 0;
                m_head->m_NextPrev = 0;
                return item;
            }
            else
            {
                m_head = m_head->m_NextPrev;
                m_head->m_NextPrev = (T)((size_t)(m_head->m_NextPrev) ^(size_t) item);
                return item;
            }
        }
private:
    T m_tail;
    T m_head;
};
#endif
