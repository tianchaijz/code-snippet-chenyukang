
/*******************************************************************************
 *
 *      print_stack.cc
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-01-04 17:37:12
 *
 *      COPYRIGHT (C) 2006~2012, Nextop INC., ALL RIGHTS RESERVED.
 *
 *******************************************************************************/

#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <assert.h>

void DumpBacktrace(int) {
    pid_t dying_pid = getpid();
    pid_t child_pid = fork();
    if (child_pid < 0) {
        perror("fork() while collecting backtrace:");
    } else if (child_pid == 0) {
        char buf[1024];
        sprintf(buf, "gdb -p %d -batch -ex bt 2>/dev/null | "
                "sed '0,/<signal handler/d'", dying_pid);
        const char* argv[] = {"sh", "-c", buf, NULL};
        execve("/bin/sh", (char**)argv, NULL);
        _exit(1);
    } else {
        waitpid(child_pid, NULL, 0);
    }
    _exit(1);
}

void BacktraceOnSegv() {
    struct sigaction action = {};
    action.sa_handler = DumpBacktrace;
    if (sigaction(SIGSEGV, &action, NULL) < 0) {
        perror("sigaction(SEGV)");
    }
    if (sigaction(SIGABRT, &action, NULL) < 0) {
        perror("sigaction(SEGV)");
    }
}



void test() {
    //assert(0);
    int* p = 0;
    *p = 0;
}

int main() {
    BacktraceOnSegv();
    //assert(0);
    test();
}
