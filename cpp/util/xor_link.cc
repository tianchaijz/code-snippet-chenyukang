#include <stdio.h>
#include <stdlib.h>

#include "item.h"
#include "xor_link.hh"

int main( int argc, char *argv[] )
{
    DoubleLinkedXOR<Item*> *list = new DoubleLinkedXOR<Item*> ();
    int x, count = 10;
    for( x = 0; x < count; x++ ) //add all items to list
    {
        Item *newItem = (Item*)malloc(sizeof(Item));
        newItem->id = x;
        list->AddToTail(newItem);
        printf("Added item: %d\n\r", newItem->id);
    }

    for( x = 0; x < count; x++ )
    {
        Item *retrievedItem = list->RemoveFromHead();
        printf("Retrieved item: %d\n\r", retrievedItem->id);
        free(retrievedItem);
    }

    delete list;

    getchar();
    return 0;
}
