//a macro to test big endian end little endian

#include <iostream>
#include <assert.h>

bool is_big() {
    const int a = 1;
    if((*(char*)(&a)) == 0)
        return true;
    return false;
}

bool is_little() {
    const int a = 1;
    if((*(char*)(&a)) == 1)
        return true;
    return false;
}
        

int main() {
    if(is_big()) {
        std::cout<<"Big endian"<<std::endl;
    }
    else {
        assert(is_little());
        std::cout<<"Little endian"<<std::endl;
    }
    return 0;
}
