#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

class Node{
public:
    int val;
    int index;
    Node() {}
    Node(int v, int idx) : val(v), index(idx) {}
};

bool cmp(const Node& l, const Node& r) {
    return l.val < r.val;
}

vector<int> twoSum(vector<int> &numbers, int target) {
    // Start typing your C/C++ solution below
    // DO NOT write int main() function
    vector<Node> vec;
    int k, v;
    for(size_t k=0; k<numbers.size(); k++) {
        vec.push_back(Node(numbers[k], k+1));
    }
    
    sort(vec.begin(), vec.end(), cmp);
        
    int i, j;
    i=0, j = numbers.size() -1;
    while(i<j) {
        int sum = vec[i].val + vec[j].val;
        if(sum == target) {
            vector<int> res ;
            int min_idx = min(vec[i].index, vec[j].index);
            int max_idx = max(vec[i].index, vec[j].index);
            res.push_back(min_idx);
            res.push_back(max_idx);
            return res;
        }
        if(sum > target) {
            j--;
        } else {
            i++;
        }
    }
}

int main() {
    vector<int> vec;
    int target;
    int nums[100] = {2,1,9,4,4,56,90,3};
    vec.push_back(5);
    vec.push_back(75);
    vec.push_back(25);
    twoSum(vec, 100);
}
