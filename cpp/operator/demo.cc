
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

using namespace std;

class Demo {
public:
    Demo() { printf("in contructor\n"); }
    ~Demo() { printf("in destructor\n"); }

    static void* operator new(size_t size) { printf("in new\n"); return  ::malloc(size); }
    static void  operator delete(void* p)  { printf("in delete\n"); ::free(p); }
};

int main() {
    Demo* p = new Demo();
    delete p;
}
