#include <iostream>
#include <vector>
#include <assert.h>
using namespace std;

#define STACK_SIZE 100

class Stacks {
private:
    int Num;
    vector<int> elems;
    vector<int> tops;

public:
    Stacks(int num): Num(num) {
        elems.resize(STACK_SIZE * Num);
        tops.resize(Num);
        for(int i=0; i<Num; i++) {
            tops[i] = 0;
        }
    }

    void push(int idx, int value);
    void pop(int idx);
    int  top(int idx);
    bool isEmpty(int idx);
};


void Stacks::push(int idx, int value) {
    int index = tops[idx] + STACK_SIZE;
    tops[idx]++;
    elems[index] = value;
}

void Stacks::pop(int idx) {
    tops[idx]--;
}

int Stacks::top(int idx) {
    int index = tops[idx] + STACK_SIZE - 1;
    return elems[index];
}

bool Stacks::isEmpty(int idx) {
    return tops[idx] == 0;
}

int main() {
    Stacks s(3);
    assert(s.isEmpty(0));
    assert(s.isEmpty(1));
    assert(s.isEmpty(2));
    s.push(0, 1);
    assert(s.top(0) == 1);
    assert(s.isEmpty(0) == false);
    s.pop(0);
    assert(s.isEmpty(0) == true);
    s.push(2, 1);
    assert(s.top(2) == 1);
    assert(s.isEmpty(2) == false);
    s.pop(2);
    assert(s.isEmpty(2) == true);
    return 0;
}
