/* The problem of Towers of Hanoi */
#include <stack>
#include <vector>
#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string.h>
using namespace std;


class Towers {
private:
    vector<stack<int> > tows;
    int getExtra(int from, int to) {
        assert(from != to);
        bool used[3];
        memset(used, false, sizeof(used));
        used[from] = true;
        used[to] = true;
        for(int i=0; i<3; i++)
            if(used[i] == false)
                return i;
    }

    void printElems(int idx) {
        vector<int> tmp;
        while(!tows[idx].empty()) {
            tmp.push_back(tows[idx].top());
            tows[idx].pop();
        }
        for(int i=tmp.size() - 1;  i>=0; i--) {
            tows[idx].push(tmp[i]);
        }
        printf("tow%d: size: %lu:\n", idx, tmp.size());
        for(int i=tmp.size() - 1; i>=0; i--) {
            printf(" %d ", tmp[i]);
        }
        printf("\n");
    }

public:
    Towers() {
        tows.resize(3);
    }

    void print() {
        stack<int> tmp;
        for(int i=0; i<tows.size(); i++) {
            printElems(i);
        }
    }

    void push(int idx, int value) {
        tows[idx].push(value);
    }

    int top(int idx) {
        return tows[idx].top();
    }

    void pop(int idx) {
        tows[idx].pop();
    }

    void move(int from, int to, int num) {
        if(tows[from].empty()) return;
        assert(tows[from].size() >= num);
        if(num == 1) {
            int top = tows[from].top();
            tows[from].pop();
            printf("move %d from %d to %d\n", top, from, to);
            tows[to].push(top);
        } else {
            int extras = getExtra(from, to);
            move(from, extras, num - 1);
            move(from, to, 1);
            move(extras, to, num - 1);
        }
    }
};

int main() {
    Towers ts;
    int num = 6;
    for(int i=num; i>=1; i--) {
        ts.push(0, i);
    }

    ts.print();
    ts.move(0, 2, num);
    ts.print();
    return 0;
}
