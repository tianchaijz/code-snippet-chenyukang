/* sort a stack in asending order */
#include <stack>
#include <vector>
#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

/* time complexity O(N^2) */
stack<int> sortStack(stack<int>& st) {
    stack<int> res;
    while(!st.empty()) {
        int tmp = st.top();
        st.pop();
        while(!res.empty() && res.top() > tmp) {
            int v = res.top();
            res.pop();
            st.push(v);
        }
        res.push(tmp);
    }
    return res;
}

int main() {
    stack<int> st;
    srand(time(NULL));
    for(int i=0; i<10000; i++) {
        st.push(rand() % 2000);
    }

    stack<int> res = sortStack(st);
    std::cout << "after sort: \n";
    int prev = res.top();
    res.pop();
    while(!res.empty()) {
        int t = res.top();
        assert(prev >= t);
        prev = t;
        res.pop();
        //std::cout << t << std::endl;
    }
    return 0;
}
