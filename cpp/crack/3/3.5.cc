/* Implement Queue using two stacks */
#include <queue>
#include <stack>
#include <vector>
#include <iostream>
#include <assert.h>
using namespace std;


class MyQueue {
private:
    stack<int> in;
    stack<int> ou;

    void transfer() {
        while(ou.empty()) {
            while(!in.empty()) {
                int t = in.top();
                ou.push(t);
                in.pop();
            }
        }
    }

public:
    MyQueue() { };

    void push_back(int value) {
        in.push(value);
    }

    void pop_front() {
        transfer();
        assert(!ou.empty());
        ou.pop();
    }

    int front() {
        transfer();
        assert(!ou.empty());
        return ou.top();
    }

    bool empty() {
        return ou.empty() && in.empty();
    }

    int size() {
        return ou.size() + in.size();
    }
};

int main() {
    MyQueue Q;
    for(int i=0; i<10; i++) {
        Q.push_back(i);
    }
    std::cout << "size: " << Q.size() << std::endl;
    while(!Q.empty()) {
        int t = Q.front();
        std::cout << t << std::endl;
        Q.pop_front();
    }
    return 0;
}
