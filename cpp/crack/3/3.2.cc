#include <iostream>
#include <assert.h>
#include <stack>
using namespace std;


class Node {
public:
    int value;
    int count;
    Node(int v, int c) :value(v), count(c) {}
};


class MyStack {
private:
    stack<int> St;
    stack<Node> Mins;

public:
    MyStack() {}

    void push(int value);
    void pop();
    int  top();
    int  min();
    bool empty();
};

void MyStack::push(int value) {
    if(Mins.empty()) {
        Mins.push(Node(value, 1));
    } else {
        Node t  = Mins.top();
        if(t.value == value) {
            Mins.pop();
            Mins.push(Node(value, ++t.count));
        }
        else if(t.value > value) {
            Mins.push(Node(value, 1));
        }
    }
    St.push(value);
}

void MyStack::pop() {
    if(St.empty()) return;
    int value = St.top();
    St.pop();
    Node t = Mins.top();
    if(t.value == value) {
        if(t.count == 1) {
            Mins.pop();
        }
        else if(t.count > 1) {
            Mins.pop();
            Mins.push(Node(t.value, --t.count));
        }
    }
}

int MyStack::min() {
    Node t = Mins.top();
    return t.value;
}

int MyStack::top() {
    return St.top();
}

bool MyStack::empty() {
    return St.empty();
}


int main() {
    MyStack s;
    s.push(1);
    s.push(2);
    s.push(3);
    s.push(0);
    std::cout << s.min() << std::endl;
    std::cout << s.top() << std::endl;
    s.pop();
    std::cout << s.top() << std::endl;
    std::cout << s.min() << std::endl;
    s.pop();
    s.pop();
    std::cout << s.min() << std::endl;
    for(int i=1; i<100; i++) {
        s.push(i);
        assert(s.min() == 1);
    }
    for(int i=1; i<100; i++) {
        s.pop();
        assert(s.min() == 1);
    }
    s.push(1);
    s.pop();
    assert(s.min() == 1);
    return 0;
}
