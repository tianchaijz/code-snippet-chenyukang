#include <iostream>
#include <vector>
#include <assert.h>
using namespace std;

#define STACK_SIZE 100

struct Node {
    int value;
    int prev;
};

class Stacks {
private:
    int Num;  //how many stacks
    int Next;
    vector<int> tops;
    vector<Node> elems;

public:
    Stacks(int num) : Num(num), Next(0) {
        tops.resize(num, -1);
        elems.resize(STACK_SIZE * Num);
        for(int i = 0; i< STACK_SIZE * Num - 1; i++) {
            elems[i].prev = i + 1;
        }
    }

    int top(int idx);
    void pop(int idx);
    void push(int idx, int value);
    bool isEmpty(int idx);
};


int Stacks::top(int idx) {
    assert(tops[idx] != -1);
    int index = tops[idx];
    return elems[index].value;
}

void Stacks::push(int idx, int value) {
    int next_id = Next;
    Next = elems[Next].prev;
    elems[next_id].prev = tops[idx];
    elems[next_id].value = value;
    tops[idx] = next_id;
}

void Stacks::pop(int idx) {
    int next_id = tops[idx];
    tops[idx] = elems[next_id].prev;
    elems[next_id].prev = Next;
    Next = next_id;
}

bool Stacks::isEmpty(int idx) {
    return tops[idx] == -1;
}

int main() {
    Stacks s(3);
    for(int i=0; i<100; i++) {
        s.push(0, i);
    }
    while(!s.isEmpty(0)) {
        //std::cout << s.top(0) << std::endl;
        s.pop(0);
    }
    assert(s.isEmpty(0));

    while(!s.isEmpty(1)) {
        std::cout << s.top(1) << std::endl;
        s.pop(1);
    }
    assert(s.isEmpty(1));

    s.push(0, 1);
    s.push(0, 2);
    s.push(1, 3);
    s.pop(0);

    std::cout << s.top(0) << std::endl;
    std::cout << s.top(1) << std::endl;

    return 0;
}
