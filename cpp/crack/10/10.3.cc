#include <iostream>
#include <stdio.h>
#include <cmath>
#include <cassert>
using namespace std;

#define epsilon  0.00001

class Line {
public:
    float slope;
    float ydist;

    Line(float s, float y) : slope(s), ydist(y) {}
};


bool intersect(Line& l1, Line& l2) {
    if((abs(l1.slope - l2.slope) > epsilon) ||
       (abs(l1.ydist - l1.ydist) < epsilon))
            return true;
    return false;
}

int main() {
    Line L1(10, 3.0);
    Line L2(32, 23.1);
    if(intersect(L1, L2)) {
        std::cout << "Yes" << std::endl;
    } else {
        std::cout << "No" << std::endl;
    }

    return 0;
}
