#include <iostream>
#include <string>
#include <vector>
using namespace std;


int _find(const vector<string>& vec, string value, int low, int high) {
    while(low <= high) {
        while(low <= high && vec[high] == "")
            high--;
        if(low > high) return -1;
        int mid = (low + high)/2;
        while(vec[mid] == "")
            mid++;
        if(vec[mid] == value) return mid;
        else if(vec[mid] < value)
            low = mid + 1;
        else high = high -1;
    }
    return -1;
}

int findPos(const vector<string> vec, string value) {
    return _find(vec, value, 0, vec.size() - 1);
}


int main() {
    vector<string> vec;
    vec.resize(13);
    vec[0] = "at";
    vec[4] = "ball";
    vec[7] = "car";
    vec[10] = "dad";

    int pos = findPos(vec, "ball");
    printf("pos: %d\n", pos);
    return 0;
}
