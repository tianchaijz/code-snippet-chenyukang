#include <iostream>
#include <vector>
using namespace std;

class Person {
public:
    int height;
    int weight;
    Person(int h, int w) : height(h), weight(w) {}
};

bool cmp(const Person& pa, const Person& pb) {
    if(pa.height != pb.height) {
        return pa.height < pb.height;
    }
    return pa.weight < pb.weight;
}

void Print(const vector<Person>& ps) {
    printf("size: %lu\n", (unsigned long)ps.size());
    for(int i=0; i<ps.size(); i++) {
        printf("(%d %d)\n", ps[i].height, ps[i].weight);
    }
}

vector<Person> findSol(vector<Person>& ps) {
    vector<int> cur;
    vector<int> idx;
    sort(ps.begin(), ps.end(), cmp);
    cur.push_back(ps[0].weight);
    idx.push_back(0);
    int i = 0;
    for(int k = 1; k<ps.size(); k++) {
        if(ps[k].weight >= cur[i]) {
            cur[++i] = ps[k].weight;
            idx.push_back(k);
        }
        else {
            int m;
            for(m=i; m>=0 && cur[m] > ps[k].weight ; m--)
                ;
            cur[m+1] = ps[k].weight;
            idx[m+1] = k;
        }
    }
    vector<Person> res;
    for(int k=0; k<=i; k++) {
        res.push_back(ps[idx[k]]);
    }
    return res;
}


int main() {
    vector<Person> ps;
    vector<Person> res;
#if 0
    ps.push_back(Person(65, 100));
    ps.push_back(Person(70, 150));
    ps.push_back(Person(56, 90));
    ps.push_back(Person(75, 190));
    ps.push_back(Person(60, 95));
    ps.push_back(Person(68, 110));
    sort(ps.begin(), ps.end(), cmp);
    Print(ps);
    vector<Person> res = findSol(ps);
    Print(res);
#endif

    ps.clear();
    res.clear();
    ps.push_back(Person(60, 100));
    ps.push_back(Person(70, 150));
    ps.push_back(Person(1, 160));
    ps.push_back(Person(3, 29));
    ps.push_back(Person(80, 90));
    Print(ps);
    res = findSol(ps);
    Print(res);
    return 0;
}
