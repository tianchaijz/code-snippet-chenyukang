#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <vector>
#include <time.h>
#include <cassert>
using namespace std;


void mergeVec(vector<int>& veca, vector<int>& vecb) {
    int i = veca.size() - 1;
    int j = vecb.size() - 1;
    int k;
    if(vecb.size() >= 1) {
        size_t newsize = veca.size() + vecb.size();
        veca.resize(newsize);
    }
    k = veca.size() - 1;
    while(i >= 0 && j >= 0) {
        if(veca[i] <= vecb[j])
            veca[k--] = vecb[j--];
        else
            veca[k--] = veca[i--];
    }
    while(j >= 0){
        veca[k--] = vecb[j--];
    }
}

void printVec(vector<int>& vec)  {
#if 0
    printf("size: %lu -> ", (unsigned long)vec.size());
    for(int i=0; i<vec.size(); i++) {
        printf(" %d ", vec[i]);
    }
    printf("\n");
#endif
    for(int i=0; i<vec.size() - 1; i++) {
        if(vec[i] > vec[i+1]) {
            printf("error: %d %d \n", vec[i], vec[i+1]);
            assert(0);
        }
    }
}

int main() {
    vector<int> veca;
    vector<int> vecb;

    srand(time(NULL));
    for(int k =0; k<100000; k++) {
        veca.clear();
        vecb.clear();
        int sizea  = rand() % 1000 + 1;
        int sizeb = rand() % 1000 + 1;
        for(int i=0; i<sizea; i++)
            veca.push_back(rand() % 100);

        for(int i=0; i<sizeb; i++)
            vecb.push_back(rand() % 100);

        sort(veca.begin(), veca.end());
        sort(vecb.begin(), vecb.end());

        printVec(veca);
        printVec(vecb);
        mergeVec(veca, vecb);
        printVec(veca);
    }

    return 0;
}
