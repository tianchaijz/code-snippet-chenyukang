#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

typedef vector<vector<int> > Matrix;


Matrix getMatrix(int m, int n) {
    Matrix res;
    vector<int> row;
    int num = 0;
    for(int i=0; i<m; i++) {
        res.push_back(row);
        for(int j=0; j<n; j++) {
            res[i].push_back(num++);
        }
    }
    return res;
}

void findPos(const Matrix& mat, int value, int* row, int* col) {
    *row = *col = -1;
    int r, c;
    r = 0, c = mat[0].size() - 1;
    while(r < mat.size() && c >= 0) {
        if(mat[r][c] == value) {
            *row = r, *col = c;
            break;
        } else if(value > mat[r][c]) {
            r++;
        } else
            c--;
    }
}

void printMatrix(const Matrix& mat) {
    for(int i=0; i<mat.size(); i++) {
        for(int j=0; j<mat[0].size(); j++) {
            printf(" %2d ", mat[i][j]);
        }
        printf("\n");
    }
}


int main() {
    Matrix res = getMatrix(10, 10);
    printMatrix(res);
    int row, col;
    row = col = 0;
    findPos(res, 29, &row, &col);
    printf("pos: %d %d\n", row, col);
    return 0;
}
