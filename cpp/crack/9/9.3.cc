#include <stdio.h>
#include <iostream>
#include <vector>
#include <cassert>
using namespace std;


int findPos(const vector<int>& vec, int l, int u, int x) {
    while(l <= u) {
        int m = (l+u) / 2;
        // printf("l:%d u:%d m:%d mv:%d\n", l, u, m, vec[m]);
        if(vec[m] == x) return m;
        if(vec[l] <= vec[m]) {
            if(x > vec[m])
                l = m + 1;
            else if(x >= vec[l]) {
                u = m - 1;
            }
            else
                l = m + 1;
        }
        else if(x < vec[m])
            u = m - 1;
        else if(x <= vec[u])
            l = m + 1;
        else
            u = m - 1;
    }
    return -1;
}

vector<int> getInput(int Num) {
    vector<int> vec;
    srand(time(NULL));
    for(int i=0; i<Num; i++) {
        vec.push_back(rand() % 100);
    }

    sort(vec.begin(), vec.end());

    int pos = rand() % vec.size();
    vector<int> res;
    for(int i=pos; i<vec.size(); i++) {
        res.push_back(vec[i]);
    }
    for(int i=0; i<pos; i++) {
        res.push_back(vec[i]);
    }
    return res;
}

void printVec(const vector<int>& vec) {
    printf("size: %lu => ", vec.size());
    for(int i=0; i<vec.size(); i++) {
        printf(" %d ", vec[i]);
    }
    printf("\n");
}

void testVec(const vector<int>& vec) {
    for(int i=0; i<vec.size(); i++) {
        int value = vec[i];
        int pos = findPos(vec, 0, vec.size() -1, value);
        if(vec[pos] != value) {
            printf("pos: %d value: %d target: %d\n", pos, vec[pos], value);
            assert(0);
        }
    }
}

int main() {
    int Num = 100000;
    for(int i=0; i<Num; i++) {
        vector<int> res = getInput(10);
        //printVec(res);
        testVec(res);
    }
    return 0;
}
