#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int cmp(string stra, string strb) {
    sort(&stra[0], &stra[0] + stra.size());
    sort(&strb[0], &strb[0] + strb.size());
    return stra > strb;
}

int main() {
    string strs[] = {
        "abc", "this", "is", "si", "cbe" , "is"};

    sort(strs, strs+6, cmp);

    for(int i=0; i<6; i++) {
        std::cout << strs[i] << std::endl;
    }
    return 0;
}
