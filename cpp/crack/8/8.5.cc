#include <vector>
#include <stdio.h>
#include <iostream>
using namespace std;


int getNum(string str, char key) {
    int num = 0;
    for(int i=0; i<str.size(); i++) {
        if(str[i] == key)
            num++;
    }
    return num;
}

void getValid(string state, int leftNum, int rightNum) {
    if(leftNum == 0 && rightNum == 0)
        std::cout << state << std::endl;
    int pastLeft = getNum(state, '(');
    int pastRight = getNum(state, ')');
    if(leftNum >= 1) {
        getValid(state + "(", leftNum-1, rightNum);
    }
    if(rightNum >=1 && pastLeft >= pastRight + 1)
        getValid(state + ")", leftNum, rightNum-1);
}


int main() {
    int num = 5;
    getValid("", num, num);
    return 0;
}
