//fill up algorithms
#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;


enum Color {
    Black,
    White,
    Red,
    Yellow,
    Green,
    Nodef
};

typedef vector<vector<Color> > Map;

bool PaintFill(Map& map, int x, int y, Color color) {
    if( x < 0 || x >= map.size()) return false;
    if( y < 0 || y >= map[0].size()) return false;
    if(map[x][y] == color) {
        map[x][y] = Nodef;
        PaintFill(map, x-1, y, color);
        PaintFill(map, x+1, y, color);
        PaintFill(map, x, y+1, color);
        PaintFill(map, x, y-1, color);
    }
    return true;
}

int main() {
    return 0;
}
