#include <iostream>
#include <stdio.h>
#include <assert.h>
using namespace std;

int fibo_rec(int n) {
    if(n == 0) return 0; //f(0) == 0
    if(n == 1) return 1; //f(1) == 1
    else if(n > 1) {
        return (fibo_rec(n-1) + fibo_rec(n-2));
    } else {
        assert(0);
    }
}


int fibo_iteractive(int n) {
    if(n == 0) return 0;
    if(n == 1) return 1;
    int a, b, c;
    a = 0, b = 1;
    for(int i=0; i<n; i++) {
        c = a + b, a = b, b = c;
    }
    return c;
}

int main() {
    for(int i=0; i<10; i++) {
        std::cout <<"recursive : " << fibo_rec(i) << std::endl;
        std::cout <<"iteractive: " << fibo_iteractive(i) << std::endl;
    }
    return 0;
}
