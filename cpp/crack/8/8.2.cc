#include <vector>
#include <iostream>
#include <cassert>
using namespace std;


typedef vector<vector<int> > Map;

void setMap(vector<vector<int> >& map, int Num) {
    map.resize(Num);
    for(int i=0; i<map.size(); i++) {
        map[i].resize(Num);
    }
}

vector<vector<bool> > visited;
static int ans = 0;

bool isFree(const vector<vector<int> >& map, int x, int y) {
    if( x <0 || x >= map.size()) return false;
    if( y <0 || y >= map[0].size()) return false;
    if(visited[x][y])
        return false;
    return true;
}

bool visit(const vector<vector<int> >& map, int x, int y) {
    if(x == (map.size()-1) && y == (map[0].size() - 1)) {
        ans++;
        return true;
    }
    visited[x][y] = true;
    if(isFree(map, x+1, y))
        visit(map, x+1, y);
    if(isFree(map, x, y+1))
        visit(map, x, y+1);
    visited[x][y] = false;
    return false;
}

int visitCount(const vector<vector<int> >& map,  int x, int y) {
    visited.resize(map.size());
    for(int i=0; i<map.size(); i++) {
        visited[i].resize(map[i].size());
        for(int j=0; j<map[i].size(); j++)
            visited[i][j] = false;
    }
    ans = 0;
    visit(map, 0, 0);
    return ans;
}


int getCount(int M, int N) {
    M--, N--;
    int ans = 1;
    int i, k;
    for(i=N+1, k=1; i<=N+M; i++) {
        ans *= i;
        if(ans%k == 0) {
            ans /= k;
            k++;
        }
        std::cout << "ans: " << ans << std::endl;
    }
    while(k<=N) {
        ans /= k;
    }
    return ans;
}

int main() {
    Map map;
    int num = 20;
    int number;
    setMap(map, num);
#if 0
    number = visitCount(map, 0, 0);
    std::cout<< "number: " << number << std::endl;
#else
    number = getCount(num, num);
    std::cout << "number: " << number << std::endl;
#endif
    return 0;
}
