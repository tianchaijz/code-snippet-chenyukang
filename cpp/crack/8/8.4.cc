#include <string>
#include <vector>
#include <iostream>
#include <set>
using namespace std;


string insertAt(string str, int pos, string value) {
    string left = str.substr(0, pos);
    string right = str.substr(pos, str.size() - pos);
    return left + value + right;
}


vector<string> getPerms(string str) {
    vector<string> res;
    if(str.size() == 0) {
        res.push_back("");
        return res;
    }
    string first = str.substr(0, 1);
    string left = str.substr(1, str.size() - 1);
    vector<string> perms = getPerms(left);
    for(int i=0; i<perms.size(); i++) {
        for(int k=0; k<=perms[i].size(); k++) {
            res.push_back(insertAt(perms[i], k, first));
        }
    }
    return res;
}

int getNum(string str) {
    int num = str.size();
    int ans = 1;
    for(int i=1; i<=num; i++) {
        ans *= i;
    }
    return ans;
}

int main() {
    string str = "abc";
    vector<string> perms = getPerms(str);
    std::cout <<"size: " << perms.size() << std::endl;
    std::cout <<"number: "<< getNum(str) << std::endl;
    set<int> sizes;
    for(int i=0; i<perms.size(); i++) {
        std::cout << perms[i] << std::endl;
        sizes.insert(perms[i].size());
    }
    for(set<int>::iterator it = sizes.begin(); it != sizes.end(); ++it) {
        std::cout << *it << std::endl;
    }
    return 0;
}
