#include <iostream>
#include <set>
using namespace std;


struct Node {
    int value;
    struct Node* next;
};

struct Node* newNode(int value) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node*));
    node->next = 0;
    node->value = value;
    return node;
}

void add(struct Node** list, int value) {
    struct Node* node = newNode(value);
    if(*list != NULL) {
        node->next = (*list);
    }
    *list = node;
}

void printList(struct Node* list) {
    struct Node* t = list;
    while(t) {
        printf(" %d ", t->value);
        t = t->next;
    }
    printf("\n");
}

void removeDuplicate(struct Node* list) {
    if( list == NULL )
        return;
    set<int> S;
    struct Node* t = list->next;
    struct Node* r = list;
    struct Node* p;
    S.insert(r->value);
    while(t) {
        if(S.find(t->value) == S.end()) {
            S.insert(t->value);
            r->next = t;
            r = r->next;
            t = t->next;
            r->next = NULL;
        } else {
            p = t;
            t = t->next;
            free(p);
        }
    }
}

int main() {
    struct Node* head = 0;
    add(&head, 1);
    add(&head, 1);
    add(&head, 2);
    add(&head, 3);
    add(&head, 4);
    add(&head, 3);
    printList(head);
    removeDuplicate(head);
    printList(head);
    return 0;
}
