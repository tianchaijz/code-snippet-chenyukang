#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
using namespace std;

struct Node {
    int value;
    struct Node* next;
};

struct Node* newNode(int value) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->value = value;
    node->next = NULL;
}

void add(struct Node** head, int value) {
    struct Node* node = newNode(value);
    if(*head != NULL) {
        node->next = *head;
    }
    *head = node;
}

void printList(struct Node* head) {
    struct Node* t = head;
    while(t) {
        printf(" %d ", t->value);
        t = t->next;
    }
    printf("\n");
}

struct Node* newList(int n) {
    struct Node* head = NULL;
    for(int i=0; i<n; i++) {
        add(&head, i);
    }
    return head;
}

struct Node* findNth(struct Node* head, int n) {
    struct Node* t = head;
    for(int i=1; i<n && t; i++) {
        t = t->next;
    }
    return t;
}

void delMiddle(struct Node* node) {
    assert(node->next);
    struct Node* t = node->next;
    node->value = node->next->value;
    node->next = node->next->next;
    free(t);
}

int main() {
    struct Node* head = newList(10);
    printList(head);
    struct Node* t = findNth(head, 3);
    printf("value: %d\n", t->value);
    delMiddle(t);
    printList(head);
    return 0;
}
