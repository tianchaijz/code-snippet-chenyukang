#include <iostream>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

struct Node {
    int value;
    struct Node* next;
};

struct Node* newNode(int value) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->value = value;
    node->next = NULL;
}

void add(struct Node** head, int value) {
    struct Node* node = newNode(value);
    if(*head != NULL) {
        node->next = *head;
    }
    *head = node;
}

void printList(struct Node* head) {
    struct Node* t = head;
    while(t) {
        printf(" %d ", t->value);
        t = t->next;
    }
    printf("\n");
}

struct Node* nthToLast(struct Node* head, int num) {
    if(head == NULL || num < 1) {
        return NULL;
    }
    struct Node* p = head;
    struct Node* q = head;
    for(int i=0; i<num-1; i++) {
        if(q == NULL) return NULL;
        q = q->next;
    }
    while(q != NULL && q->next != NULL) {
        p = p->next;
        q = q->next;
    }
    return p;
}

struct Node* nthToLastV2(struct Node* head, int num) {
    struct Node* t = head;
    struct Node* p;
    int cnt = 0;

    if(head == NULL || num < 1)
        return NULL;

    while(t) {
        cnt++;
        t = t->next;
    }
    t = head, p = 0;
    cnt = cnt - num + 1;
    while(cnt) {
        p = t;
        t = t->next;
        cnt--;
    }
    return p;
}

void test1() {
    struct Node* head = NULL;
    add(&head, 1);
    add(&head, 2);
    add(&head, 3);
    add(&head, 4);
    printList(head);
    struct Node* v = nthToLastV2(head, 5);
    printf("ret: %lu\n", (unsigned long)v);
    if(v == NULL) {
        printf("v: null\n");
    } else {
        printf("v: %d\n", v->value);
    }
}

void test2() {
    struct Node* head = NULL;
    add(&head, 1);
    add(&head, 2);
    add(&head, 3);
    add(&head, 4);
    printList(head);
    struct Node* v = nthToLast(head, 5);
    if(v == NULL) {
        printf("v: null\n");
    } else {
        printf("v: %d\n", v->value);
    }
}

int main() {
    test1();
    test2();
    return 0;
}
