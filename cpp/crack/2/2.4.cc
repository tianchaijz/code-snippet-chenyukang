#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
using namespace std;

struct Node {
    int value;
    struct Node* next;
};

struct Node* newNode(int value) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->value = value;
    node->next = NULL;
}

void add(struct Node** head, int value) {
    struct Node* node = newNode(value);
    if(*head != NULL) {
        node->next = *head;
    }
    *head = node;
}

void addEnd(struct Node** head, int value) {
    struct Node* node = newNode(value);
    if(*head == NULL) {
        *head = node;
    } else {
        struct Node* t = *head;
        while(t->next) {
            t = t->next;
        }
        t->next = node;
    }
}

void printList(struct Node* head) {
    struct Node* t = head;
    while(t) {
        printf(" %d ", t->value);
        t = t->next;
    }
    printf("\n");
}

struct Node* newList(int n) {
    struct Node* head = NULL;
    for(int i=0; i<n; i++) {
        add(&head, i);
    }
    return head;
}

struct Node* add(struct Node* l1, struct Node* l2) {
    assert(l1 && l2);
    struct Node* res = NULL;
    struct Node* p1 = l1;
    struct Node* p2 = l2;
    int carry = 0;
    int v;
    while(p1 || p2) {
        v = 0;
        if(p1) v += p1->value;
        if(p2) v += p2->value;
        v += carry;
        if(v >= 10)
            carry = 1;
        v = v % 10;
        addEnd(&res, v);
        p1 = p1 ? p1->next : NULL;
        p2 = p2 ? p2->next : NULL;
    }
    return res;
}

int main() {
    struct Node* p1 = NULL;
    struct Node* p2 = NULL;
    add(&p1, 5);
    add(&p1, 1);
    add(&p1, 3);

    add(&p2, 2);
    add(&p2, 9);
    add(&p2, 5);

    printList(p1);
    printList(p2);
    struct Node* res = add(p1, p2);
    printList(res);
    return 0;
}
