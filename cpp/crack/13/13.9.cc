#include <iostream>
#include <cassert>
using namespace std;


template <class T> class SmartPointer {
private:
    T* ref;
    size_t* refCnt;

    void dispose() {
        if(--*refCnt <= 0) {
            delete ref;
            delete refCnt;
        }
    }

public:
    explicit SmartPointer(T* ptr = 0): ref(ptr), refCnt(new size_t(1)) {}

    SmartPointer(const SmartPointer& pt): ref(pt.ref), refCnt(pt.refCnt) {
        ++*refCnt;
    }

    ~SmartPointer() {
        dispose();
    }

    SmartPointer<T>& operator= (const SmartPointer<T>& other) {
        if(this != &other) {
            dispose();
            ref = other.ref;
            refCnt = other.refCnt;
            ++*refCnt;
        }
        return *this;
    }

    T& operator*() {
        return *ref;
    }
    T* operator->() {
        return ref;
    }
};

int main() {
    int* p1 = new int(10);
    int* p2 = new int(20);
    SmartPointer<int> pt(p1);
    SmartPointer<int> pp = pt;
    pp = SmartPointer<int>(p2);
    printf("value: %d\n", *pp);
    SmartPointer<string> xx(new string("hello"));
    std::cout << "string: " << *xx << std::endl;
    return 0;
}
