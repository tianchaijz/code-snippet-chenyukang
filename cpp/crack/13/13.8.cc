#include <iostream>
#include <map>
#include <cassert>
using namespace std;


struct Node {
    int val;
    struct Node* prev;
    struct Node* next;
};

typedef struct Node Node;
typedef map<Node*, Node*> NodeMap;

Node* copy_iter(Node* cur, NodeMap& nodemap) {
    if(cur == NULL) return NULL;
    NodeMap::iterator it = nodemap.find(cur);
    if(it != nodemap.end()) {
        return it->second;
    }
    Node* node = new Node();
    node->val = cur->val;
    nodemap[cur] = node;
    node->prev = copy_iter(cur->prev, nodemap);
    node->next = copy_iter(cur->next, nodemap);
    return node;
}

Node* copyNode(Node* node) {
    NodeMap map;
    Node* res = copy_iter(node, map);
    while(res->prev)
        res = res->prev;
    return res;
}

Node* newNode(int val) {
    Node* res = new Node;
    res->val = val;
    res->prev = res->next = 0;
    return res;
}

Node* addNode(Node* head, int val) {
    assert(head);
    Node* node = newNode(val);
    node->next = head;
    head->prev = node;
    return node;
}

void printNode(Node* head) {
    for(Node* cur = head; cur != NULL; cur = cur->next) {
        printf(" %d ", cur->val);
    }
    printf("\n");
}

Node* findNode(Node* head, int val) {
    for(Node* cur = head; cur != NULL; cur = cur->next) {
        if(cur->val == val) {
            return cur;
        }
    }
    return NULL;
}

int main() {
    Node* head = newNode(1);
    head = addNode(head, 2);
    head = addNode(head, 10);
    head = addNode(head, 8);
    printNode(head);

    Node* node = findNode(head, 2);
    Node* copy = copyNode(node);
    printNode(copy);
    return 0;
}
