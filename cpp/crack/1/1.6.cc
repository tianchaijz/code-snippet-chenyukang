#include <vector>
#include <iostream>
using namespace std;

void rotate(vector<vector<int> >& matrix)  {
    int N = matrix.size();
    for(int layer=0; layer<N/2; layer++) {
        int first = layer;
        int last = N - 1 - layer;
        for(int i = first; i < last; ++i) {
            int offset = i - first;
            int top = matrix[first][i];
            matrix[first][i] = matrix[last-offset][first];
            matrix[last-offset][first] = matrix[last][last-offset];
            matrix[last][last-offset] = matrix[i][last];
            matrix[i][last] = top;
        }
    }
}


void print(const vector<vector<int> >& matrix) {
    for(int i=0; i<matrix.size(); i++) {
        for(int j=0; j<matrix[i].size(); j++) {
            std::cout <<" " << matrix[i][j] << " ";
        }
        std::cout<< std::endl;
    }
    std::cout << "====================" << std::endl;
}


int main() {
    vector<vector<int> > matrix;
    matrix.resize(3);
    matrix[0].push_back(1);
    matrix[0].push_back(2);
    matrix[0].push_back(3);

    matrix[1].push_back(4);
    matrix[1].push_back(5);
    matrix[1].push_back(6);

    matrix[2].push_back(7);
    matrix[2].push_back(8);
    matrix[2].push_back(9);

    print(matrix);

    rotate(matrix);

    print(matrix);

}
