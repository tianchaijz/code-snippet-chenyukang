#include <iostream>
#include <string>
using namespace std;


string replaceSpace(string s) {
    int cnt = 0;
    string res;
    int i;
    for(i=0; i<s.size(); i++) {
        if(s[i] == ' ') {
            res.push_back('%');
            res.push_back('2');
            res.push_back('0');
        } else
            res.push_back(s[i]);
    }
    return res;
}

void test(string s) {
    std::cout<< s << " --> " << replaceSpace(s) << std::endl;
}

int main() {
    test("abcde eed es");
    return 0;
}
