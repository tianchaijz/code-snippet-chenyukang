#include <iostream>
using namespace std;


char* remove_duplicate(char* str) {
    if(str == NULL) return NULL;
    char* p = str;
    char* q = str+1;
    int cnt = 1;
    int i;
    while(*q != '\0') {
        for(i=0; i<cnt; i++) {
            if(p[i] == *q) {
                break;
            }
        }
        if(i == cnt) {
            p[cnt++] = *q;
        }
        q++;
    }
    p[cnt] = '\0';
    return p;
}


void test(char* str) {
    printf("%s --> ", str);
    printf("%s\n", remove_duplicate(str));
}


int main() {
    char str[32];
    memset(str, 0, sizeof(str));
    strcpy(str, "aabced");
    test(str);
    strcpy(str, "aa");
    test(str);
    strcpy(str, "a");
    test(str);
    return 0;
}
