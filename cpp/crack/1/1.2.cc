#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;


void reverse(char* str) {
    int len = strlen(str);
    int i, j;
    char t;
    i = 0;
    j = len - 1;
    while(i < j) {
        t = str[i];
        str[i] = str[j];
        str[j] = t;
        i++, j--;
    }
}

void test(char* str) {
    printf("prev: %s ", str);
    reverse(str);
    printf("result: %s\n", str);
}

int main() {
    char str[10] = "abc";
    test(str);
    strcpy(str, "aaae");
    test(str);
    return 0;
}
