#include <iostream>
#include <stdio.h>
using namespace std;


bool isRotate(const string& sa, const string& sb) {
    if(sa.size() != sb.size())
        return false;

    string c = sb + sb;
    if(c.find(sa) != string::npos) {
        return true;
    }
    return false;
}

void test(const string& sa, const string& sb) {
    if(isRotate(sa, sb)) {
        std::cout << "Yes" << std::endl;
    } else {
        std::cout << "No" << std::endl;
    }
}


int main() {
    test("ab", "ba");
    test("abc", "bca");
    test("a", "a");
    test("a", "bcd");
    test("a", "bcda");

    return 0;
}
