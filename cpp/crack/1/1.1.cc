#include <iostream>
#include <vector>
#include <string>

using namespace std;

bool isUniqure(string str) {
    for(int i=0; i<str.size(); i++) {
        for(int j=i+1; j<str.size(); j++) {
            if(str[i] == str[j])
                return false;
        }
    }
    return true;
}


void test(string str) {
    if(isUniqure(str)) {
        printf("str: %s ans: True\n", str.c_str());
    } else {
        printf("str: %s ans: False\n", str.c_str());
    }
}

int main() {
    test("ab");
    test("abc");
    test("aba");
    test("a");
    test("aa");
    test("abcdea");
    return 0;
}
