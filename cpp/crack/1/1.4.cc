#include <iostream>
#include <vector>
using namespace std;

bool is_anagram(string s) {
    int len = s.size();
    int i,j;
    i = 0;
    j = len - 1;
    while(i < j) {
        if(s[i] != s[j]) {
            return false;
        }
        i++, j--;
    }
    return true;
}


void test(string s) {
    std::cout << "string: " << s;
    if(is_anagram(s)) {
        std::cout << " YES" << std::endl;
    } else {
        std::cout << " NO" << std::endl;
    }
}

int main() {
    test("a");
    test("ab");
    test("abc");
    test("aba");
    test("aaa");
    return 0;
}
