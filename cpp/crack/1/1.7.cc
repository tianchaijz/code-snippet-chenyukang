#include <iostream>
#include <vector>
using namespace std;


typedef vector<vector<int> > matrix;


void print(matrix& m) {
    printf("matrix:\n");
    for(int i=0; i<m.size(); i++){
        for(int j=0; j<m[i].size(); j++) {
            printf(" %d ", m[i][j]);
        }
        printf("\n");
    }
    printf("==============================\n");
}

void setZero(matrix& m) {
    int row, col;
    row = m.size();
    if(row == 0)
        return;
    col = m[0].size();

    vector<int> rows(row, 0);
    vector<int> cols(col, 0);

    for(int i=0; i<m.size(); i++) {
        for(int j=0; j<m[i].size(); j++) {
            if(m[i][j] == 0) {
                rows[i] = 1;
                cols[j] = 1;
            }
        }
    }

    for(int i=0; i<m.size(); i++) {
        for(int j=0; j<m[i].size(); j++) {
            if(rows[i] == 1 ||
               cols[j] == 1)
                m[i][j] = 0;
        }
    }
}


int main() {
    matrix m;
    m.resize(3);
    m[0].push_back(0);
    m[0].push_back(1);
    m[0].push_back(2);

    m[1].push_back(2);
    m[1].push_back(3);
    m[1].push_back(4);

    m[2].push_back(3);
    m[2].push_back(4);
    m[2].push_back(5);

    print(m);
    setZero(m);
    print(m);
    return 0;
}
