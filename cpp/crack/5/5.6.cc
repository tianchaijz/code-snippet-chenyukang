#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;

void printBinary(int N) {
    vector<int> res(32, 0);
    for(int i=0; i<32; i++) {
        if( N & (1<<i)) {
            res[i] = 1;
        }
    }
    printf("[value: %-10d] ", N);
    for(int i=31; i>=0; i--) {
        printf("%d", res[i]);
    }
    printf("\n");
}

int swap(int value) {
    return (( value & 0xaaaaaaaa) >> 1) | ((value & 0x55555555) << 1);
}

int main() {
    int a = 10203;
    int res = swap(a);
    printBinary(a);
    printBinary(res);
    return 0;
}
