#include <stdio.h>
#include <vector>
#include <cassert>
using namespace std;


void printBinary(int N) {
    vector<int> res(32, 0);
    for(int i=0; i<32; i++) {
        if( N & (1<<i)) {
            res[i] = 1;
        }
    }
    printf("[value: %-10d] ", N);
    for(int i=31; i>=0; i--) {
        printf("%d", res[i]);
    }
    printf("\n");
}

int diffBits(int M, int N) {
    int cnt = 0;
    while(M || N) {
        if( (M%2) != (N%2) )
            cnt++;
        M >>= 1;
        N >>= 1;
    }
    return cnt;
}

int diffBits2(int M, int N) {
    int cnt = 0;
    for(int c = M ^ N;  c != 0; c = c>>1) {
        cnt += c & 1;
    }
    return cnt;
}


void test(int m, int n) {
    assert(diffBits(m, n) == diffBits2(m, n));
}

int main() {
    srand(time(NULL));
    for(int i=0; i<10000; i++) {
        int m = rand() % 1000;
        int n = rand() % 1000;
        test(m, n);
    }
    return 0;
}
