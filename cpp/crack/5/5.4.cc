#include <stdio.h>
#include <vector>
#include <cassert>

bool isPower(int value) {
    return (value & (value - 1)) == 0;
}

int main() {
    for(int i=0; i<10; i++) {
        assert(isPower(1<<i));
    }
    return 0;
}
