#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;


bool isSet(int value, int idx) {
    return (value & (1<<idx));
}

int set(int value, int idx, bool v) {
    if(v) return value | ( 1<<idx);
    else  return value & ( ~(1<<idx));
}

int countOnes(int value) {
    int cnt = 0;
    while ( value>0 ) {
        if(value%2) cnt++;
        value >>= 1;
    }
    return cnt;
}

int firstFromIndex(int value, int pos, bool set) {
    for(int i=pos; i<32; i++) {
        if(isSet(value, i) == set) {
            return i;
        }
    }
    return -1;
}

int next(int value) {
    int res = value;
    int firstOne = firstFromIndex(value, 0, true);
    int ZeroPos  = firstFromIndex(value, firstOne+1, false);
    if(ZeroPos == 31) return -1;
    res = set(res, ZeroPos, true);
    for(int i=ZeroPos-1; i>=0; i--)
        res = set(res, i, false);

    printf("value: %d res: %d\n", value, res);
    int diff = countOnes(value) - countOnes(res);
    for(int i=0; i<diff && i < ZeroPos - 1; i++) {
        res = set(res, i, true);
    }
    return res;
}

int prev(int value) {
    int res = value;
    int firstZero = firstFromIndex(value, 0, false);
    int OnePos    = firstFromIndex(value, firstZero+1, true);
    if(OnePos == 31) return -1;
    res = set(res, OnePos, false);
    for(int i=OnePos-1; i>=0; i--)
        res = set(res, i, false);

    int diff = countOnes(value) - countOnes(res);
    for(int i=OnePos-1; diff > 0 &&  i>=0; diff--, i--) {
        res = set(res, i, true);
    }
    return res;
}

void printBinary(int N) {
    vector<int> res(32, 0);
    for(int i=0; i<32; i++) {
        if( N & (1<<i)) {
            res[i] = 1;
        }
    }
    printf("[value: %-10d] ", N);
    for(int i=31; i>=0; i--) {
        printf("%d", res[i]);
    }
    printf("\n");
}

int main() {
    int value = 35;
    int nextValue = next(value);
    int prevValue = prev(value);
    printBinary(prevValue);
    printBinary(value);
    printBinary(nextValue);

    return 0;
}
