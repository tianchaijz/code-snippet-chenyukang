#include <vector>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cassert>
using namespace std;

#define N 100

int vec_fetch(vector<int> vec, int i, int j) {
    assert(i < vec.size());
    return (vec[i] & (1 << j));
}

int findMissing(vector<int> vec) {
    int res = 0;
    for(int i=0; i<32; i++) {
        for(int j=0; j<vec.size(); j++) {
            if(vec_fetch(vec, j, i))
                res = res ^ ( 1<<i);
        }
    }
    return res;
}

int main() {
    vector<int> vec;
    for(int i=0; i<12; i++)
        vec.push_back(i);
    for(int i=13; i<N; i++)
        vec.push_back(i);
    int miss = findMissing(vec);
    std::cout << "Missing: " << miss << std::endl;
    return 0;
}
