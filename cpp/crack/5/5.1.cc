#include <stdio.h>
#include <vector>
#include <iostream>
#include <string>
using namespace std;


void printBinary(int N) {
    vector<int> res(32, 0);
    for(int i=0; i<32; i++) {
        if( N & (1<<i)) {
            res[i] = 1;
        }
    }
    printf("[value: %-10d] ", N);
    for(int i=31; i>=0; i--) {
        printf("%d", res[i]);
    }
    printf("\n");
}

int updateBits(int n, int m, int i, int j) {
    int max = ~0; /* All 1’s */
    int left = max-((1<<j)-1);
    int right=((1<<i)-1);
    int mask = left | right;
    return (n & mask) | (m << i);
}

int covert(int N, int M, int i, int j) {
    int right = (1<<(i+1)) - 1;
    int left  = ~((1<<(j+1)) - 1);
    int mask = left | right;
    return (N & mask) | ( M << i);
}

int fromBinary(string str) {
    int res = 0;
    int len = str.size();
    for(int i=0; i<len; i++) {
        if(str[i] == '1')
            res = res | (1<< (len - i - 1));
    }
    return res;
}

int main() {
    int N = fromBinary("1000000000");
    printBinary(N);
    int M = fromBinary("10101");
    printBinary(M);
    int i = 2;
    int j = 6;
    int res = covert(N, M, i, j);
    printBinary(res);
    res = updateBits(N, M, i, j);
    printBinary(res);
    return 0;
}
