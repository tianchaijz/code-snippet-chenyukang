#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
using namespace std;

string fromFloat(string value) {
    string res;
    size_t dot = value.find('.');
    string intpart = value.substr(0, dot);
    string decpart = value.substr(dot, value.size() - dot);
    std::cout << intpart << " " << decpart << std::endl;
    int v = atoi(intpart.c_str());
    while(v>0) {
        if(v%2)
            res = '1' + res;
        else
            res = '0' + res;
        v = v >> 1;
    }
    double x = atof(decpart.c_str());
    string  decstr = "";
    while(x > 0) {
        if(decstr.size() > 32) { return "ERROR"; }
        if(x == 1) {
            decstr.push_back('1');
            break;
        }
        x *= 2;
        if(x >= 1) {
            decstr.push_back('1');
            x -= 1;
        } else
            decstr.push_back('0');
    }
    std::cout << "int: " << res << std::endl;
    std::cout << "dec: " << decstr << std::endl;
    return res + "." +  decstr;
}


int main() {
    std::cout << fromFloat("19.25") << std::endl;
    return 0;
}
