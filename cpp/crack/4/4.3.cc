#include <vector>
#include <iostream>
using namespace std;


struct Node {
    int value;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int val) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->value = val;
    return node;
}

static struct Node* _create(const vector<int>& vec, int start, int end) {
    if(start > end) {
        return NULL;
    }
    int mid  = (start + end)/2;
    struct Node* node = newNode(vec[mid]);
    node->left = _create(vec, start, mid - 1);
    node->right = _create(vec, mid+1, end);
    return node;
}

struct Node* createTree(const vector<int>& vec) {
    return _create(vec, 0, vec.size() - 1);
}

static void _visitIter(struct Node* node) {
    if(node == NULL) return;
    _visitIter(node->left);
    printf(" %d ", node->value);
    _visitIter(node->right);
}

void PrintTree(struct Node* root) {
    _visitIter(root);
}

int main() {
    vector<int> vec;
    for(int i=0; i<10; i++) {
        vec.push_back(i);
    }

    struct Node* root = createTree(vec);
    PrintTree(root);
    return 0;
}
