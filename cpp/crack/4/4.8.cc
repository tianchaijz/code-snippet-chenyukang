/* You are given a binary tree in which each node contains a value.
   Design an algorithm to print all paths which sum up to that value.
   Note that it can be any path in the tree - it does not have to start at the root. */
#include <stack>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <assert.h>
using namespace std;

struct Node {
    int value;
    struct Node* left;
    struct Node* right;
    struct Node* parent;
};

struct Node* newNode(int val) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = node->parent = NULL;
    node->value = val;
    return node;
}

static struct Node* _create(const vector<int>& vec, int start, int end) {
    if(start > end) {
        return NULL;
    }
    int mid  = (start + end)/2;
    struct Node* node = newNode(vec[mid]);
    node->left = _create(vec, start, mid - 1);
    node->right = _create(vec, mid+1, end);
    return node;
}

struct Node* createTree(const vector<int>& vec) {
    return _create(vec, 0, vec.size() - 1);
}

static void _visitIter(struct Node* node) {
    if(node == NULL) return;
    _visitIter(node->left);
    printf(" %d ", node->value);
    _visitIter(node->right);
}

void PrintTree(struct Node* root) {
    _visitIter(root);
}

bool _ForSum(struct Node* root, stack<int>& buf, int sum) {
    if(sum == 0) return true;
    if(root == NULL) return false;
    if(root->value > sum)
        return false;

    buf.push(root->value);
    bool right = _ForSum(root->right, buf, sum - root->value);
    if(right) return true;

    bool left = _ForSum(root->left, buf, sum - root->value);
    if(left) return true;
    else {
        buf.pop();
        return false;
    }
}

/* this algorithms just print a path */
stack<int> FindPathForSum(struct Node* root, int sum) {
    stack<int> st;
    if(root == NULL) return st;
    if(root->value <= sum) {
        st.push(root->value);
        if(_ForSum(root->left, st, sum - root->value)) {
            return st;
        }
        if(_ForSum(root->right, st, sum - root->value)) {
            return st;
        }
    }
    st = FindPathForSum(root->left, sum);
    if(st.size() != 0)
        return st;
    st = FindPathForSum(root->right, sum);
    return st;
}

int main() {
    vector<int> vec;
    for(int i=0; i<100; i++) {
        vec.push_back(i);
    }

    struct Node* root = createTree(vec);
    //PrintTree(root);
    printf("root: %d\n", root->value);
    printf("\n");

    stack<int> path = FindPathForSum(root, 3+4+5+6+7+8);
    vector<int> res;
    printf("size: %lu\n", path.size());
    while(!path.empty()) {
        res.push_back(path.top());
        path.pop();
    }
    printf("path: ");
    for(int i=res.size() -1; i>=0; i--) {
        printf(" %d ", res[i]);
    }
    printf("\n");
    return 0;
}
