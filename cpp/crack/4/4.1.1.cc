/* Implement a function to check if a tree is balanced.
   For the purposes of this question, a balanced tree is defined to be a tree such
   that no two leaf nodes differ in distance from the root by more than one. */
#include <stdio.h>
#include <vector>
#include <list>
#include <iostream>
#include <stdlib.h>

#define abs(x) (((x) > 0) ? (x) : (-x))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

struct Node{
    int value;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int value) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = 0;
    node->value = value;
    return node;
}

int minHeight(struct Node* node) {
    if(node == NULL) return 0;
    int l = minHeight(node->left);
    int r = minHeight(node->right);
    if(l == 0)
        return r + 1;
    if(r == 0)
        return l + 1;
    int ret = min(l, r) + 1;
    return ret;
}

int maxHeight(struct Node* node) {
    if(node == NULL) return 0;
    int l = maxHeight(node->left);
    int r = maxHeight(node->right);
    int ret = max(l, r) + 1;
    return ret;
}

bool isBalance(struct Node* root) {
    int max = maxHeight(root);
    int min = minHeight(root);
    printf("max: %d min: %d\n", max, min);
    return max - min <= 1;
}

void test(struct Node* root) {
    if(isBalance(root)) {
        std::cout << "Yes" << std::endl;
    } else {
        std::cout << "No" << std::endl;
    }
}


int main() {
    struct Node* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(10);
    root->left->left->left = newNode(5);
    root->right->right = newNode(6);
    test(root);
    return 0;
}
