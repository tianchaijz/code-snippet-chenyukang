/* Write an algorithm to find the ‘next’ node (eg , in-order successor) of a given node in
a binary search
tree where each node has a link to its parent */
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <assert.h>
using namespace std;


struct Node {
    int value;
    struct Node* left;
    struct Node* right;
    struct Node* parent;
};

struct Node* newNode(int val) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = node->parent = NULL;
    node->value = val;
    return node;
}

static struct Node* _create(const vector<int>& vec, int start, int end) {
    if(start > end) {
        return NULL;
    }
    int mid  = (start + end)/2;
    struct Node* node = newNode(vec[mid]);
    node->left = _create(vec, start, mid - 1);
    node->right = _create(vec, mid+1, end);
    return node;
}

struct Node* createTree(const vector<int>& vec) {
    return _create(vec, 0, vec.size() - 1);
}

static void _visitIter(struct Node* node) {
    if(node == NULL) return;
    _visitIter(node->left);
    printf(" %d ", node->value);
    _visitIter(node->right);
}

void PrintTree(struct Node* root) {
    _visitIter(root);
}

static void _addParent(struct Node* root, struct Node* parent) {
    if(root == NULL) return;
    root->parent = parent;
    _addParent(root->left, root);
    _addParent(root->right, root);
}

void AddParentLink(struct Node* root) {
    _addParent(root, NULL);
}

struct Node* findNode(struct Node* node,  int value) {
    if(node == NULL) return NULL;
    if(node->value == value) return node;
    struct Node* t = 0;
    t = findNode(node->left, value);
    if(t) return t;
    t = findNode(node->right, value);
    if(t) return t;
    return NULL;
}

/* find the successor (inorder) */
struct Node* findSucc(struct Node* node) {
    struct Node* p = node;
    struct Node* pre = p;
    while(p) {
        if(p->right != pre && p->right != NULL) {
            p = p->right;
            while(p->left) {
                p = p->left;
            }
            return p;
        } else {
            pre = p;
            p = p->parent;
            if(p == NULL)
                return NULL;
            if(p->left == pre)
                return p;
        }
    }
    return NULL;
}

/* find the successor (inorder) , this is the version from book,
   I think there are some bugs, see the testcase below */
struct Node* findSucc2(struct Node* node) {
    if(node == NULL) return NULL;
    struct Node* p = node;
    struct Node* pre = p;
    if(p->parent == NULL || p->right != NULL) {
        if(p->right)
            p = p->right;
        while(p->left)
            p = p->left;
        return p;
    } else {
        while((pre = p->parent) != NULL) {
            if(pre->left == p) {
                return pre;
            }
            p = pre;
        }
    }
    return p;
}


int main() {
    vector<int> vec;
    for(int i=0; i<10; i++) {
        vec.push_back(i);
    }

    struct Node* root = createTree(vec);
    PrintTree(root);
    printf("\n");
    AddParentLink(root);
    printf("root: %d\n", root->value);
    for(int i=0; i<10; i++) {
        struct Node* node = findNode(root, i);
        if(node) {
            printf("find: %d ", node->value);
            struct Node* succ = findSucc(node);
            if(succ) {
                printf("succ: %d\n", succ->value);
            } else {
                printf("no succ\n");
            }
        }
        else
            printf("can not found\n");
    }

    vec.clear();
    vec.push_back(1);
    struct Node* root2 = createTree(vec);
    struct Node* node = findSucc(root2);
    assert(node == NULL);
    root2->right = newNode(2);
    assert(findSucc(root2) != NULL);
    printf("succ: %d\n", findSucc(root2)->value);

    return 0;
}
