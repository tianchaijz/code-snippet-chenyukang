/* Given a binary search tree, design an algorithm which creates a linked list
   of all the nodes at each depth (eg, if you have a tree with depth D,
   you’ll have D linked lists) */
#include <vector>
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

struct Node {
    int value;
    struct Node* left;
    struct Node* right;
};

struct LinkNode {
    int value;
    struct LinkNode* next;
};

struct Node* newNode(int val) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->value = val;
    return node;
}

static struct Node* _create(const vector<int>& vec, int start, int end) {
    if(start > end) {
        return NULL;
    }
    int mid  = (start + end)/2;
    struct Node* node = newNode(vec[mid]);
    node->left = _create(vec, start, mid - 1);
    node->right = _create(vec, mid+1, end);
    return node;
}

struct Node* createTree(const vector<int>& vec) {
    return _create(vec, 0, vec.size() - 1);
}

static void _visitIter(struct Node* node) {
    if(node == NULL) return;
    _visitIter(node->left);
    printf(" %d ", node->value);
    _visitIter(node->right);
}

void PrintTree(struct Node* root) {
    _visitIter(root);
}

struct LinkNode* newLink(int value) {
    struct LinkNode* link = (struct LinkNode*)malloc(sizeof(struct LinkNode));
    link->next = NULL;
    link->value = value;
    return link;
}

void covertLink(struct Node* root, vector<struct LinkNode*>& res) {
    if(root == NULL) return;
    queue<struct Node*> Q;
    struct LinkNode *now, *pre;
    int newLevel, curCount, nxtCount;
    now = pre = 0;
    newLevel = curCount = 1;
    nxtCount = 0;
    Q.push(root);
    while(!Q.empty()) {
        struct Node* node = Q.front();
        Q.pop();
        curCount--;
        now = newLink(node->value);
        if(pre)
            pre->next = now;
        pre = now;
        if(newLevel) {
            res.push_back(now);
            newLevel = 0;
        }
        if(node->left) {
            Q.push(node->left);
            nxtCount++;
        }
        if(node->right) {
            Q.push(node->right);
            nxtCount++;
        }
        if(curCount == 0) {
            curCount = nxtCount;
            nxtCount = 0;
            newLevel = 1;
            pre = 0;
        }
    }

}

void printList(struct LinkNode* head) {
    struct LinkNode* t = head;
    while(t) {
        printf(" %d ", t->value);
        t = t->next;
    }
}

int main() {
    vector<int> vec;
    for(int i=0; i<10; i++) {
        vec.push_back(i);
    }

    struct Node* root = createTree(vec);
    printf("BinaryTree:");
    PrintTree(root);
    printf("\n");

    vector<struct LinkNode*> res;

    covertLink(root, res);

    printf("res size: %lu\n", res.size());
    for(int i=0; i<res.size(); i++) {
        printList(res[i]);
        printf("\n");
    }

    return 0;
}
