/* You have two very large binary trees: T1, with millions of nodes, and T2,
   with hundreds of nodes. Create an algorithm to decide if T2 is a subtree of T1. */
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <assert.h>
using namespace std;

struct Node {
    int value;
    struct Node* left;
    struct Node* right;
    struct Node* parent;
};

struct Node* newNode(int val) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = node->parent = NULL;
    node->value = val;
    return node;
}

static struct Node* _create(const vector<int>& vec, int start, int end) {
    if(start > end) {
        return NULL;
    }
    int mid  = (start + end)/2;
    struct Node* node = newNode(vec[mid]);
    node->left = _create(vec, start, mid - 1);
    node->right = _create(vec, mid+1, end);
    return node;
}

struct Node* createTree(const vector<int>& vec) {
    return _create(vec, 0, vec.size() - 1);
}

static void _visitIter(struct Node* node) {
    if(node == NULL) return;
    _visitIter(node->left);
    printf(" %d ", node->value);
    _visitIter(node->right);
}

void PrintTree(struct Node* root) {
    _visitIter(root);
}

bool isSame(struct Node* root1, struct Node* root2) {
    if(root1 == NULL && root2 == NULL) return true;
    if(root2 == NULL) return true;
    if(root1->value != root2->value) return false;
    return isSame(root1->left, root2->left) &&
        isSame(root1->right, root2->right);
}

bool isSubTree(struct Node* root1, struct Node* root2) {
    if(root2 == NULL) return true;
    if(root1 == NULL && root2 == NULL) return true;
    if(root1 == NULL) return false;
    //printf("root1: %d root2: %d\n", root1->value, root2->value);
    if(root1->value == root2->value) {
        if(isSame(root1, root2))
            return true;
    } else {
        return isSubTree(root1->left, root2) ||
            isSubTree(root1->right, root2);
    }
}


int main() {
    int num = 1000;
    vector<int> vec;
    for(int i=0; i<num; i++){
        vec.push_back(i);
    }
    struct Node* root1 = createTree(vec);
    for(int i=0; i<num; i++) {
        vec.clear();
        vec.push_back(i);
        struct Node* root2 = createTree(vec);
        if(isSubTree(root1, root2)) {
            printf("Yes: %d\n", i);
        } else {
            printf("No: %d\n", i);
        }
    }
    return 0;
}
