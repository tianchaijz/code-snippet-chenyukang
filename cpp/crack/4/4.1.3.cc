/* A little difference with 4.1, 4.1 need to compare the leaf depth, but this is not considered in
   this problem, and 4.1.3.cc is a optimiztion for this */
#include <stdio.h>
#include <vector>
#include <list>
#include <iostream>
#include <stdlib.h>
using namespace std;

#define abs(x) (((x) > 0) ? (x) : (-x))
#define max(a, b) (((a) > (b)) ? (a) : (b))

struct Node{
    int value;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int value) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = 0;
    node->value = value;
    return node;
}

int isBalance(struct Node* root) {
    if(root == NULL) return 0;
    int l = isBalance(root->left);
    int r = isBalance(root->right);
    if(l == -1 || r == -1)
        return -1;
    if(abs(l - r) > 1)
        return -1;
    return max(l, r) + 1;
}

void test(struct Node* root) {
    if(isBalance(root) != -1) {
        std::cout << "Yes" << std::endl;
    } else {
        std::cout << "No" << std::endl;
    }
}

int main() {
    struct Node* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->left->left = newNode(5);
    root->right->right = newNode(6);
    test(root);
    return 0;
}
