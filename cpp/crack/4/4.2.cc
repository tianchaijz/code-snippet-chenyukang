/*Given a directed graph, design an algorithm to find out whether there is a
  route between two nodes.*/
#include <iostream>
#include <list>
#include <vector>
#include <assert.h>
using namespace std;

class Graph {
    int V, E;
    vector<vector<int> > adj;

private:
    bool dfs(int u, int v, vector<bool>& visited) const {
        visited[u] = true;
        for(int i=0; i<adj[u].size(); i++) {
            int x = adj[u][i];
            if(x == v)
                return true;
            if(visited[x] == false) {
                if(dfs(x, v, visited))
                    return true;
            }
        }
        return false;
    }

public:
    Graph(int v): V(v) {
        adj.resize(v);
    }

    void addEdge(int u, int v);
    bool isReachable(int u, int v) const;
};

void Graph::addEdge(int u, int v) {
    adj[u].push_back(v);
}

bool Graph::isReachable(int u, int v) const {
    vector<bool> visited(V, false);
    return dfs(u, v, visited);
}

int main() {
    Graph graph(5);
    graph.addEdge(0, 4);
    graph.addEdge(4, 2);
    if(graph.isReachable(0, 2)) {
        std::cout << "Yes" << std::endl;
    } else {
        std::cout << "No" << std::endl;
    }

    if(graph.isReachable(0, 3)) {
        std::cout << "Yes" << std::endl;
    } else {
        std::cout << "No" << std::endl;
    }
    return 0;
}
