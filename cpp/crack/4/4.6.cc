/* Design an algorithm and write code to find the first common ancestor of two nodes
   in a binary tree. Avoid storing additional nodes in a data structure.
   NOTE: This is not necessarily a binary search tree.

   IF this is a binary tree, given node a and node b, we just need to find the first node (v)
   from root meet the requirement  a <= v <= b */
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <assert.h>
using namespace std;


struct Node {
    int value;
    struct Node* left;
    struct Node* right;
    struct Node* parent;
};

struct Node* newNode(int val) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = node->parent = NULL;
    node->value = val;
    return node;
}

static struct Node* _create(const vector<int>& vec, int start, int end) {
    if(start > end) {
        return NULL;
    }
    int mid  = (start + end)/2;
    struct Node* node = newNode(vec[mid]);
    node->left = _create(vec, start, mid - 1);
    node->right = _create(vec, mid+1, end);
    return node;
}

struct Node* createTree(const vector<int>& vec) {
    return _create(vec, 0, vec.size() - 1);
}

static void _visitIter(struct Node* node) {
    if(node == NULL) return;
    _visitIter(node->left);
    printf(" %d ", node->value);
    _visitIter(node->right);
}

void PrintTree(struct Node* root) {
    _visitIter(root);
}

struct Node* findNode(struct Node* node,  int value) {
    if(node == NULL) return NULL;
    if(node->value == value) return node;
    struct Node* t = 0;
    t = findNode(node->left, value);
    if(t) return t;
    t = findNode(node->right, value);
    if(t) return t;
    return NULL;
}

#define TWO  2
#define ONE  1
#define NONE 0

static int covers(struct Node* node, struct Node* p, struct Node* q) {
    int res = NONE;
    if(node == NULL) return res;
    if(node == p || node == q)
        res += 1;
    res += covers(node->left, p, q);
    if(res == TWO) return res;
    return res += covers(node->right, p, q);
}

struct Node* commonAcestor(struct Node* root, struct Node* p, struct Node* q) {
    if(root == NULL || p == NULL || q == NULL) return NULL;
    if(p == q && (root->left == p || root->right == p)) return root;
    int leftRes = covers(root->left, p, q);
    if(leftRes == TWO) {
        if(root->left == p || root->left == q)
            return root->left;
        return commonAcestor(root->left, p, q);
    }

    int rightRes = covers(root->right, p, q);
    if(rightRes == TWO) {
        if(root->right == p || root->right == q)
            return root->right;
        return commonAcestor(root->right, p, q);
    }

    if(rightRes == ONE && leftRes == ONE)
        return root;
    if(rightRes == ONE || leftRes == ONE) {
        if(root == p || root == q)
            return root;
    }
}

int main() {
    vector<int> vec;
    for(int i=0; i<10; i++) {
        vec.push_back(i);
    }

    struct Node* root = createTree(vec);
    PrintTree(root);
    printf("\n");

    struct Node* a = findNode(root, 9);
    struct Node* b = findNode(root, 6);
    struct Node* c = commonAcestor(root, a, b);
    printf("common Acestor: %d\n", c->value);
    return 0;
}
