
puts = function(args) {
    console.log(args);
}


var sayHi;

if(true) {
    sayHi = function sayHi(){
        alert("hi here from 1");
    };
} else {
    sayHi = function sayHi() {
        alert("hi here from 2");
    };
}

//sayHi();

function createCompareFunction(propertyName) {
    return function(obj1, obj2) {
        var value1 = obj1[propertyName];
        var value2 = obj2[propertyName];
        if(value1 < value2) {
            return -1;
        } else if (value1 > value2) {
            return 1;
        } else {
            return 0;
        }
    };
}


cmp = createCompareFunction("age");

function Person(name, age) {
    this.name = name;
    this.age = age;
}

Person.prototype = {
    constructor : Person,
    sayName: function() {
        alert(this.name);
    }
};

var person1 = new Person("xiao", 20);
var person2 = new Person("hello", 21);

//person1.sayName();
//alert(person1["age"]);

// if(cmp(person1, person2) < 0) {
//     alert("person2 is larger");
// } else if (cmp(person1, person2) > 0) {
//     alert("person1 is larger");
// } else {
//     alert("same age!");
// }


function factorial(num) {
    if (num <= 1) {
        return 1;
    } else {
        return num * factorial(num - 1);
    }
}


console.log(factorial(10));

function factorial_with_callee(num) {
    if (num <= 1) {
        return 1;
    } else {
        return num * aguments.callee(num-1);
    }
}

console.log(factorial(10));


function createArray() {
    var result = new Array();
    for(var i=0; i<10; i++) {
        result[i] = function() {
            return i;
        };
    }
    return result;
}

var res = createArray();
console.log(res[1]());


function createArray() {
    var result = new Array();
    for(var i=0; i<10; i++) {
        result[i] = function(num) {
            return function() {
                return num;
            };
        }(i);
    }
    return result;
}

var bar = createArray();
console.log(bar[1]());
console.log(bar[3]());

var demo = function(count) {
    for(var i = 0; i<count; i++) {
        console.log(i);
    }
};

function outputNumber(count) {
    demo(10);
    var i;
    console.log(i);
}

outputNumber(10);

var time_now = (function() {
    var date = new Date();
    var seconds = date.getTime();
    return seconds;
});


console.log(time_now());
console.log(time_now());

var book = {
    _year: 2004,
    edition:  1
};

Object.defineProperty(book, "year", {
    get: function() {
        return this._year;
    },
    set: function(newValue) {
        if(newValue >= 2004) {
            this._year = newValue;
            this.edition = newValue - 2004;
        }
    }
});

book.year = 2015;
console.log(book.year);
console.log(book.edition);

// book.__defineSetter__("edition",
//                       function(newValue) {
//                           this.edition = newValue;
//                       });

// book.edition = 3;
// console.log(book.edition);

var book_demo = {};
Object.defineProperties(book_demo, {
    _year: {
        value: 2004
    },
    edition: {
        value: 1
    },
    year: {
        get: function() {
            puts("now year_get");
            return this._year;
        },
        set: function(newValue) {
            puts("begin set");
            puts(newValue);
            puts(newValue > 2004);
            if(newValue > 2004) {
                this._year = newValue;
                this.edition = newValue - 2004;
                console.log("set now");
                puts(this._year);
                puts(this.edition);
            }
        }
    }
});


console.log(book_demo.year);
book_demo.year = 2009;
console.log(book_demo.year);
console.log(book_demo.edition);

// var descriptor = Object.getOwnPropertyDescriptor(book_demo, "_year");
// console.log(descriptor.value);
// console.log(descriptor.configurable);


function Person() {
}

Person.prototype.name = 'Nich';
Person.prototype.age = 29;
Person.prototype.job = "Software Engineer";
Person.prototype.sayName = function() {
    puts("here:" + this.name);
};

var person1 = new Person();
person1.sayName();

var o = {
    toString: function() {
        puts("now in toString");
    }
};

for(var prop in o) {
    console.log(prop);
}

puts(Object.keys(Person.prototype));
puts(Object.getOwnPropertyNames(Person.prototype));

function Person() {

}

var friend = new Person();

Person.prototype = {
    constructor: Person,
    name: 'Nich',
    age: 29,
    job: 'job title',
    sayName: function() {
        puts(this.name);
    }
};

friend.sayName();

String.prototype.startsWith = function(text) {
    return this.indexOf(text) == 0;
};

var msg = "This is demo";
puts(msg.startsWith("This"));
puts(msg.startsWith("No"));


function Person(name, age, job) {
    this.name = name;
    this.age = age;
    this.job = job;
    this.friends = ["Shelby", "Court"];
}

Person.prototype = {
    constructor: Person,
    sayName: function() {
        puts(this.name);
    }};

var person1 = new Person("Nich", 29, "software eng");
var person2 = new Person("Greg", 20, "haha");
person1.friends.push("Van");

puts(person1.friends);
puts(person2.friends);

var age = 29;
function sayAge() {
    puts(this.age);
}

