
var http = require('http');
var url = require('url');

function start(route) {
    console.log("starting server ...")
    function onRequest(request, response) {
	var path = url.parse(request.url).pathname

	route(path)
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.write("Hello World");
	response.end();
    }
    http.createServer(onRequest).listen(8888);
    console.log("server started ...")
}

exports.start = start

