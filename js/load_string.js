
function loadScriptString(code) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    try {
        script.appendChild(document.createTextNode(code));
    } catch(ex) {
        script.text = code;
    }
    document.body.appendChild(script);
}

var div = document.createElement("div");
div.id = "MyDiv";
div.className = "box";
var textNode = document.createTextNode("Hello world!");
div.appendChild(textNode);
document.body.appendChild(div);

var divs = document.getElementsByTagName("div"),
    i,
    div;

for(i=0, len=divs.length; i<len; i++) {
    div = document.createElement("div");
    document.body.appendChild(div);
    console.log("now");
}

