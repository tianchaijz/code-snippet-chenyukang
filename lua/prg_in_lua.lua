#!/usr/bin/env lua
function fact(n)
   if n == 0 then 
      return 1
   else
      return n * fact(n - 1)
   end
end

---[[ this is commend --]]


if 0 then
   print("yes")
else
   print("no")
end

print(math.sin(3))

table = {}
for x=1, 1000 do
   table[x] = x
end


x = 10
local i = 1
while i <= x do
   local x = i*2
   print(x)
   i = i + 1
end

-- if 1 then print("yes") else print("no") end

-- line = io.read()
-- print(line)

-- local line   
-- repeat
--    line = io.read()
-- until line ~= ""
-- print(line)
   
--print("enter a number:")
--a = io.read("*number")
--print(fact(a))

for i=1, 100, 4 do
   print(i)
end

print(os.date())

print([[ this is
	 a good test ]])

function unpack(list, idx)
   idx = idx or 1
   if list[idx] then
      return list[idx], unpack(list, idx+1)
   end
end

find = string.find
print(type(find))
a = {"Hello", "ll"}

print(type(unpack))
print(find(unpack(a)))

print("chen", "yukang")

function add(...)
   local s = 0
   for i, v in ipairs{...} do
      s = s + v
   end
   return s
end

print(add(1, 2, 3, 4))