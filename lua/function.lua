
function norm(x, y) 
   local n = x^2 + y^2
   return math.sqrt(n)
end

function twice(x) 
   return x*3
end

function test()
   line = io.read()
   print("message:" .. line)
   n = tonumber(line)
   if n == nil then
      error(line .. "is not a valid number")
   else
      print(n*2)
   end
end

function link_a_list()
   list = nil
   for line in io.lines() do
      v = tonumber(line)
      if v == nil then
	 break
      else
	 list = {next=list, value=v}
      end
   end
   return list
end

function print_list(list)
   l = list
   while l do
      print("value:" .. l.value)
      l = l.next
   end
end

function loop_demo()
   for i=10, 1, -1 do
      print("value:" .. i)
   end
end

function find_value_in_list(list, value)
   local res = nil
   l = list
   while l  do
      if l.value == value then
	 res = l
	 break
      else
	 l = l.next
      end
   end
   return res
end

revDays = {["Sunday"]=1, ["Monday"]=2,
	   ["Tuesday"]=3, ["Wednesday"]=4,
	   ["Thursday"]=5, ["Friday"]=6,
	   ["Staurday"]=7};


function print_days(days)
   print(days["Monday"])
   local x = days
   for i,v in ipairs(x) do
      print("x")
      print(i)
      print(x[i])
   end
end

function maximum(list) 
   local index = 1
   local max = list[index]
   for i, val in ipairs(list) do 
      if val > max then 
	 index = i
	 max = val
      end
   end
   return index, max
end




function create_matrix(N, M) 
   matrix = {}
   for i=1, N do 
      matrix[i] = {}
      for j=1, M do
	 matrix[i][j] = 0
      end
   end
   return matrix
end



   