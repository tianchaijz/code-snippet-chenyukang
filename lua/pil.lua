#!/usr/bin/evn lua

function fact(n)
   if n == 0 then
      return 1
   else
      return n * fact(n-1)
   end
end

days = {"Sunday", "Monday", "Tuesday", "Wednesday",
        "Thursday", "Friday", "Saturday"}

revDays = {["Sunday"] = 1,   ["Monday"] = 2,
           ["Tuesday"] = 3,  ["Wednesday"] = 4,
           ["Thursday"] = 5, ["Friday"] = 6,
           ["Saturday"] = 7}

function print_days(days)
   for k,v in ipairs(days) do
      revDays[v] = k
      print(revDays[v])
   end
end


function f(a, b)
   return a or b end

-- print(f(2, 4, 5))


function add_sum(list)
   local sum = 0
   for i,v in ipairs(list) do
      sum = sum+v
      print("Now:" .. sum)
   end
   return sum
end

function unpack(t, i)
   i = i or 1
   if t[i] then
      return t[i], unpack(t, i+1)
   end
end


function add (...)
   local s = 0
   for i, v in ipairs{...} do
      s = s + v
   end
   return s
end

   
network = {
  {name = "grauna",  IP = "210.26.30.34"},
  {name = "arraial", IP = "210.26.30.23"},
  {name = "lua",     IP = "210.26.23.12"},
  {name = "derain",  IP = "210.26.23.20"},
}

function newCounter()
   local i = 0
   return function()
      i = i+1
      return i
   end
end

function foo(n)
   if n > 0 then
      print("msg"..n)
      return foo(n-1) end
end


function iter()
   while true do
      print "enter your experssion:"
      local l = io.read()
      if l == nil then break end
      local func = assert(loadstring("return " .. l))
      res = func()
      if res == nil then
         print("the value of your expression is: nil")
      else
         print("the value of your expression is: nil" .. res)
      end
   end
end

