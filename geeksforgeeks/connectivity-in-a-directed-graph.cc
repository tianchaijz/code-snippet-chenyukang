//http://www.geeksforgeeks.org/connectivity-in-a-directed-graph/
#include <iostream>
#include <vector>
using namespace std;

class Graph {
private:
    int V;
    std::vector<std::vector<int> > adj;

public:
    Graph(int v):V(v) {
        adj.resize(v);
    }

    void addEdge(int u, int v) {
        adj[u].push_back(v);
    }

    bool isSCC() const;
    void dfs(int u, bool visited[]) const;

};


void Graph::dfs(int u, bool visited[]) const {
    visited[u] = true;
    for(int i=0; i<adj[u].size(); i++) {
        int v = adj[u][i];
        if(!visited[v])
            dfs(v, visited);
    }
}

bool Graph::isSCC() const {

    int u = 0;
    bool visited[V];
    memset(visited, false, sizeof(visited));
    dfs(u, visited);
    for(int i=0; i<V; i++) {
        if(!visited[i])
            return false;
    }
    memset(visited, false, sizeof(visited));
    Graph copy(V);
    for(int i=0; i<V; i++) {
        for(int j=0; j<adj[i].size(); j++) {
            copy.addEdge(adj[i][j], i);
        }
    }
    copy.dfs(u, visited);
    for(int i=0; i<V; i++) {
        if(!visited[i])
            return false;
    }
    return true;
}

int main() {
    // Create graphs given in the above diagrams
    Graph g1(5);
    g1.addEdge(0, 1);
    g1.addEdge(1, 2);
    g1.addEdge(2, 3);
    g1.addEdge(3, 0);
    g1.addEdge(2, 4);
    g1.addEdge(4, 2);
    g1.isSCC()? cout << "Yes\n" : cout << "No\n";

    Graph g2(4);
    g2.addEdge(0, 1);
    g2.addEdge(1, 2);
    g2.addEdge(2, 3);
    g2.isSCC()? cout << "Yes\n" : cout << "No\n";

    return 0;
}
