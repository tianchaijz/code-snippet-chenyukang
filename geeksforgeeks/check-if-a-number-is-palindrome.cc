#include <iostream>
#include <vector>
using namespace std;


bool isPalindrome(int value) {
    int r = 0;
    int t = value;
    while(t) {
        r = r*10 + t%10;
        t/=10;
    }
    if(r == value)
        return true;
    return false;
}

void test(int value) {
    if(value < 0)
        value = -value;
    if(isPalindrome(value)) {
        std::cout << "Yes" << std::endl;
    } else {
        std::cout << "No" << std::endl;
    }
}

int main() {
    test(1);
    test(121);
    test(13);
    test(12321);
    test(12);
    test(88);
    test(8999);
    return 0;
}
