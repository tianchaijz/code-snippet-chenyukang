#include <iostream>
#include <string>
#include <list>
#include <stack>
#include <vector>

using namespace std;

#define INF 0x7fffffff

struct Edge{
    int v;
    int weight;
    Edge(int v, int w):v(v), weight(w) {}
};

class Graph {
    int V;
    list<struct Edge>* adj;
    void _topological_sort(int v, bool visited[], stack<int>& stack);
public:
    Graph(int v);
    ~Graph();
    void addEdge(int u, int v, int w);
    void TopologicalSort(stack<int>& stack);
    void shortestPath(int v);
};

Graph::Graph(int v):V(v) {
    adj = new list<struct Edge>[V];
}

Graph::~Graph() {
    delete [] adj;
}

void Graph::addEdge(int u, int v, int w) {
    adj[u].push_back(Edge(v, w));
}

void Graph::_topological_sort(int v, bool visited[], stack<int>& stack) {
    visited[v] = true;
    for(list<struct Edge>::iterator it = adj[v].begin(); it != adj[v].end(); ++it) {
        int u = it->v;
        if(visited[u] == false)
            _topological_sort(u, visited, stack);
    }
    stack.push(v);
}


void Graph::TopologicalSort(stack<int>& stack) {
    bool visited[V];
    for(int i=0; i<V; i++)
        visited[i] = false;
    for(int i=0; i<V; i++) {
        if(visited[i] == false) {
            _topological_sort(i, visited, stack);
        }
    }
}

void Graph::shortestPath(int v) {
    stack<int> st;
    int dist[V];

    TopologicalSort(st);

    for(int i=0; i<V; i++)
        dist[i] = INF;
    dist[v] = 0;

    while(!st.empty()) {
        int u = st.top();
        st.pop();
        if(dist[u] == INF)
            continue;
        for(list<struct Edge>::iterator it = adj[u].begin();
            it != adj[u].end(); ++it) {
            int v = it->v;
            int w = it->weight;
            if(dist[v] > dist[u] + w) {
                dist[v] = dist[u] + w;
            }
        }
    }
    for(int i=0; i<V; i++) {
        if(dist[i] == INF)
            printf("INF ");
        else
            printf("%d ", dist[i]);
    }
    printf("\n");
}

int main() {
    Graph g(6);
    g.addEdge(0, 1, 5);
    g.addEdge(0, 2, 3);
    g.addEdge(1, 3, 6);
    g.addEdge(1, 2, 2);
    g.addEdge(2, 4, 4);
    g.addEdge(2, 5, 2);
    g.addEdge(2, 3, 7);
    g.addEdge(3, 4, -1);
    g.addEdge(4, 5, -2);

    int s = 1;
    cout << "Following are shortest distances from source " << s <<" \n";
    g.shortestPath(s);
    return 0;

}
