//C treat 'a' as integer, but C++ treat 'a' as character
//clang:
//warning: treating 'c' input as 'c++' when in C++ mode, this behavior is deprecated
#include <stdio.h>

int main() {
    printf("%lu\n", sizeof('a'));
    return 0;
}
