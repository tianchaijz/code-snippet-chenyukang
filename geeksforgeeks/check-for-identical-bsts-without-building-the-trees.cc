#include <stdio.h>
#include <stdlib.h>


int isSameBST(int arr1[], int arr2[], int len) {
    return 0;
}

// Driver program to test above functions
int main()
{
    int a[] = {8, 3, 6, 1, 4, 7, 10, 14, 13};
    int b[] = {8, 10, 14, 3, 6, 4, 1, 7, 13};
    int n = sizeof(a)/sizeof(a[0]);
    printf("%s\n", isSameBST(a, b, n)?
           "BSTs are same":"BSTs not same");
    return 0;
}
