#include <iostream>
#include <queue>
using namespace std;


struct Node {
    struct Node* left;
    struct Node* right;
    int data;
};

struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = 0;
    return node;
}


int treeHeight_iter(struct Node* node, int depth) {
    if(node == 0) return depth;
    return max(treeHeight_iter(node->left, depth + 1),
               treeHeight_iter(node->right, depth + 1));
}

int treeHeight_recursive(struct Node* root) {
    if(root == 0) {
        return 1;
    }
    return max(treeHeight_iter(root->left, 1),
               treeHeight_iter(root->right, 1));
}

int treeHeight(struct Node* root) {
    queue<struct Node*> Q;
    Q.push(root);
    int height = 0;
    while(1) {
        int count = Q.size();
        if(count == 0) {
            return height;
        }
        height++;
        while(count) {
            struct Node* p = Q.front();
            Q.pop();
            count--;
            if(p->left)
                Q.push(p->left);
            if(p->right)
                Q.push(p->right);
        }
    }
    return 0;
}

int main() {
    // Let us create binary tree shown in above diagram
    struct Node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);

    cout << "Height of tree is " << treeHeight(root) << std::endl;
    cout << "Height of tree is " << treeHeight_recursive(root) << std::endl;
    return 0;

}
