#include <stdio.h>
#include <iostream>
using namespace std;

#define MAX 200

char table[] = "0123456789abcdefghijklmnopqrstuvwxyz";

void revstr(char* str) {
    int i = 0;
    int j = strlen(str) -1;
    while(i < j) {
        char t = str[i];
        str[i] = str[j];
        str[j] = t;
        i++, j--;
    }
}

char* itoa(int value, char* buf, int base) {
    bool neg = false;
    int cnt = 0;
    if(base == 10 && value < 0) {
        neg = true;
        value = -value;
    }
    if(value == 0) {
        buf[0] = '0';
        buf[1] = '\0';
        return buf;
    }

    while(value) {
        buf[cnt++] = table[value%base];
        value /= base;
    }
    if(neg)
        buf[cnt++] = '-';
    buf[cnt] = '\0';
    revstr(buf);
    return buf;
}


int main() {
    char str[MAX];
    memset(str, 0, sizeof(str));

    int value = 1567;
    cout << "Base: 10 -> " << itoa(value, str, 10) << std::endl;
    cout << "Base: 10 -> " << itoa(-value, str, 10) << std::endl;
    cout << "Base: 2 -> " << itoa(value, str, 2) << std::endl;
    cout << "Base: 8 -> " << itoa(value, str, 8) << std::endl;
    cout << "Base: 16 -> " << itoa(value, str, 16) << std::endl;
    return 0;
}
