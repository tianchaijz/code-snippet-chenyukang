//http://www.geeksforgeeks.org/increasing-subsequence-of-length-three-with-maximum-product/
//not a right solution

#include <iostream>
#include <stdio.h>
#include <assert.h>
using namespace std;


void printArr(int arr[], int len) {
    for(int i=0; i<len; i++) {
        printf(" %2d ", arr[i]);
    }
    printf("\n");
}

void ans(int arr[], int len) {
    int LGL[len];
    int LGR[len];

    printArr(arr, len);

    //========================================
    //fill LGR from right to left
    LGR[len - 1] = -1;
    for(int i = len -2; i>=0; i--) {
        if(arr[i+1] > arr[i]) {
            if(arr[i+1] > LGR[i+1])
                LGR[i] = arr[i+1];
            else
                LGR[i] = LGR[i+1];
        } else {
            if(arr[i] >= LGR[i+1])
                LGR[i] = -1;
            else
                LGR[i] = LGR[i+1];
        }
    }

    printArr(LGR, len);

    //========================================
    //fill LGL from right to left
    LGL[0] = -1;
    for(int i=1; i<len; i++) {
        int max = -1;
        for(int k=0; k<i; k++) {
            if(arr[k] < arr[i] && arr[k] > max)
                max = arr[k];
        }
        LGL[i] = max;
    }
    printArr(LGL, len);

    int max = -1;
    int v1, v2, v3;
    for(int i=1; i < len - 1; i++) {
        int v = LGR[i] * arr[i] * LGL[i];
        if( v > max ) {
            max = v;
            v1 = LGL[i], v2 = arr[i], v3 = LGR[i];
        }
    }
    printf("ans: %d\n", max);
    printf("%d %d %d\n", v1, v2, v3);
}

int main() {
    //int arr[] = {6, 7, 8, 1, 9, 2, 3, 10};
    int arr[] = {1, 5, 10, 8, 9};
    int len = sizeof(arr) / (sizeof(int));
    ans(arr, len);
    return 0;
}
