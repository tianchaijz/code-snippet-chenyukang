#include <iostream>
using namespace std;

#define N 4


bool prefer_this(int wprefer[N][N], int woman, int preman, int man) {
    for(int i=0; i<N; i++) {
        if(wprefer[woman][i] == preman)
            return false;
        if(wprefer[woman][i] == man)
            return true;
    }
    return false;
}

void stableMarriage(int mprefer[N][N], int wprefer[N][N]) {
    int wpartner[N];
    bool mfree[N];

    memset(wpartner, -1, sizeof(wpartner));
    memset(mfree, true, sizeof(mfree));

    int freecount = N;
    while(freecount) {
        int man = -1;
        for(int i=0; i<N; i++) {
            if(mfree[i] == true) {
                man = i;
                break;
            }
        }

        for(int i=0; i<N && mfree[man] == true; i++) {
            int woman = mprefer[man][i];
            woman -= N;
            if(wpartner[woman] == -1) {
                freecount--;
                mfree[man] = false;
                wpartner[woman] = man;
                printf("set woman:%d man: %d\n", woman + N, man);
            } else {
                int preman = wpartner[woman];
                if(prefer_this(wprefer, woman, preman, man)) {
                    mfree[man] = false;
                    wpartner[woman] = man;
                    mfree[preman] = true;
                }
            }
        }
    }

    for(int i=0; i<N; i++) {
        printf("Gril: %d Boy: %d\n", i+N, wpartner[i]);
    }
}

// Driver program to test above functions
int main() {
    int mprefer[N][N] = { {7, 5, 6, 4},
                          {5, 4, 6, 7},
                          {4, 5, 6, 7},
                          {4, 5, 6, 7}};

    int wprefer[N][N] = { {0, 1, 2, 3},
                          {0, 1, 2, 3},
                          {0, 1, 2, 3},
                          {0, 1, 2, 3}};

    stableMarriage(mprefer, wprefer);
    return 0;
}
