//http://www.geeksforgeeks.org/convert-a-given-binary-tree-to-doubly-linked-list-set-2/
//covert a binary tree to double link list
#include <iostream>
#include <stdio.h>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

// A utility function to create a new tree node
struct Node *newNode(int data) {
    struct Node *node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = NULL;
    return(node);
}

// Standard Inorder traversal of tree
void inorder(struct Node *root) {
    if (root != NULL) {
        inorder(root->left);
        printf("\t%d",root->data);
        inorder(root->right);
    }
}

struct Node* fixLeft(struct Node* root) {
    static struct Node* cur = 0;
    struct Node* l = root;
    if(root->left) {
        fixLeft(root->left);
    }
    root->left = cur;
    cur = root;
    if(root->right) {
        l = fixLeft(root->right);
    }
    return l;
}

struct Node* fixRight(struct Node* node) {
    struct Node* last = node;
    struct Node* prev = node->left;
    while(prev != NULL) {
        prev->right = last;
        prev = prev->left;
        last = last->left;
    }
    return last;
}


struct Node* BTToDLL(struct Node* root) {
    struct Node* last = fixLeft(root);
    return fixRight(last);
}

// Traverses the DLL from left tor right
void printList(struct Node *root) {
    while (root != NULL) {
        printf("\t%d", root->data);
        root = root->right;
    }
    printf("\n");
}

// Driver program to test above functions
int main(void) {
    // Let us create the tree shown in above diagram
    struct Node *root = newNode(10);
    root->left        = newNode(12);
    root->right       = newNode(15);
    root->left->left  = newNode(25);
    root->left->right = newNode(30);
    root->right->left = newNode(36);

    printf("\n\t\tInorder Tree Traversal\n\n");
    inorder(root);

    struct Node *head = BTToDLL(root);

    printf("\n\n\t\tDLL Traversal\n\n");
    printList(head);
    return 0;
}
