//http://www.geeksforgeeks.org/counting-sort/
//time complexity: O(n+K), where n is the number of elements and K is the range of number

#include <stdio.h>
#include <stdlib.h>


void countSort(int* arr, int len) {
    int i;
    int max = arr[0];
    for(i=1; i<len; i++) {
        if(max < arr[i])
            max = arr[i];
    }
    int count[max+1];
    int output[len];

    for(i=0; i<= max; i++) {
        count[i] = 0;
    }

    for(i=0; i<len; i++) {
        count[arr[i]]++;
    }
    for(i=1; i<= max; i++) {
        count[i] += count[i-1];
    }

    for(i=len-1; i>=0; i--) {
        int v = arr[i];
        output[count[v]-1] = v;
        count[v]--;
    }
    for(i=0; i<len; i++) {
        arr[i] = output[i];
    }
}


int main() {
    int i;
    int arr[] = {170, 45, 75, 90, 802, 24, 2, 66, 1};
    int len = sizeof(arr)/sizeof(int);
    for(i=0; i<len; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    countSort(arr, len);
    for(i=0; i<len; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return 0;
}
