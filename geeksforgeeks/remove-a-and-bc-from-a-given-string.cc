#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
using namespace std;


int stringFilter(char str[]) {
    int len = strlen(str);
    int k = 0;
    bool preva = false;
    for(int i=0; i<len; i++) {
        if(str[i] == 'c') {
            if(preva == false) {
                str[k++] = 'c';
            }
            preva = false;
        }
        else if(str[i] != 'a' && str[i] != 'b') {
            if(preva)
                str[k++] = 'a';
            str[k++] = str[i];
            preva = false;
        } else {
            if(str[i] == 'a') {
                if(preva)
                    str[k++] = 'a';
                preva = true;
            }
        }
    }
    if(preva)
        str[k++] = 'a';
    str[k] = '\0';
    return 0;
}

int main() {
    char str1[] = "ad";
    stringFilter(str1);
    cout << str1 << endl;

    char str2[] = "acbac";
    stringFilter(str2);
    cout << str2 << endl;

    char str3[] = "aaac";
    stringFilter(str3);
    cout << str3 << endl;

    char str4[] = "react";
    stringFilter(str4);
    cout << str4 << endl;

    char str5[] = "aa";
    stringFilter(str5);
    cout << str5 << endl;

    char str6[] = "ababaac";
    stringFilter(str6);
    cout << str6 << endl;

    return 0;
}
