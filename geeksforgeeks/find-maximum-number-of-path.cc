//http://www.geeksforgeeks.org/find-edge-disjoint-paths-two-vertices/
// this is not a good solution

#include <stdio.h>
#include <iostream>
using namespace std;

#define MAX 8

int visited[MAX];

int traverse(int graph[MAX][MAX], int v, int t) {
    if(v == t) {
        return 1;
    }
    for(int k=0; k<MAX; k++) {
        if(graph[v][k] == 1 ) {
            graph[v][k] = 2;
            if(traverse(graph, k, t))
                return 1;
            graph[v][k] = 1;
        }
    }
    return 0;
}

int max_path(int graph[MAX][MAX], int s, int t) {
    int ans = 0;
    memset(visited, 0, sizeof(visited));

    for(int i=0; i<MAX; i++) {
        for(int j=0; j<MAX; j++) {
            printf(" %d ", graph[i][j]);
        }
        printf("\n");
    }

    printf("=================================\n");
    for(int k=0; k<MAX; k++) {
        if(graph[s][k]) {
            graph[s][k] = 2;
            if(traverse(graph, k, t))
                ans++;
            else
                graph[s][k] = 1;
        }
    }

    for(int i=0; i<MAX; i++) {
        for(int j=0; j<MAX; j++) {
            printf(" %d ", graph[i][j]);
        }
        printf("\n");
    }

    return ans;
}

int main() {
    // Let us create a graph shown in the above example
    int graph[MAX][MAX] = { {0, 1, 1, 1, 0, 0, 0, 0},
                        {0, 0, 1, 0, 0, 0, 0, 0},
                        {0, 0, 0, 1, 0, 0, 1, 0},
                        {0, 0, 0, 0, 0, 0, 1, 0},
                        {0, 0, 1, 0, 0, 0, 0, 1},
                        {0, 1, 0, 0, 0, 0, 0, 1},
                        {0, 0, 0, 0, 0, 1, 0, 1},
                        {0, 0, 0, 0, 0, 0, 0, 0}
    };

    int s = 0;
    int t = 7;

    printf("ans: %d\n", max_path(graph, s, t));
    return 0;
}
