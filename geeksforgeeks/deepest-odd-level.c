/*******************************************************************************
 *
 *      deepest-odd-level.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-11-16 09:51:28
 *
 *******************************************************************************/

// C program to find depth of the deepest odd level leaf node
#include <stdio.h>
#include <stdlib.h>

// A utility function to find maximum of two integers
int max(int x, int y) { return (x > y)? x : y; }

// A Binary Tree node
struct Node
{
    int data;
    struct Node *left, *right;
};

// A utility function to allocate a new tree node
struct Node* newNode(int data)
{
    struct Node* node = (struct Node*) malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = NULL;
    return node;
}

int depthOfOddLeaf_iter(struct Node* root, int depth) {
    if(root == 0) {
        return 0;
    }
    if(root->left == 0 && root->right == 0 && depth%2 == 1) {
        return depth;
    }
    return max(depthOfOddLeaf_iter(root->left, depth+1),
               depthOfOddLeaf_iter(root->right, depth+1));
}


int depthOfOddLeaf(struct Node* root) {
    int depth = 1;
    if(root == 0) {
        return 0;
    }
    return depthOfOddLeaf_iter(root, depth);
}

// Driver program to test above functions
int main()
{
    struct Node* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->right->left = newNode(5);
    root->right->right = newNode(6);
    root->right->left->right = newNode(7);
    root->right->right->right = newNode(8);
    root->right->left->right->left = newNode(9);
    root->right->right->right->right = newNode(10);
    root->right->right->right->right->left = newNode(11);

    printf("%d is the required depth\n", depthOfOddLeaf(root));
    return 0;
}
