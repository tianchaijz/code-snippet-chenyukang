//http://www.geeksforgeeks.org/merge-a-linked-list-into-another-linked-list-at-alternate-positions/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

struct Node {
    int data;
    struct Node* next;
};

struct Node* newNode(int data) {
    struct Node* n = (struct Node*)malloc(sizeof(struct Node));
    n->data = data;
    n->next = 0;
    return n;
}

void addNode(struct Node** head, int data) {
    if(*head == 0) {
        *head = newNode(data);
    } else {
        struct Node* node = newNode(data);
        node->next = *head;
        *head = node;
    }
}

void merge(struct Node* p, struct Node** q) {
    struct Node* pp = p;
    struct Node* qq = *q;
    struct Node* cur_q;
    while(pp && qq) {
        cur_q = qq;
        qq = qq->next;
        cur_q->next = pp->next;
        pp->next = cur_q;
        pp = cur_q->next;
    }
    if(cur_q)
        *q = qq;
}

void Print(struct Node* head) {
    struct Node* node = head;
    while(node) {
        printf(" %d ", node->data);
        node = node->next;
    }
    printf("\n");
}

int main() {
    struct Node* p = 0;
    struct Node* q = 0;
    addNode(&p, 3);
    addNode(&p, 2);
    addNode(&p, 1);
    printf("first link list:\n");
    Print(p);


    addNode(&q, 8);
    addNode(&q, 7);
    addNode(&q, 6);
    addNode(&q, 5);
    addNode(&q, 4);
    printf("second link list:\n");
    Print(q);

    merge(p, &q);

    printf("first link list:\n");
    Print(p);

    printf("second link list:\n");
    Print(q);

    struct Node* q1 = 0;
    struct Node* q2 = 0;
    addNode(&q1, 1);

    addNode(&q2, 3);
    addNode(&q2, 2);

    merge(q1, &q2);
    printf("q1:\n");
    Print(q1);
    printf("q2:\n");
    Print(q2);
    return 0;
}
