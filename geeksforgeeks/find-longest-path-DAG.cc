//http://www.geeksforgeeks.org/find-longest-path-directed-acyclic-graph/
//the overall complexity is : O(V+E)
#include <iostream>
#include <stdio.h>
#include <list>
#include <stack>
#include <vector>
using namespace std;

#define NINF INT_MIN

class Edge {
public:
    int v;
    int w;
    Edge(int _v, int _w): v(_v), w(_w) {}
};

class Graph {
    int V;
    list<Edge>* adj;
    void _topological_sort(int v, bool visited[], stack<int>& stack);
    void TopologicalSort(vector<int>& res);
public:
    Graph(int v);
    ~Graph();
    void addEdge(int u, int v, int weight);
    void LongestPath(int s);
};

Graph::Graph(int v):V(v) {
    adj = new list<Edge>[V];
}

Graph::~Graph() {
    delete [] adj;
}

void Graph::addEdge(int u, int v, int w) {
    adj[u].push_back(Edge(v, w));
}

void Graph::_topological_sort(int v, bool visited[], stack<int>& stack) {
    visited[v] = true;
    for(list<Edge>::iterator it = adj[v].begin(); it != adj[v].end(); ++it) {
        int v = it->v;
        if(visited[v] == false)
            _topological_sort(v, visited, stack);
    }
    stack.push(v);
}

void Graph::TopologicalSort(vector<int>& res) {
    bool visited[V];
    stack<int> stack;
    for(int i=0; i<V; i++)
        visited[i] = false;
    for(int i=0; i<V; i++) {
        if(visited[i] == false) {
            _topological_sort(i, visited, stack);
        }
    }
    while(!stack.empty()) {
        int v = stack.top();
        res.push_back(v);
        stack.pop();
    }
    std::cout << std::endl;
}

void Graph::LongestPath(int s) {
    vector<int> topoRes;
    TopologicalSort(topoRes);
    int dist[V];
    for(int i=0; i<V; i++) {
        dist[i] = NINF;
    }
    dist[s] = 0;
    std::cout<< "Topological Sort: " << std::endl;
    for(int i=0; i<topoRes.size(); ++i) {
        std::cout << topoRes[i] << " ";
    }
    std::cout<< std::endl;

    for(int i=0; i<topoRes.size(); ++i) {
        int u = topoRes.at(i);
        if(dist[u] != NINF) {
            for(list<Edge>::iterator it = adj[u].begin(); it != adj[u].end(); ++it) {
                Edge e = *it;
                if(dist[e.v] < dist[u] + e.w) {
                    dist[e.v] = dist[u] + e.w;
                }
            }
        }
    }

    std::cout << std::endl;
    for(int i=0; i<V; i++) {
        if(dist[i] == NINF)
            std::cout << "INF" << " ";
        else
            std::cout << dist[i] << " ";
    }
    std::cout << std::endl;
}

int main() {
    Graph g(6);
    g.addEdge(0, 1, 5);
    g.addEdge(0, 2, 3);
    g.addEdge(1, 3, 6);
    g.addEdge(1, 2, 2);
    g.addEdge(2, 4, 4);
    g.addEdge(2, 5, 2);
    g.addEdge(2, 3, 7);
    g.addEdge(3, 5, 1);
    g.addEdge(3, 4, -1);
    g.addEdge(4, 5, -2);

    cout << "Following is topological sort result: \n";
    g.LongestPath(1);
    return 0;
}
