/*******************************************************************************
 *
 *      print-postorder-from-given-inorder-and-preorder-traversals.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-11-16 10:01:32
 *
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int find(int arr[], int key, int len) {
    int i;
    for(i=0; i<len; i++) {
        if(arr[i] == key)
            return i;
    }
    return -1;
}


void printPostOrder(int in[], int pre[], int n) {
    int root = pre[0];
    int index = find(in, root, n);
    //we have left subtree
    if(index > 0) {
        printPostOrder(in, pre + 1, index);
    }
    //we have right substree
    if(index >=0 && index != n -1) {
        printPostOrder(in + index + 1, pre + index + 1, n-index-1);
    }
    printf(" (%d) ", root);
}


// Driver program to test above functions
int main()
{
    int in[] = {4, 2, 5, 1, 3, 6};
    int pre[] =  {1, 2, 4, 5, 3, 6};
    int n = sizeof(in)/sizeof(in[0]);
    printf("Postorder traversal \n");
    printPostOrder(in, pre, n);
    printf("\n");
    return 0;
}
