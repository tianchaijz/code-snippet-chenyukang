//http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion-and-without-stack/
//Inorder Tree traverse without recursion and without stack

#include <stdio.h>
#include <iostream>
using namespace std;

struct Node {
    int v;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->v = data;
    node->left = node->right = 0;
    return node;
}


int MorrisTraversal(struct Node* root) {
    if(root == 0) {
        return 1;
    }
    struct Node* current = root;
    struct Node* pre;
    while(current) {
        if(current->left == 0) {
            printf(" %d ", current->v);
            current = current->right;
        } else {
            pre = current->left;
            while(pre->right && pre->right != current) {
                pre = pre->right;
            }

            if(pre->right == 0) {
                pre->right = current;
                current = current->left;
            } else { //fix the right child of predessor
                pre->right = NULL;
                printf(" %d ", current->v);
                current = current->right;
            }
        }
    }
    return 0;
}

int main() {
    /* Constructed binary tree is
       1
       /   \
       2      3
       /  \
       4     5
    */
    struct Node *root = newNode(1);
    root->left        = newNode(2);
    root->right       = newNode(3);
    root->left->left  = newNode(4);
    root->left->right = newNode(5);

    MorrisTraversal(root);
    return 0;
}
