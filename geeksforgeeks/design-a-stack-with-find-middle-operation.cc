#include <iostream>
#include <stdlib.h>

using namespace std;

struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
};

class Stack {
public:
    Stack():head(NULL), mid(NULL), count(0) {}
    ~Stack();
    void push(int data);
    int pop();
    struct Node* top();
    struct Node* findMid();
    void Print();

private:
    struct Node* head;
    struct Node* mid;
    int count;
};


Stack::~Stack() {
    struct Node* t = head;
    struct Node* n;
    while(t) {
        n = t->next;
        free(t);
        t = n;
    }
}

void Stack::push(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->prev = node->next = 0;
    node->next = this->head;
    if(this->head) {
        this->head->prev = node;
    }
    this->count++;
    this->head = node;
    if(this->mid == NULL)
        this->mid = this->head;
    else if(this->count%2) { //move mid up
        mid = this->mid->prev;

    }
}

int Stack::pop() {
    if(this->count == 0)
        return -1;
    struct Node* t = this->head;
    this->head = t->next;
    if(this->count%2) { //move mid down
        this->mid = this->mid->next;
    }
    this->count--;
    free(t);
    return 0;
}

struct Node* Stack::top() {
    if(this->count == 0) {
        return 0;
    } else {
        return this->head;
    }
}


struct Node* Stack::findMid() {
    if(this->count == 0)
        return 0;
    else {
        return this->mid;
    }
}

void Stack::Print() {
    struct Node* t = this->head;
    printf("vals(%d): ", this->count);
    while(t) {
        printf("%d ", t->data);
        t = t->next;
    }
    printf("\nmid: ");
    if(this->mid) {
        printf("%d\n", this->mid->data);
    } else {
        printf(" NULL\n");
    }
}

int main() {
    Stack* st = new Stack();
    st->push(1);
    st->Print();
    st->push(2);
    st->Print();
    st->push(3);
    st->push(4);
    st->Print();
    st->push(5);
    st->Print();
    for(int i=6; i<20; i++) {
        st->push(i);
        st->Print();
    }
    st->Print();
    st->pop();
    st->Print();
    st->pop();
    st->Print();
    st->pop();
    st->Print();
    for(int i=16; i>0; i--) {
        st->pop();
        st->Print();
    }
    delete st;
    return 0;

}
