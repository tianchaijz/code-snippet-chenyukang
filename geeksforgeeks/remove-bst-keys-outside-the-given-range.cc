#include <vector>
#include <iostream>
using namespace std;

class Node{
public:
    int value;
    Node* left;
    Node* right;

    Node(int v): value(v), left(NULL), right(NULL) {}
};

class BinTree {
    Node* root;

private:
    Node* _insert(Node* root, int value) {
        if(root == NULL) {
            return new Node(value);
        }
        else if(root->value > value)
            root->left = _insert(root->left, value);
        else
            root->right = _insert(root->right, value);
        return root;
    }

    void _inorder(Node* root) {
        if(root == NULL) return;
        _inorder(root->left);
        printf(" %d ", root->value);
        _inorder(root->right);
    }

    Node* _remove(Node* root, int min, int max) {
        if(root == NULL) return NULL;
        root->left = _remove(root->left, min, max);
        root->right = _remove(root->right, min, max);
        Node* ret;
        if(root->value > max) {
            ret = root->left;
            delete root;
            return ret;
        }
        if(root->value < min) {
            ret = root->right;
            delete root;
            return ret;
        }
        return root;
    }

public:
    BinTree(): root(NULL) { }

    void insert(int value) {
        root = _insert(root, value);
    }

    void inorderTraversal() {
        _inorder(root);
        std::cout<< std::endl;
    }

    void removeOutsideRange(int min, int max) {
        root = _remove(root, min, max);
    }
};

int main() {
    BinTree tree;
    tree.insert(6);
    tree.insert(-13);
    tree.insert(14);
    tree.insert(-8);
    tree.insert(15);
    tree.insert(13);
    tree.insert(7);

    cout << "Inorder traversal of the given tree is: ";
    tree.inorderTraversal();

    tree.removeOutsideRange(-10, 13);

    cout << "Inorder traversal of the modified tree is: ";
    tree.inorderTraversal();
    return 0;
}
