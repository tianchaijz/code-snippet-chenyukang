#include <iostream>
#include <stdlib.h>
using namespace std;


void printCombination_iter(int arr[], int start, int len, int out[], int now, int max) {
    //printf("start: %d now: %d max: %d\n", start, now, max);
    if(now >= max) {
        for(int i=0; i<max; i++) {
            printf(" %d ", out[i]);
        }
        printf("\n");
        return;
    }
    for(int i=start; i<len; i++) {
        out[now] = arr[i];
        printCombination_iter(arr, i+1, len, out, now+1, max);
    }
}


void printCombination(int arr[], int len, int r) {
    int out[len];
    memset(out, 0, sizeof(out));
    for(int i=0; i<=len - r; i++) {
        out[0] = arr[i];
        printCombination_iter(arr, i+1, len, out, 1, r);
    }
}

// Driver program to test above functions
int main()
{
    int arr[] = {1, 2, 3, 4, 5};
    int r = 3;
    int n = sizeof(arr)/sizeof(arr[0]);
    printCombination(arr, n, r);
}
