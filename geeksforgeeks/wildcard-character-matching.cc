#include <iostream>
#include <string>
#include <vector>
using namespace std;


bool match(const char* pattern, const char* str) {
    if(*pattern == '\0' && *str == '\0')
        return true;
    if(*pattern == '\0' && *str != '\0')
        return false;
    if(*str == '\0' && *pattern != '\0') {
        if(*(pattern+1) == '\0' &&
           *pattern == '*') {
            return true;
        }
        return false;
    }
    if(*pattern == '*') {
        return match(pattern+1, str) ||
            match(pattern, str+1);
    }
    if(*pattern == '?' || (*pattern == *str)) {
        return match(pattern+1, str+1);
    }
    return false;
}


void test(string pattern, string str) {
    if(match(pattern.c_str(), str.c_str())) {
        std::cout << "Yes" << std::endl;
    }
    else {
        std::cout<< "No" << std::endl;
    }
}


int main() {
    test("g*ks", "geeks"); // Yes
    test("ge?ks*", "geeksforgeeks"); // Yes
    test("g*k", "gee");  // No because 'k' is not in second
    test("*pqrs", "pqrst"); // No because 't' is not in first
    test("abc*bcd", "abcdhghgbcd"); // Yes
    test("abc*c?d", "abcd"); // No because second must have 2 instances of 'c'
    test("*c*d", "abcd"); // Yes
    test("*?c*d", "abcd"); // Yes
    return 0;
}
