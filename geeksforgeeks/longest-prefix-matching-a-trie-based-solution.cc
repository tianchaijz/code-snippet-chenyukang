#include <iostream>
#include <stdio.h>
#include <map>
#include <vector>
using namespace std;

#define MAX 26

class TrieNode {
    bool end;
    TrieNode* children[MAX];

public:
    TrieNode(): end(false) {
        for(int i=0; i<MAX; i++) {
            children[i] = 0;
        }
    }

    ~TrieNode() {
        for(int i=0; i<MAX; i++) {
            delete(children[i]);
        }
    }

    void setEnd() { end = true; }
    bool isEnd() { return end; }
    TrieNode* get(char c) { return children[c - 'a']; }
    TrieNode* add(char c) {
        int idx = c - 'a';
        children[idx] = new TrieNode();
        return children[idx];
    }
};

class Trie {
    TrieNode* root;
public:
    Trie() : root(NULL) { root = new TrieNode(); }
    ~Trie() { delete root; }

    void insert(string s);
    string getPrefix(string s);
};

void Trie::insert(string s) {
    TrieNode* cur = root;
    int i;
    for(i=0; i<s.size(); i++) {
        char c = s[i];
        if(cur->get(c) == 0) {
            cur = cur->add(c);
        } else {
            cur = cur->get(c);
        }
    }
    cur->setEnd();
}

string Trie::getPrefix(string s) {
    TrieNode* cur = root;
    string res;
    string now;
    for(int i=0; i<s.size(); i++) {
        char c = s[i];
        if(cur->get(c) != 0) {
            cur = cur->get(c);
            now.push_back(c);
            if(cur->isEnd() && now.size() > res.size()) {
                res = now;
                now = "";
            }
        } else {
            return res;
        }
    }
    return res;
}


int main() {
    Trie tree;
    vector<string> strs;
    strs.push_back("are");
    strs.push_back("area");
    strs.push_back("base");
    strs.push_back("cat");
    strs.push_back("cater");
    strs.push_back("children");
    strs.push_back("basement");
    for(int i=0; i<strs.size(); i++) {
        tree.insert(strs[i]);
    }

    string res = tree.getPrefix("caterer");
    std::cout << res << std::endl;

    res = tree.getPrefix("basemexy");
    std::cout << res << std::endl;

    res = tree.getPrefix("child");
    std::cout << res << std::endl;

    return 0;
}
