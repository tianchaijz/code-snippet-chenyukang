/*******************************************************************************
 *
 *      c-good-c++-error.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-10-27 21:13:19
 *
 *******************************************************************************/

#include <stdio.h>

/* call a function before declear it*/
int main() {
    foo();
    return 0;
}

int foo() {
    return 1;
}


////////////////////////////////////////
//void pointer in C can assigned to some other int*
int foo() {
    void* p = 0;
    int* iptr = p;
    return 0;
}

////////////////////////////////////////
