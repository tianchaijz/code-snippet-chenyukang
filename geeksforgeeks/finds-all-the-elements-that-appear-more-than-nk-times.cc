#include <iostream>
#include <vector>
using namespace std;

struct Node {
    int val;
    int count;
    Node():val(0), count(0) {}
};

typedef struct Node Node;


void moreThanNdK(int arr[], int size, int k) {
    if(k < 2) return;
    vector<Node> vec(k);
    for(int i=0; i<size; i++) {
        bool found = false;
        for(int k=0; k<vec.size(); k++) {
            if(vec[i].val == arr[i]) {
                vec[i].count++;
                found = true;
            }
        }
        if(!found) {
            int k;
            for(k=0; k<vec.size(); k++) {
                if(vec[i].count == 0) {
                    vec[i].val = arr[i];
                    vec[i].count = 1;
                }
            }
            if(k == vec.size()) {
                for(int m = 0; m<vec.size(); m++) {
                    vec[i].count--;
                }
            }
        }
    }

    for(int i=0; i<vec.size(); i++) {
        int cnt = 0;
        for(int j=0; j<size; j++) {
            if(arr[j] == vec[i].val)
                cnt++;
        }
        if ( cnt * k > size) {
            printf("val: %d  count: %d", vec[i].val);
        }
    }
    printf("\n");
}

int main() {
    cout << "First Test\n";
    int arr1[] = {4, 5, 6, 7, 8, 4, 4};
    int size = sizeof(arr1)/sizeof(arr1[0]);
    int k = 3;
    moreThanNdK(arr1, size, k);

    cout << "\nSecond Test\n";
    int arr2[] = {4, 2, 2, 7};
    size = sizeof(arr2)/sizeof(arr2[0]);
    k = 3;
    moreThanNdK(arr2, size, k);

    cout << "\nThird Test\n";
    int arr3[] = {2, 7, 2};
    size = sizeof(arr3)/sizeof(arr3[0]);
    k = 2;
    moreThanNdK(arr3, size, k);

    cout << "\nFourth Test\n";
    int arr4[] = {2, 3, 3, 2};
    size = sizeof(arr4)/sizeof(arr4[0]);
    k = 3;
    moreThanNdK(arr4, size, k);

    return 0;
}
