/*******************************************************************************
 *
 *      find-minimum-element-in-a-sorted-and-rotated-array.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-11-16 18:01:19
 *
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define min(a, b) ((a) < (b) ? (a) : (b))

int findMin(int arr[], int left, int right) {
    if(right < left)
        return arr[0];
    if(left == right)
        return arr[left];

    int mid = left + (right - left)/2;
    //check the whether the mid+1 is smallest one
    if(mid < right && arr[mid+1] < arr[mid])
        return arr[mid+1];

    if(mid > left && arr[mid-1] > arr[mid])
        return arr[mid];

    //special case for duplicated numbers
    //bad case for O(N)
    if(arr[left] == arr[mid] && arr[right] == arr[mid])
        return min(findMin(arr, left, mid-1), findMin(arr, mid+1, right));

    if(arr[mid] < arr[right])
        return findMin(arr, left, mid-1);
    return findMin(arr, mid+1, right);
}

// Driver program to test above functions
int main()
{
    int arr1[] =  {5, 6, 1, 2, 3, 4};
    int n1 = sizeof(arr1)/sizeof(arr1[0]);
    printf("The minimum element is %d\n", findMin(arr1, 0, n1-1));

    int arr2[] =  {1, 2, 3, 4};
    int n2 = sizeof(arr2)/sizeof(arr2[0]);
    printf("The minimum element is %d\n", findMin(arr2, 0, n2-1));

    int arr3[] =  {1};
    int n3 = sizeof(arr3)/sizeof(arr3[0]);
    printf("The minimum element is %d\n", findMin(arr3, 0, n3-1));

    int arr4[] =  {1, 2};
    int n4 = sizeof(arr4)/sizeof(arr4[0]);
    printf("The minimum element is %d\n", findMin(arr4, 0, n4-1));

    int arr5[] =  {2, 1};
    int n5 = sizeof(arr5)/sizeof(arr5[0]);
    printf("The minimum element is %d\n", findMin(arr5, 0, n5-1));

    int arr6[] =  {5, 6, 7, 1, 2, 3, 4};
    int n6 = sizeof(arr6)/sizeof(arr6[0]);
    printf("The minimum element is %d\n", findMin(arr6, 0, n6-1));

    int arr7[] =  {1, 2, 3, 4, 5, 6, 7};
    int n7 = sizeof(arr7)/sizeof(arr7[0]);
    printf("The minimum element is %d\n", findMin(arr7, 0, n7-1));

    int arr8[] =  {2, 3, 4, 5, 6, 7, 8, 1};
    int n8 = sizeof(arr8)/sizeof(arr8[0]);
    printf("The minimum element is %d\n", findMin(arr8, 0, n8-1));

    int arr9[] =  {3, 4, 5, 1, 2};
    int n9 = sizeof(arr9)/sizeof(arr9[0]);
    printf("The minimum element is %d\n", findMin(arr9, 0, n9-1));


    int arr10[] =  {5, 6, 1, 2, 3, 4};
    int n10 = sizeof(arr10)/sizeof(arr10[0]);
    printf("The minimum element is %d\n", findMin(arr10, 0, n10-1));

    int arr12[] =  {1, 1, 0, 1};
    int n12 = sizeof(arr12)/sizeof(arr12[0]);
    printf("The minimum element is %d\n", findMin(arr12, 0, n12-1));

    int arr13[] =  {1, 1, 2, 2, 3};
    int n13 = sizeof(arr13)/sizeof(arr13[0]);
    printf("The minimum element is %d\n", findMin(arr13, 0, n13-1));

    int arr14[] =  {3, 3, 3, 4, 4, 4, 4, 5, 3, 3};
    int n14 = sizeof(arr14)/sizeof(arr14[0]);
    printf("The minimum element is %d\n", findMin(arr14, 0, n14-1));

    int arr15[] =  {2, 2, 2, 2, 2, 2, 2, 2, 0, 1, 1, 2};
    int n15 = sizeof(arr15)/sizeof(arr15[0]);
    printf("The minimum element is %d\n", findMin(arr15, 0, n15-1));

    int arr16[] =  {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1};
    int n16 = sizeof(arr16)/sizeof(arr16[0]);
    printf("The minimum element is %d\n", findMin(arr16, 0, n16-1));

    int arr17[] =  {2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2};
    int n17 = sizeof(arr17)/sizeof(arr17[0]);
    printf("The minimum element is %d\n", findMin(arr17, 0, n17-1));

    return 0;
}
