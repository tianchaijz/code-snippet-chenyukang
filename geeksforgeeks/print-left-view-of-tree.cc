//http://www.geeksforgeeks.org/print-left-view-binary-tree/
//print left view of a tree

#include <stdio.h>
#include <iostream>
using namespace std;

struct Node {
    int v;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->v = data;
    node->left = node->right = 0;
    return node;
}

void _leftView(struct Node* node, int now, int* level) {
    if(node == 0) return;

    if(now > *level) {
        printf(" %d ", node->v);
        *level = now;
    }
    _leftView(node->left, now+1, level);
    _leftView(node->right, now+1, level);
}

void leftView(struct Node* root) {
    int level = 0;
    int now = 1;
    _leftView(root, now, &level);
}


// Driver Program to test above functions
int main() {
    struct Node *root = newNode(12);
    root->left = newNode(10);
    root->right = newNode(30);
    root->right->left = newNode(25);
    root->right->right = newNode(40);

    leftView(root);
    printf("\n");
    return 0;
}
