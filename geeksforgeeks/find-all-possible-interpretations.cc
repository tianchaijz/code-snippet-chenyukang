#include <iostream>
#include <string>
#include <vector>
using namespace std;

string alphabet = "-abcdefghijklmnopqrstuvwxyz";

string getNext(string prev, int index) {
    if(index == 0) {
        return prev;
    }
    return prev + alphabet[index];
}

void printIter(int arr[], int len, int index, string now) {
    if(index == len) {
        std::cout<< now << " ";
        return;
    }

    int k = arr[index];
    if(k >= 0 && k <= 26) {
        string next = getNext(now, k);
        printIter(arr, len, index+1, next);
    }
    if(index < len - 1) {
        k = k * 10 + arr[index+1];
        if(k >= 0 && k <= 26) {
            string next = getNext(now, k);
            printIter(arr, len, index+2, next);
        }
    }
}

void printAllInterpretations(int arr[], int len) {
    string s;
    printIter(arr, len, 0, s);
    std::cout << std::endl;
}

int main() {
    // aacd(1,1,3,4) amd(1,13,4) kcd(11,3,4)
    // Note : 1,1,34 is not valid as we don't have values corresponding
    // to 34 in alphabet
    int arr[] = {1, 1, 3, 4};
    int len = sizeof(arr)/sizeof(arr[0]);
    printAllInterpretations(arr, len);

    // aaa(1,1,1) ak(1,11) ka(11,1)
    int arr2[] = {1, 1, 1};
    len = sizeof(arr2)/sizeof(arr2[0]);
    printAllInterpretations(arr2, len);

    // bf(2,6) z(26)
    int arr3[] = {2, 6};
    len = sizeof(arr3)/sizeof(arr3[0]);
    printAllInterpretations(arr3, len);

    // ab(1,2), l(12)
    int arr4[] = {1, 2};
    len = sizeof(arr4)/sizeof(arr4[0]);
    printAllInterpretations(arr4, len);

    // a(1,0} j(10)
    int arr5[] = {1, 0};
    len = sizeof(arr5)/sizeof(arr5[0]);
    printAllInterpretations(arr5, len);

    // "" empty string output as array is empty
    int arr6[] = {};
    printAllInterpretations(arr6, 0);

    // abba abu ava lba lu
    int arr7[] = {1, 2, 2, 1};
    len = sizeof(arr7)/sizeof(arr7[0]);
    printAllInterpretations(arr7, len);

    return 0;
}
