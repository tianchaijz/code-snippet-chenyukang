#include <iostream>
#include <stdio.h>
using namespace std;

void nextGreatest(int arr[], int len) {
    int max = arr[len-1];
    arr[len-1] = -1;
    for(int k = len -2; k>=0; k--) {
        int t = arr[k];
        arr[k] = max;
        if(t > max)
            max = t;
    }
}

void printArray(int arr[], int size) {
    for(int i=0; i<size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    int arr[] = {16, 17, 4, 3, 5, 2};
    int size = sizeof(arr)/sizeof(arr[0]);
    nextGreatest (arr, size);
    printf ("The modified array is: \n");
    printArray (arr, size);
}
