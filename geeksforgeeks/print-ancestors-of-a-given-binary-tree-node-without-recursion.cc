//http://leetcode.com/2010/10/binary-tree-post-order-traversal.html
//traverse a tree without recurion.

#include <iostream>
#include <stack>
#include <vector>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = 0;
    node->data = data;
    return node;
}

void PrintAncesters_v1(struct Node* root, int target) {
    if(root == NULL) return;
    struct Node* prev = NULL;
    stack<struct Node*> st;
    st.push(root);
    bool found = false;

    while(!st.empty()) {
        struct Node* now = st.top();
        if(now->data == target) {
            found = true;
            break;
        }
        if( !prev || prev->left == now || prev->right == now) {
            if(now->left)
                st.push(now->left);
            else if(now->right)
                st.push(now->right);
        } else if( now->left == prev) {
            if(now->right)
                st.push(now->right);
        } else {
            st.pop();
        }
        prev = now;
    }

    printf("%-2d: ", target);
    if(found) {
        prev = st.top();
        st.pop();
        while(!st.empty()) {
            struct Node* cur = st.top();
            if(cur->left == prev || cur->right == prev) {
                printf(" %-2d ", cur->data);
                prev = cur;
            }
            st.pop();
        }
    }
    printf("\n");
}

void PrintAncesters_v2(struct Node* root, int target) {
    if(root == NULL) return;
    stack<struct Node*> st;
    stack<struct Node*> out;
    st.push(root);
    bool found  = false;
    while(!st.empty()) {
        struct Node* cur = st.top();
        out.push(cur);
        if(cur->data == target) {
            found = true;
            break;
        }
        st.pop();
        if(cur->left)
            st.push(cur->left);
        if(cur->right)
            st.push(cur->right);
    }

    printf("%-2d: ", target);
    struct Node* prev = out.top();
    out.pop();
    while(found && !out.empty()) {
        struct Node* cur = out.top();
        if(prev == NULL || cur->left == prev || cur->right == prev) {
            printf(" %-2d ", cur->data);
            prev = cur;
        }
        out.pop();
    }
    printf("\n");
}



// Driver program to test above functions
int main()
{
    // Let us construct a binary tree
    struct Node* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->left->left->left = newNode(8);
    root->left->right->right = newNode(9);
    root->right->right->left = newNode(10);

    printf("Following are all keys and their ancestors\n");
    for (int key = 1; key <= 11; key++) {

        PrintAncesters_v1(root, key);
        PrintAncesters_v2(root, key);
    }

    return 0;
}
