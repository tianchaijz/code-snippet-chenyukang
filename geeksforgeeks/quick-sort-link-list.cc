//quick sort on link list
//http://www.geeksforgeeks.org/quicksort-for-linked-list/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
};


void printList(struct Node* root) {
    struct Node* node = root;
    while(node) {
        printf(" %d ", node->data);
        node = node->next;
    }
    printf("\n");
}

struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->prev = node->next = 0;
    return node;
}

void push(struct Node** head, int data) {
    struct Node* node = newNode(data);
    if(*head == 0) {
        *head = node;
    } else {
        node->next = *head;
        (*head)->prev = node;
        *head = node;
    }
}

struct Node* last(struct Node* root) {
    struct Node* node = root;
    while(node->next) {
        node = node->next;
    }
    return node;
}

void swap(struct Node* l, struct Node* r) {
    int t = r->data;
    r->data = l->data;
    l->data = t;
}

struct Node* partition(struct Node* head, struct Node* tail) {
    struct Node* node = head;
    struct Node* prev = head->prev;
    for(node = head; node != tail; node = node->next) {
        if(node->data < tail->data) {
            prev = (prev == NULL)? head : prev->next;
            swap(prev, node);
        }
    }
    prev = (prev == NULL)? head : prev->next;
    swap(prev, tail);
    return prev;
}

void quickSort_iter(struct Node* head, struct Node* tail) {
    if(head != NULL && head != tail && tail->next != head) {
        struct Node* p = partition(head, tail);
        quickSort_iter(head, p->prev);
        quickSort_iter(p->next, tail);
    }
}

void quickSort(struct Node* head) {
    if(head == NULL ||
       head->next == NULL) {
        return;
    }

    struct Node* tail = last(head);
    quickSort_iter(head, tail);
}

int main() {
    struct Node* a = NULL;
    push(&a, 5);
    push(&a, 20);
    push(&a, 4);
    push(&a, 3);
    push(&a, 30);
    push(&a, 100);

    cout << "Linked List before sorting \n";
    printList(a);

    quickSort(a);
    printList(a);
    return 0;
}
