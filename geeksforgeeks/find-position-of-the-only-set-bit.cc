#include <iostream>
#include <vector>
using namespace std;

bool isPower(int value) {
    return (value & (value - 1)) == 0;
}


int firstPos(int value) {
    if(!isPower(value))
        return -1;
    int pos = 1;
    int t = 1;
    while(value != t) {
        pos++;
        t = t<<1;
    }
    return pos;
}

void test(int value) {
    int pos = firstPos(value);
    printf("value: %d pos: %d\n", value, pos);
}

int main() {
    test(1);
    test(16);
    test(12);
    test(128);
    return 0;
}
