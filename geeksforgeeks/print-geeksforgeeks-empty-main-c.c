/*******************************************************************************
 *
 *      print-geeksforgeeks-empty-main-c.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-11-14 06:12:29
 *
 *******************************************************************************/

#include <stdio.h>
#include <string.h>

void startup_func(void) __attribute__((constructor));

void startup_func() {
    printf("hello from constructor: startup_func\n");
}


int main() {
    return 0;
}
