#include <iostream>
#include <string.h>
#include <stdlib.h>
using namespace std;

void PrintIter(char arr[], int len, string s, int k) {
    if( k == 0) {
        std::cout<< s << std::endl;
        return;
    }
    for(int i=0; i<len; i++) {
        string n = s + arr[i];
        PrintIter(arr, len, n, k-1);
    }
}

void PrintCombination(char arr[], int len, int k) {
    char buf[len];
    string s;
    PrintIter(arr, len, s, k);
}

int main() {
    char arr[] = "abcd";
    int k = 2;
    int len = sizeof(arr)/(sizeof(arr[0])) - 1;
    printf("now len: %d\n", len);
    PrintCombination(arr, len, k);
    return 0;
}
