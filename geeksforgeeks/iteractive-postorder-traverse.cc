//http://leetcode.com/2010/10/binary-tree-post-order-traversal.html
//traverse a tree without recurion.

#include <iostream>
#include <stack>
#include <vector>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = 0;
    node->data = data;
    return node;
}

void PostOrderTraversel_v1(struct Node* root) {
    if(root == NULL) return;
    stack<struct Node*> st;
    struct Node* prev  = NULL;
    st.push(root);
    while(!st.empty()) {
        struct Node* now = st.top();
        //next
        if( !prev || prev->left == now || prev->right == now) {
            if(now->left)
                st.push(now->left);
            else if(now->right)
                st.push(now->right);
            else {
                printf(" %d ", now->data);
                st.pop();
            }
        }
        //up from left
        else if(now->left == prev) {
            if(now->right)
                st.push(now->right);
            else {
                printf(" %d ", now->data);
                st.pop();
            }
        }
        //up from right
        else if(now->right == prev) {
            printf(" %d ", now->data);
            st.pop();
        }
        prev = now;
    }
    printf("\n");
}


void PostOrderTraversel_v2(struct Node* root) {
    if(root == NULL) return;
    struct Node* prev = NULL;
    stack<struct Node*> st;
    st.push(root);

    while(!st.empty()) {
        struct Node* now = st.top();
        if( !prev || prev->left == now || prev->right == now) {
            if(now->left)
                st.push(now->left);
            else if(now->right)
                st.push(now->right);
        } else if( now->left == prev) {
            if(now->right)
                st.push(now->right);
        } else {
            printf(" %d ", now->data);
            st.pop();
        }
        prev = now;
    }
    printf("\n");
}

void PostOrderTraversel_v3(struct Node* root) {
    if(root == NULL) return;
    stack<struct Node*> st;
    stack<struct Node*> out;
    st.push(root);
    while(!st.empty()) {
        struct Node* cur = st.top();
        out.push(cur);
        st.pop();
        if(cur->left)
            st.push(cur->left);
        if(cur->right)
            st.push(cur->right);
    }

    while(!out.empty()) {
        struct Node* cur = out.top();
        printf(" %d ", cur->data);
        out.pop();
    }
    printf("\n");
}



// Driver program to test above functions
int main()
{
    // Let us construct a binary tree
    struct Node* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->left->left->left = newNode(8);
    root->left->right->right = newNode(9);
    root->right->right->left = newNode(10);

    PostOrderTraversel_v1(root);
    PostOrderTraversel_v2(root);
    PostOrderTraversel_v3(root);
    return 0;
}
