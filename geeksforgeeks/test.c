
/*******************************************************************************
 *
 *      test.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-10-24 09:10:43
 *
 *      COPYRIGHT (C) 2006~2012, Nextop INC., ALL RIGHTS RESERVED.
 *
 *******************************************************************************/

#include <stdio.h>

int func(int a) {
    int res;
    for(int i=0; i<a; i++)
        res += i;
    return res;
}

int main() {
    int a = func(10);
    int arr[a];
    printf("size: %lu\n", sizeof(arr)/sizeof(int));
    return 0;
}
