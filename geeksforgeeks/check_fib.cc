//http://www.geeksforgeeks.org/check-number-fibonacci-number/
#include <math.h>
#include <iostream>
using namespace std;

bool isPerfectSquare(int x) {
    int s = sqrt(x);
    return s*s == x;
}


bool isFibonacci(int x) {
    return isPerfectSquare(5*x*x + 4)  ||
        isPerfectSquare(5*x*x - 4);
}


int main() {
    int k;
    for(k=0; k<10000; k++) {
        if(isFibonacci(k)) {
            std::cout<< k << " is a fibonacci" << std::endl;
        } else {
            //std::cout << k << " is not a fibonacci " << std::endl;
        }
    }

    return 0;
}
