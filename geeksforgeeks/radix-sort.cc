//http://www.geeksforgeeks.org/radix-sort/
#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

int getMax(int arr[], int len) {
    int max = arr[0];
    for(int i=0; i<len; i++) {
        if(arr[i] > max)
            max = arr[i];
    }
    return max;
}

void print(int arr[], int len) {
    for(int i=0; i<len; i++) {
        printf(" %d ", arr[i]);
    }
    printf("\n");
}

void countSort(int arr[], int len, int exp) {
    int count[10] = { 0 };
    int output[len];

    for(int i=0; i<len; i++) {
        count[(arr[i]/exp)%10]++;
    }
    for(int i=1; i<=9; i++) {
        count[i] += count[i-1];
    }

    for(int i=len-1; i>=0; i--) {
        int v = arr[i];
        output[count[(v/exp)%10] -1 ] = v;
        count[(v/exp)%10]--;
    }

    for(int i=0; i<len; i++) {
        arr[i] = output[i];
    }
}

void radixsort(int arr[], int len) {
    int max = getMax(arr, len);

    for(int exp = 1; exp < max; exp = exp * 10) {
        printf("exp: %d\n", exp);
        countSort(arr, len, exp);
        print(arr, len);
    }
}



int main() {
    int arr[] = {170, 45, 75, 90, 802, 24, 2, 66};
    int n = sizeof(arr)/sizeof(arr[0]);
    radixsort(arr, n);
    print(arr, n);
    return 0;
}
