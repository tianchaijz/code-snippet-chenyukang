//Kosaraju’s algorithms to find SCC in DAG,
//first use topological sort to find teriminal node (which is a node with node output degree),
//this node will be the initial node for traversel reverse graph.
//time complexity is : O(V+E)
#include <iostream>
#include <stdio.h>
#include <list>
#include <stack>
using namespace std;

class Graph {
    int V;
    list<int>* adj;
    void _topological_sort(int v, bool visited[], stack<int>& stack);
    void printSCC_iter(int v, bool visited[]);
public:
    Graph(int v);
    ~Graph();
    void addEdge(int v, int w);
    void TopologicalSort(stack<int>& stack);
    void PrintSCC();
};

Graph::Graph(int v):V(v) {
    adj = new list<int>[V];
}

Graph::~Graph() {
    delete [] adj;
}

void Graph::addEdge(int v, int w) {
    adj[v].push_back(w);
}

void Graph::_topological_sort(int v, bool visited[], stack<int>& stack) {
    visited[v] = true;
    for(list<int>::iterator it = adj[v].begin(); it != adj[v].end(); ++it) {
        int u = *it;
        if(visited[u] == false)
            _topological_sort(u, visited, stack);
    }
    stack.push(v);
}

void Graph::TopologicalSort(stack<int>& stack) {
    bool visited[V];
    for(int i=0; i<V; i++)
        visited[i] = false;
    for(int i=0; i<V; i++) {
        if(visited[i] == false) {
            _topological_sort(i, visited, stack);
        }
    }
}

void Graph::printSCC_iter(int v, bool visited[]) {
    visited[v] = true;
    std::cout << " " << v;
    for(list<int>::iterator it = adj[v].begin(); it != adj[v].end(); ++it) {
        int u = *it;
        if(visited[u] == false) {
            printSCC_iter(u, visited);
        }
    }
}

void Graph::PrintSCC() {
    bool visited[V];
    memset(visited, false, sizeof(visited));

    stack<int> stack;
    TopologicalSort(stack);

    Graph rev(V);
    for(int i=0; i<V; i++) {
        for(list<int>::iterator it = adj[i].begin();
            it != adj[i].end();
            ++it) {
            int x = *it;
            rev.addEdge(x, i);
        }
    }

    std::cout << "Strong connected component:\n";
    while(!stack.empty()) {
        int u = stack.top();
        stack.pop();
        if(!visited[u]) {
            rev.printSCC_iter(u, visited);
            std::cout<< std::endl;
        }
    }
}

int main() {
    Graph g(5);
    g.addEdge(1, 0);
    g.addEdge(0, 2);
    g.addEdge(2, 1);
    g.addEdge(0, 3);
    g.addEdge(3, 4);
    g.PrintSCC();
    return 0;
}
