#include <iostream>
using namespace std;

int n = 4;

struct Node {
    int data;
    int row;
    int col;
};

class MinHeap {
    int K;
    struct Node* nodes;
private:
    void swap(int l, int r) {
        struct Node t = nodes[l];
        nodes[l] = nodes[r];
        nodes[r] = t;
    }

    void Heapify(int k) {
        int l = 2*k + 1;
        int r = 2*k + 2;
        int min = k;
        if(l < K && nodes[l].data < nodes[min].data)
            min = l;
        if( r < K && nodes[r].data < nodes[min].data)
            min = r;
        if(min != k) {
            swap(min, k);
            Heapify(min);
        }
    }

public:
    MinHeap(int arr[][4], int k) : K(k) {
        nodes = (struct Node*)malloc(sizeof(struct Node) * K);
        for(int i=0; i<K; i++) {
            nodes[i].data = arr[i][0];
            nodes[i].row = i;
            nodes[i].col = 1;
        }
        int s = (K-1)/2;
        while(s >= 0) {
            Heapify(s);
            s--;
        }
    }

    ~MinHeap() {
        free(nodes);
    }

    struct Node minNode() {
        return nodes[0];
    }

    void replaceMin(struct Node& node) {
        nodes[0] = node;
        Heapify(0);
    }
};

int* mergeKArrays(int arr[][4], int k) {
    int* output = (int*)malloc(sizeof(int) * (n*k));
    memset(output, 0, sizeof(int) * (n*k));

    MinHeap heap(arr, k);
    int count = 0;
    for(count = 0; count < n*k; count++) {
        struct Node now = heap.minNode();
        output[count] = now.data;
        if(now.col < n) {
            now.data = arr[now.row][now.col];
            now.col++;
        } else  {
            now.data = INT_MAX;
        }
        heap.replaceMin(now);
    }
    return output;
}

void printArray(int* arr, int k) {
    for(int i=0; i<k; i++) {
        printf(" %d ", arr[i]);
    }
}

int main() {
    //int arr[][5] =  {{2, 6, 12, 34, 72},
    //               {1, 9, 20, 1000, 1991},
    //               {23, 34, 90, 2000, 2001}};
    int arr[][4] = {{3, 4, 6, 7},
                    {1, 2, 5, 8}};

    int k = sizeof(arr)/sizeof(arr[0]);
    n = sizeof(arr[0])/sizeof(arr[0][0]);
    printf("k:%d n:%d\n", k, n);
    int* output = mergeKArrays(arr, k);

    cout << "Merged array is " << endl;
    printArray(output, n*k);
    printf("\n");
    return 0;
}
