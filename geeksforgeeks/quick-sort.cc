#include <iostream>
#include <stdio.h>
using namespace std;

void print_array(int arr[], int len) {
    for(int i=0; i<len; i++) {
        printf(" %d ", arr[i]);
    }
    printf("\n");
}

int partion(int arr[], int begin, int end) {
    int idx = begin - 1;
    for(int i=begin; i<end; i++) {
        if(arr[i] < arr[end]) {
            idx++;
            swap(arr[idx], arr[i]);
        }
    }
    idx++;
    swap(arr[idx], arr[end]);
    return idx;
}

void quick_sort(int arr[], int begin, int end) {
    if(begin < end) {
        int p = partion(arr, begin, end);
        quick_sort(arr, begin, p-1);
        quick_sort(arr, p+1, end);
    }
}

void test_one() {
    int arr[] = {1, 2};
    int len = sizeof(arr)/(sizeof(int));
    quick_sort(arr, 0, len-1);
    print_array(arr, len);
}

int main() {
    int arr[] = {4, 5, 3, 10, 12, 43, 11};
    int len = sizeof(arr)/(sizeof(int));

    print_array(arr, len);
    quick_sort(arr, 0, len-1);
    print_array(arr, len);

    test_one();
    return 0;
}
