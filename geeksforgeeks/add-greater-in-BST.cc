//http://www.geeksforgeeks.org/add-greater-values-every-node-given-bst/
//traverse BST and add the right value into sum
//Complexity: O(n)

#include <iostream>
#include <stdio.h>
using namespace std;

struct Node{
    int data;
    struct Node* left;
    struct Node* right;
};

struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = 0;
    return node;
}

struct Node* insert(struct Node* root, int data) {
    if(root == 0) {
        return newNode(data);
    }
    if(root->data > data) {
        root->left = insert(root->left, data);
    }
    if(root->data <= data) {
        root->right = insert(root->right, data);
    }
    return root;
}

void InorderTrav(struct Node* root) {
    if(root) {
        InorderTrav(root->left);
        printf(" %d ", root->data);
        InorderTrav(root->right);
    }
}

void modify(struct Node* root, int* sum) {
    if(root->right) {
        modify(root->right, sum);
    }
    *sum = *sum + root->data;
    root->data = *sum;
    if(root->left) {
        modify(root->left, sum);
    }
}

int main() {
    /* Let us create following BST
       50
       /     \
       30      70
       /  \    /  \
       20   40  60   80 */
    struct Node *root = NULL;
    root = insert(root, 50);
    insert(root, 30);
    insert(root, 20);
    insert(root, 40);
    insert(root, 70);
    insert(root, 60);
    insert(root, 80);

    InorderTrav(root);
    printf("\n");

    int sum = 0;
    modify(root, &sum);
    InorderTrav(root);
    printf("\n");
    return 0;
}
