#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <queue>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

bool printAncestors(struct Node* root, int data) {
    if(root == NULL)
        return false;
    if(root->data == data)
        return true;

    if(printAncestors(root->left, data) ||
       printAncestors(root->right, data)) {
        std::cout <<" " << root->data <<  " ";
        return true;
    }
    return false;
}


struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = 0;
    return node;
}

#ifdef RECURSIVE
void traverse(struct Node* node, int pre, int parent[]) {
    if(node == 0)
        return;
    int data = node->data;
    parent[data] = pre;
    if(node->left) {
        traverse(node->left, data, parent);
    }
    if(node->right) {
        traverse(node->right, data, parent);
    }
}
#endif

void traverse_iter(struct Node* node, int parent[]) {
    if(node == 0)
        return;
    queue<struct Node*> Q;
    Q.push(node);
    struct Node* p;
    parent[node->data] = -1;
    while(!Q.empty()) {
        struct Node* now = Q.front();
        Q.pop();
        if(now->left) {
            p = now->left;
            parent[p->data] = now->data;
            Q.push(p);
        }
        if(now->right) {
            p = now->right;
            parent[p->data] = now->data;
            Q.push(p);
        }
    }
}


void printAncestors(int parent[], int key) {
    int p = parent[key];
    while(p != -1) {
        printf(" %d ", p);
        p = parent[p];
    }
}

// Driver program to test above functions
int main() {
    // Let us construct a binary tree
    struct Node* root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->left->left->left = newNode(8);
    root->left->right->right = newNode(9);
    root->right->right->left = newNode(10);

    int parent[11];
    memset(parent, -1, sizeof(parent));
    traverse_iter(root, parent);

    printf("Following are all keys and their ancestors\n");
    for (int key = 1; key <= 10; key++)
    {
        printf("%d: ", key);
        printAncestors(parent, key);
        printf("\n");
    }

    return 0;
}
