//http://www.geeksforgeeks.org/print-geeksforgeeks-empty-main-c/
//you can compile the program using:
//g++ -Dfunc=main print-msg-with-emptymain.cc
#include <stdio.h>
#include <iostream>
using namespace std;

int func() {
    printf("Hello world\n");
    return 0;
}

#if 0
int main() {
    return 0;
}
#endif
