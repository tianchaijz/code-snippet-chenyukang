//http://www.geeksforgeeks.org/increasing-subsequence-of-length-three-with-maximum-product/
//not a right solution

#include <iostream>
#include <stdio.h>
#include <assert.h>
using namespace std;


void printArr(int arr[], int len) {
    for(int i=0; i<len; i++) {
        printf(" %2d ", arr[i]);
    }
    printf("\n");
}

void ans(int arr[], int len) {
    int LGR[len];
    int SGR[len];

    printArr(arr, len);

    //========================================
    //fill LGR from right to left
    LGR[len - 1] = -1;
    for(int i = len -2; i>=0; i--) {
        if(arr[i+1] > LGR[i+1]) {
            LGR[i] = arr[i+1];
        }
        else
            LGR[i] = LGR[i+1];
    }

    printArr(LGR, len);

    //========================================
    //fill SGR from right to left
    SGR[len - 1] = -1;
    SGR[len - 2] = -1;
    int k;
    for(int i = len -3; i>=0; i--) {
        int v = arr[i+1];
        if(v == LGR[i]) {
            //SGR[i] = SGR[i+1];
            int x = i+1;
            int m = -1;
            printf("now: %d \n", x);
            for(; x < len; x++){
                if(arr[x] != LGR[i] && arr[x] > m)
                    m = arr[x];
            }
            if(m != -1)
                SGR[i] = m;
        } else if( v < LGR[i]) {
            k = max(v, SGR[i+1]);
            if(k < arr[i]) {
                SGR[i] = -1;
            }
            else
                SGR[i] = k;
        } else {
            assert(0);
        }
    }

    printArr(SGR, len);

    int max = -1;
    int v1, v2, v3;
    for(int i=1; i < len - 1; i++) {
        if(LGR[i] * arr[i] * SGR[i] > max) {
            max =  arr[i] * SGR[i] * LGR[i];
            v1 = arr[i], v2 = SGR[i], v3 = LGR[i];
        }
    }
    printf("ans: %d\n", max);
    printf("%d %d %d\n", v1, v2, v3);
}

int main() {
    //int arr[] = {6, 7, 8, 1, 9, 2, 3, 10};
    int arr[] = {1, 5, 10, 8, 9};
    int len = sizeof(arr) / (sizeof(int));
    ans(arr, len);
    return 0;
}
