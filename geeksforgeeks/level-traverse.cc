//http://www.geeksforgeeks.org/level-order-tree-traversal/
//http://leetcode.com/2010/09/printing-binary-tree-in-level-order.html

#include <stdio.h>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};


struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = 0;
    return node;
}

void printLevelOrder(struct Node* root) {
    queue<Node*> Q;
    Q.push(root);
    while(!Q.empty()) {
        Node* now = Q.front();
        printf(" %d ", now->data);
        Q.pop();
        if(now->left) Q.push(now->left);
        if(now->right) Q.push(now->right);
    }
}


/* Driver program to test above functions*/
int main() {
    struct Node *root = newNode(1);
    root->left        = newNode(2);
    root->right       = newNode(3);
    root->left->left  = newNode(4);
    root->left->right = newNode(5);

    printf("Level Order traversal of binary tree is \n");
    printLevelOrder(root);
    printf("\n");
    return 0;
}
