//http://www.geeksforgeeks.org/in-place-convert-a-given-binary-tree-to-doubly-linked-list/

#include <iostream>
#include <stdio.h>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

// A utility function to create a new tree node
struct Node *newNode(int data) {
    struct Node *node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = NULL;
    return(node);
}

// Standard Inorder traversal of tree
void inorder(struct Node *root) {
    if (root != NULL) {
        inorder(root->left);
        printf("\t%d",root->data);
        inorder(root->right);
    }
}

struct Node* lastRight(struct Node* root) {
    struct Node* node = root;
    while(node->right) {
        node = node->right;
    }
    return node;
}

struct Node* lastLeft(struct Node* root) {
    struct Node* node = root;
    while(node->left) {
        node = node->left;
    }
    return node;
}

struct Node* BTToDLL(struct Node* root) {
    if(root == NULL)
        return root;
    if(root->left) {
        struct Node* left = BTToDLL(root->left);
        struct Node* right = lastRight(left);
        right->right = root;
        root->left = right;
    }
    if(root->right) {
        struct Node* right = BTToDLL(root->right);
        struct Node* left = lastLeft(right);
        left->left = root;
        root->right = left;
    }
    return root;
}

// Traverses the DLL from left tor right
void printList(struct Node *root) {
    while (root != NULL) {
        printf("\t%d", root->data);
        root = root->right;
    }
    printf("\n");
}

// Driver program to test above functions
int main(void) {
    // Let us create the tree shown in above diagram
    struct Node *root = newNode(10);
    root->left        = newNode(12);
    root->right       = newNode(15);
    root->left->left  = newNode(25);
    root->left->right = newNode(30);
    root->right->left = newNode(36);

    printf("\n\t\tInorder Tree Traversal\n\n");
    inorder(root);

    struct Node *head = BTToDLL(root);
    while(head->left)
        head = head->left;

    printf("\n\n\t\tDLL Traversal\n\n");
    printList(head);
    return 0;
}
