//http://www.geeksforgeeks.org/count-set-bits-floating-point-number/
#include <iostream>
#include <stdio.h>
using namespace std;

unsigned bits(unsigned char x) {
    unsigned int count = 0;
    while(x) {
        x &= (x-1);
        count++;
    }
    return count;
}

unsigned int countBits(float n) {
    unsigned s = sizeof(float)/(sizeof(unsigned char));
    unsigned char* p = (unsigned char*)&n;
    int k;
    unsigned res = 0;
    for(k=0; k<s; k++) {
        res += bits(*p);
        p++;
    }
    return res;
}

int main() {
    float n;
    while(cin >> n) {
        std::cout << "count Bits: " << countBits(n) << std::endl;
    }
    return 0;
}
