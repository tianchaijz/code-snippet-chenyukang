//http://www.geeksforgeeks.org/remove-all-nodes-which-lie-on-a-path-having-sum-less-than-k/
#include <iostream>
#include <string>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};


// A utility function to create a new Binary Tree node with given data
struct Node* newNode(int data) {
    struct Node* node = (struct Node*) malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = NULL;
    return node;
}

// print the tree in LVR (Inorder traversal) way.
void print(struct Node *root) {
    if (root != NULL) {
        print(root->left);
        printf("%d ",root->data);
        print(root->right);
    }
}

#define max(l, r) (((l) > (r)) ? (l) : (r))

struct Node* Clear_iter(struct Node* root, int* sum, int expect) {
    if(root == 0)
        return 0;

    int data = root->data;
    int lsum = *sum + data;
    int rsum = *sum + data;
    root->left = Clear_iter(root->left, &lsum, expect);
    root->right = Clear_iter(root->right, &rsum, expect);
    data = max(lsum, rsum);
    if(data < expect) {
        free(root);
        root = 0;
    } else {
        *sum = data;
    }
    return root;
}

struct Node* Clear(struct Node* root, int expect) {
    int data = 0;
    return Clear_iter(root, &data, expect);
}

int main() {
    int k = 45;
    struct Node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->left->left->left = newNode(8);
    root->left->left->right = newNode(9);
    root->left->right->left = newNode(12);
    root->right->right->left = newNode(10);
    root->right->right->left->right = newNode(11);
    root->left->left->right->left = newNode(13);
    root->left->left->right->right = newNode(14);
    root->left->left->right->right->left = newNode(15);

    printf("Tree before truncation:\n");
    print(root);
    printf("\n");

    //root = prune(root, k); // k is 45
    root = Clear(root, k);
    printf("\nTree after truncation:\n");
    print(root);
    printf("\n");

    return 0;
}
