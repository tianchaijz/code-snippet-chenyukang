#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

bool printAncestors(struct Node* root, int data) {
    if(root == NULL)
        return false;
    if(root->data == data)
        return true;

    if(printAncestors(root->left, data) ||
       printAncestors(root->right, data)) {
        std::cout <<" " << root->data <<  " ";
        return true;
    }
    return false;
}


struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = 0;
    return node;
}


int main() {

    /* Construct the following binary tree
       1
       /   \
       2      3
       /  \
       4     5
       /
       7
    */
    struct Node *root = newNode(1);
    root->left        = newNode(2);
    root->right       = newNode(3);
    root->left->left  = newNode(4);
    root->left->right = newNode(5);
    root->left->left->left  = newNode(7);

    printAncestors(root, 7);
    printf("\n");
    return 0;
}
