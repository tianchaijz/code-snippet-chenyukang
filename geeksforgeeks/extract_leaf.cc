//http://www.geeksforgeeks.org/connect-leaves-doubly-linked-list/
//extrace all the leafs of a binary tree
//Note the order in ExtraceLeaf

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};

void InorderTrav(struct Node* node) {
    if(node != 0) {
        InorderTrav(node->left);
        printf(" %d ", node->data);
        InorderTrav(node->right);
    }
}

struct Node* newNode(int data) {
    struct Node* n = (struct Node*)malloc(sizeof(struct Node));
    n->data = data;
    n->left = n->right = 0;
    return n;
}

struct Node* ExtractLeaf(struct Node* root, Node** list) {
    if(root == 0)
        return 0;

    if(root->left == 0 && root->right == 0) {
        root->right = *list;
        if(*list)
            (*list)->left = root;
        *list = root;
        return 0;
    }

    if(root->right) {
        root->right = ExtractLeaf(root->right, list);
    }

    if(root->left) {
        root->left = ExtractLeaf(root->left, list);
    }

    return root;
}



void PrintList(struct Node* node) {
    if(node) {
        printf(" %d ", node->data);
        PrintList(node->right);
    }
}

int main() {
    struct Node* head = 0;
    struct Node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->right = newNode(6);
    root->left->left->left = newNode(7);
    root->left->left->right = newNode(8);
    root->right->right->left = newNode(9);
    root->right->right->right = newNode(10);

    printf("Inorder Trvaersal of given Tree is:\n");
    InorderTrav(root);
    printf("\n");

    ExtractLeaf(root, &head);
    printf("Leafs:\n");
    PrintList(head);
    printf("\n");

    printf("Modified tree: \n");
    InorderTrav(root);
    printf("\n");
    return 0;
}
