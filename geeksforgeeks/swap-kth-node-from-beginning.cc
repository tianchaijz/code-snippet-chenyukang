#include <iostream>
#include <vector>
#include <list>
#include <assert.h>
using namespace std;

struct Node {
    int data;
    struct Node* next;
};

void push(struct Node** head, int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->next = *head;
    *head = node;
}

void printList(struct Node* head) {
    struct Node* t = head;
    while(t) {
        printf("%d ", t->data);
        t = t->next;
    }
    printf("\n");
}

int countList(struct Node* head) {
    int count = 0;
    struct Node* t = head;
    while(t) {
        count++;
        t = t->next;
    }
    return count;
}

void swapKth(struct Node** head, int k) {
    int count = countList(*head);
    if(k > count) return;
    struct Node *p, *q, *t1, *t2, *t, *pre;
    int num = 0;
    p = q = NULL;
    t = *head;
    pre = 0;
    while(t) {
        num++;
        if(num == k) {
            p = t;
            t1 = pre;
        }
        if(count - num + 1 == k) {
            q = t;
            t2 = pre;
        }
        if(p && q) break;
        pre = t;
        t = t->next;
    }
    if(t2 == p) {
        if(t1) t1->next = q;
        p->next = q->next;
        q->next = p;
    }
    else if(t1 == q) {
        t2->next = p;
        q->next = p->next;
        p->next = q;
    }
    else {
        t = p->next;
        p->next = q->next;
        q->next = t;
        if(t2) t2->next = p;
        if(t1) t1->next = q;
        if(*head == p) {
            *head = q;
        }
        else if(*head == q) {
            *head = p;
        }
    }
}

int main() {
    // Let us create the following linked list for testing
    // 1->2->3->4->5->6->7->8
    struct Node *head = NULL;
    for (int i = 8; i >= 1; i--)
        push(&head, i);

    cout << "Original Linked List: ";
    printList(head);

    for (int k = 1; k <10 ; k++) {
        swapKth(&head, k);
        cout << "Modified List for k = " << k << endl;
        printList(head);
    }
    return 0;
}
