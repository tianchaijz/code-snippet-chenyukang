//http://www.geeksforgeeks.org/depth-first-traversal-for-a-graph/
//classic DFS

#include <iostream>
#include <stdio.h>
#include <list>
using namespace std;

class Graph {
    int V;
    list<int>* adj;
    void _DFS(int v, bool visited[]);
public:
    Graph(int v);
    ~Graph();
    void addEdge(int v, int w);
    void DFS(int v);
};

Graph::Graph(int v):V(v) {
    adj = new list<int>[V];
}

Graph::~Graph() {
    delete [] adj;
}

void Graph::addEdge(int v, int w) {
    adj[v].push_back(w);
}

void Graph::_DFS(int v, bool visited[]) {
    visited[v] = true;
    std::cout<<" " << v << " ";
    for(list<int>::iterator it = adj[v].begin(); it != adj[v].end(); ++it) {
        int u = *it;
        if(visited[u] == false)
            _DFS(u, visited);
    }
}

void Graph::DFS(int v) {
    bool visited[V];
    for(int i=0; i<V; i++)
        visited[i] = false;
    _DFS(v, visited);
}

int main() {
    Graph g(4);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 0);
    g.addEdge(2, 3);
    g.addEdge(3, 3);
    cout << "Following is Depth First Traversal (starting from vertex 2) \n";
    g.DFS(2);
    cout << endl;
    return 0;
}
