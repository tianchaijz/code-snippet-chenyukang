/*******************************************************************************
 *
 *      print-ancestors-of-a-given-node-in-binary-tree.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2013-11-16 19:17:05
 *
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define true 1
#define false 0

/* A binary tree node has data, pointer to left child
   and a pointer to right child */
struct node
{
    int data;
    struct node* left;
    struct node* right;
};

/* Helper function that allocates a new node with the
   given data and NULL left and right pointers. */
struct node* newnode(int data) {
    struct node* node = (struct node*)
        malloc(sizeof(struct node));
    node->data = data;
    node->left = NULL;
    node->right = NULL;

    return(node);
}

int printAncestors(struct node* node, int target) {
    if(node == 0)
        return false;
    if(node->data == target) {
        return true;
    }
    if(printAncestors(node->left, target) ||
       printAncestors(node->right, target)) {
        printf(" %d ", node->data);
        return true;
    } else {
        return false;
    }
}

/* Driver program to test above functions*/
int main()
{

    /* Construct the following binary tree
       1
       /   \
       2      3
       /  \
       4     5
       /
       7
    */
    struct node *root = newnode(1);
    root->left        = newnode(2);
    root->right       = newnode(3);
    root->left->left  = newnode(4);
    root->left->right = newnode(5);
    root->left->left->left  = newnode(7);

    printAncestors(root, 7);
    printf("\n");
    return 0;
}
