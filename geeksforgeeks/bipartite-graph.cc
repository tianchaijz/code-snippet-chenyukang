#include <iostream>
#include <vector>
#include <list>
#include <queue>
using namespace std;

#define  N 4

/* Use BFS to color vertex, if finding a edge(u, v) color(u) and color(v) is the same color, then this edge is not a bipartite graph, NOTE: this implimentation is only work for a
   strong connected graph */
bool isBipartite(int graph[][N], int src) {
    int color[N];
    for(int i=0; i<N; i++)
        color[i] = -1;
    color[src] = 1;
    queue<int> Q;
    Q.push(src);
    while(!Q.empty()) {
        int u = Q.front();
        Q.pop();
        for(int i=0; i<N; i++) {
            int v = graph[u][i];
            if(v == 0) continue;
            if(color[i] == -1) {
                color[i] = 1 - color[u];
                Q.push(v);
            } else if(color[i] == color[u]) {
                return false;
            }
        }
    }
    return true;
}

int main() {
    int G[][N] = {{0, 1, 0, 1},
                  {1, 0, 1, 0},
                  {0, 1, 0, 1},
                  {1, 0, 1, 0}
    };

    isBipartite(G, 0) ? cout << "Yes" << std::endl : cout << "No" << std::endl;
    return 0;
}
