//http://www.geeksforgeeks.org/eulerian-path-and-circuit/
//Time Complexity: O(V+E)
//Just for undirected graph,
//for a undirected graph, the number of nodes with odd degree will not be 1

#include <iostream>
#include <list>
using namespace std;

class Graph {
    int V;
    list<int> *adj;

private:
    bool isConnected() const;
    void dfs(int v, bool visited[]) const;
public:
    Graph(int v);
    ~Graph();

    void addEdge(int u, int v);
    int isEulerian() const;
};

Graph::Graph(int v) : V(v) {
    adj = new list<int> [V];
}

Graph::~Graph() {
    delete [] adj;
}

void Graph::addEdge(int u, int v) {
    adj[u].push_back(v);
    adj[v].push_back(u);
}

void Graph::dfs(int u, bool visited[]) const {
    visited[u] = true;
    list<int>::iterator it;
    for(it = adj[u].begin(); it != adj[u].end(); it++) {
        int v = *it;
        if(visited[v] == false)
            dfs(v, visited);
    }
}

bool Graph::isConnected() const {
    bool visited[V];
    for(int i=0; i<V; i++)
        visited[i] = false;

    int x;
    for(x=0; x<V; x++) {
        if(adj[x].size() != 0)
            break;
    }

    if(x == V) {
        return true;
    }

    dfs(x, visited);

    for(int i=0; i<V; i++) {
        if(visited[i] == false && adj[i].size() > 0)
            return false;
    }
    return true;
}

// 0 - not Eulerian
// 1 - Eulerian
// 2 - Eulerian cycle
int Graph::isEulerian() const {
    if(!isConnected())
        return 0;

    int odd = 0;
    for(int i=0; i<V; i++) {
        if(adj[i].size() & 1)
           odd++;
    }

    if(odd > 2) return 0;
    if(odd == 2) return 1;
    return 2; //odd == 0
}


void test(const Graph& g) {
    int r = g.isEulerian();
    if(r == 0) {
        std::cout << "No Eulerian" << std::endl;
    } else if(r == 1) {
        std::cout << "Eulerian" << std::endl;
    } else {
        std::cout << "Eulerian Cycle" << std::endl;
    }
}


int main() {
    // Let us create and test graphs shown in above figures
    Graph g1(5);
    g1.addEdge(1, 0);
    g1.addEdge(0, 2);
    g1.addEdge(2, 1);
    g1.addEdge(0, 3);
    g1.addEdge(3, 4);
    test(g1);

    Graph g2(5);
    g2.addEdge(1, 0);
    g2.addEdge(0, 2);
    g2.addEdge(2, 1);
    g2.addEdge(0, 3);
    g2.addEdge(3, 4);
    g2.addEdge(4, 0);
    test(g2);

    Graph g3(5);
    g3.addEdge(1, 0);
    g3.addEdge(0, 2);
    g3.addEdge(2, 1);
    g3.addEdge(0, 3);
    g3.addEdge(3, 4);
    g3.addEdge(1, 3);
    test(g3);

    // Let us create a graph with 3 vertices
    // connected in the form of cycle
    Graph g4(3);
    g4.addEdge(0, 1);
    g4.addEdge(1, 2);
    g4.addEdge(2, 0);
    test(g4);

    // Let us create a graph with all veritces
    // with zero degree
    Graph g5(3);
    test(g5);
    return 0;
}
