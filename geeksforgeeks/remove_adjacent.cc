//http://www.geeksforgeeks.org/recursively-remove-adjacent-duplicates-given-string/
//the answer is not unique
// since zaazxxxzy may get output:
// y or zy

#include <string.h>
#include <iostream>
#include <stack>
using namespace std;


string remove_adj(const string& str) {
    unsigned int len = str.length();
    char st[len];
    unsigned int cnt = 0;
    memset(st, 0, sizeof(st));

    for(int k=0; k<len; ) {
        char v = str[k];
        if(cnt == 0) {
            st[cnt++] = v;
            k++;
        }
        else if(st[cnt-1] == v) {
            st[--cnt] = '\0';
            while(k<len && str[k] == v)
                k++;
        } else {
            st[cnt++] = v;
            k++;
        }
    }
    st[cnt] = '\0';
    return string(st);
}




int main() {
    string s;
    while(cin >> s) {
        std::cout << remove_adj(s) << std::endl;
    }
    return 0;
}
