#include <iostream>
#include <list>
using namespace std;

class Graph {
    int V;
    list<int> *adj;

private:
    bool isConnected() const;
    void dfs(int v, bool visited[]) const;
    void printEulerTour_iter(int u);
    void rmEdge(int u, int v);
    int  dfsCount(int u, bool visited[]);
    bool isValidNext(int u, int v);
public:
    Graph(int v);
    ~Graph();

    int  isEulerian() const;
    void addEdge(int u, int v);
    void printEulerTour();
};

Graph::Graph(int v) : V(v) {
    adj = new list<int> [V];
}

Graph::~Graph() {
    delete [] adj;
}

void Graph::addEdge(int u, int v) {
    list<int>::iterator it;
    it = find(adj[u].begin(), adj[u].end(), v);
    if(it == adj[u].end())
        adj[u].push_back(v);
    else
        *it = v;
    it = find(adj[v].begin(), adj[v].end(), u);
    if(it == adj[v].end())
        adj[v].push_back(u);
    else
        *it = u;
}


void Graph::dfs(int u, bool visited[]) const {
    visited[u] = true;
    list<int>::iterator it;
    for(it = adj[u].begin(); it != adj[u].end(); it++) {
        int v = *it;
        if(visited[v] == false)
            dfs(v, visited);
    }
}

bool Graph::isConnected() const {
    bool visited[V];
    for(int i=0; i<V; i++)
        visited[i] = false;

    int x;
    for(x=0; x<V; x++) {
        if(adj[x].size() != 0)
            break;
    }

    if(x == V)
        return true;

    dfs(x, visited);
    for(int i=0; i<V; i++) {
        if(visited[i] == false && adj[i].size() > 0)
            return false;
    }
    return true;
}

// 0 - not Eulerian
// 1 - Eulerian
// 2 - Eulerian cycle
int Graph::isEulerian() const {
    if(!isConnected())
        return 0;

    int odd = 0;
    for(int i=0; i<V; i++) {
        if(adj[i].size() & 1)
            odd++;
    }

    if(odd > 2) return 0;
    if(odd == 2) return 1;
    return 2; //odd == 0
}


void Graph::rmEdge(int u, int v) {
    list<int>::iterator it;
    it = find(adj[u].begin(), adj[u].end(), v);
    *it = -1;
    it = find(adj[v].begin(), adj[v].end(), u);
    *it = -1;
}

int Graph::dfsCount(int u, bool visited[]) {
    int count = 1;
    visited[u] = true;
    list<int>::iterator it;
    for(it = adj[u].begin(); it != adj[u].end(); ++it) {
        int v = *it;
        if(v != -1 && visited[v]) {
            count += dfsCount(v, visited);
        }
    }
    return count;
}

bool Graph::isValidNext(int u, int v) {
    int count = 0;
    list<int>::iterator it;
    for(it = adj[u].begin(); it != adj[u].end(); ++it) {
        if(*it != -1)
            count++;
    }
    if(count == 1)
        return true;

    bool visited[V];
    memset(visited, false, sizeof(visited));
    int precnt = dfsCount(u, visited);

    rmEdge(u, v);
    memset(visited, false, sizeof(visited));
    int nowcnt = dfsCount(u, visited);
    addEdge(u, v);

    return (precnt > nowcnt)? false : true;
}

void Graph::printEulerTour_iter(int u) {
    list<int>::iterator it;
    for(it = adj[u].begin(); it!= adj[u].end(); ++it) {
        int v = *it;
        if(v != -1 && isValidNext(u, v)) {
            std::cout<<" " << u << " --> " << v;
            rmEdge(u, v);
            printEulerTour_iter(v);
        }
    }
}

void Graph::printEulerTour() {
    if(!isEulerian()) {
        return;
    }

    int u = 0;
    //if the are nodes with odd degree, choose one,
    //otherwise use 0 as starting node
    for(int i=0; i<V; i++) {
        if(adj[i].size() & 1) {
            u = i;
            break;
        }
    }

    printEulerTour_iter(u);
    std::cout << std::endl;
}


int main() {
    // Let us first create and test graphs shown in above figure
    Graph g1(4);
    g1.addEdge(0, 1);
    g1.addEdge(0, 2);
    g1.addEdge(1, 2);
    g1.addEdge(2, 3);
    g1.printEulerTour();

    Graph g2(3);
    g2.addEdge(0, 1);
    g2.addEdge(1, 2);
    g2.addEdge(2, 0);
    g2.printEulerTour();

    Graph g3(5);
    g3.addEdge(1, 0);
    g3.addEdge(0, 2);
    g3.addEdge(2, 1);
    g3.addEdge(0, 3);
    g3.addEdge(3, 4);
    g3.addEdge(3, 2);
    g3.addEdge(3, 1);
    g3.addEdge(2, 4);
    g3.printEulerTour();
    return 0;
}
