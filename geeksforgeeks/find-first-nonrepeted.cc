//http://www.geeksforgeeks.org/find-first-non-repeating-character-stream-characters/
//when reading a stream, you need to tell the first non-repeated char in O(1) time
//a potential optimization, we do not need link list,


#include <string>
#include <iostream>
using namespace std;

#define MAX 256

typedef struct Node {
    struct Node* next;
    struct Node* prev;
    char value;
} Node;


typedef struct Store {
    Node* head;
    Node* tail;
    bool  repeated[MAX];
    Node* nodes[MAX];
}Store;


void AddToStore(Store& store, char v) {
    Node* node;
    if(store.repeated[v]) return;

    if(store.nodes[v] == 0) {
        store.nodes[v] = node = (Node*)malloc(sizeof(Node));
        node->value = v;
        node->prev = node->next = 0;
        //add node
        if(store.head == 0) {
            store.head = store.tail = node;
        } else {
            store.tail->next = node;
            node->prev = store.tail;
            store.tail = node;
        }
    } else {
        //delete node
        store.repeated[v] = true;
        node = store.nodes[v];

        if(store.head == node)
            store.head = node->next;
        if(store.tail == node)
            store.tail = node->prev;

        if(node->next)
            node->next->prev = node->prev;
        if(node->prev)
            node->prev->next = node->next;
        free(node);
    }
}

char FirstNonRepeat(const Store& store) {
    if(store.head == 0) {
        return '*';
    }
    return store.head->value;
}


void FreeStore(Store& store) {
    Node* n = store.head;
    Node* next = 0;
    while(n) {
        next = n->next;
        free(n);
        n = next;
    }
}

int main() {
    string s;
    cin >> s;
    Store store;

    store.head = store.tail = 0;
    memset(store.repeated, 0, sizeof(store.repeated));
    memset(store.nodes, 0, sizeof(store.nodes));

    Node* head = 0;
    Node* tail = 0;
    for(int k=0; k<s.size(); k++) {
        std::cout<<"reading : " << s[k] << std::endl;
        AddToStore(store, s[k]);
        std::cout<<"first non-repeated char: " << FirstNonRepeat(store) << std::endl;
    }

    FreeStore(store);
    return 0;
}
