//http://www.geeksforgeeks.org/tree-isomorphism-problem/
#include <iostream>
#include <string>
using namespace std;

struct Node {
    int data;
    struct Node* left;
    struct Node* right;
};


struct Node* newNode(int data) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->data = data;
    node->left = node->right = 0;
    return node;
}

bool isIsomorphicIter(struct Node* root1, struct Node* root2) {
    if( root1 == NULL && root2 == NULL)
        return true;
    if ((root1 == NULL && root2 != NULL) ||
        (root2 == NULL && root1 != NULL) ||
        (root1->data != root2->data))
        return false;
    return
        (isIsomorphicIter(root1->left, root2->left) &&
         isIsomorphicIter(root1->right, root2->right)) ||
        (isIsomorphicIter(root1->left, root2->right) &&
         isIsomorphicIter(root1->right, root2->left));
}

bool isIsomorphic(struct Node* root1, struct Node* root2) {
    return isIsomorphicIter(root1, root2);
}


int main() {
    // Let us create trees shown in above diagram
    struct Node *n1 = newNode(1);
    n1->left        = newNode(2);
    n1->right       = newNode(3);
    n1->left->left  = newNode(4);
    n1->left->right = newNode(5);
    n1->right->left  = newNode(6);
    n1->left->right->left = newNode(7);
    n1->left->right->right = newNode(8);

    struct Node *n2 = newNode(1);
    n2->left         = newNode(3);
    n2->right        = newNode(2);
    n2->right->left   = newNode(4);
    n2->right->right   = newNode(5);
    n2->left->right   = newNode(6);
    n2->right->right->left = newNode(8);
    n2->right->right->right = newNode(7);

    if (isIsomorphic(n1, n2) == true)
        cout << "Yes";
    else
        cout << "No";
    std::cout << std::endl;
    return 0;
}
