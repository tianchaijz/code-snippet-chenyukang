#include <vector>
#include <iostream>
using namespace std;


int findPeakIter(int arr[], int low, int high, int num) {
    if(low == high)
        return arr[low];
    int mid = (low + high) / 2;
    if((mid == 0 || arr[mid-1] <= arr[mid]) &&
       (mid == num - 1 || arr[mid + 1] <= arr[mid]))
        return arr[mid];
    if(mid > 1 && arr[mid - 1] > arr[mid])
        return findPeakIter(arr, low, mid - 1, num);
    else
        return findPeakIter(arr, low, mid - 1, num);
}

int findPeak(int arr[], int len) {
    return findPeakIter(arr, 0, len - 1, len);
}

/* Driver program to check above functions */
int main()
{
    int arr[] = {1, 3, 20, 4, 1, 0};
    int n = sizeof(arr)/sizeof(arr[0]);
    printf("Index of a peak point is %d\n", findPeak(arr, n));
    return 0;
}
