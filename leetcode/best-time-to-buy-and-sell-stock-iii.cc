#include <iostream>
#include <vector>
#include <cassert>
using namespace std;

class Solution {
public:

    int maxProfit_iter(vector<int>& prices, int begin, int end) {
        int min = INT_MAX;
        int res = 0;
        for(int i=begin; i<=end; i++) {
            if(prices[i] < min)
                min = prices[i];
            int diff = prices[i] - min;
            if(diff > res)
                res = diff;
        }
        return res;
    }

    int maxProfit_slow(vector<int> &prices) {
        int ans = 0;
        if(prices.size() == 0)
            return 0;
        for(int i=1; i<prices.size(); i++) {
            int pro =
                maxProfit_iter(prices, 0, i) +
                maxProfit_iter(prices, i+1, prices.size() - 1);
            ans = max(pro, ans);
        }
        return ans;
    }


    int maxProfit(vector<int>& prices) {
        vector<int> dp(prices.size() + 1, 0);

        if(prices.size() == 0)
            return 0;
        int ans = 0;
        int minval = prices[0];
        int profit = 0;
        int len = prices.size();
        int diff;
        for(int i=1; i<len; i++) {
            diff = prices[i] - minval;
            profit = max(profit, diff);
            dp[i] = profit;
            minval = min(minval, prices[i]);
        }

        profit = 0;
        int maxval = prices[len-1];
        for(int i=len-2; i>=0; i--) {
            diff = maxval - prices[i];
            profit = max(profit, diff);
            ans = max(ans, profit + dp[i]);
            maxval = max(maxval, prices[i]);
        }
#if 0
        for(int i=0; i<len; i++){
            printf("%d ", dp[i]);
        }
        printf("\n");
#endif
        return ans;
    }


};


int test0() {
    vector<int> vec;
    vec.push_back(1);
    vec.push_back(2);
    Solution p;
    int ans = p.maxProfit(vec);
    printf("ans: %d\n", ans);
    return 0;
}

int test1() {
    vector<int> vec;
    vec.push_back(1);
    vec.push_back(2);
    vec.push_back(4);
    Solution p;
    int ans = p.maxProfit(vec);
    printf("ans: %d\n", ans);
    return 0;
}

int test2() {
    vector<int> vec;
    vec.push_back(6);    vec.push_back(1);
    vec.push_back(3);    vec.push_back(2);
    vec.push_back(4);    vec.push_back(7);
    Solution p;
    int ans = p.maxProfit(vec);
    printf("ans: %d\n", ans);
    return 0;
}

int main() {
    test0();
    test1();
    test2();
    return 0;
}
