

# Definition for singly-linked list.
class ListNode
    attr_accessor :val, :next
    def initialize(val)
        @val = val
        @next = nil
    end
end

# @param {ListNode} head
# @return {ListNode}
def delete_duplicates(head)
  return head if head.nil? or head.next.nil?
  cur, nxt = head, head.next
  while nxt
    if nxt.val == cur.val
      nxt = nxt.next
    else
      cur.next = nxt
      cur = nxt
      nxt = nxt.next
    end
  end
  cur.next = nil
  head
end
