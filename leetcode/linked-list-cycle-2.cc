#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode* detectCycle(ListNode *head) {
        if(head == NULL || head->next == NULL)
            return NULL;
        ListNode* t1 = head;
        ListNode* t2 = head;
        ListNode* meet = NULL;
        while(t1 && t2) {
            t1 = t1->next;
            if(t2->next == NULL)
                return NULL;
            t2 = t2->next->next;
            if(t1 == t2) {
                meet = t1;
                break;
            }
        }
        t1 = head, t2 = meet;
        if(t1 == t2) return t1;
        while(t1 && t2 ) {
            t1 = t1->next;
            t2 = t2->next;
            if(t1 == t2)
                return t1;
        }
        return NULL;
    }
};

void test() {
    Solution p;
    ListNode* root = new ListNode(3);;
    root->next = new ListNode(2);
    root->next->next = new ListNode(0);
    root->next->next->next = new ListNode(-4);
    root->next->next->next->next = root->next;
    std::cout << p.detectCycle(root)->val << std::endl;
}

int main() {
    Solution p;
    ListNode* root = new ListNode(1);
    std::cout << p.detectCycle(root) << std::endl;
    root->next = new ListNode(2);
    root->next->next = root;
    std::cout << p.detectCycle(root)->val << std::endl;
    test();
    return 0;
}
