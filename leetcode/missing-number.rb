

# @param {Integer[]} nums
# @return {Integer}
def missing_number(nums)
  return 0 if nums.all? { |x| x > 0 }
  n = nums.size
  sum = n  * (n + 1) / 2
  sum - nums.reduce(&:+)
end


