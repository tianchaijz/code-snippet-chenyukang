#include <iostream>
using namespace std;

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        if(m == n) return head;
        ListNode* p = head;
        ListNode* pre = NULL;
        int k = m;
        while(k > 1) {
            pre = p;
            p = p->next;
            k--;
        }
        ListNode* q = p->next;
        ListNode* f = p;
        ListNode* t;
        ListNode* r = NULL;
        int diff = n - m;
        while(diff > 0) {
            t = q->next;
            if(pre) {
                q->next = pre->next;
                pre->next = q;
            }
            else {
                if(r == NULL) {
                    q->next = f;
                    r = q;
                } else {
                    q->next = r;
                    r = q;
                }
            }
            q = t;
            f->next = t;
            diff--;
        };
        return r != NULL? r : head;
    }
};

void print(ListNode* node) {
    ListNode* p = node;
    while(p) {
        printf("%d ", p->val);
        p = p->next;
    }
    printf("\n");
}

int main() {
    ListNode* head = new ListNode(1);
    head->next = new ListNode(2);
    head->next->next = new ListNode(3);
    head->next->next->next = new ListNode(4);
    Solution p;
    ListNode* res = p.reverseBetween(head, 3, 4);
    print(res);
    return 0;
}
