#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int search(int A[], int n, int target, int dir) {
        int l = 0;
        int r = n;
        while(l <= r) {
            if(l == r) {
                if(A[l] == target) return l;
                else return -1;
            }
            int mid = (l + r) / 2;
            if(A[mid] < target)
                l = mid + 1;
            else if(A[mid] > target)
                r = mid - 1;
            else {
                if(dir == -1) {
                    if(l + 1 == r )
                        return A[l] == target? l : r;
                    r = mid;
                }
                else {
                    if(l + 1 == r)
                        return A[r] == target? r : l;
                    l = mid;
                }
            }
        }
        return -1;
    }

    vector<int> searchRange(int A[], int n, int target) {
        vector<int> res;
        int left = search(A, n-1, target, -1);
        res.push_back(left);
        if(left == -1) {
            res.push_back(-1);
            return res;
        }
        int right = search(A, n-1, target, 1);
        res.push_back(right);
        return res;
    }
};

int main() {
    int A[] = {5, 7, 7, 8, 8, 10};
    Solution p;
    vector<int> res = p.searchRange(A, sizeof(A)/sizeof(int), 10);
    for(int i=0; i<res.size(); i++) {
        std::cout << res[i] << std::endl;
    }
    return 0;
}
