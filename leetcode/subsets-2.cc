#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    void generate(vector<vector<int> >& res, vector<vector<int> > es, int num) {
        if(num >= es.size()) return;
        vector<int> now = es[num];
        int len = res.size();
        for(int j=0; j<len; j++) {
            for(int i=0; i<now.size(); i++) {
                vector<int> x = res[j];
                int k = i+1;
                while(k) {
                    x.push_back(now[i]);
                    k--;
                }
                res.push_back(x);
            }
        }
        generate(res, es, num+1);
    }

    vector<vector<int> > subsetsWithDup(vector<int> &S) {
        vector<vector<int> > es;
        vector<vector<int> > res;
        sort(S.begin(), S.end());
        for(int i=0; i<S.size(); i++) {
            int len = es.size();
            if( len == 0 || es[len-1][0] != S[i]) {
                es.push_back(vector<int>(1, S[i]));
            } else {
                es[len-1].push_back(S[i]);
            }
        }
        res.push_back(vector<int>(0, 0));
        generate(res, es, 0);
        return res;

    }
};


void print(const vector<vector<int> >& vec) {
    for(int i=0; i<vec.size(); i++) {
        for(int j=0; j<vec[i].size(); j++)
            std::cout << vec[i][j] << " ";
        std::cout << std::endl;
    }
}

int main() {
    vector<int> vec;
    vec.push_back(1);
    vec.push_back(2);
    vec.push_back(2);
    Solution p;
    vector<vector<int> > res = p.subsetsWithDup(vec);
    print(res);
    return 0;
}
