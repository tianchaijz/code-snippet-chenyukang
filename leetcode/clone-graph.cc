#include <map>
#include <set>
#include <vector>
#include <iostream>
using namespace std;

/**
 * Definition for undirected graph. */
struct UndirectedGraphNode {
    int label;
    vector<UndirectedGraphNode *> neighbors;
    UndirectedGraphNode(int x) : label(x) {};
};

class Solution {
public:
    UndirectedGraphNode *cloneNode(UndirectedGraphNode *node,
                                   map<int, UndirectedGraphNode*>& table) {
        if(node == NULL) return NULL;
        int label = node->label;
        if(table.find(label) != table.end()) {
            return table[label];
        } else {
            UndirectedGraphNode* newOne = new UndirectedGraphNode(label);
            table[label] = newOne;

            for(int i=0; i<node->neighbors.size(); i++) {
                newOne->neighbors.push_back(cloneNode(node->neighbors[i], table));
            }
            return newOne;
        }
    }

    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
        map<int, UndirectedGraphNode* > table;
        return cloneNode(node, table);
    }
};

int main() {
    return 0;
}
