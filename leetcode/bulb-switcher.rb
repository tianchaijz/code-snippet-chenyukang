
# @param {Integer} n
# @return {Integer}
def bulb_switch(n)
  i = 1
  ans = 0
  while i * i  <= n
    ans += 1
    i += 1
  end
  ans
end
