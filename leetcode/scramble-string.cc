#include <iostream>
#include <string>
#include <vector>
#include <cassert>
using namespace std;


class Solution {
public:
    bool isScramble(string s1, string s2) {
        int vec[26];
        memset(vec, 0, sizeof(vec));
        for(int i=0; i<s1.size(); i++)
            vec[s1[i]-'a']++;
        for(int i=0; i<s2.size(); i++)
            vec[s2[i]-'a']--;
        for(int i=0; i<26; i++) {
            if(vec[i] != 0)
                return false;
        }
        if(s1.size() == 1 && s2.size() == 1)
            return true;
        for(int i=1; i<s1.size(); i++) {
            if(isScramble(s1.substr(0, i), s2.substr(0, i)) &&
               isScramble(s1.substr(i), s2.substr(i)))
                return true;
            if(isScramble(s1.substr(0, i), s2.substr(s2.size() - i)) &&
               isScramble(s1.substr(i), s2.substr(0, s2.size() - i)))
                return true;
        }
        return false;
    }
};

int main() {
    Solution p;
    bool ans = p.isScramble("rgtae", "great");
    printf("ans: %d\n", (int)ans);
    return 0;
}
