#include <stdio.h>
#include <string.h>

bool canWinNim(int n) {
  return n % 4;
}

int main() {
  int n;
  scanf("%d", &n);
  printf("res: %d\n", canWinNim(n));
}
