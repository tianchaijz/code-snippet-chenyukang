#include <string>
#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    string covert(int num) {
        string res;
        while(num) {
            char a = '0';
            a = a + num%10;
            res = a + res;
            num /= 10;
        }
        return res;
    }

    string generate(string now) {
        if(now.size() == 0)
            return now;
        vector<int> nums;
        vector<char> vals;
        string res;
        char prev = now[0];
        int time = 0;
        for(int i=0; i<now.size(); i++) {
            if(now[i] == prev) {
                time++;
            } else {
                nums.push_back(time);
                vals.push_back(prev);
                prev = now[i];
                time = 1;
            }
        }
        if(time) {
            nums.push_back(time);
            vals.push_back(prev);
        }

        for(int i=0; i<nums.size(); i++) {
            int num = nums[i];
            string x = covert(num);
            res = res + x + vals[i];
        }
        return res;
    }

    string countAndSay(int n) {
        string next = "1";
        //std::cout <<next << std::endl;
        for(int i=0; i<n-1; i++) {
            next = generate(next);
            //std::cout << next << std::endl;
        }
        return next;
    }
};

int main() {
    Solution p;
    string res = p.countAndSay(6);
    std::cout << "res: " << res << std::endl;
    return 0;
}
