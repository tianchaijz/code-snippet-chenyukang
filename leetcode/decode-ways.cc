#include <iostream>
#include <string>
#include <cassert>
#include <vector>
using namespace std;

class Solution {
public:
    int numDecodings(string s) {
        if(s.size() == 0) return 0;
        vector<int> dp(s.size() + 1, 0);
        dp[0] = 0;
        if(s[0] >= '1' && s[0] <= '9') {
            dp[0] = 1;
        } else {
            return 0;
        }
        for(int i=1; i<s.size(); i++) {
            string n = s.substr(i-1, 2);
            int x = atoi(n.c_str());
            if(s[i] == '0') {
                if (x >=1 && x <= 26) {
                    if(i == 1)
                        dp[i] = 1;
                    else
                        dp[i] = dp[i-2];
                }
                else
                    return 0;
            } else if(s[i-1] == '0') {
                dp[i] = dp[i-1];
            }
            else {
                if(x >=1 && x <= 26) {
                    if(i == 1)
                        dp[i] = 2;
                    else
                        dp[i] = dp[i-1] + dp[i-2];
                } else
                    dp[i] = dp[i-1];
            }
        };
        return dp[s.size()-1];
    }
};


void test1() {
    Solution p;
    std::cout << p.numDecodings("10") << std::endl;
    std::cout << p.numDecodings("11") << std::endl;
    std::cout << p.numDecodings("110") << std::endl;
    std::cout << p.numDecodings("100") << std::endl;
    std::cout << p.numDecodings("101") << std::endl;
    std::cout << p.numDecodings("01") << std::endl;
    std::cout << p.numDecodings("18") << std::endl;
    std::cout << p.numDecodings("11") << std::endl;
    std::cout << p.numDecodings("") << std::endl;
    std::cout << p.numDecodings("0") << std::endl;
    std::cout << p.numDecodings("1") << std::endl;
    std::cout << p.numDecodings("10") << std::endl;
    std::cout << p.numDecodings("90") << std::endl;
    std::cout << p.numDecodings("1212") << std::endl;
}

void test2() {
    Solution p;
    string s = "4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948";
    std::cout << p.numDecodings(s) << std::endl;
}

int main() {
    test1();
    test2();
    return 0;
}
