#include <string>
#include <set>
#include <vector>
#include <map>
#include <queue>
#include <unordered_set>
#include <iostream>
using namespace std;

struct Node {
    string str;
    int step;

    Node(string s, int n) : str(s), step(n) {}
};

class Solution {
public:
    //pass all the testcases
    int ladderLength(string start, string end, unordered_set<string> &dict) {
        dict.insert(start);
        dict.insert(end);

        //BFS
        set<string> visited;
        queue<Node> Q;
        Q.push(Node(start, 1));
        visited.insert(start);
        while(!Q.empty()) {
            Node now = Q.front();
            Q.pop();
            if(now.str == end)  {
                return now.step;
            }
            string val = now.str;
            for(int i=0; i<val.size(); i++) {
                for(char k = 'a'; k<= 'z'; k++) {
                    if(k == val[i]) continue;
                    string next = val;
                    next[i] = k;
                    if(dict.find(next) != dict.end() &&
                       visited.find(next) == visited.end()) {
                        visited.insert(next);
                        Q.push(Node(next, now.step+1));
                    }
                }
            }
        }
        return 0;
    }

#if 0
    //why this is failed for big testcase
    //build table is slow?
    int ladderLength_v2(string start, string end, unordered_set<string> &dict) {
        map<string, set<string> > table;
        //build the table;
        dict.insert(start);
        dict.insert(end);
        for(unordered_set<string>::iterator it = dict.begin();
            it != dict.end(); ++it) {
            string key = *it;
            for(int i=0; i<start.size(); i++) {
                for(char x = 'a'; x <= 'z'; x++) {
                    if(x == key[i]) continue;
                    string editStr = key;
                    editStr[i] = x;
                    unordered_set<string>::iterator t = dict.find(editStr);
                    if(t != dict.end()) {
                        table[key].insert(editStr);
                        table[editStr].insert(key);
                    }
                }
            }
        }

        //BFS
        set<string> visited;
        queue<Node> Q;
        Q.push(Node(start, 1));
        visited.insert(start);
        while(!Q.empty()) {
            Node& now = Q.front();
            Q.pop();
            if(now.str == end)  { //target is at 1
                return now.step;
            }
            set<string>& s = table[now.str];
            for(set<string>::iterator it = s.begin(); it != s.end(); ++it) {
                string next = *it;
                if(visited.find(next) == visited.end()) {
                    visited.insert(next);
                    Q.push(Node(next, now.step + 1));
                }
            }
        }
        return 0;
    }
#endif

#if 0
    //try avoid string compare
    int ladderLength_v3(string start, string end, unordered_set<string> &dict) {
        map<string, int> strMap;
        vector<set<int> > table;

        if(start.size() != end.size())
            return 0;
        strMap[start] = 0;
        strMap[end] = 1;
        int idx = 2;
        for(unordered_set<string>::iterator it = dict.begin();
            it != dict.end(); ++it) {
            if(*it == start || *it == end)
                continue;
            strMap[*it] = idx++;
        }
        table.resize(dict.size() + 2);

        //build the table;
        for(map<string, int>::iterator it = strMap.begin();
            it != strMap.end(); ++it) {
            string key = it->first;
            int    idx = it->second;
            for(int i=0; i<start.size(); i++) {
                for(char x = 'a'; x <= 'z'; x++) {
                    if(x == key[i]) continue;
                    string editStr = key;
                    editStr[i] = x;
                    map<string, int>::iterator t = strMap.find(editStr);
                    if(t != strMap.end()) {
                        table[idx].insert(t->second);
                        table[t->second].insert(idx);
                    }
                }
            }
        }

        //BFS
        vector<bool> visited(table.size(), false);

        queue<Node> Q;
        Q.push(Node(0, 1));
        while(!Q.empty()) {
            Node& now = Q.front();
            Q.pop();
            visited[now.value] = true;
            if(now.value == 1)  { //target is at 1
                return now.step;
            }
            set<int>& s = table[now.value];
            for(set<int>::iterator it = s.begin(); it != s.end(); ++it) {
                int next = *it;
                if(!visited[next]) {
                    Q.push(Node(next, now.step + 1));
                }
            }
        }
        return 0;
    }
#endif
};

void test1() {
    unordered_set<string> dict;
    dict.insert("hot");
    dict.insert("dot");
    dict.insert("dog");
    dict.insert("lot");
    dict.insert("log");
    Solution p;
    int ans = p.ladderLength("hit", "cog", dict);
    std::cout << "ans: " << ans << std::endl;
}

void test2() {
    unordered_set<string> dict;
    dict.insert("a");
    dict.insert("b");
    dict.insert("c");
    Solution p;
    int ans = p.ladderLength("a", "c", dict);
    std::cout << "ans2: " << ans << std::endl;
}


int main() {
    test2();
    return 0;
}
