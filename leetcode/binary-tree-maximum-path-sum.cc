#include <vector>
#include <iostream>
#include <stdio.h>
#include <climits>
using namespace std;

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
private:
    int ans;
public:

    int maxPathSum(TreeNode *root) {
        ans = INT_MIN;
        int val = iter(root);
        return max(val, ans);
    }

    int iter(TreeNode* node) {
        if(node == NULL) return 0;
        int left = iter(node->left);
        int right = iter(node->right);
        int val = node->val;
        if(left > 0)
            val += left;
        if(right > 0)
            val += right;
        ans = max(ans, val);
        return max(node->val, max(left + node->val, right + node->val));
    }

#if 0
    //bad code !
    void iter_bad(TreeNode* root, int& upVal, int& downVal) {
        if(root == NULL) {
            upVal = downVal = INT_MIN;
            return;
        }

        int val = root->val;
        int left1, left2;
        int right1, right2;
        int now = INT_MIN;
        if(root->left == NULL && root->right == NULL) {
            upVal = downVal = now = val;

        } else {
            iter_bad(root->left, left1, left2);
            iter_bad(root->right, right1, right2);
            int up = max(left1, right1);
            upVal = val;
            if(up > 0)
                upVal = up + val;
            if(left1 == INT_MIN)
                downVal = right1 + val;
            else if(right1 == INT_MIN)
                downVal = left1 + val;
            else
                downVal = left1 + right1 + val;
            int maxV = max(upVal, downVal);
            int maxDown = max(right1, right2);
            now = max(maxV, maxDown);

        }
        if(now > ans) {
            ans = now;
        }
    }
#endif

};


void test0() {
    TreeNode* root = new TreeNode(1);
    Solution p;
    int ans = p.maxPathSum(root);
    std::cout <<"ans: " << ans << std::endl;
}


void test1() {
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    root->right = new TreeNode(3);
    Solution p;
    int ans = p.maxPathSum(root);
    std::cout <<"ans: " << ans << std::endl;
}

void test2() {
    TreeNode* root = new TreeNode(2);
    TreeNode* left = root->left = new TreeNode(1);
    TreeNode* right = root->right = new TreeNode(3);
    left->left = new TreeNode(99);
    left->right = new TreeNode(100);
    right->left = new TreeNode(4);
    right->right = new TreeNode(4);
    Solution p;
    int ans = p.maxPathSum(root);
    std::cout <<"ans: " << ans << std::endl;
}

void test3() {
    TreeNode* root = new TreeNode(-3);
    Solution p;
    int ans = p.maxPathSum(root);
    std::cout <<"ans: " << ans << std::endl;
}


void test4() {
    TreeNode* root = new TreeNode(1);
    root->right = new TreeNode(-2);
    Solution p;
    int ans = p.maxPathSum(root);
    std::cout <<"ans: " << ans << std::endl;
}


int main() {
    test0();
    test1();
    test2();
    test3();
    test4();
    return 0;
}
