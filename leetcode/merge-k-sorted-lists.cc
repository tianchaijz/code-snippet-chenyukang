#include <vector>
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cassert>
#include <queue>
using namespace std;


/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};


struct cmp {
    bool operator() (ListNode* a, ListNode* b) {
        return a->val > b->val;
    }
};


class Solution {
public:
    ListNode *mergeKLists(vector<ListNode *> &lists) {
        vector<ListNode*> vec = lists;
        ListNode res(0);
        ListNode* p = &res;
        priority_queue<ListNode*, vector<ListNode*>, cmp> Q;
        for(int i=0; i<lists.size(); i++) {
            if(lists[i] != NULL)
                Q.push(lists[i]);
        }
        while(!Q.empty()) {
            ListNode* t = Q.top();
            Q.pop();
            if(t->next)
                Q.push(t->next);
            t->next = NULL;
            p->next = t;
            p = t;
        }
        return res.next;
    }
};

ListNode* createList(int begin, int step, int num) {
    ListNode res(0);
    ListNode* p = &res;
    for(int i=0; i<=num;  i++) {
        p->next = new ListNode(begin + step*i);
        p = p->next;
    }
    return res.next;
}

void print(ListNode* head) {
    ListNode* p = head;
    while(p) {
        printf("%d ", p->val);
        p = p->next;
    }
    printf("\n");
}


void test1() {
    ListNode* l1 = createList(1, 2, 5);
    ListNode* l2 = createList(3, 3, 6);

    Solution p;
    print(l1);
    print(l2);
    vector<ListNode*> vec;
    vec.push_back(l1);
    vec.push_back(l2);
    ListNode* res = p.mergeKLists(vec);
    print(res);
}


void test2() {
    ListNode* l1 = createList(1, 2, 5);
    ListNode* l2 = createList(3, 3, 6);
    ListNode* l3 = createList(10, 3, 6);

    Solution p;
    print(l1);
    print(l2);
    print(l3);
    vector<ListNode*> vec;
    vec.push_back(l1);
    vec.push_back(l2);
    vec.push_back(l3);
    ListNode* res = p.mergeKLists(vec);
    print(res);
}

int main() {
    test2();
    return 0;
}
