#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Solution {
public:
    vector<int> grayCode(int n) {
        vector<int> res;
        for(int i=0; i<pow(2.0, n); i++) {
            res.push_back((i/2) ^ (i));
        }
        return res;
    }
};


int main() {
    Solution p;
    vector<int> res = p.grayCode(4);
    return 0;
}
