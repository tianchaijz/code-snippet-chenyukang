#include <stdio.h>
#include <vector>
#include <iostream>
using namespace std;


class Solution {
public:
    int accum(int num) {
        int ans = 1;
        for(int k=1; k<=num; k++) {
            ans *= k;
        }
        return ans;
    }

    void iter(vector<int>& ans, vector<bool>& used, int index, int k) {
        if(index == used.size()) return;
        int left = accum(used.size() - 1 - index);
        if(k <= left) {
            for(int i=1; i<used.size(); i++) {
                if(!used[i]) {
                    ans.push_back(i);
                    used[i] = true;
                    break;
                }
            }
            iter(ans, used, index+1, k);
        } else {
            int i;
            for(i=1; i<used.size(); i++) {
                if(used[i]) continue;
                if(k - left <= 0)
                    break;
                else
                    k -= left;
            }
            ans.push_back(i);
            used[i] = true;
            iter(ans, used, index+1, k);
        }
    }

    string getPermutation(int n, int k) {
        string str;
        vector<int> ans;
        vector<bool> used(n+1, false);
        iter(ans, used, 1, k);
        for(int i=0; i<ans.size(); i++) {
            char x = '0' + ans[i];
            str.push_back(x);
        }
        return str;
    }
};


void test(int n, int k) {
    Solution p;
    string res = p.getPermutation(n, k);
    std::cout << res << std::endl;
}

int main() {
    Solution p;
    test(3, 6);
    test(3, 2);
    test(3, 5);
    test(8, 1);
    test(8, p.accum(8));
    test(9, 1);
    test(9, p.accum(9));
    return 0;
}
