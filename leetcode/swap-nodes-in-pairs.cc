#include <iostream>
#include <cassert>
using namespace std;


/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
        ListNode* x = head;
        ListNode* res = NULL;
        ListNode* prev = NULL;
        while(x) {
            ListNode* n = x->next;
            if(n) {
                ListNode* next = n->next;
                n->next = x;
                x->next = NULL;
                if(prev == NULL)
                    prev = x;
                else {
                    prev->next = n;
                    prev = x;
                }
                if(res == NULL)
                    res = n;
                x = next;
            } else {
                if(prev)
                    prev->next = x;
                break;
            }
        }
        if(res == NULL) res = x;
        return res;
    }
};

int main() {
    return 0;
}
