#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* singleNumber(int* nums, int numsSize, int* returnSize) {
  int xor = 0;
  int a = 0;
  int b = 0;
  int* res = (int*)malloc(sizeof(int) * 2);

  for(int i=0 ;i < numsSize; i++) {
    xor = xor ^ nums[i];
  }
  int mask = 1;
  while((xor & mask) == 0)
    mask <<= 1;

  for(int i=0; i < numsSize; i++) {
    if(nums[i] & mask)
      a ^= nums[i];
    else
      b ^= nums[i];
  }
  res[0] = a, res[1] = b;
  *returnSize = 2;
  return res;
}

int main() {
  return 0;
}
