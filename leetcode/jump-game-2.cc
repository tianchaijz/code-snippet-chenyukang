//Construct a Map, and using DFS
//Memory Limit Exceeded
#include <iostream>
#include <vector>
#include <set>
using namespace std;

class Solution {
public:
    vector<set<int> > construct(int A[], int n) {
        vector<set<int> > res(n);
        for(int i=0; i<n; i++) {
            for(int k=1; k<=A[i]; k++) {
                res[i].insert(k + i);
            }
        }
        return res;
    }

    bool dfs(vector<set<int> >& map, vector<bool>& visit, int now, int n) {
        if(now == n -1) return true;
        visit[now] = true;
        for(set<int>::iterator it = map[now].begin(); it != map[now].end(); ++it) {
            int next = *it;
            if(next <n && visit[next] == false) {
                if(dfs(map, visit, next, n))
                    return true;
            }
        }
        return false;
    }

    bool canJump(int A[], int n) {
        vector<set<int> > map = construct(A, n);
        vector<bool> visit(n, false);
        return dfs(map, visit, 0, n);
    }
};

int main() {
    Solution p;
    int A[] ={ 2,3,1,1,4 };
    int size = sizeof(A)/sizeof(int);
    int res = p.canJump(A, size);
    std::cout << res << std::endl;

    int B[] = {3,2,1,0,4};
    size = sizeof(B)/sizeof(int);
    res = p.canJump(B, size);
    std::cout << res << std::endl;

    int C[] = {1,2,2,6,3,6,1,8,9,4,7,6,5,6,8,2,
               6,1,3,6,6,6,3,2,4,9,4,5,9,8,2,2,
               1,6,1,6,2,2,6,1,8,6,8,3,2,8,5,8,
               0,1,4,8,7,9,0,3,9,4,8,0,2,2,5,5,
               8,6,3,1,0,2,4,9,8,4,4,2,3,2,2,5,
               5,9,3,2,8,5,8,9,1,6,2,5,9,9,3,9,
               7,6,0,7,8,7,8,8,3,5,0};

    size = sizeof(B)/sizeof(int);
    res = p.canJump(C, size);
    std::cout << res << std::endl;
    return 0;
}
