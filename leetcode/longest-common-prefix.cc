#include <iostream>
#include <vector>
#include <string>
using namespace std;


class Solution {
public:

    string common_prefix(string a, string b) {
        string res;
        for(int i=0; i<a.size() && i<b.size(); i++) {
            if(a[i] != b[i]) break;
            res = res + a[i];
        }
        return res;
    }

    string longestCommonPrefix(vector<string> &strs) {
        string ans;
        if(strs.size() == 0)
            return ans;
        ans = strs[0];
        if(strs.size() == 1)
            return ans;
        for(int i=1; i<strs.size(); i++) {
            ans = common_prefix(ans, strs[i]);
        }
        return ans;
    }
};

void test(vector<string> &strs) {
    Solution p;
    string ans = p.longestCommonPrefix(strs);
    std::cout << "ans: " << ans << std::endl;
}

int main() {
    vector<string> vec;
    vec.push_back("acbe");
    vec.push_back("ac");
    test(vec);
    return 0;
}
