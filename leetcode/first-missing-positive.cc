#include <stdio.h>
#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    int firstMissingPositive2(int A[], int n) {
        int i = 0;
        while (i < n)
        {
            if (A[i] != (i+1) && A[i] >= 1 && A[i] <= n && A[A[i]-1] != A[i])
                swap(A[i], A[A[i]-1]);
            else
                i++;
        }
        for (i = 0; i < n; ++i)
            if (A[i] != (i+1))
                return i+1;
        return n+1;
    }

    int firstMissingPositive(int A[], int n) {
        int lost = 1;
        int min = 0x7fffffff;
        for(int i=0; i<n; i++) {
            if(A[i] > 0 && A[i] < min)
                min = A[i];
        }
        if(min > 1 || min == 0x7fffffff)
            return 1;
        lost = min + 1;
        while(true) {
            bool found = false;
            for(int i=0; i<n; i++) {
                if(A[i] == lost) {
                    lost++;
                    found = true;
                }
            }
            if(!found)
                return lost;
        }
    }
};

int main() {
    Solution p;
    int A[] = {2, 1};
    int lost = p.firstMissingPositive(A, 2);
    std::cout << "lost:" << lost << std::endl;

    int B[] = {4, 8 , 9, 11,12};
    lost = p.firstMissingPositive2(B, 5);
    std::cout << "lost: " << lost << std::endl;
    return 0;

}
