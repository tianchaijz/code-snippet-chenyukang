#include <iostream>
using namespace std;


class Solution {
public:
    int climbStairs(int n) {
        if (n== 0) return 0;
        int k=0;
        int Arr[n+2];
        Arr[1] = 1;
        Arr[2] = 2;
        for(k=3; k<=n; k++) {
            Arr[k] = Arr[k-1] + Arr[k-2];
        }
        return Arr[n];
    }
};


int main() {
    Solution p;
    std::cout << p.climbStairs(1) << std::endl;
    std::cout << p.climbStairs(2) << std::endl;
    std::cout << p.climbStairs(10) << std::endl;
    return 0;
}
