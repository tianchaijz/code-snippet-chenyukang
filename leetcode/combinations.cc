#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

class Solution {
public:
    void generate(vector<vector<int> > &res, vector<int> now,  int num, int size, int k) {
        if(now.size() == k) {
            res.push_back(now);
        }
        for(int i=num+1; i<=size; i++) {
            vector<int> next = now;
            next.push_back(i);
            generate(res, next, i, size, k);
        }
    }

    vector<vector<int> > combine(int n, int k) {
        vector<vector<int> > res;
        vector<int> now;
        generate(res, now, 0, n, k);
        return res;
    }
};


int main() {
    Solution p;
    vector<vector<int> > res = p.combine(6, 3);
    for(int i=0; i<res.size(); i++) {
        for(int j=0; j<res[i].size(); j++) {
            printf("%d ", res[i][j]);
        }
        printf("\n");
    }
    return 0;
}
