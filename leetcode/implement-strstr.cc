#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
using namespace std;

class Solution {
public:
    char *strStr(char *haystack, char *needle) {
        int len1 = strlen(haystack);
        int len2 = strlen(needle);
        for(int i=0; i<=len1-len2; i++) {
            int k;
            for(k=0; k<len2; k++) {
                if(haystack[i+k] != needle[k])
                    break;
            }
            if(k == len2)
                return &haystack[i];
        }
        return NULL;
    }
};

int main() {
    Solution s;
    char s1[] = "ab";
    char s2[] = "b";
    char* p = s.strStr(s1, s2);
    if(p==NULL) {
        printf("null\n");
    } else {
        printf("%s\n", p);
    }
    return 0;
}
