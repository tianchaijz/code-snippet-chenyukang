#include <vector>
#include <assert.h>
#include <iostream>
using namespace std;

class Solution {
public:

    int candy(vector<int> &ratings) {
        int ans = 0;
        vector<int> vec(ratings.size(), 0);
        vec[0] = 1;
        for(int i=1; i<ratings.size(); i++) {
            if(ratings[i] > ratings[i-1])
                vec[i] = vec[i-1] + 1;
            else
                vec[i] = 1;
        }
        for(int i=ratings.size() - 2; i>=0; i--) {
            if(ratings[i] > ratings[i+1] && vec[i] <= vec[i+1] )
                vec[i] = vec[i+1] + 1;
        }
        for(int i=0; i<vec.size(); i++) {
            //printf("%d ", vec[i]);
            ans += vec[i];
        }
        return ans;
    }
};

#if 0
int candy_slow(vector<int> &ratings) {
    int ans = 0;
    if(ratings.size() == 0) return ans;
    vector<int> vec(ratings.size(), 0);
    vec[0] = 1;
    for(int i=1; i<ratings.size(); i++) {
        if(ratings[i] == ratings[i-1])
            vec[i] = 1;
        else if(ratings[i] > ratings[i-1])
            vec[i] = vec[i-1] +1;
        else {
            if(vec[i-1] == 1) { //adjust
                vec[i] = 1;
                int k = i;
                while(k>0 && ratings[k-1] > ratings[k] && vec[k-1] == vec[k]) {
                    vec[k-1]++;
                    k--;
                }
            } else {
                vec[i] = vec[i-1] -1;
            }
        }
    }
    for(int i=0; i<vec.size(); i++) {
        ans += vec[i];
    }
    return ans;
}
#endif

int main() {
    Solution p;
    vector<int> vec(3);
    vec[0] = 1, vec[1] = 0, vec[2] = 2;
    int ans = p.candy(vec);
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
