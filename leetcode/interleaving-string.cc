#include <iostream>
#include <vector>
#include <string>
#include <cassert>
using namespace std;


class Solution {
public:
    int dfs(string s1, string s2, string s3, int i, int j,
            vector<vector<int> >& dp) {
        if(dp[i][j] != -1)
            return dp[i][j];
        else {
            int idx = i + j;
            if(i < s1.size() && s3[idx] == s1[i]) {
                if(dfs(s1, s2, s3, i+1, j, dp)) {
                    dp[i][j] = 1;
                    return 1;
                }
            }

            if(j < s2. size() && s3[idx] == s2[j]) {
                if(dfs(s1, s2, s3, i, j+1, dp)) {
                    dp[i][j] = 1;
                    return 1;
                }
            }
            if(i == s1.size() && j == s2.size()) {
                dp[i][j] = 1;
                return 1;
            }
            dp[i][j] = 0;
            return 0;
        }
    }
    bool isInterleave(string s1, string s2, string s3) {
        vector<vector<int> > dp;
        int l1 = s1.size();
        int l2 = s2.size();
        int l3 = s3.size();
        if(l3 != l1 + l2) return false;

        dp.resize(l1+1);
        for(int i=0; i<l1+1; i++) {
            dp[i] = vector<int>(l2+1, -1);
        }
        return dfs(s1, s2, s3, 0, 0, dp) == 1;
    }
};

bool test(string a, string b, string c) {
    Solution p;
    return p.isInterleave(a, b, c);
}

int main() {
    assert(test("", "b", "b"));
    assert(test("aabcc", "dbbca", "aadbbcbcac"));
    assert(test("aabcc", "dbbca", "aadbbbaccc") == false);
    return 0;
}
