#include <iostream>
using namespace std;


class Solution {
public:
    int searchInsert(int A[], int n, int target) {
        int i;
        for(i=0; i<n; i++) {
            if(A[i] >= target)
                return i;
        }
        return i;
    }
};


/*
  [1,3,5,6], 5 → 2
  [1,3,5,6], 2 → 1
  [1,3,5,6], 7 → 4
  [1,3,5,6], 0 → 0
*/

void test(int A[], int N, int target) {
    Solution p;
    int pos = p.searchInsert(A, N, target);
    std::cout << "pos: "<< pos << std::endl;
}

int main() {
    int A[] = {1, 3, 5, 6};
    test(A, 4, 5);
    test(A, 4, 2);
    test(A, 4, 7);
    test(A, 4, 0);
    return 0;
}
