#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;


/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode* ToBST_iter(vector<ListNode*>& vec, int first, int last) {
        if(first > last) return NULL;
        if(first == last) {
            TreeNode* node = new TreeNode(vec[first]->val);
            return node;
        }
        int mid = (first + last) / 2;
        TreeNode* node = new TreeNode(vec[mid]->val);
        node->left = ToBST_iter(vec, first, mid - 1);
        node->right = ToBST_iter(vec, mid + 1, last);
        return node;
    }

    TreeNode *sortedListToBST(ListNode *head) {
        vector<ListNode*> vec;
        if(head == NULL) return NULL;
        ListNode* t = head;
        while(t) {
            vec.push_back(t);
            t = t->next;
        }
        return ToBST_iter(vec, 0, vec.size()-1);
    }
};

int main() {
    Solution p;
    ListNode* head = new ListNode(1);
    TreeNode* res = p.sortedListToBST(head);
    printf("res: %lu\n", (unsigned long)(res->val));

    return 0;
}
