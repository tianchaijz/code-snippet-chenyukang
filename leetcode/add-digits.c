#include <stdio.h>


int addDigits(int num) {
  return num - 9 * ((num - 1) / 9);
}

int main() {
  int n;
  scanf("%d", &n);
  printf("result: %d\n", addDigits(n));
  return 0;
}
