#include <iostream>
#include <stdio.h>
#include <cassert>
#include <vector>
#include <stack>
using namespace std;

/**
 * Definition for binary tree */
struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
private:
    void postorderTraversal_rec(vector<int>& vec, TreeNode* root) {
        if(root == NULL) return;
        if(root->left)
            postorderTraversal_rec(vec, root->left);
        if(root->right)
            postorderTraversal_rec(vec, root->right);
        vec.push_back(root->val);
    }

public:
    vector<int> postorderTraversal(TreeNode *root) {
        vector<int> res;
        TreeNode* past = 0;
        TreeNode* cur = root;
        stack<TreeNode*> st;
        if(root == NULL) return res;
        st.push(cur);
        while(!st.empty()) {
            cur = st.top();
            if((cur->left == NULL && cur->right == NULL) ||
               (past != NULL && (past == cur->left || past == cur->right))) {
                res.push_back(cur->val);
                st.pop();
                past = cur;
            }
            else {
                if(cur->right) st.push(cur->right);
                if(cur->left)  st.push(cur->left);
            }
        }
        return res;
    }
};


void printVec(const vector<int>& vec) {
    printf("size(%lu): ",  vec.size());
    for(int i=0; i<vec.size(); i++) {
        printf(" %d ", vec[i]);
    }
    printf("\n");
}

int test1() {
    vector<int> res;
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    root->left->left = new TreeNode(4);
    root->left->right = new TreeNode(5);
    root->right = new TreeNode(3);
    root->right->left = new TreeNode(6);
    Solution p;
    res = p.postorderTraversal(root);
    printVec(res);
    return 0;
}

int test2() {
    vector<int> res;
    TreeNode* root = new TreeNode(1);
    root->right = new TreeNode(2);
    root->right->right = new TreeNode(3);
    Solution p;
    res = p.postorderTraversal(root);
    printVec(res);
    return 0;
}

int test3() {
    vector<int> res;
    TreeNode* root = NULL;
    Solution p;
    res = p.postorderTraversal(root);
    printVec(res);
    return 0;
}

int main() {
    test1();
    test2();
    test3();
    return 0;
}
