#include <iostream>
#include <cassert>
#include <queue>
#include <stdio.h>
using namespace std;

struct TreeLinkNode {
    int val;
    TreeLinkNode *left;
    TreeLinkNode *right;
    TreeLinkNode *next;
    TreeLinkNode(int v) : val(v), left(NULL), right(NULL), next(NULL)
        {}
};

class Solution {
private:
    void level_do(TreeLinkNode* root) {
        queue<TreeLinkNode*> Q;
        TreeLinkNode* prev = NULL;
        if(root == NULL) return;
        Q.push(root);
        Q.push(NULL);
        while(!Q.empty()) {
            TreeLinkNode* node = Q.front();
            Q.pop();
            if(node == NULL) {
                if(Q.empty())
                    break;
                else {
                    Q.push(NULL);
                    prev = NULL;
                }
            } else {
                if(prev == NULL) prev = node;
                else {
                    prev->next = node;
                    prev = node;
                }
                if(node->left) Q.push(node->left);
                if(node->right) Q.push(node->right);
            }
        }
    }

public:
    void connect(TreeLinkNode* root) {
        level_do(root);
    }
};

int main() {
    TreeLinkNode* root = new TreeLinkNode(1);
    root->left = new TreeLinkNode(2);
    root->right = new TreeLinkNode(3);
    Solution p;
    p.connect(root);
    if(root->left->next) {
        int val = root->left->next->val;
        printf("next: %d\n", val);
    }
    return 0;
}
