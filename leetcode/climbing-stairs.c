

int climbStairs(int n) {
  if(n <= 2) return n;
  int p, q, x;
  p = 1, q = 2;
  while(n - 2 > 0) {
    x = p + q;
    p = q, q = x;
    n--;
  }
  return q;
}

int main() {
  return 0;
}
