#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;


/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
private:
    TreeNode* ToBST_iter(vector<int>& num, int first, int last) {
        if(first > last) return NULL;
        if(first == last) {
            TreeNode* node = new TreeNode(num[first]);
            return node;
        }
        int mid = (first + last) / 2;
        TreeNode* node = new TreeNode(num[mid]);
        node->left = ToBST_iter(num, first, mid - 1);
        node->right = ToBST_iter(num, mid + 1, last);
        return node;
    }

public:
    TreeNode *sortedArrayToBST(vector<int> &num) {
        if(num.size() == 0) return NULL;
        return ToBST_iter(num, 0, num.size() - 1);
    }
};


int main() {
    return 0;
}
