#include <stdio.h>
#include <iostream>
#include <queue>
#include <assert.h>
#include <vector>
#include <stack>
using namespace std;

/**
   Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:

    void flatten(TreeNode* root) {
        if(root == NULL) return;
        stack<TreeNode*> st;
        TreeNode* prev = NULL;
        st.push(root);
        while(!st.empty()) {
            TreeNode* n = st.top();
            st.pop();
            if(prev) {
                prev->left = NULL;
                prev->right = n;
            }
            prev = n;
            if(n->right) st.push(n->right);
            if(n->left) st.push(n->left);
        }
    }
};


void test() {
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    Solution p;
    p.flatten(root);
    assert(root->left == NULL);
    printf("right: %d\n", root->right->val);
}

int main() {
    TreeNode* root = new TreeNode(0);
    Solution p;
    p.flatten(root);
    //p.pre_iter(root);
    return 0;
}
