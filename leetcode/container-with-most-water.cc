#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

class Solution {
public:
    int maxArea(vector<int> &height) {
        int ans = 0;
        int head = 0;
        int tail = height.size() - 1;
        while(head < tail) {
            int area = abs(tail - head) * min(height[head], height[tail]);
            ans = max(area, ans);
            if(height[head] < height[tail])
                head++;
            else
                tail--;
        }
        return ans;
    }
};


int main() {
    return 0;
}
