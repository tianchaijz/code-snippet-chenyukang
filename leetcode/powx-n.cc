#include <cmath>
#include <iostream>
using namespace std;

#define EPS 0.00000000000000000001

class Solution {
public:
    double pow(double x, int n) {
        if(n == 1) return x;
        if(n == 0) return 1;
        if(abs(abs(x) - 1) < EPS) {
            if(x > 0) return x;
            else {
                if(n%2) return x;
                else return -x;
            }
        }
        if(n < 0) return 1.0/(pow(x, -n));
        if(n == 2) return x*x;
        if(n%2) return x * pow(x, n - 1);
        else {
            double res = pow(x, n/2);
            return res * res;
        }
    }
};

int main() {
    double x = 103.32;
    Solution p;
    double res = p.pow(0.00001, 2147483647);
    std::cout << "res: " << res << std::endl;
    res = p.pow(1.00000, -2147483648);
    std:: cout << "res: " << res << std::endl;
    res = p.pow(0.44894, -5);
    std:: cout << "res: " << res << std::endl;
    return 0;
}
