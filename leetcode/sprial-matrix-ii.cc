#include <stdio.h>
#include <vector>
using namespace std;

class Solution {
private:
    int step;
public:

    void iter(vector<vector<int> >& matrix, int row, int col) {
        for(int j=col; j<matrix[0].size() - col; j++) {
            matrix[row][j] = step++;
        }
        for(int i=row+1;  i<matrix.size() - row; i++) {
            matrix[i][matrix[0].size() - col - 1] = step++;
        }
        if(matrix.size() - row - 1 > row) {
            for(int j=matrix[0].size() - col - 2; j>=col; j--) {
                matrix[matrix.size() - row - 1][j] = step++;
            }
        }
        if(col < matrix[0].size() - col - 1) {
            for(int i=matrix.size() - row - 2; i>row; i--) {
                matrix[i][col] = step++;
            }
        }
    }


    vector<vector<int> > generateMatrix(int n) {
        vector<vector<int> > res;
        res.resize(n);
        for(int i=0; i<n; i++)
            res[i].resize(n);
        if(n == 0)
            return res;
        step = 1;
        int i, j;
        bool row, col;
        i = j = 0;
        do {
            iter(res, i, j);
            row = col = false;
            if((i+1) * 2 < res.size()){
                i++;
                row = true;
            }
            if((j+1) * 2 < res[0].size()) {
                j++;
                col = true;
            }
        }while(row && col);
        return res;
    }
};

void test(int r) {
    vector<vector<int> > res;
    Solution p;
    res = p.generateMatrix(r);

    for(int i=0; i<r; i++) {
        for(int j=0; j<r; j++) {
            printf("%d ", res[i][j]);
        }
        printf("\n");
    }
    printf("=======================\n");
}

int main() {
    test(1);
    test(2);
    test(5);
    test(3);
    return 0;
}
