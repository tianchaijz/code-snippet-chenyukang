#include <iostream>
#include <cassert>
using namespace std;

class Solution {
public:
    int divide(int dividend, int divisor) {
        assert(divisor != 0);
        if(divisor == 1)
            return dividend;
        int rev = 0;
        if((dividend < 0 && divisor > 0) ||
           (dividend > 0 && divisor < 0)) {
            rev = 1;
        }
        long long a = dividend >= 0 ? dividend : -(long long)dividend;
        long long b = divisor >= 0 ? divisor : -(long long)divisor;
        long long result = 0;

        while(a >= b) {
            long long c = b;
            for(int i=0; a >= c; ++i, c <<= 1) {
                a -= c;
                result += (1 << i);
            }
        }

        return rev == 1 ? (-result) : (result);
    }
};

int main() {
    Solution p;
    assert(p.divide(1, 1) == 1);
    assert(p.divide(7, 3) == 2);
    assert(p.divide(9, 3) == 3);
    assert(p.divide(100, 3) == 33);
    assert(p.divide(100, -3) == -33);
    assert(p.divide(2147483647, 2) == 1073741823);
    return 0;
}
