#include <iostream>
#include <vector>
#include <cassert>
using namespace std;

class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
        int len = triangle.size();
        int dp[len+1][len+1];

        if(triangle.size() == 0)
            return 0;
        if(triangle[0].size() == 0)
            return 0;
        dp[0][0] = triangle[0][0];
        for(int i=1; i<triangle.size(); i++) {
            for(int j=0; j<triangle[i].size(); j++) {
                int l, r;
                l = r = INT_MAX;
                if(j-1 >= 0 && j-1 <= triangle[i-1].size() - 1)
                    l = dp[i-1][j-1];
                if(j >= 0 && j <= triangle[i-1].size() - 1)
                    r = dp[i-1][j];

                dp[i][j] = min(l, r) + triangle[i][j];
            }
        }
        int ans = INT_MAX;
        for(int j=0; j<triangle[len-1].size(); j++){
            ans = min(ans, dp[len-1][j]);
        }
        return ans;
    }
};

int main() {
    vector<vector<int> > triangle;
    triangle.resize(4);
    triangle[0].push_back(2);
    triangle[1].push_back(3);
    triangle[1].push_back(4);
    triangle[2].push_back(6);
    triangle[2].push_back(5);
    triangle[2].push_back(7);
    triangle[3].push_back(4);
    triangle[3].push_back(1);
    triangle[3].push_back(8);
    triangle[3].push_back(3);
    Solution p;
    int ans = p.minimumTotal(triangle);

    printf("ans: %d\n", ans);
    return 0;
}
