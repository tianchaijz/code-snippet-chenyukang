#include <stdio.h>
#include <vector>
#include <iostream>
#include <cassert>
using namespace std;

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        ListNode res(0);
        ListNode* p = &res;
        int carry = 0;
        int sum = 0;
        while(l1 && l2) {
            sum = l1->val + l2->val + carry;
            if(sum >= 10) {
                carry = sum / 10;
                sum %= 10;
            } else
                carry = 0;
            p->next = new ListNode(sum);
            p = p->next;
            l1 = l1->next;
            l2 = l2->next;
        }
        ListNode* left = (l1 != NULL) ? l1 : l2;
        while(left) {
            sum = carry + left->val;
            if(sum >= 10) {
                carry = sum / 10;
                sum %= 10;
            } else
                carry = 0;
            p->next = new ListNode(sum);
            p = p->next;
            left = left->next;
        }
        if(carry)
            p->next = new ListNode(carry);
        return res.next;
    }
};

void print(ListNode* node) {
    ListNode* p = node;
    while(p) {
        printf("%d ", p->val);
        p = p->next;
    }
    printf("\n");
}

void test0() {
    Solution p;
    ListNode* ans = p.addTwoNumbers(NULL, NULL);
    print(ans);
}

void test1() {
    Solution p;
    ListNode* l1 = new ListNode(1);
    ListNode* ans = p.addTwoNumbers(l1, NULL);
    print(ans);
}

void test2() {
    Solution p;
    ListNode* l1 = new ListNode(1);
    ListNode* l2 = new ListNode(9);
    ListNode* ans = p.addTwoNumbers(l1, l2);
    print(ans);
}

void test3() {
    Solution p;
    ListNode* l1 = new ListNode(2);
    l1->next = new ListNode(4);
    l1->next->next = new ListNode(3);

    ListNode* l2 = new ListNode(5);
    l2->next = new ListNode(6);
    l2->next->next = new ListNode(4);

    ListNode* ans = p.addTwoNumbers(l1, l2);
    print(ans);
}

int main() {
    test0();
    test1();
    test2();
    test3();
    return 0;
}
