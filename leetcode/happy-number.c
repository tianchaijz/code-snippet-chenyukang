#include <stdio.h>
#include <string.h>

#define bool int

int next(int n) {
  int res = 0;
  while(n) {
    res += (n%10) * (n%10);
    n /= 10;
  }
  return res;
}

bool isHappy(int n) {
  if( n == 1) return 1;
  int tortoise = next(n);
  int hare = next(tortoise);
  if (hare == 1 || tortoise == 1)
    return 1;
  while(tortoise != hare) {
    tortoise = next(tortoise);
    hare = next(next(hare));
    if(tortoise == 1 || hare == 1)
      return 1;
  }
  return 0;
}

int main() {
  printf("res: %d\n", isHappy(19));
  printf("res: %d\n", isHappy(102));
  return 0;
}
