#include <vector>
#include <string>
#include <stack>
#include <cassert>
#include <iostream>

using namespace std;

class Solution {
public:
    int largestRectangleArea(vector<int> &height) {
        if(height.size() == 0) return 0;
        stack<int> st;
        int ans = -1;
        for(int i=0; i<height.size(); i++) {
            if(st.empty() ||
               height[st.top()] <= height[i]) {
                st.push(i);
                continue;
            }
            while(!st.empty() && height[st.top()] > height[i])  {
                int k = st.top();
                st.pop();
                if(!st.empty()) {
                    int area = (i - st.top() - 1) * height[k];
                    ans = max(ans, area);
                } else {
                    ans = max(ans, i * height[k]);
                }
            }
            st.push(i);
        }

        while(!st.empty()) {
            int i = st.top();
            st.pop();
            if(!st.empty()) {
                ans = max(ans, (int)(height.size() - st.top() - 1) * height[i]);
            } else
                ans = max(ans, (int)(height.size()) * height[i]);
        }
        return ans;
    }
};

void test0() {
    vector<int> vec(1, 0);
    Solution p;
    int ans = p.largestRectangleArea(vec);
    std::cout << "ans: " << ans << std::endl;
}

void test1() {
    vector<int> vec(6, 0);
    vec[0] = 2, vec[1] = 1, vec[2] = 5;
    vec[3] = 6, vec[4] = 2, vec[5] = 3;

    Solution p;
    int ans = p.largestRectangleArea(vec);
    std::cout << "ans: " << ans << std::endl;
}

int main() {
    test0();
    test1();
    return 0;
}
