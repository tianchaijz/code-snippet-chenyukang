//DP solution
#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    bool canJump(int A[], int n) {
        int longest = 0;
        for(int start=0; start<n && start<=longest ; start++) {
            longest = (start + A[start]) > longest ? (start + A[start]) : longest;
            if(longest >= n - 1) return true;
        }
        return false;
    }
};

int main() {
    Solution p;
    int A[] ={ 2,3,1,1,4 };
    int size = sizeof(A)/sizeof(int);
    int res = p.canJump(A, size);
    std::cout << res << std::endl;

    int B[] = {3,2,1,0,4};
    size = sizeof(B)/sizeof(int);
    res = p.canJump(B, size);
    std::cout << res << std::endl;

    int C[] = {1,2,2,6,3,6,1,8,9,4,7,6,5,6,8,2,
               6,1,3,6,6,6,3,2,4,9,4,5,9,8,2,2,
               1,6,1,6,2,2,6,1,8,6,8,3,2,8,5,8,
               0,1,4,8,7,9,0,3,9,4,8,0,2,2,5,5,
               8,6,3,1,0,2,4,9,8,4,4,2,3,2,2,5,
               5,9,3,2,8,5,8,9,1,6,2,5,9,9,3,9,
               7,6,0,7,8,7,8,8,3,5,0};

    size = sizeof(B)/sizeof(int);
    res = p.canJump(C, size);
    std::cout << res << std::endl;
    return 0;
}
