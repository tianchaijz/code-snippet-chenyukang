#include <iostream>
#include <cassert>
#include <stdio.h>
#include <vector>
using namespace std;

class Solution {
public:
    int numDistinct(string S, string T) {
        if(S.size() < T.size()) return 0;
        int len = S.size();
        vector<vector<int> > DP(S.size() + 1);
        for(int i=0; i<DP.size(); i++)
            DP[i] = vector<int>(T.size(), 0);
        int count = 0;
        for(int i=0; i<S.size(); i++) {
            if(S[i] == T[0])
                count++;
            DP[i][0] = count;
        }
        for(int i=1; i<S.size(); i++) {
            for(int j=1; j<T.size(); j++) {
                if(S[i] == T[j])
                    DP[i][j] = DP[i-1][j] + DP[i-1][j-1];
                else
                    DP[i][j] = DP[i-1][j];
            }
        }
        return DP[S.size() - 1][T.size() - 1];
    }
};

int main() {
    Solution p;
    std::cout << p.numDistinct("rabbbit", "rabbit") << std::endl;
    return 0;
}
