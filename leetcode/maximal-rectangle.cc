#include <iostream>
#include <vector>
#include <stack>
using namespace std;


class Solution {
public:
    int largestRectangleArea(vector<int> &height) {
        if(height.size() == 0) return 0;
        stack<int> st;
        int ans = -1;
        for(int i=0; i<height.size(); i++) {
            if(st.empty() ||
               height[st.top()] <= height[i]) {
                st.push(i);
                continue;
            }
            while(!st.empty() && height[st.top()] > height[i])  {
                int k = st.top();
                st.pop();
                if(!st.empty()) {
                    int area = (i - st.top() - 1) * height[k];
                    ans = max(ans, area);
                } else {
                    ans = max(ans, i * height[k]);
                }
            }
            st.push(i);
        }

        while(!st.empty()) {
            int i = st.top();
            st.pop();
            if(!st.empty()) {
                ans = max(ans, (int)(height.size() - st.top() - 1) * height[i]);
            } else
                ans = max(ans, (int)(height.size()) * height[i]);
        }
        return ans;
    }

    int maximalRectangle(vector<vector<char> > &matrix) {
        vector<int> vec;
        int ans = -1;
        int row = matrix.size();
        if(row == 0) return 0;
        int col = matrix[0].size();
        if(col == 0) return 0;

        vec.resize(col, 0);
        for(int i=0; i<row; i++) {
            for(int j=0; j<col; j++) {
                if(matrix[i][j] == '0')
                    vec[j] = 0;
                else
                    vec[j]++;
            }
            int area = largestRectangleArea(vec);
            ans = max(ans, area);
        }
        return ans;
    }
};

int main() {
    return 0;
}
