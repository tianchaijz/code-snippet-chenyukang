#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    bool hasCycle(ListNode *head) {
        if(head == NULL || head->next == NULL)
            return false;
        ListNode* t1 = head;
        ListNode* t2 = head;
        while(t1 && t2) {
            t1 = t1->next;
            if(t2->next == NULL)
                return false;
            t2 = t2->next->next;
            if(t1 == t2)
                return true;
        }
        return false;
    }
};

int main() {
    ListNode* root = new ListNode(1);
    Solution p;
    std::cout << p.hasCycle(root) << std::endl;
    root->next = new ListNode(2);
    root->next->next = root;
    std::cout << p.hasCycle(root) << std::endl;
    return 0;
}
