#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    void generate(vector<vector<int> >& res, vector<int> es, int num) {
        if(num >= es.size()) return;
        int len = res.size();
        for(int j=0; j<len; j++) {
            vector<int> x = res[j];
            x.push_back(es[num]);
            res.push_back(x);
        }
        generate(res, es, num+1);
    }

    vector<vector<int> > subsets(vector<int> &S) {
        vector<vector<int> > res;
        sort(S.begin(), S.end());
        res.push_back(vector<int>(0, 0));
        generate(res, S, 0);
        return res;

    }
};


void print(const vector<vector<int> >& vec) {
    for(int i=0; i<vec.size(); i++) {
        for(int j=0; j<vec[i].size(); j++)
            std::cout << vec[i][j] << " ";
        std::cout << std::endl;
    }
}

int main() {
    vector<int> vec;
    vec.push_back(1);
    vec.push_back(2);
    vec.push_back(3);
    Solution p;
    vector<vector<int> > res = p.subsetsWithDup(vec);
    print(res);
    return 0;
}
