#include <iostream>
#include <string>
#include <cassert>
#include <vector>
#include <stdlib.h>
using namespace std;

class Solution {
public:

    char lower(char c) {
        return 'a' + c - 'A';
    }

    bool isPalindrome(string s) {
        if(s.size() == 0)
            return true;
        string str;
        for(int i=0; i<s.size(); i++) {
            if(s[i] >= 'a' && s[i] <= 'z')
                str = str + s[i];
            else if(s[i] >= 'A' && s[i] <= 'Z')
                str = str + lower(s[i]);
            else if(s[i] >= '0' && s[i] <= '9')
                str = str + s[i];
        }
        if(str.size() <= 1) return true;
        int i, j;
        i = 0, j = str.size() -1;
        while(i < j) {
            if(str[i] != str[j])
                return false;
            i++, j--;
        }
        return true;
    }
};


int main() {
    Solution p;
    string s ="A man, a plan, a canal: Panama";
    std::cout << p.isPalindrome(s) << std::endl;

    s = "race a car";
    std::cout << p.isPalindrome(s) << std::endl;
    return 0;
}
