#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;


class Solution {
public:
    string convert(string s, int nRows) {
        string result;
        if (nRows <= 1 || s.size() <= 1) return s;
        for (int i = 0; i < nRows; i++) {
            for (int j = 0, index = i; index < s.size();
                 j++, index = (2 * nRows - 2) * j + i) {
                result.append(1, s[index]); // 垂直元素
                if (i == 0 || i == nRows - 1) continue; // 斜对角元素
                if (index + (nRows - i - 1) * 2 < s.size())
                    result.append(1, s[index + (nRows - i - 1) * 2]);
            } }
        return result;
    }
};


int main() {
    return 0;
}
