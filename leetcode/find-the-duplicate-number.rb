
# @param {Integer[]} nums
# @return {Integer}
def find_duplicate(nums)
  slow, fast = 0, 0
  while true
    slow = nums[slow]
    fast = nums[nums[fast]]
    break if slow == fast
  end

  finder = 0
  while true
    slow = nums[slow]
    finder = nums[finder]
    return slow if slow == finder
  end
end


puts find_duplicate([1, 2, 3, 1])
