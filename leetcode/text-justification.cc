#include <iostream>
#include <string>
#include <vector>
#include <cassert>
using namespace std;

class Solution {
public:
    string padLine(vector<string>& words, int head, int tail, int len, int L, int last) {
        int num = tail - head;
        string line;
        if(last) {
            line = words[head];
            for(int k=head+1; k<words.size(); k++) {
                line = line + ' ' + words[k];
            }
        } else {
            int pad = L - len;
            int perSpace = (num > 1) ? pad / (num - 1) : 0;
            int extra = (num > 1 ) ? pad % (num - 1) : -1;
            line = words[head];
            for(int k=head+1; k<tail; k++) {
                int addSpace = perSpace + 1;
                if(k <= extra + head)
                    addSpace++;
                line = line + string(addSpace, ' ') + words[k];
            }
        }
        if(line.size() < L)
            line = line + string(L - line.size(), ' ');
        return line;
    }

    vector<string> fullJustify(vector<string> &words, int L) {
        vector<string> res;
        int i, k;
        for(i=0; i<words.size(); ) {
            int len = words[i].size() + 1;
            for(k=i+1; k<words.size(); k++) {
                if((len + words[k].size()) > L)
                    break;
                else {
                    len = len + words[k].size() + 1;
                }
            }
            len--;
            int last = (k == words.size()) ? 1 : 0;
            string line = padLine(words, i, k, len, L, last);
            res.push_back(line);
            i = k;
        }
        return res;
    }
};


void test0() {
    Solution p;
    vector<string> words;
    words.push_back("a");
    words.push_back("b");
    words.push_back("c");
    words.push_back("d");
    words.push_back("e");
    vector<string> res = p.fullJustify(words, 3);
    for(int i=0; i<res.size(); i++) {
        std::cout << res[i] << std::endl;
    }
}

void test1() {
    Solution p;
    vector<string> words;
    words.push_back("a");
    words.push_back("b");
    words.push_back("c");
    words.push_back("d");
    words.push_back("e");
    vector<string> res = p.fullJustify(words, 1);
    for(int i=0; i<res.size(); i++) {
        std::cout << res[i] << std::endl;
    }
}

//["Listen","to","many,","speak","to","a","few."], 6
void test2() {
    Solution p;
    vector<string> words;
    words.push_back("Listen");
    words.push_back("to");
    words.push_back("many");
    words.push_back("speak");
    words.push_back("to");
    words.push_back("a");
    words.push_back("few");
    vector<string> res = p.fullJustify(words, 6);
    for(int i=0; i<res.size(); i++) {
        std::cout << res[i] << std::endl;
    }
}

void test3() {
    Solution p;
    vector<string> words;
    words.push_back("This");
    words.push_back("is");
    words.push_back("an");
    words.push_back("example");
    words.push_back("of");
    words.push_back("text");
    words.push_back("justification.");
    vector<string> res = p.fullJustify(words, 16);
    for(int i=0; i<res.size(); i++) {
        std::cout << res[i] << std::endl;
    }
}

int main() {
    test3();
    return 0;
    test0();
    test1();
    test2();
    return 0;
}
