#include <iostream>
#include <stdio.h>
#include <stack>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

//iteractive version
void in_order_traversal_iterative(BinaryTree *root) {
    stack<BinaryTree*> s;
    BinaryTree *current = root;
    while (!s.empty() || current) {
        if (current) {
            s.push(current);
            current = current->left;
        } else {
            current = s.top();
            s.pop();
            cout << current->data << " ";
            current = current->right;
        }
    }
}

class Solution {
public:
    void inorder_iter(TreeNode* node, vector<int>& res) {
        if(node == NULL) return;
        inorder_iter(node->left, res);
        res.push_back(node->val);
        inorder_iter(node->right, res);
    }

    vector<int> inorderTraversal(TreeNode *root) {
        vector<int> res;
        inorder_iter(root, res);
        return res;
    }
};


int main() {
    return 0;
}
