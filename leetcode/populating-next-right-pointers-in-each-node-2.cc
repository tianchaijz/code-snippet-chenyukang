#include <iostream>
#include <cassert>
using namespace std;

struct TreeLinkNode {
    TreeLinkNode *left;
    TreeLinkNode *right;
    TreeLinkNode *next;
    TreeLinkNode(int val) : left(NULL), right(NULL), next(NULL)
        {}
};


class Solution {
private:
    TreeLinkNode* findNext(TreeLinkNode* p) {
        while(p) {
            if(p->left) {
                return p->left;
            }
            if(p->right) {
                return p->right;
            }
            p = p->next;
        }
        return NULL;
    }

    void connect_iter(TreeLinkNode *root) {
        if(root == NULL) return;
        if(root->left) {
            if(root->left->next == NULL) {
                TreeLinkNode* right = root->right;
                if(right)
                    root->left->next = right;
                else {
                    TreeLinkNode* p = findNext(root->next);
                    if(p != root->left)
                        root->left->next = p;
                }
            }
        }
        if(root->right) {
            TreeLinkNode* p = findNext(root);
            if(p != root->right)
                root->right->next = p;
        }
        connect_iter(root->left);
        connect_iter(root->right);
    }

public:
    void connect(TreeLinkNode* root) {
        connect_iter(root);
    }
};


int main() {
    TreeLinkNode* root = new TreeLinkNode(1);
    root->left = new TreeLinkNode(2);
    root->right = new TreeLinkNode(3);
    Solution p;
    p.connect(root);
    return 0;
}
