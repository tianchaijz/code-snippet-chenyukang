#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    void nextPermutation(vector<int> &num) {
        if(num.size() == 0)
            return;
        int i;
        int head = -1;
        int tail = 0;
        for(i=num.size()-1; i >= 1 ; i--) {
            if(head != -1 && i <= head)
                break;
            for(int k = i - 1; k >= 0; k--) {
                if(num[k] < num[i] && k > head) {
                    head = k;
                    tail = i;
                    break;
                }
            }
        }

        if(head != -1) {
            swap(num[head], num[tail]);
            for(int m = head+1; m < num.size()-1; m++) {
                for(int l = m+1; l < num.size(); l++) {
                    if(num[m] > num[l])
                        swap(num[m], num[l]);
                }
            }
        }

        if(head == -1) {
            int start = 0;
            int end = num.size() - 1;
            while(start < end) {
                swap(num[start], num[end]);
                start++;
                end--;
            }
        }
    }
};


void test0() {
    vector<int> vec;
    for(int i=1; i<=5; i++) {
        vec.push_back(i);
    }

    Solution p;
    for(int k=0; k<10; k++) {
        p.nextPermutation(vec);
    }
}

void test1() {
    vector<int> vec;
    int data[] = {4,2,0,2,3,2,0};
    for(int i=0; i<sizeof(data)/sizeof(int); i++) {
        vec.push_back(data[i]);
    }
    Solution p;
    p.nextPermutation(vec);
}

int main() {
    test1();
    return 0;
}
