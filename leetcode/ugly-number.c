#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define bool int
#define false 0
#define true 1

bool isUgly_v1(int num) {
  if(num <= 0) return false;
  while(num % 2 == 0) {
    num /= 2;
  }

  int i = 3;
  while((i * i) > 0 && (i * i) <= num) {
    while((num % i) == 0) {
      if(i != 2 && i != 3 && i != 5) return false;
      num /= i;
    }
    i += 2;
  }
  if(num > 2 && num != 2 && num != 3 && num != 5)
    return false;
  return true;
}

bool isUgly(int num) {
  if(num <= 0) return false;
  while(num % 2 == 0) num /= 2;
  while(num % 3 == 0) num /= 3;
  while(num % 5 == 0) num /= 5;
  return num == 1;
}

int main() {
  assert(isUgly(14) == false);
  assert(isUgly(2147483647) == false);
}
