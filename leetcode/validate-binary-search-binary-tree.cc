#include <iostream>
#include <cassert>
#include <stdio.h>
#include <climits>
using namespace std;

struct TreeNode{
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int val) : val(val), left(NULL), right(NULL) {}
};


#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

class Solution {
    bool isValid(TreeNode* node, int& minval, int& maxval) {
        if(node == NULL) {
            return true;
        }
        int val = node->val;
        int rmin, rmax, lmin, lmax;
        rmin = lmin = INT_MAX;
        rmax = lmax = INT_MIN;
        if(!isValid(node->left, lmin, lmax) ||
           !isValid(node->right, rmin, rmax))
            return false;
        if(lmax >= val || rmin <= val)
            return false;
        minval = min(val, min(lmin, rmin));
        maxval = max(val, max(rmax, rmax));
        return true;
    }

public:

    bool isValidBST(TreeNode* root) {
        if(root == NULL) return true;
        int minval, maxval;
        minval = INT_MAX;
        maxval = INT_MIN;
        return isValid(root, minval, maxval);
    }
};

void test() {
    TreeNode* root = new TreeNode(10);
    root->left = new TreeNode(5);
    root->right = new TreeNode(15);
    root->right->left = new TreeNode(6);
    root->right->right = new TreeNode(20);
    Solution p;
    bool res = p.isValidBST(root);
    printf("res: %d\n", res);
}


int main() {
    test();
    return 0;
}
