
//Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* deleteDuplicates(struct ListNode* head) {
  if(head == 0 || head->next == 0)
    return head;
  struct ListNode* cur = head;
  struct ListNode* nxt = head->next;
  while(nxt) {
    if(cur->val == nxt->val)
      nxt = nxt->next;
    else {
      cur->next = nxt;
      cur = nxt;
      nxt = nxt->next;
    }
  }
  cur->next = 0;
  return head;
}

int main() {
  return 0;
}
