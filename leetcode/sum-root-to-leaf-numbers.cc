#include <iostream>
#include <stdio.h>
#include <cassert>

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    void iter(TreeNode* root, int& sum, int now) {
        if(root == NULL) return;
        int val = now*10 + root->val;
        if(root->left == NULL && root->right == NULL) {
            sum += val;
        }
        iter(root->left, sum, val);
        iter(root->right, sum, val);
    }

    int sumNumbers(TreeNode *root) {
        int sum = 0;
        int now = 0;
        iter(root, sum, now);
        return sum;
    }
};


int main() {
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    root->right = new TreeNode(3);
    root->right->right = new TreeNode(4);
    Solution p ;
    int sum = p.sumNumbers(root);
    std::cout << sum << std::endl;
    return 0;
}
