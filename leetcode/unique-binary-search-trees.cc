#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;

class Solution {
public:
    int numTrees(int n) {
        vector<int> vec(n+1, 0);
        vec[0] = 1;
        vec[1] = 1;
        for(int i=2; i<=n; i++) {
            for(int j=0; j<i; j++) {
                vec[i] += (vec[j] * vec[i-1-j]);
            }
        }
        return vec[n];
    }
};


int main() {
    Solution p;
    std::cout << p.numTrees(3) << std::endl;
    std::cout << p.numTrees(2) << std::endl;
    std::cout << p.numTrees(1) << std::endl;
    std::cout << p.numTrees(5) << std::endl;
    return 0;
}
