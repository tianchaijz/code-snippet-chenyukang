#include <vector>
#include <iostream>
using namespace std;


class Solution {
public:
    int minPathSum(vector<vector<int> > &grid) {
        vector<vector<int> > dp(grid.size());
        for(int i=0; i<dp.size(); i++) {
            dp[i] = vector<int>(grid[i].size(), 0);
        }

        dp[0][0] = grid[0][0];
        for(int i=1; i<dp.size(); i++) {
            dp[i][0] = dp[i-1][0] + grid[i][0];
        }
        for(int j=1; j<dp[0].size(); j++) {
            dp[0][j] = dp[0][j-1] + grid[0][j];
        }

        for(int i=1; i<dp.size(); i++) {
            for(int j=1; j<dp[0].size(); j++) {
                dp[i][j] = min(dp[i-1][j], dp[i][j-1]) + grid[i][j];
            }
        }
        return dp[dp.size() - 1][dp[0].size() - 1];
    }
};

int main() {
    return 0;
}
