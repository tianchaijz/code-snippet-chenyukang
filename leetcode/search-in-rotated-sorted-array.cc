#include <iostream>
#include <stdio.h>
using namespace std;

class Solution {
public:
    int search(int A[], int n, int target) {
        int first = 0;
        int last = n - 1;
        while(first < last) {
            int mid = (first + last) / 2;
            if(A[mid] == target) return mid;
            if(A[mid] >= A[first]) {
                if(target >= A[first] && target < A[mid])
                    last = mid - 1;
                else
                    first = mid + 1;
            } else {
                if(target > A[mid] && target <= A[last])
                    first = mid + 1;
                else
                    last = mid - 1;
            }
        }
        if(A[last] == target)
            return last;
        return -1;
    }
};

#if 0
int search(int A[], int n, int target) {
    int first = 0;
    int last = n - 1;
    while(first < last) {
        int mid = (first + last) / 2;
        //printf("first: %d last: %d mid: %d\n", first, last, mid);
        if(A[mid] == target) return mid;
        if(A[first] == target) return first;
        if(A[last] == target) return last;
        if(A[mid] > A[first] && A[mid] > A[last]) {
            if(target > A[mid])
                first = mid + 1;
            else if(target >= A[first])
                last = mid;
            else
                first = mid + 1;
        }
        else if(A[mid] < A[first] && A[mid] < A[last]) {
            if(target < A[mid])
                last = mid - 1;
            else if(target <= A[last])
                first = mid;
            else
                last = mid - 1;
        }
        else {
            if(target > A[mid])
                first = mid + 1;
            else
                last = mid -1;
        }
    }
    if(A[last] == target)
        return last;
    return -1;
}
#endif

int main() {
    Solution p;
    int A[] = {2, 1};
    int len = sizeof(A)/sizeof(int);
    int pos;
    pos = p.search(A, len, 1);
    std::cout <<"pos: " << pos << std::endl;
    pos = p.search(A, len, 4);
    std::cout <<"pos: " << pos << std::endl;
    pos = p.search(A, len, 2);
    std::cout <<"pos: " << pos << std::endl;
    pos = p.search(A, len, 3);
    std::cout <<"pos: " << pos << std::endl;

    pos = p.search(A, len, 5);
    std::cout <<"pos: " << pos << std::endl;
    return 0;
}
