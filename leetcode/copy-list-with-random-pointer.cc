#include <iostream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
using namespace std;


/**
 * Definition for singly-linked list with a random pointer.
 */
struct RandomListNode {
    int label;
    RandomListNode *next, *random;
    RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
};

class Solution {
public:
    RandomListNode* copyRandomList(RandomListNode *head) {
        std::map<RandomListNode*, RandomListNode*> table;
        RandomListNode* t = head;
        RandomListNode* res = NULL;
        RandomListNode* prev = NULL;
        while(t) {
            RandomListNode* x = NULL;
            RandomListNode* r = NULL;
            if(table.find(t) != table.end()) {
                x = table[t];
            } else {
                x = new RandomListNode(t->label);
                table[t] = x;
            }
            if(t->random != NULL) {
                if(table.find(t->random) != table.end()) {
                    r = table[t->random];
                } else {
                    r = new RandomListNode(t->random->label);
                    table[t->random] = r;
                }
            }
            x->random = r;
            if(res == NULL) res = x;
            if(prev != NULL) prev->next = x;
            prev = x;
            t = t->next;
        }
        return res;
    }

};


int main() {
    return 0;
}
