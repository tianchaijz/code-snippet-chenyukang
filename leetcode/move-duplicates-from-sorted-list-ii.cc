#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        if(head == NULL || head->next == NULL)
            return head;
        ListNode res(0);
        ListNode* now = &res;
        ListNode* t = head;
        ListNode* q;
        while(t) {
            if(t->next == NULL || t->next->val != t->val) {
                q = t->next;
                now->next = t;
                t->next = NULL;
                now = now->next;
                t = q;
            } else {
                int v = t->val;
                q = t;
                while(q && q->val == v)
                    q = q->next;
                t = q;
            }
        }
        return res.next;
    }
};


void print(ListNode* head) {
    if(head == NULL) {
        printf("empty\n");
    }
    ListNode* t = head;
    printf("\n");
    while(t) {
        printf("%d ", t->val);
        t = t->next;
    }
    printf("\n");
}

int main() {
    ListNode* head = new ListNode(1);
    head->next = new ListNode(2);
    head->next->next = new ListNode(2);
    print(head);
    Solution p;
    ListNode* res = p.deleteDuplicates(head);
    print(res);
    return 0;
}
