#include <string>
#include <set>
#include <vector>
#include <map>
#include <queue>
#include <unordered_set>
#include <iostream>
#include <algorithm>
using namespace std;


struct Node {
    int step;
    int prev;
    int index;
    Node(int s, int p, int i) : step(s), prev(p), index(i) {}
};

class Solution {
public:

    int dist(const string& a, const string& b) {
        int dist = 0;
        for(int i=0; i<a.size(); i++) {
            if(a[i] != b[i])
                dist++;
            if(dist >= 2) return dist;
        }
        return dist;
    }

    void get_ans(vector<vector<string> >& ans,
                 vector<string>& words,
                 vector<int>& prevs,
                 int idx) {
        vector<string> trace;
#if 0
        for(int i=0; i<prevs.size(); i++) {
            printf("%d ", prevs[i]);
        }
        printf("\n");
#endif
        while(prevs[idx] != -1) {
            trace.insert(trace.begin(), words[idx]);
            idx = prevs[idx];
        }
        trace.insert(trace.begin(), words[idx]);
        ans.push_back(trace);
    }

    //pass all the testcases
    vector<vector<string> > findLadders(string start, string end,
                                        unordered_set<string> &dict) {

        vector<vector<string> > ans;
        dict.insert(start);
        dict.insert(end);
        vector<string> words;
        int start_idx, end_idx;
        for(unordered_set<string>::iterator it = dict.begin();
            it != dict.end(); ++it) {
            words.push_back(*it);
            if(*it == start) start_idx = words.size() - 1;
            if(*it == end  ) end_idx = words.size() - 1;
        }

        typedef vector<vector<int> > WordMap;
        WordMap wordmap;

        wordmap.resize(words.size());
        for(int i=0; i<words.size(); i++) {
            for(int j=i; j<words.size(); j++) {
                if(dist(words[i], words[j]) == 1) {
                    wordmap[i].push_back(j);
                    wordmap[j].push_back(i);
                }
            }
        }

#if 0
        for(int i=0; i<wordmap.size(); i++) {
            printf("(%d:%s) : ", i, words[i].c_str());
            for(int j=0; j<wordmap[i].size(); j++) {
                printf("(%d:%s)", wordmap[i][j], words[wordmap[i][j]].c_str());
            }
            printf("\n");
        }
#endif

        vector<int> visited(words.size(), INT_MAX);
        vector<int>  prevs(words.size(), -1);
        queue<Node> Q;
        Q.push(Node(0, -1, start_idx));
        visited[start_idx] = 0;
        int minStep = INT_MAX;
        while(!Q.empty()) {
            Node now = Q.front();
            int idx = now.index;
            int step = now.step;
            //      printf("step: %d index: %d str: %s\n", step, idx, words[idx].c_str());
            prevs[idx] = now.prev;
            Q.pop();
            if(idx == end_idx) {
                minStep = min(minStep, now.step);
                get_ans(ans, words, prevs, idx);
            } else {
                for(int i=0; i<wordmap[idx].size(); i++) {
                    int next = wordmap[idx][i];
                    if(step + 1 > visited[next]) continue;
                    visited[next] = step+1;
                    Q.push(Node(step+1, idx, next));
                }
            }
        }
        return ans;
    }
};

void test1() {
    unordered_set<string> dict;
    dict.insert("hot");
    dict.insert("dot");
    dict.insert("dog");
    dict.insert("lot");
    dict.insert("log");
    Solution p;
    vector<vector<string> > ans = p.findLadders("hit", "cog", dict);
    printf("ans size: %lu\n", ans.size());
    for(int i=0; i<ans.size(); i++) {
        std::cout << "size: " << ans[i].size() << " : ";
        for(int j=0; j<ans[i].size(); j++) {
            printf("%s ->", ans[i][j].c_str());
        }
        printf("\n");
    }
}

void test2() {
    unordered_set<string> dict;
    dict.insert("a");
    dict.insert("b");
    dict.insert("c");
    Solution p;
    vector<vector<string> > ans = p.findLadders("a", "c", dict);
    for(int i=0; i<ans.size(); i++) {
        std::cout << "size: " << ans[i].size() << " : ";
        for(int j=0; j<ans[i].size(); j++) {
            printf("%s ->", ans[i][j].c_str());
        }
        printf("\n");
    }
}

void test3() {
    const char* str[] = {"si","go","se","cm","so","ph","mt","db","mb","sb","kr","ln","tm","le","av","sm","ar","ci","ca","br","ti","ba","to","ra","fa","yo","ow","sn","ya","cr","po","fe","ho","ma","re","or","rn","au","ur","rh","sr","tc","lt","lo","as","fr","nb","yb","if","pb","ge","th","pm","rb","sh","co","ga","li","ha","hz","no","bi","di","hi","qa","pi","os","uh","wm","an","me","mo","na","la","st","er","sc","ne","mn","mi","am","ex","pt","io","be","fm","ta","tb","ni","mr","pa","he","lr","sq","ye"};
    unordered_set<string> dict;
    for(int i=0; i<sizeof(str)/sizeof(char*); i++) {
        dict.insert(str[i]);
    }
    Solution p;
    vector<vector<string> > ans = p.findLadders("qa", "sq", dict);
    for(int i=0; i<ans.size(); i++) {
        std::cout << "size: " << ans[i].size() << " : ";
        for(int j=0; j<ans[i].size(); j++) {
            printf("%s ->", ans[i][j].c_str());
        }
        printf("\n");
    }
}


int main() {
    test1();
    test2();
    test3();
    return 0;
}
