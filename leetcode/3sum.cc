#include <vector>
#include <stdio.h>
#include <cassert>
using namespace std;

class Solution {
public:
    vector<vector<int> > threeSum(vector<int> &num) {
        vector<vector<int> > ans;
        sort(num.begin(), num.end());

        if(num.size() < 3) return ans;
        for(int first = 0; first < num.size(); first++) {
            int target = -num[first];
            int head = first + 1;
            int tail = num.size() - 1;
            while(head < tail) {
                int v = num[head] + num[tail];
                if(v == target) {
                    vector<int> now(3, 0);
                    now[0] = num[first];
                    now[1] = num[head];
                    now[2] = num[tail];
                    ans.push_back(now);
                    head++, tail--;
                    while(head < tail && num[head] == num[head-1])
                        head++;
                    while(tail > head && num[tail] == num[tail+1])
                        tail--;

                }
                else if(v > target)
                    tail--;
                else
                    head++;
            }
            while(first < num.size()-1 && num[first+1] == num[first])
                first++;
        }
        return ans;
    }
};

void test(vector<int>& vec) {
    Solution p;
    vector<vector<int> > ans = p.threeSum(vec);
    for(int i=0; i<ans.size(); i++) {
        for(int j=0; j<ans[i].size(); j++) {
            printf("%d ", ans[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main() {
    vector<int> vec;
    test(vec);
    vec.resize(6);
    vec[0] = -1, vec[1] = 0, vec[2] = 1;
    vec[3] = 2,  vec[4] = -1, vec[5] = -4;
    test(vec);

    vec.clear();
    vec = vector<int>(10, 0);
    test(vec);

    vec.clear();
    vec.resize(5, 0);
    vec[0] = -2;
    vec[1] = 0;
    vec[2] = 1;
    vec[3] = 1;
    vec[4] = 2;
    test(vec);
    return 0;
}
