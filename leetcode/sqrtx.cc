#include <iostream>
using namespace std;

class Solution {
public:
    int sqrt(int x) {
        int left = 0;
        int right = x;
        if(x <= 1) return x;
        while(left+1 < right) {
            int mid = left +  (right - left) / 2;
            std::cout << mid << std::endl;
            if(mid == x/mid)
                return mid;
            else if(x/mid < mid)
                right = mid;
            else
                left = mid;
        }
        return left;
    }
};


int main() {
    Solution p;
    int res = p.sqrt(2147395599);
    std::cout << "res:"<< res << std::endl;

    res = p.sqrt(2147483647);
    std::cout << "res:"<< res << std::endl;
    res = p.sqrt(2);
    std::cout << "res:"<< res << std::endl;
    res = p.sqrt(2147395599);

    return 0;
}
