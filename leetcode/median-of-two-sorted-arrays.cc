#include <vector>
#include <iostream>
#include <stdio.h>
#include <assert.h>
using namespace std;

class Solution {
public:
    double findMedianSortedArrays(int A[], int m, int B[], int n) {
        int len = m + n;
        if((len % 2) == 1) {
            return findKth(A, m, B, n, len/2 + 1);
        } else {
            double a = (double)findKth(A, m, B, n, len/2);
            double b = (double)findKth(A, m, B, n, len/2+1);
            return (a + b)/2.0;
        }
    }

    //find the kth from A and B
    int findKth(int A[], int m, int B[], int n, int k) {
        assert(A && B);
        if(m <= 0) return B[k-1];
        if(n <= 0) return A[k-1];
        if(k <= 1) return min(A[0], B[0]);
        if(B[n/2] >= A[m/2]) {
            if((m/2 + n/2 + 1) >= k)
                return findKth(A, m, B, n/2, k);
            else
                return findKth(A + m/2 + 1, m - (m/2 + 1), B, n, k - (m/2 + 1));
        } else {
            if((m/2 + n/2 + 1) >= k)
                return findKth(A, m/2, B, n, k);
            else
                return findKth(A, m, B + (n/2 + 1), n - (n/2 + 1), k - (n/2 + 1));
        }
    }
};


int main() {
    Solution p;
    int A[] = {1, 2, 10};
    int B[] = {3, 4, 5};
    double ans = p.findMedianSortedArrays(A, 3, B, 3);
    std::cout << "ans: " << ans << std::endl;

    int C[] = {1};
    int D[] = {2, 3, 4};
    ans = p.findMedianSortedArrays(C, 1, D, 3);
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
