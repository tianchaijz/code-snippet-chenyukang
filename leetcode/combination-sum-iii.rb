

def iter k, n, index, cur, res
  sum = cur.reduce(&:+).to_i
  return if cur.size() > k || sum > n
  return res.push cur if cur.size == k && sum == n
  return if index > 9
  iter k, n, index+1, cur + [index], res
  iter k, n, index+1, cur, res
end

def combination_sum3(k, n)
  res = []
  iter k, n, 1, [], res
  return res
end


res = combination_sum3 1, 10
puts res.inspect
