#include <map>
#include <vector>
using namespace std;

/**
 * Definition for a point. */
struct Point {
     int x;
     int y;
     Point() : x(0), y(0) {}
     Point(int a, int b) : x(a), y(b) {}
};


class Solution {
public:
    int maxPoints(vector<Point> &points) {
        map<float, int> table;
        int ans = 0;
        int duplicate = 1;
        for(int i=0; i<points.size(); i++) {
            table.clear();
            duplicate = 1;
            int x = points[i].x;
            int y = points[i].y;
            for(int j=0; j<points.size(); j++)  {
                if( i == j) continue;
                int _x = points[j].x;
                int _y = points[j].y;
                if( _x == x && _y == y) {
                    duplicate++;
                    continue;
                }
                float k = (x - _x == 0) ? (INT_MAX) : ((float)( y - _y) / (float)( x - _x));
                table[k]++;
            }
            ans = (duplicate > ans ? duplicate : ans);
            for(map<float, int>::iterator it = table.begin(); it != table.end(); ++it) {
                int now = it->second + duplicate;
                ans = (now > ans) ?  now : ans;
            }
        }
        return ans;
    }
};

int main() {
    return 0;
}
