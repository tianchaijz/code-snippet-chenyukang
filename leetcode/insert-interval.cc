#include <iostream>
#include <vector>
using namespace std;


/**
 * Definition for an interval. */
struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};

void print(vector<Interval>& vecs) {
    for(int i=0; i<vecs.size(); i++) {
        printf("[%d %d] ", vecs[i].start, vecs[i].end);
    }
    printf("\n");
}


class Solution {
public:
    vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
        int i, k;
        int start, end;
        for(i=0; i<intervals.size(); i++) {
            if(intervals[i].start >= newInterval.start) {
                break;
            }
        }
        if(i !=0 && intervals[i-1].end >= newInterval.start) {
            start = intervals[i-1].start;
            i--;
        } else {
            start = newInterval.start;
        }

        end = newInterval.end;
        for(k=i; k<intervals.size(); k++) {
            if(intervals[k].start > end) {
                break;
            } else {
                end = max(end, intervals[k].end);
            }
        }

        Interval addOne(start, end);
        intervals.erase(intervals.begin() + i, intervals.begin() + k);
        intervals.insert(intervals.begin() + i, addOne);
        return intervals;
    }
};

void test1() {
    vector<Interval> vecs;
    vecs.push_back(Interval(1, 3));
    vecs.push_back(Interval(6, 9));
    Solution p;
    Interval add(2, 5);
    p.insert(vecs, add);
    print(vecs);
}

void test2() {
    vector<Interval> vecs;
    vecs.push_back(Interval(1, 2));
    vecs.push_back(Interval(3, 5));
    vecs.push_back(Interval(6, 7));
    vecs.push_back(Interval(8, 10));
    vecs.push_back(Interval(12, 16));
    Solution p;
    Interval add(4, 9);
    p.insert(vecs, add);
    print(vecs);
}

int main() {
    test1();
    test2();
    return 0;
}
