#include <vector>
#include <stdio.h>
#include <cassert>
using namespace std;

class Solution {
public:
    vector<vector<int> > fourSum(vector<int> &num, int target) {
        vector<vector<int> > ans;
        sort(num.begin(), num.end());

        if(num.size() < 4) return ans;
        for(int i=0; i<= num.size() - 4; i++) {
            int value = target - num[i];
            vector<vector<int> > vals = threeSum(num, value, i+1);
            for(int k=0; k<vals.size(); k++) {
                vals[k].insert(vals[k].begin(), num[i]);
                ans.push_back(vals[k]);
            }
            while(i < num.size() && num[i] == num[i+1])
                i++;
        }
        return ans;
    }

    vector<vector<int> > threeSum(vector<int> &num, int target, int index) {
        vector<vector<int> > ans;
        if(num.size() - index + 1  < 3)
            return ans;
        for(int first = index; first < num.size(); first++) {
            int value = target-num[first];
            int head = first + 1;
            int tail = num.size() - 1;
            while(head < tail) {
                int v = num[head] + num[tail];
                if(v == value) {
                    vector<int> now(3, 0);
                    now[0] = num[first];
                    now[1] = num[head];
                    now[2] = num[tail];
                    ans.push_back(now);
                    head++, tail--;
                    while(head < tail && num[head] == num[head-1])
                        head++;
                    while(tail > head && num[tail] == num[tail+1])
                        tail--;

                }
                else if(v > value)
                    tail--;
                else
                    head++;
            }
            while(first < num.size()-1 && num[first+1] == num[first])
                first++;
        }
        return ans;
    }
};

void test(vector<int>& vec, int target) {
    Solution p;
    vector<vector<int> > ans = p.fourSum(vec, target);
    for(int i=0; i<ans.size(); i++) {
        for(int j=0; j<ans[i].size(); j++) {
            printf("%d ", ans[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main() {
    vector<int> vec;
    test(vec, 0);
    vec.resize(6);
    vec[0] = 1, vec[1] = 0, vec[2] = -1;
    vec[3] = 0,  vec[4] = -2, vec[5] = 2;
    test(vec, 0);
    return 0;
}
