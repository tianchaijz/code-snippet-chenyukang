#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
using namespace std;

string op[4] = {"+", "-", "*", "/"};

class Solution {
public:
    bool isOp(string t) {
        for(int i=0; i<4; i++) {
            if(t == op[i])
                return true;
        }
        return false;
    }

    int cal(string op, int v1, int v2) {
        if(op == "+")
            return v1 + v2;
        else if(op == "-")
            return v1 - v2;
        else if(op == "*")
            return v1 * v2;
        else if(op == "/")
            return v1 / v2;
        else {
            assert(0);
            return -1;
        }
    }

    int evalRPN(vector<string> &tokens) {
        stack<int> values;
        for(int i=0; i<tokens.size(); i++) {
            string t = tokens[i];
            if(!isOp(t)) {
                values.push(atoi(t.c_str()));
            } else {
                int v2 = values.top(); values.pop();
                int v1 = values.top(); values.pop();
                int res = cal(t, v1, v2);
                values.push(res);
            }
        }
        return values.top();
    }
};

int main() {
    Solution p;
    vector<string> tokens;
    tokens.push_back("1");
    tokens.push_back("3");
    tokens.push_back("-");
    tokens.push_back("3");
    tokens.push_back("*");
    int res = p.evalRPN(tokens);
    printf("res: %d\n", res);
    return 0;
}
