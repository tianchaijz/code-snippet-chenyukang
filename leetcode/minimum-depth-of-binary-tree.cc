#include <iostream>
#include <stdio.h>
using namespace std;


/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    int iter(TreeNode* node) {
        if(node == NULL) return 0;
        int left = iter(node->left);
        int right = iter(node->right);
        int val;
        if(node->left == NULL && node->right != NULL)
            val = right + 1;
        else if(node->left != NULL && node->right == NULL)
            val = left + 1;
        else
            val = min(left, right) + 1;
        return val;
    }

    int minDepth(TreeNode *root) {
        return iter(root);
    }
};

int main() {
    return 0;
}
