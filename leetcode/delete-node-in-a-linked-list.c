#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

void deleteNode(struct ListNode* node) {
  struct ListNode* next = node->next;
  assert(next != NULL);
  node->val = next->val;
  node->next = next->next;
}


int main() {

  struct ListNode* head = (struct ListNode*)malloc(sizeof(struct ListNode));
  head->val = 1;

  int i;
  struct ListNode* next = head;
  struct ListNode* delete = NULL;
  for(i = 2; i <= 4; i++) {
    next->next = malloc(sizeof(struct ListNode));
    next = next->next;
    next->val = i;
    next->next = NULL;
    if(i == 3)
      delete = next;
  }

  deleteNode(delete);
  next = head;
  while(next != NULL) {
    printf("%d\n", next->val);
    next = next->next;
  }
  return 0;
}
