#include <iostream>
#include <assert.h>
#include <stdio.h>

#define max(a, b) ((a) > (b) ? (a) : (b))

class Solution {
public:
    void merge(int A[], int m, int B[], int n) {
        int last = m + n -1;
        int i = m-1;
        int j = n-1;
        while(i>=0 && j>=0) {
            int m = -1;
            if(A[i] > B[j]) {
                m = A[i];
                i--;
            } else {
                m = B[j];
                j--;
            }
            A[last--] = m;
        }
        while(i>=0)
            A[last--] = A[i--];
        while(j>=0)
            A[last--] = B[j--];
    }
};


int main() {
    Solution p;
    int A[] = {0};
    int B[] = {1};
    p.merge(A, 0, B, 1);
    for(int i=0; i<1; i++) {
        printf(" %d ", A[i]);
    }
    printf("\n");
    return 0;
}
