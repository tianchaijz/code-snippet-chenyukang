#include <iostream>
#include <vector>
using namespace std;


class Solution {
public:
    int lengthOfLastWord(const char *s) {
        const char* q = s;
        int ans = 0;
        int inword = 0;
        while(*q != '\0') {
            if(*q == ' ') {
                inword = 0;
                q++;
                continue;
            }
            if(!inword) {
                inword = 1;
                ans = 1;
            } else {
                ans++;
            }
            q++;
        }
        return ans;
    }
};

int main() {
    Solution p;
    std::cout << p.lengthOfLastWord("Hello World") << std::endl;
    return 0;
}
