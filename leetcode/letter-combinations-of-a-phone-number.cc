#include <string>
#include <vector>
#include <set>
#include <iostream>
using namespace std;

class Solution {
    vector<string> table;
public:
    vector<string> letterCombinations(string digits) {
        vector<string> ans;
        if(digits == "") {
            ans.push_back("");
            return ans;
        }
        table.clear();
        table.resize(10);
        table[2] = "abc", table[3] = "def";
        table[4] = "ghi", table[5] = "jkl";
        table[6] = "mno", table[7] = "pqrs";
        table[8] = "tuv", table[9] = "wxyz";

        for(int i=0; i<digits.size(); i++) {
            int index = digits[i] - '0';
            if(index >= 2 && index <= 9) {
                string& s = table[index];
                if(ans.size() == 0) {
                    for(int k=0; k<s.size(); k++) {
                        ans.push_back(string(1, s[k]));
                    }
                } else {
                    vector<string> tmp;
                    for(int k=0; k<s.size(); k++) {
                        for(int m=0; m<ans.size(); m++) {
                            string t = ans[m] + s[k];
                            tmp.push_back(t);
                        }
                    }
                    ans = tmp;
                }
            }
        }

        return ans;
    }
};

void test0() {
    Solution p;
    vector<string> ans = p.letterCombinations("");
    for(int i=0; i<ans.size(); i++) {
        std::cout << ans[i] << std::endl;
    }
}

void test1() {
    Solution p;
    vector<string> ans = p.letterCombinations("23");
    for(int i=0; i<ans.size(); i++) {
        std::cout << ans[i] << std::endl;
    }
}

void test2() {
    Solution p;
    vector<string> ans = p.letterCombinations("233483");
    for(int i=0; i<ans.size(); i++) {
        std::cout << ans[i] << std::endl;
    }
}

int main() {
    test0();
    test1();
    test2();
    return 0;
}
