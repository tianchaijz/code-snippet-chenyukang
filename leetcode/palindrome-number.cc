
#include <iostream>
#include <stdio.h>
using namespace std;

class Solution {
public:
    int reverse(int x) {
        if(x == 0) return 0;
        int res = 0;
        for(; x; x /= 10) {
            res = res * 10 + (x % 10);
        }
        if(res > INT_MAX || res < INT_MIN)
            return -1;
        return res;
    }
    bool isPalindrome(int x) {
        if(x<0) return false;
        int rev = reverse(x);
        return rev == x;
    }
};


int main() {
    Solution p;
    std::cout << p.isPalindrome(121) << std::endl;
    std::cout << p.isPalindrome(222) << std::endl;
    std::cout << p.isPalindrome(2) << std::endl;
    std::cout << p.isPalindrome(-2) << std::endl;
    return 0;
}
