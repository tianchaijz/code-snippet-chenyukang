

#include <iostream>
#include <stdio.h>
#include <set>
#include <vector>
using namespace std;

string table = "123456789";

class Solution {
private:
    vector<set<char> > rows;
    vector<set<char> > cols;
    vector<set<char> > subs;
    vector<vector<int> > cells;

public:

    int id(int i, int j) {
        int id = i/3 * 3 + j/3;
        return id;
    }

    void init(const vector<vector<char> >& board) {
        int r = board.size();
        int c = board[0].size();
        rows.clear(), cols.clear(), subs.clear();
        rows.resize(r);
        cols.resize(c);
        subs.resize((r*c)/9);

        for(int i=0; i<board.size(); i++) {
            for(int j=0; j<board[0].size(); j++) {
                char x = board[i][j];
                if(x == '.') continue;
                rows[i].insert(x);
                cols[j].insert(x);
                subs[id(i, j)].insert(x);
            }
        }
    }

    bool good(char x, int i, int j) {
        set<char>& sub = subs[id(i, j)];
        if(rows[i].find(x) != rows[i].end()) {
            return false;
        }
        if(cols[j].find(x) != cols[j].end()) {
            return false;
        }
        if(sub.find(x) != sub.end()) {
            return false;
        }
        return true;
    }

    void put(vector<vector<char> >& board, char x, int i, int j) {
        subs[id(i, j)].insert(x);
        rows[i].insert(x);
        cols[j].insert(x);
        board[i][j] = x;
    }

    void unset(vector<vector<char> >& board, char x, int i, int j) {
        subs[id(i, j)].erase(x);
        rows[i].erase(x);
        cols[j].erase(x);
        board[i][j] = '.';
    }

    bool dfs(vector<vector<char> >& board, int num) {
        int i = cells[0][num];
        int j = cells[1][num];
        for(int m=0; m<table.size(); m++) {
            char x = table[m];
            if(!good(x, i, j))
                continue;
            put(board, x, i, j);
            if(num == cells[0].size()-1)
                return true;
            if(dfs(board, num+1))
                return true;
            unset(board, x, i, j);
        }
        return false;
    }

    void solveSudoku(vector<vector<char> > &board) {
        int startx, starty;
        int num = 0;
        startx = starty = -1;
        init(board);
        cells.clear();
        cells.resize(2);
        for(int i=0; i<board.size(); i++) {
            for(int j=0; j<board[0].size(); j++) {
                if(board[i][j] == '.') {
                    cells[0].push_back(i);
                    cells[1].push_back(j);
                }
            }
        }
        dfs(board, 0);
    }
};

void print(vector<vector<char> >& board) {
    printf("\n");
    for(int i=0; i<board.size(); i++) {
        for(int j=0; j<board[i].size(); j++) {
            printf("%c", board[i][j]);
        }
        printf("\n");
    }
    printf("====================\n");
}


void test1() {
    string A[9] = {"..9748...",
                   "7........",
                   ".2.1.9...",
                   "..7...24.",
                   ".64.1.59.",
                   ".98...3..",
                   "...8.3.2.",
                   "........6",
                   "...2759.."};
    vector<vector<char> > board(9);
    for(int i=0; i<9; i++) {
        for(int j=0; j<A[i].size(); j++) {
            board[i].push_back(A[i][j]);
        }
    }
    Solution p;
    p.solveSudoku(board);
    print(board);
}


void test2() {
    string A[9] = {"..9748...",
                   "7........",
                   ".2.1.....",
                   "..7...24.",
                   ".64.1.59.",
                   ".98...3..",
                   "...8.3.2.",
                   "........6",
                   "...2759.."};
    vector<vector<char> > board(9);
    for(int i=0; i<9; i++) {
        for(int j=0; j<A[i].size(); j++) {
            board[i].push_back(A[i][j]);
        }
    }
    Solution p;
    p.solveSudoku(board);
    print(board);
}

void test3() {
    string A[9] = {".........",
                   ".........",
                   ".........",
                   ".........",
                   ".........",
                   ".........",
                   ".........",
                   ".........",
                   "........."};
    vector<vector<char> > board(9);
    for(int i=0; i<9; i++) {
        for(int j=0; j<A[i].size(); j++) {
            board[i].push_back(A[i][j]);
        }
    }
    Solution p;
    p.solveSudoku(board);
    print(board);
}


int main() {
    test1();
    test2();
    test3();
    return 0;
}
