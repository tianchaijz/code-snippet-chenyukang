

# Definition for a binary tree node.
class TreeNode
    attr_accessor :val, :left, :right
    def initialize(val)
        @val = val
        @left, @right = nil, nil
    end
end

# @param {TreeNode} root
# @param {Integer} k
# @return {Integer}
def find_iter(node, k, cur)
  return cur.last if cur.size == k
  find_iter node.left, k, cur if node.left
  return cur.last if cur.size == k
  cur << node.val
  find_iter node.right, k, cur if node.right
  return cur.last if cur.size == k
end


def kth_smallest(root, k)
  find_iter(root, k, [])
end

a = TreeNode.new(0)
b = TreeNode.new(1)
c = TreeNode.new(2)

b.left = a
b.right = c

puts kth_smallest(b, 3)
