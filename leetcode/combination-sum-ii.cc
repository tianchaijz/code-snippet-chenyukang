#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>
using namespace std;


class Solution {
public:
    void dfs(vector<vector<int> >& ans, vector<int>& candidates, vector<int> buf, int target, int index) {
        if(target == 0) {
            bool found = false;
            int i, k;
            for(i=0; i<ans.size() && found == false; i++) {
                if(ans[i].size() != buf.size())
                    continue;
                for(k=0; k<ans[i].size(); k++) {
                    if(ans[i][k] != buf[k]) {
                        break;
                    }
                }
                if(k == ans[i].size())
                    found = true;
            }
            if(!found)
                ans.push_back(buf);
            return;
        }
        if(index >= candidates.size())
            return;

        if(candidates[index] > target)
            return;

        //do not choose from index
        dfs(ans, candidates, buf, target, index+1);

        //choose from index
        buf.push_back(candidates[index]);
        dfs(ans, candidates, buf, target - candidates[index], index+1);
    }

    vector<vector<int> > combinationSum2(vector<int> &candidates, int target) {
        vector<vector<int> > ans;
        vector<int> now;
        sort(candidates.begin(), candidates.end());
        dfs(ans, candidates, now, target, 0);
        return ans;
    }
};

int main() {
    vector<vector<int> > ans;
    vector<int> vec;
    int v[] = {10,1,2,7,6,1,5};
    for(int i=0; i<sizeof(v)/sizeof(int); i++){
        vec.push_back(v[i]);
    }

    int target = 8;
    Solution p;
    ans = p.combinationSum2(vec, target);
    for(int i=0; i<ans.size(); i++) {
        for(int j=0; j<ans[i].size(); j++) {
            printf("%d ", ans[i][j]);
        }
        printf("\n");
    }
    return 0;
}
