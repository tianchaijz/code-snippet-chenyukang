#include <iostream>
#include <vector>
using namespace std;

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


class Solution {
    int postPos;

public:
    TreeNode *iter(vector<int> &inorder, vector<int> &postorder,
                   int start, int tail) {
        if(start > tail) return NULL;
        if(start == tail) {
            postPos--;
            return new TreeNode(inorder[start]);
        }
        int val = postorder[postPos--];
        int i = 0;
        for(i=start; i<=tail; i++) {
            if(inorder[i] == val)
                break;
        }
        TreeNode* node = new TreeNode(val);
        node->right = iter(inorder, postorder, i+1, tail);
        node->left  = iter(inorder, postorder, start, i-1);
        return node;
    }

    TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder) {
        if(inorder.size() == 0 || postorder.size() == 0)
            return NULL;
        if(inorder.size() == 1) {
            return new TreeNode(inorder[0]);
        }
        postPos = postorder.size() - 1;
        int start = 0;
        int tail = inorder.size() - 1;
        postPos = postorder.size() - 1;
        TreeNode* res = iter(inorder, postorder, start, tail);
        return res;
    }
};

int main() {
    vector<int> inorder;
    vector<int> postorder;
    inorder.push_back(2);
    inorder.push_back(1);
    inorder.push_back(3);

    postorder.push_back(2);
    postorder.push_back(3);
    postorder.push_back(1);

    Solution p;
    TreeNode* res = p.buildTree(inorder, postorder);
    std::cout << res->val << std::endl;
    std::cout << res->right->val << std::endl;
    std::cout << res->left->val << std::endl;
    return 0;
}
