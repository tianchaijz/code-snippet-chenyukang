#include <iostream>
#include <vector>
#include <map>
using namespace std;


class Solution {
public:
    vector<string> anagrams(vector<string> &strs) {
        typedef map<string, vector<string> > Dict;
        vector<string> res;
        Dict S;
        for(int i=0; i<strs.size(); i++) {
            string tmp = strs[i];
            sort(tmp.begin(), tmp.end());
            if(S.find(tmp) == S.end()) {
                S[tmp] = vector<string>(1, strs[i]);
            } else {
                S[tmp].push_back(strs[i]);
            }
        }

        for(Dict::iterator it = S.begin(); it != S.end(); ++it) {
            vector<string>& vec = it->second;
            if(vec.size() <= 1) continue;
            res.insert(res.begin(), vec.begin(), vec.end());
        }
        return res;
    }
};

int main() {
    return 0;
}
