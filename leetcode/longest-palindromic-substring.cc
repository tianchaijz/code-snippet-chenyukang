#include <iostream>
#include <vector>
#include <cassert>
#include <stdio.h>
using namespace std;

class Solution {
public:
    string longestPalindrome(string s) {
        int ans  = 0;
        int start = 0;
        if(s.size() < 2)
            return s;
        for(int i=0; i<s.size(); i++) {
            int l, r;
            l = r = i;
            while(r < s.size() && s[r] == s[i])
                r++;
            while(l >= 0 && s[l] == s[i])
                l--;
            for(; l>=0 && r <= s.size() - 1; l--, r++) {
                if(s[l] != s[r]) {
                    break;
                }
            }
            l++, r--;
            int len = r - l + 1;
            if(len > ans) {
                ans = len;
                start = l;
            }
        }
        return s.substr(start, ans);
    }
};

int main() {
    Solution p;
    string ans = p.longestPalindrome("bb");
    std::cout << "ans: " << ans << std::endl;

    ans = p.longestPalindrome("abba");
    std::cout << "ans: " << ans << std::endl;

    ans = p.longestPalindrome("abcba");
    std::cout << "ans: " << ans << std::endl;

    ans = p.longestPalindrome("ccc");
    std::cout << "ans: " << ans << std::endl;

    ans = p.longestPalindrome("akcba");
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
