#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
using namespace std;

class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int index = 0;
        int res = 0;
        for(int i=0; i<prices.size(); i++) {
            if(prices[i] < prices[index])
                index = i;
            int diff = prices[i] - prices[index];
            if(diff > res)
                res = diff;
        }
        return res;
    }
};

int main() {
    Solution p;
    vector<int> x;
    x.push_back(4);
    x.push_back(3);
    x.push_back(2);
    x.push_back(1);
    std::cout << p.maxProfit(x) << std::endl;
    return 0;
}
