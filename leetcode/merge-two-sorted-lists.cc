#include <iostream>

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
private:
    ListNode* copy(ListNode* list) {
        if(list == NULL) return NULL;
        ListNode* head = new ListNode(list->val);
        ListNode* t = head;
        ListNode* x = list->next;
        while(x) {
            t->next = new ListNode(x->val);
            t = t->next;
            x = x->next;
        }
        return head;
    }

public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        if(l1 == NULL && l2 == NULL) return NULL;
        if(l1 == NULL) return copy(l2);
        if(l2 == NULL) return copy(l1);
        ListNode* res = NULL;
        ListNode* t = NULL;
        ListNode* p = NULL;
        int v;
        while(l1 && l2) {
            if(l1->val >= l2->val) {
                v = l2->val;
                l2 = l2->next;
            } else {
                v = l1->val;
                l1 = l1->next;
            }
            p = new ListNode(v);
            if(res == NULL) {
                res = p;
                t = p;
            } else {
                t->next = p;
                t = t->next;
            }
        }
        ListNode* left = NULL;
        if(l1 || l2)
            left = (l1 != NULL) ? l1 : l2;
        while(left) {
            p = new ListNode(left->val);
            t->next = p;
            left = left->next;
            t = t->next;
        }
        return res;
    }
};

int main() {
    return 0;
}
