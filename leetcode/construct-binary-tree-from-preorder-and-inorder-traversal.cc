#include <iostream>
#include <vector>
using namespace std;

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
    int prepos;

public:
    TreeNode *iter(vector<int> &preorder, vector<int> &inorder,
                   int start, int tail) {
        if(start > tail) return NULL;
        if(start == tail) {
            prepos++;
            return new TreeNode(inorder[start]);
        }
        int val = preorder[prepos++];
        int i;
        for(i=start; i<=tail; i++) {
            if(inorder[i] == val)
                break;
        }
        TreeNode* node = new TreeNode(val);
        node->left = iter(preorder, inorder, start, i-1);
        node->right = iter(preorder, inorder, i+1, tail);
        return node;
    }

    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
        if(preorder.size() == 0 || inorder.size() == 0)
            return NULL;
        if(preorder.size() == 1)
            return new TreeNode(preorder[0]);
        prepos = 0;
        int start = 0;
        int tail = inorder.size() - 1;
        TreeNode* res = iter(preorder, inorder, start, tail);

        return res;
    }
};

int main() {
    return 0;
}
