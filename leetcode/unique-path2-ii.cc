//dfs search with memozation.
#include <iostream>
#include <vector>
#include <cassert>
using namespace std;

class Solution {
public:
    int dfs(vector<vector<int> > & obstacleGrid,
             vector<vector<int> >& dp, int row, int col) {
        if(obstacleGrid[row][col] == 1)
            return 0;
        if(dp[row][col] != 0)
            return dp[row][col];
        int up, le;
        up = le = 0;
        if(row >= 1)
            up = dfs(obstacleGrid, dp, row-1, col);
        if(col >= 1)
            le = dfs(obstacleGrid, dp, row, col-1);
        dp[row][col] = up + le;
        return dp[row][col];
    }

    int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
        int row, col;
        row = obstacleGrid.size();
        if(row == 0) return 0;
        col = obstacleGrid[0].size();

        if(obstacleGrid[0][0] == 1)
            return 0;
        vector<vector<int> > dp;
        dp.resize(row);
        for(int i=0; i<row; i++)
            dp[i].resize(col);

        dp[0][0] = 1;
        return dfs(obstacleGrid, dp, row-1, col-1);
    }
};

int main() {
    vector<vector<int> > matrix;
    matrix.resize(99);
    for(int i=0; i<99; i++) {
        matrix[i].resize(99);
    }
    matrix[1][1] = 1;
    Solution p;
    int ans = p.uniquePathsWithObstacles(matrix);
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
