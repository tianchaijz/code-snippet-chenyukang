#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <tr1/unordered_set>

using namespace std;
using namespace tr1;

class Solution {
public:
    bool wordBreak(string s, unordered_set<string> &dict) {
        vector<bool> dp(s.length() + 1, false);
        dp[0] = true;
        for(size_t i = 1; i <= s.length(); i++) {
            for(int j = i - 1; j >= 0; j--) {
                if(dp[j] && dict.find(s.substr(j, i - j)) != dict.end()) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[s.length()];
    }

};


int main() {
    string s = "bccdbacdbdacddabbaaaadababadad";
    tr1::unordered_set<string> dict;
    const char* keys[50] =  {"cbc","bcda","adb","ddca","bad","bbb","dad","dac",
                             "ba","aa","bd","abab","bb","dbda","cb","caccc","d",
                             "dd","aadb","cc","b","bcc","bcd","cd","cbca","bbd",
                             "ddd","dabb","ab","acd","a","bbcc","cdcbd","cada",
                             "dbca","ac","abacd","cba","cdb","dbac","aada","cdcda",
                             "cdc","dbc","dbcb","bdb","ddbdd","cadaa","ddbc","babb"};

    for(int i=0; i<50; i++) {
        dict.insert(keys[i]);
    }

    Solution* so = new Solution();
    if(so->wordBreak(s, dict)) {
        std::cout<<"yes"<<std::endl;
    } else {
        std::cout <<"no"<<std::endl;
    }

    delete p;
    return 0;
}
