#include <string>
#include <iostream>
using namespace std;

class Solution {
public:
    int romanToInt(string num) {
        int res = 0;
        string symbol[]={"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        int value[]=    {1000,900,500,400, 100, 90,  50, 40,  10, 9,   5,  4,   1};
        int size = sizeof(value) / sizeof(int);
        while(num.size() > 0) {
            for(int i=0; i<size; i++) {
                if(num.find(symbol[i]) == 0) {
                    res += value[i];
                    num = num.substr(symbol[i].size(), num.size() - symbol[i].size());
                }
            }
        }
        return res;
    }
};

int main() {
    Solution p;
    std::cout << p.intToRoman("DCXXI") << std::endl;
    return 0;
}
