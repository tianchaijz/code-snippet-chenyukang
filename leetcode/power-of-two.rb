
for i in 0...5
  puts "Value of local variable is #{i}"
end


def is_power_of_two(n)
  n > 0 && (n & (n-1) == 0)
end
