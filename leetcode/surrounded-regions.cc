#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

int dir[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

class Node {
public:
    int x;
    int y;
    Node(int xx, int yy): x(xx), y(yy) {}
};

class Solution {
private:
    bool valid(int x, int y, vector<vector<char> >& board) {
        if(x<0 || x>=board.size()) return false;
        if(y<0 || y>=board[0].size()) return false;
        return true;
    }

    void rounded(int x, int y, vector<vector<bool> >& visited,
                 vector<vector<char> >& board, vector<Node>& res, bool& surrounded) {
        visited[x][y] = true;
        res.push_back(Node(x, y));
        for(int i=0; i<4; i++) {
            int nx = x + dir[i][0];
            int ny = y + dir[i][1];
            if(!valid(nx, ny, board)) {
                surrounded = false;
                continue;
            }
            if(!visited[nx][ny] && (board[nx][ny] == 'O')) {
                rounded(nx, ny, visited, board, res, surrounded);
            }
        }
    }

public:
    void solve(vector<vector<char> >& board) {
        if(board.size() == 0) return;
        vector<vector<bool> > visited;
        int row = board.size();
        int col = board[0].size();
        visited.resize(row);
        for(int k=0; k<row; k++) {
            visited[k] = vector<bool>(col, false);
        }
        for(int i=0; i<row; i++) {
            for(int j=0; j<col; j++) {
                if(board[i][j] == 'X') continue;
                if(visited[i][j]) continue;
                vector<Node> res;
                bool surrounded = true;
                rounded(i, j, visited, board, res, surrounded);
                if(surrounded){
                    for(int k=0; k<res.size(); k++) {
                        board[res[k].x][res[k].y] = 'X';
                    }
                }
            }
        }
    }
};

int main() {
    Solution p;
    vector<vector<char> > board(1);
    board[0].push_back('O');
    p.solve(board);
    return 0;
}
