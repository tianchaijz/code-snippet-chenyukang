

# a simple version, O(nlogn) time
def find_min(nums)
  nums.sort.first
end

def find_min_v1(nums)
  l, r = 0, nums.size - 1
  while l < r
    mid = (r - l)/2 + l
    if nums[mid] > nums[r]
      l = mid + 1
    else
      r = mid
    end
  end
  return nums[l]
end

def find_min(nums)
  l, r = 0, nums.size - 1
  while l < r
    mid = (r - l)/2 + l
    if nums[mid] > nums[r]
      l = mid + 1
    elsif nums[mid] < nums[r]
      r = mid
    else
      r -= 1
    end
  end
  return nums[l]
end


#puts find_min([4, 5, 6, 7, 0, 1, 2])

puts find_min([3,3,1,3])
puts find_min2([3,3,1,3])
