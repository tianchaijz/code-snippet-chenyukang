#include <iostream>
#include <cassert>
#include <stack>
using namespace std;

class Solution {
public:
    bool isValid(string s) {
        if(s.size()%2) return false;
        stack<char> buf;
        for(int i=0; i<s.size(); i++) {
            char t = s[i];
            if(t == '(' || t == '{' || t == '[')
                buf.push(t);
            else {
                if(buf.empty()) return false;
                if(t == ')') {
                    if(buf.top() != '(') return false;
                } else if(t == '}') {
                    if(buf.top() != '{') return false;
                } else if(t == ']') {
                    if(buf.top() != '[') return false;
                } else
                    return false;
                buf.pop();
            }
        }
        if(buf.empty())
            return true;
        return false;
    }
};


int main() {
    return 0;
}
