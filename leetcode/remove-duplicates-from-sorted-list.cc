#include <iostream>
using namespace std;

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        if(head == NULL) return NULL;
        ListNode* res = head;
        ListNode* p = head;
        ListNode* t = head->next;
        p->next = NULL;
        while(t) {
            if(t->val == p->val) {
                t = t->next;
            } else {
                p->next = t;
                p = p->next;
                t = t->next;
                p->next = NULL;
            }
        }
        return res;
    }
};

int main() {
    return 0;
}
