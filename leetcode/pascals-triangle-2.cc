#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <vector>
using namespace std;


class Solution {
public:
    vector<int> getRow(int numRows) {
        vector<int> res;
        res.push_back(1);
        if(numRows < 1) return res;
        for(int i=0; i<numRows; i++) {
            vector<int> next;
            next.push_back(1);
            for(int j=0; j<res.size()-1; j++) {
                next.push_back(res[j] + res[j+1]);
            }
            next.push_back(1);
            res = next;
        }
        return res;
    }
};

int main() {
    Solution p;
    vector<int> res = p.getRow(3);
    for(int i=0; i<res.size(); i++) {
        printf("%d ", res[i]);
    }
    printf("\n");
    return 0;
}
