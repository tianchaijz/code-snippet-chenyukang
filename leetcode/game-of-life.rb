
def get_neighbor_count board, i, j
  dir = [[-1, 0], [0, -1], [1, 0], [0, 1],
         [-1, -1], [1, 1], [-1, 1], [1, -1]
        ]
  m = board.size
  n = board.first.size
  dir.map { |d|
    _i, _j = i + d[0], j + d[1]
    (_i >= 0 && _i < m && _j >= 0 && _j < n &&
     board[_i][_j] == 1) ? 1 : 0
  }.reduce(&:+)
end

def game_of_life(board)
  m = board.size
  n = board.first.size

  buf = (1..m).map{ |_| Array.new n, 0}
  for i in 0...m
    for j in 0...n
      count = get_neighbor_count board, i, j
      if board[i][j] == 1
        (count < 2 || count > 3) ? buf[i][j] = 0 : buf[i][j] = 1
      else
        count == 3 ? buf[i][j] = 1 : buf[i][j] = 0
      end
    end
  end

  for i in 0...m
    for j in 0...n
      board[i][j] = buf[i][j]
    end
  end
end

a = [[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0]]
game_of_life(a)
puts a.inspect
