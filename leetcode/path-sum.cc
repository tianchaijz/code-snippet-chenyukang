#include <iostream>
#include <cassert>
#include <stdio.h>
using namespace std;


/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    bool node_iter(TreeNode *node, int sum) {
        if(sum == 0 && node == NULL) return true;
        if(node == NULL) return false;
        int left = sum - node->val;
        if(left == 0 && node->left == 0 && node->right == 0)
            return true;
        if(node->left && node_iter(node->left, left))
            return true;
        if(node->right && node_iter(node->right, left))
            return true;
        return false;
    }

    bool hasPathSum(TreeNode *root, int sum) {
        if(root == NULL) return false;
        return node_iter(root, sum);
    }
};


/*
5
/ \
4   8
/   / \
11  13  4
/  \      \
7    2      1
*/
void test1() {
    TreeNode* root = new TreeNode(5);
    root->left = new TreeNode(4);
    root->right = new TreeNode(8);
    root->left->left = new TreeNode(11);
    root->right->left = new TreeNode(13);
    root->right->right = new TreeNode(4);
    TreeNode* t = root->left->left;
    t->left = new TreeNode(7);
    t->right = new TreeNode(2);
    root->right->right->right = new TreeNode(4);

    Solution p;
    std::cout << p.hasPathSum(root, 22) << std::endl;
}

void test2() {
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    Solution p;
    std::cout << p.hasPathSum(root, 1) << std::endl;
}

int main() {
    test1();
    test2();
    return 0;
}
