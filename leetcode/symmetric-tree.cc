#include <iostream>
#include <stdio.h>
#include <stack>
#include <vector>
using namespace std;


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


class Solution {

    bool isSame(TreeNode* ll, TreeNode* rr) {
        if(ll == rr) return true;
        if(ll == NULL || rr == NULL) return false;
        if(ll->val != rr->val) return false;
        return isSame(ll->right, rr->left) &&
            isSame(ll->left, rr->right);
    }

public:
    bool isSymmetric(TreeNode* root) {
        if(root == NULL) return true;
        return isSame(root->left, root->right);
    }
};

int main() {
    return 0;
}
