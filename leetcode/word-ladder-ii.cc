#include <string>
#include <set>
#include <vector>
#include <map>
#include <queue>
#include <unordered_set>
#include <iostream>
#include <algorithm>
using namespace std;

struct Node {
    string str;
    vector<string> trace;
    unordered_set<string> flags;
    int step;
    Node(string s, int n) : str(s), step(n) {}
};

class Solution {
public:
    //pass all the testcases
    vector<vector<string> > findLadders(string start, string end,
                                        unordered_set<string> &dict) {
        dict.insert(start);
        dict.insert(end);

        //BFS
        vector<vector<string> > ans;
        queue<Node> Q;
        Node node(start, 1);
        node.trace.push_back(start);
        node.flags.insert(start);
        Q.push(node);
        int best = -1;
        while(!Q.empty()) {
            Node now = Q.front();
            Q.pop();
            string val = now.str;
            vector<string>& trace = now.trace;
            unordered_set<string>& flags = now.flags;
            if(best != -1) {
                if(now.step > best)
                    return ans;
                else if(now.step == best && val != end)
                    continue;
            }

            if(val == end)  {
                best = now.step;
                ans.push_back(trace);
                continue;
            }
            for(int i=0; i<val.size(); i++) {
                for(char k = 'a'; k<= 'z'; k++) {
                    if(k == val[i]) continue;
                    string next = val;
                    next[i] = k;
                    if(dict.find(next) != dict.end() && flags.find(next) == flags.end()) {
                        vector<string> t = trace;
                        unordered_set<string> f = flags;
                        Node n(next, now.step+1);
                        n.trace = t;
                        n.trace.push_back(next);
                        n.flags.insert(next);
                        Q.push(n);
                    }
                }
            }
        }
        return ans;
    }
};

void test1() {
    unordered_set<string> dict;
    dict.insert("hot");
    dict.insert("dot");
    dict.insert("dog");
    dict.insert("lot");
    dict.insert("log");
    Solution p;
    vector<vector<string> > ans = p.findLadders("hit", "cog", dict);
    for(int i=0; i<ans.size(); i++) {
        std::cout << "size: " << ans[i].size() << " : ";
        for(int j=0; j<ans[i].size(); j++) {
            printf("%s ->", ans[i][j].c_str());
        }
        printf("\n");
    }
}

void test2() {
    unordered_set<string> dict;
    dict.insert("a");
    dict.insert("b");
    dict.insert("c");
    Solution p;
    vector<vector<string> > ans = p.findLadders("a", "c", dict);
    for(int i=0; i<ans.size(); i++) {
        std::cout << "size: " << ans[i].size() << " : ";
        for(int j=0; j<ans[i].size(); j++) {
            printf("%s ->", ans[i][j].c_str());
        }
        printf("\n");
    }
}


void test3() {
    const char* str[] = {"si","go","se","cm","so","ph","mt","db","mb","sb","kr","ln","tm","le","av","sm","ar","ci","ca","br","ti","ba","to","ra","fa","yo","ow","sn","ya","cr","po","fe","ho","ma","re","or","rn","au","ur","rh","sr","tc","lt","lo","as","fr","nb","yb","if","pb","ge","th","pm","rb","sh","co","ga","li","ha","hz","no","bi","di","hi","qa","pi","os","uh","wm","an","me","mo","na","la","st","er","sc","ne","mn","mi","am","ex","pt","io","be","fm","ta","tb","ni","mr","pa","he","lr","sq","ye"};
    unordered_set<string> dict;
    for(int i=0; i<sizeof(str)/sizeof(char*); i++) {
        dict.insert(str[i]);
    }
    Solution p;
    vector<vector<string> > ans = p.findLadders("qa", "sq", dict);
    for(int i=0; i<ans.size(); i++) {
        std::cout << "size: " << ans[i].size() << " : ";
        for(int j=0; j<ans[i].size(); j++) {
            printf("%s ->", ans[i][j].c_str());
        }
        printf("\n");
    }
}


int main() {
    test3();
    return 0;
}
