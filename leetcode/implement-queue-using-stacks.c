#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
  int* arr;
  int max_size;
  int cur_size;
} Queue;

/* Create a queue */
void queueCreate(Queue *queue, int maxSize) {
  queue->max_size = maxSize;
  queue->cur_size = 0;
  queue->arr = (int*)malloc(sizeof(int) * maxSize);
}

/* Push element x to the back of queue */
void queuePush(Queue *queue, int element) {
  int cur = queue->cur_size;
  queue->arr[cur] = element;
  queue->cur_size++;
}

/* Removes the element from front of queue */
void queuePop(Queue *queue) {
  if(queue->cur_size <= 0)
    return;
  int i = 0;
  while(i < queue->cur_size - 1) {
    queue->arr[i] = queue->arr[i+1];
    i++;
  }
  queue->cur_size--;
}

/* Get the front element */
int queuePeek(Queue *queue) {
  return queue->arr[0];
}

/* Return whether the queue is empty */
bool queueEmpty(Queue *queue) {
  return queue->cur_size == 0;
}

/* Destroy the queue */
void queueDestroy(Queue *queue) {
  free(queue->arr);
  free(queue);
}

int main() {
  Queue q;
  queueCreate(&q, 10);
  queuePush(&q, 1);
  assert(queuePeek(&q) == 1);
  queuePop(&q);
  assert(queueEmpty(&q));

  queuePush(&q, 1);
  queuePush(&q, 2);
  queuePop(&q);
  assert(queueEmpty(&q) == false);
  assert(queuePeek(&q) == 2);
  return 0;
}
