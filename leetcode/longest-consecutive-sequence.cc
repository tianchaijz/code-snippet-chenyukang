#include <set>
#include <map>
#include <vector>
#include <climits>
#include <iostream>
#include <unordered_set>
using namespace std;


class Solution {
private:
    enum Order {
        DESC,
        INCR
    };
    std::unordered_set<int> hash;

public:
    int hashFind(int value, int& limit, Order order) {
        unordered_set<int>::iterator it;
        int num = 0;
        while((it = hash.find(value)) != hash.end()) {
            limit = value;
            if(order == DESC)
                value--;
            else
                value++;
            num++;
        }
        return num;
    }

    int longestConsecutive(vector<int> &num) {
        int ans = -1;
        hash.clear();
        for(int i=0; i<num.size(); i++) {
            hash.insert(num[i]);
        }

        int minValue, maxValue;
        minValue = INT_MAX;
        maxValue = INT_MIN;
        for(int i=0; i<num.size(); i++) {
            if(num[i] >= minValue && num[i] <= maxValue)
                continue;
            int min, max;
            min = max = num[i];
            int descLen = hashFind(num[i]-1, min, DESC);
            int incrLen = hashFind(num[i], max, INCR);
            int len = descLen + incrLen;
            if(len >= ans) {
                ans = len;
                minValue = min;
                maxValue = max;
            }
        }
        return ans;
    }
};

int main() {
    Solution p;
    int vec[] = {100, 4, 200, 1, 3, 2};
    vector<int> in;
    for(int i=0; i<sizeof(vec)/sizeof(int); i++)
        in.push_back(vec[i]);
    int ans = p.longestConsecutive(in);
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
