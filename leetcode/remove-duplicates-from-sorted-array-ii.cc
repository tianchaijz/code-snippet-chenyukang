#include <stdio.h>
#include <iostream>
#include <queue>
#include <assert.h>
#include <vector>
#include <stack>
using namespace std;


class Solution {
public:

    int removeDuplicates(int A[], int n) {
        if(n <= 2) return n;
        int index=2;
        for(int i = 2; i<n; i++) {
            if(A[i] != A[index-2])
                A[index++] = A[i];
        }
        return index;
    }

    int removeDuplicates_notgood(int A[], int n) {
        int time = 0;
        int pos = 0;
        if(n < 1) return 0;
        time = 1;
        pos = 1;
        for(int i=1; i<n; i++) {
            if(A[pos-1] != A[i]) {
                A[pos++] = A[i];
                time = 1;
            } else {
                if(time + 1 > 2)
                    continue;
                else {
                    A[pos++] = A[i];
                    time++;
                }
            }
        }
        return pos;
    }
};

int main() {
    int A[] = {1,2, 2, 2, 2, 3, 3};
    Solution p;
    int len = p.removeDuplicates(A, sizeof(A)/sizeof(int));
    for(int i=0; i<len; i++) {
        printf(" %d ", A[i]);
    }
    printf("\n");
    return 0;
}
