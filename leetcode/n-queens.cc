#include <vector>
#include <iostream>
#include <assert.h>
using namespace std;


class Solution {
public:

    bool valid(const vector<string>& map, int row, int col) {
        for(int i = row-1; i>=0; i--) {
            if(map[i][col] == 'Q') return false;
        }
        for(int k = 1; row - k >=0 && col - k >=0; k++) {
            if(map[row-k][col-k] == 'Q') return false;
        }
        for(int k = 1; row - k >=0 && col + k < map[0].size(); k++) {
            if(map[row-k][col+k] == 'Q') return false;
        }
        return true;
    }

    void dfs(vector<string>& map, int index, vector<vector<string> >& ans) {
        if(index >= map.size()) {
            ans.push_back(map);
        } else {
            for(int i=0; i<map[index].size(); i++) {
                map[index][i] = 'Q';
                if(valid(map, index, i)) {
                    dfs(map, index+1, ans);
                }
                map[index][i] = '.';
            }
        }
    }

    vector<vector<string> > solveNQueens(int n) {
        vector<vector<string> > ans;
        vector<string>  Map(n);
        for(int i=0; i<Map.size(); i++) {
            Map[i] = string(n, '.');
        }

        dfs(Map, 0, ans);
        return ans;
    }
};

int main() {
    Solution p;
    vector<vector<string> > ans = p.solveNQueens(10);

    for(int i=0; i<ans.size(); i++) {
        for(int j=0; j<ans[i].size(); j++) {
            std::cout << ans[i][j] << std::endl;
        }
        std::cout << "------------------------------" << std::endl;
    }
    return 0;
}
