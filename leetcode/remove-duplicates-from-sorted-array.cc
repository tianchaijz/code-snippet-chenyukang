#include <iostream>
#include <stdio.h>

class Solution {
public:
    int removeDuplicates(int A[], int n) {
        int num = 0;
        if(n == 0) return num;
        num = 1;
        for(int i=1; i<n; i++) {
            if(A[i] != A[num-1]) {
                A[num] = A[i];
                num++;
            }
        }
        return num;
    }
};


int main() {
    Solution p;
    int a[] = {1, 3, 3, 4, 5, 6, 6, 9};
    std::cout << p.removeDuplicates(a, 8) << std::endl;
    return 0;
}
