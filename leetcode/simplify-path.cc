#include <iostream>
#include <vector>
#include <stdio.h>
#include <stack>
#include <cassert>
using namespace std;


class Solution {
public:
    string simplifyPath(string path) {
        vector<string> vecs;
        for(int i=1; i<path.size(); ) {
            if(path[i] == '/') {
                i++;
                continue;
            }
            if(i == path.size() - 1) {
                if(path[i] != '.') {
                    string s = path.substr(i, 1);
                    vecs.push_back(s);
                }
                break;
            }
            for(int j=i+1; j<path.size(); ) {
                if(path[j] == '/' || j == path.size() - 1) {
                    string s;
                    if(path[j] != '/')
                        s = path.substr(i, j-i+1);
                    else s = path.substr(i, j-i);
                    if(s != "") {
                        vecs.push_back(s);
                    }
                    i = j+1;
                    break;
                } else {
                    j++;
                }
            }
        }
#if 0
        for(int i=0; i<vecs.size(); i++) {
            printf("%s <-> ", vecs[i].c_str());
        }
        printf("\n");
#endif
        stack<string> dir;
        dir.push("/");
        for(int i=0; i<vecs.size(); i++) {
            if(vecs[i] == ".") continue;
            else if(vecs[i] == "..") {
                if(dir.size() > 1)
                    dir.pop();
            }
            else {
                dir.push(vecs[i]);
            }
        }
        string res;
        while(!dir.empty()) {
            if(res == "")
                res = dir.top();
            else if(dir.size() != 1)
                res = dir.top() + "/" + res;
            else
                res = dir.top() + res;
            dir.pop();
        }
        return res;
    }
};


int main() {
    Solution p;
    std::cout << p.simplifyPath("/.") << std::endl;
    std::cout << p.simplifyPath("/..") << std::endl;
    std::cout << p.simplifyPath("/../a") << std::endl;
    std::cout << p.simplifyPath("/abcd/") << std::endl;
    std::cout << p.simplifyPath("/abc") << std::endl;
    std::cout << p.simplifyPath("/abc/.") << std::endl;
    std::cout << p.simplifyPath("//abc/../cd/") << std::endl;
    std::cout << p.simplifyPath("/a/./b/../../c/") << std::endl;
    std::cout << p.simplifyPath("/../") << std::endl;
    std::cout << p.simplifyPath("/home//foo/") << std::endl;
    std::cout << p.simplifyPath("/home/foo/cykang/now///") << std::endl;

    return 0;
}
