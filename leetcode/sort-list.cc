#include <iostream>
#include <stdio.h>
#include <assert.h>
using namespace std;

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

void print(ListNode* head) {
    ListNode* p = head;
    while(p) {
        printf("%d ", p->val);
        p = p->next;
    }
    printf("\n");
}


class Solution {
public:
    ListNode* merge(ListNode* left, ListNode* right) {
        ListNode* res = NULL;
        ListNode* pre = NULL;
        if(left != NULL && right == NULL) return left;
        if(right != NULL && left == NULL) return right;
        while(left && right) {
            ListNode* t = left->val < right->val ? left : right;
            if(res == NULL) {
                res = t;
                pre = res;
            }
            else {
                pre->next = t;
                pre = pre->next;
            }
            if(t == left) left = left->next;
            else
                right = right->next;
        }
        if(left) pre->next = left;
        else if(right) pre->next = right;
        return res;
    }

    ListNode *merge_iter(ListNode *head) {
        if(head == NULL) return NULL;
        if(head->next == NULL) return head;

        ListNode* p = head;
        ListNode* q = head;
        while(q->next != NULL && q->next->next != NULL ) {
            p = p->next;
            q = q->next;
            q = q->next;
        }
        q = p->next;
        p->next = NULL;
        ListNode* left = merge_iter(head);
        ListNode* right = merge_iter(q);
        ListNode* res = merge(left, right);
        return res;
    }

    ListNode* sortList(ListNode* head) {
        return merge_iter(head);
    }
};


int test1() {
    Solution p;
    ListNode* l1 = new ListNode(1);
    ListNode* l2 = new ListNode(2);
    l2->next =  new ListNode(3);
    ListNode* res = p.merge(l1, l2);
    print(res);
}

int test2() {
    Solution p;
    ListNode* l1 = new ListNode(10);
    l1->next = new ListNode(2);
    l1->next->next = new ListNode(4);
    l1->next->next->next = new ListNode(5);
    ListNode* res = p.sortList(l1);
    print(res);
}

int main() {
    test1();
    test2();
    return 0;
}
