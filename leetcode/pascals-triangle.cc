#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <vector>
using namespace std;


class Solution {
public:
    vector<vector<int> > generate(int numRows) {
        vector<vector<int> > res;
        if(numRows < 1) return res;
        vector<int> x;
        x.push_back(1);
        res.push_back(x);
        for(int i=1; i<numRows; i++) {
            vector<int> next;
            next.push_back(1);
            for(int j=0; j<res[i-1].size()-1; j++) {
                next.push_back(res[i-1][j] + res[i-1][j+1]);
            }
            next.push_back(1);
            res.push_back(next);
        }
        return res;
    }
};


int main() {
    Solution p;
    vector<vector<int> > res = p.generate(5);
    for(int i=0; i<res.size(); i++) {
        for(int j=0; j<res[i].size(); j++) {
            printf(" %d ", res[i][j]);
        }
        printf("\n");
    }
    return 0;
}
