#include <vector>
#include <stdio.h>
#include <cassert>
using namespace std;

class Solution {
public:
    int threeSumClosest(vector<int> &num, int target) {
        int diff = INT_MAX;
        int ans = 0;
        sort(num.begin(), num.end());

        if(num.size() < 3) return ans;
        for(int first = 0; first < num.size(); first++) {
            int value = target-num[first];
            int head = first + 1;
            int tail = num.size() - 1;
            while(head < tail) {
                int v = num[head] + num[tail];
                if(v == value) {
                    return target;
                }
                else if(v > value) {
                    if(abs(v - value)  < diff) {
                        ans = v + num[first];
                        diff = abs(v - value);
                    }
                    tail--;
                } else {
                    if(abs(value - v)  < diff) {
                        ans = v + num[first];
                        diff = abs(value - v);
                    }
                    head++;
                }
            }
            while(first < num.size()-1 && num[first+1] == num[first])
                first++;
        }
        return ans;
    }
};

void test(vector<int>& vec, int target) {
    Solution p;
    int ans = p.threeSumClosest(vec, target);
    printf("ans: %d\n", ans);
}

int main() {
    vector<int> vec;
    test(vec, 10);
    vec.resize(4);
    vec[0] = -1, vec[1] = 2, vec[2] = 1, vec[3] = -4;
    test(vec, 1);
    return 0;
}
