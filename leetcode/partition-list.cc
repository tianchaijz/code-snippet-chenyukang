
#include <iostream>
#include <cassert>
#include <vector>
using namespace std;


/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};


class Solution {
public:
    ListNode *partition(ListNode *head, int x) {
        vector<ListNode*> less;
        vector<ListNode*> bigger;
        if(head == NULL) return NULL;
        ListNode* t = head;
        while(t) {
            int v = t->val;
            if(v<x) {
                less.push_back(t);
            } else {
                bigger.push_back(t);
            }
            ListNode* p = t->next;
            t->next = 0;
            t = p;
        }
        ListNode* res = NULL;
        ListNode* prev = NULL;
        for(int i=0; i<less.size(); i++) {
            if(res == NULL) res = less[i];
            if(prev == NULL) prev = less[i];
            else {
                prev->next = less[i];
                prev = prev->next;
            }
        }
        for(int i=0; i<bigger.size(); i++) {
            if(res == NULL) res = bigger[i];
            if(prev == NULL) prev = bigger[i];
            else {
                prev->next = bigger[i];
                prev = prev->next;
            }
        }
        return res;
    }
};

int main() {
    return 0;
}
