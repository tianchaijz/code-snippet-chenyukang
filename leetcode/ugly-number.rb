

# @param {Integer} num
# @return {Boolean}
def is_ugly(num)
  return false if num <= 0
  factors = []
  while num % 2 == 0
    factors << 2
    num /= 2
  end
  i = 3
  while i * i <= num
    while num % i == 0
      factors << i
      num /= i
    end
    i += 2
  end
  factors << num if num > 2
  return factors.uniq.all? { |x| [2, 3, 5].include? x }
end

def is_ugly num
  return false if num <= 0
  while num % 2 == 0
    num /= 2
  end
  while num % 3 == 0
    num /= 3
  end
  while num % 5 == 0
    num /= 5
  end
  return num == 1
end

puts is_ugly(2147483647)
puts is_ugly(2147483647)
