#include <iostream>
#include <stdio.h>
#include <queue>
#include <vector>
using namespace std;

/**
 * Definition for singly-linked list */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    void reOrderList2(ListNode* head) {
        if(head == NULL || head->next == NULL)
            return;
        // split the list into two part, if length of list is odd,
        // first part will have a more one

        ListNode* fast = head;
        ListNode* slow = head;
        while(1) {
            if(fast->next != NULL && fast->next->next != NULL) {
                fast = fast->next->next;
            } else break;
            slow = slow->next;
        }

        ListNode* head1 = head;
        ListNode* head2 = slow->next;
        slow->next = NULL;

        //reverse latter part
        ListNode* cur = head2;
        ListNode* nxt = cur->next;
        ListNode*   p = NULL;
        while(nxt) {
            p = nxt->next;
            nxt->next = cur;
            cur = nxt;
            nxt = p;
        }
        head2 = cur;

        //merge the two sublist
        ListNode* p1, *p2;
        while(head1 || head2) {
            ListNode* p1 = head1->next;
            ListNode* p2 = head2->next;
            head1->next = head2;
            head2->next = p1;
            head1 = p1, head2 = p2;
        }
    }

    void reorderList(ListNode *head) {
        if(head == NULL || head->next == NULL)
            return;
        vector<ListNode*> vec;
        ListNode* x = NULL;
        ListNode* t = head;
        while(t) {
            vec.push_back(t);
            x = t;
            t = t->next;
            x->next = NULL;
        }
        ListNode* prev = NULL;
        int i = 0;
        int j = vec.size()-1;
        while(i < j) {
            vec[i]->next = vec[j];
            if(prev != NULL) {
                prev->next = vec[i];
            }
            prev = vec[j];
            i++, j--;
        }
        if(i == j) {
            prev->next = vec[i];
        }
    }
};

void print(ListNode* node) {
    ListNode* t = node;
    while(t) {
        printf(" %d ", t->val);
        t = t->next;
    }
    printf("\n");
}

void test1() {
    ListNode* node = new ListNode(1);
    Solution p;
    p.reorderList(node);
    print(node);
}

void test2() {
    ListNode* node = new ListNode(1);
    node->next = new ListNode(2);
    Solution p;
    p.reorderList(node);
    print(node);
}


void test3() {
    ListNode* node = new ListNode(1);
    node->next = new ListNode(2);
    node->next->next = new ListNode(3);
    Solution p;
    p.reorderList(node);
    print(node);
}


void test4() {
    ListNode* node = new ListNode(1);
    node->next = new ListNode(2);
    node->next->next = new ListNode(3);
    node->next->next->next = new ListNode(4);
    Solution p;
    p.reorderList(node);
    print(node);
}


void test5() {
    ListNode* node = new ListNode(1);
    node->next = new ListNode(2);
    node->next->next = new ListNode(3);
    node->next->next->next = new ListNode(4);
    node->next->next->next->next = new ListNode(5);
    Solution p;
    p.reorderList(node);
    print(node);
}



int main() {
    test1();
    test2();
    test3();
    test4();
    test5();
    return 0;
}
