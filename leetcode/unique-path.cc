#include <iostream>
#include <stdio.h>
using namespace std;

class Solution {
private:
    int comb(int n, int m) {
        if(n<m)
            return 0;
        if(n==m)
            return 1;
        double res = 1;
        for(double i = 1; i <= n - m; i+=1) {
            res *= (i+m);
            res /= i;
        }
        return res;
    }

public:
    //C[m+n-2, m-1] =
    int uniquePaths(int m, int n) {
        if(m <= 1 || n <= 1) {
            return 1;
        }
        return comb(m+n-2, m-1);
    }
};

int main() {
    Solution* p = new Solution();
    std::cout << p->uniquePaths(3, 3) << std::endl;
    std::cout << p->uniquePaths(4, 4) << std::endl;
    std::cout << p->uniquePaths(5, 5) << std::endl;
    std::cout << p->uniquePaths(32, 12) << std::endl;
    return 0;
}
