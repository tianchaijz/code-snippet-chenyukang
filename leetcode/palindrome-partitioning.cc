#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
using  namespace std;

class Solution {
    vector<vector<string> > ans;
public:
    bool isPalindrome(string s) {
        int i = 0;
        int j = s.size() - 1;
        while(i < j) {
            if(s[i] != s[j])
                return false;
            i++;
            j--;
        }
        return true;
    }

    void iter(string s, vector<string> now) {
        //std::cout << "s: " << s << std::endl;
        if(s.size() == 0) {
            ans.push_back(now);
        }
        for(int i=1; i<=s.size(); i++) {
            string pre = s.substr(0, i);
            if(isPalindrome(pre)) {
                vector<string> next = now;
                next.push_back(pre);
                string left = s.substr(i, s.size() - i);
                //std::cout << "prev: " << pre << std::endl;
                //std::cout << "left: " << left << std::endl;
                iter(left, next);
            }
        }
    }

    vector<vector<string> > partition(string s) {
        vector<string> now;
        ans.clear();
        iter(s, now);
        return ans;
    }
};


int main() {
    Solution p;
    vector<vector<string> > res = p.partition("aaaa");
    for(int i=0; i<res.size(); i++) {
        printf("[ ");
        for(int j=0; j<res[i].size(); j++) {
            printf("%s ", res[i][j].c_str());
        }
        printf(" ]\n");
    }
#if 0
    std::cout << p.isPalindrome("a") << std::endl;
    std::cout << p.isPalindrome("ab") << std::endl;
    std::cout << p.isPalindrome("aba") << std::endl;
#endif
    return 0;
}
