#include <stdio.h>
#include <vector>
#include <string>
#include <set>
#include <unordered_set>
#include <iostream>
using namespace std;

class Solution {
public:
    void iter(vector<vector<int> >& res, vector<int>& num, vector<int> now, vector<int>& visited, int index) {
        if(num.size() <= now.size()) {
            res.push_back(now);
            return;
        }
        for(int i=0; i<num.size(); i++) {
            if(visited[i] == 0) {
                if(i >=1 && num[i] == num[i-1] && visited[i-1] == 0)
                    continue;
                visited[i] = 1;
                now.push_back(num[i]);
                iter(res, num, now, visited, index+1);
                now.pop_back();
                visited[i] = 0;
            }
        }
    }

    vector<vector<int> > permuteUnique(vector<int> &num) {
        vector<vector<int> > res;
        vector<int> now;
        vector<int> visited(num.size(), 0);
        sort(num.begin(), num.end());
        if(num.size() == 0) return res;
        if(num.size() == 1) {
            res.push_back(num);
            return res;
        }
        iter(res, num, now, visited, 0);
        return res;
    }
};

void test(int* v, int num) {
    vector<int> vec;
    for(int i=0; i<num; i++) {
        vec.push_back(v[i]);
    }
    Solution p;
    vector<vector<int> > res = p.permuteUnique(vec);
    for(int i=0; i<res.size(); i++) {
        for(int j=0; j<res[i].size(); j++) {
            printf("%d ", res[i][j]);
        }
        printf("\n");
    }
    printf("size: %lu\n", res.size());
}

int main() {
    int vec[] = {1,-1,1,2,-1,2,2,-1};
    test(vec, sizeof(vec)/sizeof(int));
    return 0;
}
