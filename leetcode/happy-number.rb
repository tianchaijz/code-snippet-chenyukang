
require 'set'

def next_number n
  n.to_s.split('').map{ |c|
    (c.to_i) * (c.to_i)
  }.inject(0) { |s, i| s += i }
end

def is_happy n
  return true if n == 1
  s = Set.new
  while true
    _n = next_number n
    return true if _n == 1
    return false if s.include? _n
    s.add _n
    n = _n
  end
  return true
end
