#include <iostream>
#include <stdio.h>
#include <set>
#include <vector>
using namespace std;

class Solution {
private:
    vector<set<char> > rows;
    vector<set<char> > cols;
    vector<set<char> > subs;

public:
    bool isValidSudoku(vector<vector<char> > &board) {
        int r = board.size();
        int c = board[0].size();
        rows.clear(), cols.clear(), subs.clear();
        rows.resize(r);
        cols.resize(c);
        subs.resize((r*c)/9);

        for(int i=0; i<board.size(); i++) {
            for(int j=0; j<board[0].size(); j++) {
                char x = board[i][j];
                if(x == '.') continue;
                int setid = i/3 * 3 + j/3;
                set<char>& sub = subs[setid];
                if(rows[i].find(x) != rows[i].end()) {
                    return false;
                }
                if(cols[j].find(x) != cols[j].end()) {
                    return false;
                }
                if(sub.find(x) != sub.end()) {
                    return false;
                }
                rows[i].insert(x);
                cols[j].insert(x);
                sub.insert(x);
            }
        }
        return true;
    }
};


int main() {
    vector<vector<char> > board(9);
    string A[9] = {".87654321","2........","3........",
                   "4........","5........","6........",
                   "7........","8........","9........"};
    for(int i=0; i<9; i++) {
        for(int j=0; j<A[i].size(); j++) {
            board[i].push_back(A[i][j]);
        }
    }
    Solution p;
    std::cout << p.isValidSudoku(board) << std::endl;
    return 0;
}
