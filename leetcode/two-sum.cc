#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Elem{
public:
    Elem(int v, int i) : val(v), index(i) { }
    int val;
    int index;
};

struct myless {
    bool operator ()(const Elem& a, const Elem& b) const {
        if( a.val != b.val ) {
            if(a.val < b.val) return true;
            if(a.val > b.val) return false;
        }
        if( a.val == b.val ) {
            if(a.index < b.index) return true;
            if(a.index > b.index) return false;
        }
        return false;
    }
};


class Solution {
public:

    vector<int> twoSum(const vector<int> &numbers, int target) {
        map<int, int> table;
        vector<int> res;
        for(int i=0; i<numbers.size(); i++) {
            table.insert(pair<int,int>(numbers[i], i));
        }
        for(int i=0; i<numbers.size(); i++) {
            for(int j=i+1; j<numbers.size(); j++) {
                if(numbers[i] + numbers[j] == target ) {
                    res.push_back(i+1);
                    res.push_back(j+1);
                    return res;
                }
            }
        }
        return res;
    }
};

int main() {
    vector<int> vec;
    vec.push_back(2);
    vec.push_back(1);
    vec.push_back(9);
    vec.push_back(4);
    vec.push_back(4);
    vec.push_back(56);
    vec.push_back(90);
    vec.push_back(3);

    Solution* p = new Solution();

    vector<int> ans = p->twoSum(vec, 8);
    for(int i=0; i<ans.size(); i++) {
        std::cout<<ans[i]<< std::endl;
    }
    delete p;
}
