#include <time.h>
#include <vector>
#include <set>
#include <iostream>
#include <cassert>
using namespace std;


void getLucky(set<int>& res, int totalNum, int requestNum) {
    assert(totalNum > 0 && totalNum >= requestNum);
    srand(time(NULL));
    while(1) {
        int id = (int)((double)rand()/RAND_MAX * totalNum);
        res.insert(id);
        if(res.size() == requestNum)
            break;
    }
}


int main() {
    int totalNum = 300000;
    int requestNum = 100000;
    set<int> res;
    getLucky(res, totalNum, requestNum);

    std::cout << "ans size: " << res.size() << std::endl;
    assert(res.size() == requestNum);
    for(set<int>::iterator it = res.begin(); it != res.end(); ++it) {
        assert(*it < totalNum);
    }
    return 0;
}
