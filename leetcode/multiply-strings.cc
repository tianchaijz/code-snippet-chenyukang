#include <iostream>
#include <stdio.h>
using namespace std;

char toc(int v) {
    return '0' + v;
}

int toi(char v) {
    return v - '0';
}

class Solution {
public:

    string multiply_one(string num, int x) {
        string ans;
        int carry = 0;
        for(int k=num.size() -1; k >= 0; k--) {
            int v = num[k] - '0';
            v = carry + v * x;
            if(v >= 10) {
                carry = v / 10;
                v = v % 10;
            } else {
                carry = 0;
            }
            ans = toc(v) + ans;
        }
        if(carry) {
            ans = toc(carry) + ans;
        }
        return ans;
    }

    string sum(string num1, string num2) {
        int carry = 0;
        int l1 = num1.size() - 1;
        int l2 = num2.size() - 1;
        string ans;
        while(l1 >= 0 || l2 >= 0 || carry) {
            int n1 = l1 >=0 ? toi(num1[l1]) : 0;
            int n2 = l2 >=0 ? toi(num2[l2]) : 0;
            int v = n1 + n2 + carry;
            if(v >= 10) {
                carry = v / 10;
                v = v % 10;
            } else
                carry = 0;
            ans = toc(v) + ans;
            l1--, l2--;
        }
        return ans;
    }

    string multiply(string num1, string num2) {
        string ans;
        if(num1 == "0" || num2 == "0")
            return "0";
        for(int k = num2.size()-1;  k >= 0; k--) {
            int v = toi(num2[k]);
            if(v == 0) continue;
            string t = multiply_one(num1, v);
            int diff = num2.size() - k - 1;
            t = t + string(diff, '0');
            ans = sum(ans, t);
        }
        return ans;
    }
};


int main() {
    Solution p;
    string ans = p.multiply_one("304", 9);
    std::cout << "ans: " << ans << std::endl;
    ans = p.sum("99", "1001");
    std::cout << "ans: " << ans << std::endl;
    ans = p.multiply("2", "1");
    std::cout << "ans: " << ans << std::endl;
    ans = p.multiply("293992", "103838298");
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
