#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>
using namespace std;


class Solution {
private:
    vector<int> counts;
public:
    void dfs(vector<vector<int> >& ans, vector<int>& candidates, vector<int> buf, int target, int index) {
        if(target == 0) {
            ans.push_back(buf);
            return;
        }
        if(index >= candidates.size())
            return;

        if(candidates[index] > target)
            return;

        //do not choose from index
        dfs(ans, candidates, buf, target, index+1);

        //choose from index
        for(int k=1; k<= counts[index]; k++) {
            if(k * candidates[index] <= target) {
                buf.push_back(candidates[index]);
                dfs(ans, candidates, buf, target - k * candidates[index], index+1);
            }
        }
    }

    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        vector<vector<int> > ans;
        vector<int> now;
        sort(candidates.begin(), candidates.end());
        counts = vector<int>(candidates.size(), 0);
        for(int i=0; i<candidates.size(); i++) {
            counts[i] = target/candidates[i];
        }
        dfs(ans, candidates, now, target, 0);
        return ans;
    }
};

int main() {
    vector<vector<int> > ans;
    vector<int> vec(4, 0);
    int target = 7;
    vec[0] = 2, vec[1] = 3, vec[2] = 6, vec[3] = 7;
    Solution p;
    ans = p.combinationSum(vec, target);
    for(int i=0; i<ans.size(); i++) {
        for(int j=0; j<ans[i].size(); j++) {
            printf("%d ", ans[i][j]);
        }
        printf("\n");
    }
    return 0;
}
