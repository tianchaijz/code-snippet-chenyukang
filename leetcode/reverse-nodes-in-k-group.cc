#include <vector>
#include <iostream>
#include <stdio.h>
#include <cassert>
using namespace std;


/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

void print(ListNode* head);

class Solution {
public:
    ListNode *reverse(ListNode *res, ListNode* h, int k) {
        assert(res && h);
        ListNode* p = h;
        while(k) {
            ListNode* q = p->next;
            p->next = res->next;
            res->next = p;
            p = q;
            k--;
        }
        return h;
    }

    ListNode *reverseKGroup(ListNode *head, int k) {
        ListNode res(0);
        ListNode* p = &res;

        if(k == 1 || head == NULL)
            return head;

        ListNode* n = head;
        ListNode* q = NULL;
        int num = 0;
        while(n != NULL) {
            q = n;
            num = 0;
            while(q) {
                num++;
                q = q->next;
                if(num == k)
                    break;
            }
            if(num == k) {
                p = reverse(p, n, k);
                n = q;
            } else {
                p->next = n;
                break;
            }
        }
        return res.next;
    }
};

ListNode* createList(int num) {
    ListNode res(0);
    ListNode* p = &res;
    for(int i=1; i<=num;  i++) {
        p->next = new ListNode(i);
        p = p->next;
    }
    return res.next;
}

void print(ListNode* head) {
    ListNode* p = head;
    while(p) {
        printf("%d ", p->val);
        p = p->next;
    }
    printf("\n");
}


int main() {
    ListNode* list = createList(11);
    print(list);
    Solution p;
    ListNode* ans = p.reverseKGroup(list, 2);
    print(ans);
    return 0;
}
