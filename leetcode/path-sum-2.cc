#include <iostream>
#include <cassert>
#include <stdio.h>
#include <vector>
using namespace std;


/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    bool node_iter(TreeNode *node, int sum, vector<vector<int> >& res, vector<int> now) {
        if(sum == 0 && node == NULL) return true;
        if(node == NULL) return false;
        //printf("node: %d sum: %d\n", node->val ,sum);
        int left = sum - node->val;
        vector<int> next = now;
        next.push_back(node->val);
        if(left == 0 && node->left == 0 && node->right == 0) {
            res.push_back(next);
            return true;
        }
        if(node->left) {
            node_iter(node->left, left, res, next);
        }
        if(node->right)
            node_iter(node->right, left, res, next);
        return false;
    }

    vector<vector<int> > pathSum(TreeNode *root, int sum) {
        vector<vector<int> > res;
        vector<int> now;
        if(root == NULL) return res;
        node_iter(root, sum, res, now);
        return res;
    }
};



void print(const vector<vector<int> >& res) {
    for(int i=0; i<res.size(); i++) {
        for(int j=0; j<res[i].size(); j++) {
            printf("%d ", res[i][j]);
        }
        printf("\n");
    }
}

/*
5
/ \
4   8
/   / \
11  13  4
/  \      \
7    2      1
*/
void test1() {
    TreeNode* root = new TreeNode(5);
    root->left = new TreeNode(4);
    root->right = new TreeNode(8);
    root->left->left = new TreeNode(11);
    root->right->left = new TreeNode(13);
    root->right->right = new TreeNode(4);
    TreeNode* t = root->left->left;
    t->left = new TreeNode(7);
    t->right = new TreeNode(2);
    root->right->right->left = new TreeNode(5);
    root->right->right->right = new TreeNode(1);

    Solution p;
    vector<vector<int> > res = p.pathSum(root, 22);
    print(res);
}

void test2() {
    TreeNode* root = new TreeNode(1);
    root->left = new TreeNode(2);
    Solution p;
    vector<vector<int> > res = p.pathSum(root, 22);
    print(res);
}

int main() {
    test1();
    test2();
    return 0;
}
