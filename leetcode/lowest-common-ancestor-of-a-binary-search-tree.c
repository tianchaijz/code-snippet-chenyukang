
/* Definition for a binary tree node. */
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

#define NULL 0

struct TreeNode* lowestCommonAncestor(struct TreeNode* root,
                                      struct TreeNode* p,
                                      struct TreeNode* q) {
  if(root == NULL || p == NULL || q == NULL)
    return NULL;
  if((p->val <= root->val && root->val <= q->val) ||
     (p->val >= root->val && root->val >= q->val))
    return root;
  if(p->val < root->val && q->val < root->val) {
    return lowestCommonAncestor(root->left, p, q);
  }
  if(p->val > root->val && q->val > root->val) {
    return lowestCommonAncestor(root->right, p, q);
  }
  return NULL;
}



int main() {
  return 0;
}



