#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

class Solution {
public:
    int jump(int A[], int n) {
        int head = 0;
        int tail = 0;
        int ans = 0;
        if(n <= 1) return 0;
        while(head <= tail) {
            ans++;
            int old_tail = tail;
            for(int i=head; i<=old_tail; i++) {
                int next = A[i] + i;
                if(next >= n - 1)
                    return ans;
                if(next > tail)
                    tail = next;
            }
            head = old_tail + 1;
        }
        return ans;
    }
};


void test(int A[], int n) {
    Solution p;
    int ans = p.jump(A, n);
    std::cout << "ans: " << ans << std::endl;
}

int main() {
    int A[] = {2, 3, 1, 1, 4};
    test(A, 5);

    int B[] = {1, 2};
    test(B, 2);

    int C[] = {1};
    test(C, 1);

    int D[] = {1,2,1,1,1};
    test(D, 5);
    return 0;
}
