#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool isAnagram(char* s, char* t) {
  if(strlen(s) != strlen(t)) return false;
  int set_s[26], set_t[26], i;
  memset(set_s, 0, sizeof(set_s));
  memset(set_t, 0, sizeof(set_t));

  for(i = 0; i<strlen(s); i++)
    set_s[s[i]-'a']++, set_t[t[i]-'a']++;
  for(i = 0; i<26; i++)
    if(set_s[i] != set_t[i]) return false;

  return true;
}

int main() {
  return 0;
}
