#include <iostream>
#include <vector>
#include <stdio.h>
#include <cassert>
using namespace std;

class Solution {
public:

    bool good_start(int start, vector<int>& diff) {
        int now = 0;
        for(int i = start; i < diff.size(); i++) {
            now += diff[i];
            if(now < 0) return false;
        }
        for(int i=0; i<start; i++) {
            now += diff[i];
            if(now < 0) return false;
        }
        return true;
    }

    int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
        vector<int> diff(gas.size(), 0);
        int sumCost = 0;
        int sumDiff = 0;
        for(int i=0; i<gas.size(); i++) {
            diff[i] = gas[i] - cost[i];
            sumCost += cost[i];
        }

        if(sumDiff < 0) return -1;

        for(int i=0; i<gas.size(); i++) {
            if(diff[i] < 0) continue;
            if(gas[i] >= sumCost) //a lucky station
                return i;
            if(good_start(i, diff))
                return i;
        }
        return -1;
    }
};

int main() {
    return 0;
}
