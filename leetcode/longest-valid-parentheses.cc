#include <stack>
#include <vector>
#include <iostream>
using namespace std;


struct Node {
    int pos;
    char val;
    Node(int p, char v) : pos(p), val(v) {}
};

class Solution {
public:
    int longestValidParentheses(string s) {
        stack<Node> st;
        int ans = 0;
        for(int i=0; i<s.size(); i++) {
            if(s[i] == '(') {
                st.push(Node(i, s[i]));
            } else if (s[i] == ')') {
                if(!st.empty()) {
                    Node n = st.top();
                    st.pop();
                    for(int k = n.pos; k<=i; k++) {
                        s[k] = 'x';
                    }
                }
            }
        }
        for(int i=0; i<s.size(); i++) {
            int num = 0;
            while(i < s.size() &&  s[i] == 'x') {
                i++;
                num++;
            }
            if(num > ans)
                ans = num;
        }
        return ans;
    }
};

void test(string str) {
    Solution p;
    int ans = p.longestValidParentheses(str);
    printf("%s -> %d\n", str.c_str(), ans);
}

int main() {
    test("()");
    test("()()");
    test("(())");
    test("(()");
    test(")()())");
    test("()(())");
    test("(()()");
    return 0;
}
