#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

struct Node {
    int head;
    int tail;
    Node(int h, int t) : head(h), tail(t) {}
};

class Solution {
private:
    int strSize;
    int len;
public:

    bool dfs(vector<vector<Node> > &table, int index, int visitNum,
             vector<bool> visited) {
        if(visitNum == table.size())
            return true;

        if(strSize + 1 - index < (table.size() - visitNum) * len)
            return false;
        for(int i=0; i<table.size(); i++) {
            if(visited[i]) continue;
            for(int k=0; k<table[i].size(); k++) {
                if(table[i][k].head == index)  {
                    visited[i] = true;
                    if(dfs(table, table[i][k].tail + 1, visitNum+1, visited))
                        return true;
                    visited[i] = false;
                }
            }
        }
        return false;
    }

    vector<int> findSubstring(string S, vector<string> &L) {
        vector<vector<Node> > table;
        vector<bool> starts(S.size(), false);
        vector<int> ans;

        if(L.size() == 0)
            return ans;

        len = L[0].size();
        strSize = S.size();

        if(S.size() < len * L.size())
            return ans;
        for(int i=0; i<L.size(); i++) {
            vector<Node> v;
            int start = 0;
            size_t pos = 0;
            while(start < S.size() &&
                  (((pos = S.find(L[i], start)) != string::npos)))  {
                v.push_back(Node(pos, pos + L[i].size() - 1));
                starts[pos] = true;
                start = pos + 1;
            }
            if(v.size() != 0)
                table.push_back(v);
        }

        if(table.size() != L.size())
            return ans;

#if 0
        for(int i=0; i<table.size(); i++) {
            for(int k = 0; k<table[i].size(); k++) {
                Node p = table[i][k];
                printf("(%d %d) ", p.head, p.tail);
            }
            printf("\n");
        }
#endif

        int row = L.size();
        int size = starts.size();
        for(int i=0; i<=  size - len * row; i++) {
            if(starts[i]) {
                vector<bool> visited(table.size(), false);
                if(dfs(table, i, 0, visited))
                    ans.push_back(i);
            }
        }
        return ans;
    }
};

int main() {
    string S = "barfoothefoobarman";
    vector<string> vec;
    vec.push_back("foo");
    vec.push_back("bar");
    Solution p;
    vector<int> ans = p.findSubstring(S, vec);
    for(int i=0; i<ans.size(); i++) {
        printf("%d ", ans[i]);
    }
    printf("\n");
    return 0;
}
