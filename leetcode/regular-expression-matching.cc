#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
using namespace std;

/*
isMatch("aa","a") → false
isMatch("aa","aa") → true
isMatch("aaa","aa") → false
isMatch("aa", "a*") → true
isMatch("aa", ".*") → true
isMatch("ab", ".*") → true
isMatch("aab", "c*a*b") → true
*/

class Solution {
public:
    bool isMatch_iter(const char* s, const char* p, char prev) {
        if(*s == '\0') {
            //printf("left: %s\n", p);
            if(*p == '\0')
                return true;
            while(*p != '\0') {
                if(*p == '*')
                    p++;
                else if(*(p+1) == '*')
                    p = p + 2;
                else
                    return false;
            }
            return true;
        }

        if(*p == '.') {
            return isMatch_iter(s+1, p+1, '.');
        } else if(*p == '*') {
            if(prev == *s || prev == '.') {
                return isMatch_iter(s+1, p+1, '\0') ||
                    isMatch_iter(s+1, p, prev);
            } else {
                return isMatch_iter(s, p+1, '\0');
            }
        } else {
            if(*(p+1) == '*') {
                if(*s == *p) return isMatch_iter(s+1, p+1, *p) ||
                                 isMatch_iter(s, p+1, *p);
                return isMatch_iter(s, p+1, *p);
            }
            if(*s == *p)
                return isMatch_iter(s+1, p+1, *p);
            else
                return false;
        }
    }


    bool isMatch(const char *s, const char *p) {
        isMatch_iter(s, p, '\0');
    }
};

void test(const char* s, const char* p) {
    Solution x;
    printf("%s vs %s -> ", s, p);
    if(x.isMatch(s, p)) {
        std::cout << "True" << std::endl;
    } else {
        std::cout << "False" << std::endl;
    }
}

int main() {
    test("a", "ab*b*b*");
    test("a", "ab*");
    test("ab", ".*c");
    test("ab", ".*c*");
    test("aa", "a");
    test("aa", "aa");
    test("aaa", "aa");
    test("aa", "a*");
    test("aa", ".*");
    test("ab", ".*");
    test("abcde", ".*");
    test("aab", "c*a*b*");
    return 0;
}
