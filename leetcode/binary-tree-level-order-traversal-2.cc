#include <iostream>
#include <stdio.h>
#include <stack>
#include <vector>
#include <queue>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


class Solution {
public:
    vector<vector<int> > levelOrderBottom(TreeNode *root) {
        vector<vector<int> > res;
        vector<int> now;
        queue<TreeNode*> Q;
        if(root == NULL) return res;
        Q.push(root);
        Q.push(NULL);
        while(!Q.empty()) {
            TreeNode* node = Q.front();
            Q.pop();
            if(node == NULL) {
                res.push_back(now);
                now.clear();
                if(Q.empty()) break;
                else Q.push(NULL);
            } else {
                now.push_back(node->val);
                if(node->left) Q.push(node->left);
                if(node->right) Q.push(node->right);
            }
        }
        //reverse
        vector<vector<int> > r;
        for(int i=res.size()-1; i>=0; i--)
            r.push_back(res[i]);
        return r;
    }
};

int main() {
    return 0;
}
