#include <iostream>
#include <cassert>
using namespace std;

// "0" => true
// " 0.1 " => true
// "abc" => false
// "1 a" => false
// "2e10" => true

class Solution {
public:

    bool isNumber(const char *s) {
        if(s == NULL) return false;
        char* p;
        strtod(s, &p);
        if( p == s ) return false;
        for(; *p; ++p) {
            if(!isspace(*p)) return false;
        }
        return true;
    }

    bool op(char c) {
        if( c == '.' || c == 'e')
            return true;
        return false;
    }

    bool isSign(char c) {
        if( c == '-' || c == '+') return true;
        return false;
    }

    bool validChar(char c) {
        if(( c >= '0' && c <= '9' ) || op(c) || isSign(c))
            return true;
        return false;
    }

    bool isNumber_wrong(const char *s) {
        if(s == NULL) return false;
        int len = strlen(s);
        int i, k, j;
        for(i=0; i<len; ) {
            if(s[i] == ' ')
                i++;
            else break;
        }
        if(i == len || !validChar(s[i]))
            return false;
        int dot, exp;
        int signNum = 0;
        dot  = exp = 0;
        if(isSign(s[i])) {
            signNum = 1;
            i++;
        }
        for(k = i; k<len; k++) {
            if(s[k] == '.') {
                if(dot) return false;
                dot = 1;
            }
            else if(s[k] == 'e') {
                if(exp) return false;
                exp = 1;
            }
            else if(isSign(s[k])) return false;
            if(validChar(s[k])) continue;
            else {
                for(j=k; j<len; j++)
                    if(s[j] != ' ')
                        return false;
                break;
            }
        }
        //printf("i: %d k: %d\n", i, k);
        for(j=i; j<k; j++) {
            if(s[j] == '.' ) {
                if(j == i && j == k-1)
                    return false;
            }
            if(s[j] == 'e') {
                if( j == i || j == k-1)
                    return false;
            }
        }
        return true;
    }
};


int main() {
    Solution p;
    assert(!p.isNumber(".e1"));
    assert(p.isNumber("46.e3"));
    assert(!p.isNumber("6+1"));
    assert(p.isNumber("-1."));
    assert(p.isNumber("+1."));
    assert(!p.isNumber("e9"));
    assert(!p.isNumber(".."));
    assert(!p.isNumber(" "));
    assert(p.isNumber("0"));
    assert(p.isNumber(" 0.1"));
    assert(!p.isNumber("abc"));
    assert(!p.isNumber("1 a"));
    assert(p.isNumber("2e10"));
    assert(!p.isNumber("e"));
    assert(!p.isNumber("1.1 0"));
    assert(p.isNumber(".1"));
    assert(p.isNumber("10.1"));
    assert(!p.isNumber("."));
    assert(p.isNumber("3."));
    return 0;
}
