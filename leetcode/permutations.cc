#include <iostream>
#include <cassert>
#include <stdio.h>
#include <vector>
using namespace std;

class Solution {
private:
    void permute(vector<vector<int> >& res, vector<int>& num,
                 vector<bool>& used, vector<int> now) {
        if(now.size() == num.size()) {
            res.push_back(now);
            return;
        }
        int k;
        for(k=0; k<used.size(); k++) {
            if(!used[k]) {
                used[k] = true;
                vector<int> next = now;
                next.push_back(num[k]);
                permute(res, num, used, next);
                used[k] = false;
            }
        }
    }
public:
    vector<vector<int> > permute(vector<int> &num) {
        vector<vector<int> > res;
        vector<bool> used(num.size(), false);
        vector<int> now;
        permute(res, num, used, now);
        return res;
    }
};

int main() {
    Solution p;
    vector<int> x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    vector<vector<int> > res = p.permute(x);
    for(int i=0; i<res.size(); i++) {
        for(int j=0; j<res[i].size(); j++) {
            printf(" %d ", res[i][j]);
        }
        printf("\n");
    }
    return 0;
}
