#include <vector>
#include <iostream>
using namespace std;

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};


void print(ListNode* node) {
    ListNode* t = node;
    while(t) {
        printf("%d ", t->val);
        t = t->next;
    }
    printf("\n");
}


class Solution {
public:
    ListNode *insert(ListNode* p, ListNode* n) {
        ListNode* t = p;
        ListNode* prev = NULL;
        while(t && t->val < n->val) {
            prev = t;
            t = t->next;
        }
        if(prev == NULL) { //first
            //printf("now: %d p:%d\n", n->val, p->val);
            n->next = p;
            return n;
        } else {
            prev->next = n;
            n->next = t;
        }
        return p;
    }

    ListNode *insertionSortList(ListNode *head) {
        if(head == NULL) return NULL;
        ListNode* h = head;
        ListNode* n = head->next;
        h->next = NULL;
        while(n) {
            ListNode* p = n->next;
            h = insert(h, n);
            n = p;
        }
        return h;
    }
};


int main() {
    ListNode* t = new ListNode(3);
    t->next = new ListNode(4);
    t->next->next = new ListNode(1);
    Solution p;
    ListNode* res = p.insertionSortList(t);
    print(res);
    return 0;
}
