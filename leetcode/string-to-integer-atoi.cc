#include <stdio.h>
#include <iostream>
using namespace std;


class Solution {
public:
    int atoi(const char *str) {
        int negative = 0;
        double value = 0;
        while(*str != 0 && (!(*str >= '0' && *str <= '9'))) {
            if(*str == '-' || *str == '+' ) {
                negative = (*str == '-') ? 1 : 0;
                str++;
                break;
            } else if(*str != ' ')
                break;
            str++;
        }
        while(*str >= '0' && *str <= '9') {
            value = value * 10 + (*str - '0');
            str++;
        }

        if(negative) value = -value;
        if(value >= INT_MAX) return INT_MAX;
        if(value <= INT_MIN) return INT_MIN;
        return (int)value;
    }
};


int main() {
    Solution p;
    std::cout << p.atoi("123") << std::endl;
    std::cout << p.atoi("-123") << std::endl;
    std::cout << p.atoi(" -39392") << std::endl;
    std::cout << p.atoi(" - 39392") << std::endl;
    std::cout << p.atoi(" +10") << std::endl;
    std::cout << p.atoi(" + 10") << std::endl;
    return 0;
}
