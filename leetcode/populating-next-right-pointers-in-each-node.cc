#include <iostream>
#include <cassert>
using namespace std;

struct TreeLinkNode {
    TreeLinkNode *left;
    TreeLinkNode *right;
    TreeLinkNode *next;
};



class Solution {
public:
    void connect_iter(TreeLinkNode *root) {
        if(root == NULL || root->right == NULL)
            return;
        root->left->next = root->right;
        connect_iter(root->left);
        connect_iter(root->right);
    }

    void second_pass(TreeLinkNode* root) {
        if(root == NULL || root->right == NULL)
            return;
        if(root->right->next == NULL) {
            if(root->next) {
                TreeLinkNode* right_next = root->next->left;
                root->right->next = right_next;
            }
        }
        second_pass(root->left);
        second_pass(root->right);
    }

    void connect(TreeLinkNode* root) {
        connect_iter(root);
        second_pass(root);
    }
};


int main() {
    return 0;
}
