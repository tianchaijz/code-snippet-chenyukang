#include <iostream>
#include <string>
#include <cassert>
using namespace std;


class Solution {
public:
    bool match_rec(const char *s, const char* p) {
        //printf("s: %s p: %s last: %c\n", s, p, last);
        if(*s == 0 && *p == 0)
            return true;
        if(*s == '\0') {
            while(*p != '\0') {
                if(*p != '*') return false;
                p++;
            }
            return true;
        }
        if(*s == *p) {
            return match_rec(s+1, p+1);
        } else if(*p == '?') {
            return match_rec(s+1, p+1);
        } else if(*p == '*') {
            if(match_rec(s, p+1))
                return true;
            const char* n = s;
            while(*n == *s) {
                n++;
            }
            n--;
            while(n >= s) {
                if(match_rec(n+1, p+1))
                    return true;
                n--;
            }
        }
        return false;
    }

    bool isMatch_v2(const char *s, const char *p) {
        if(s == NULL && p == NULL)
            return true;
        if(p == NULL)
            return false;
        int slen = strlen(s);
        int plen = 0;
        for(int k=0; k<strlen(p); k++) {
            if(p[k] != '*')
                plen++;
        }
        if(plen > slen) return false;
        return match_rec(s, p);
    }

    bool isMatch(const char *s, const char *p) {
        const char* star=NULL;
        const char* ss=s;
        while (*s){
            if ((*p=='?')||(*p==*s)) {s++; p++; continue;}
            if (*p=='*') { star=p++; ss=s;continue;}
            if (star) { p = star+1; s=++ss;continue;}
            return false;
        }
        while (*p=='*'){p++;}
        return !*p;
    }
};

bool test(const char* text, const char* pattern) {
    Solution p;
    return p.isMatch(text, pattern);
}

int main() {
    assert(test("aaa", "*") == true);
    assert(test("a", "a") == true);
    assert(test("a", "a*") == true);
    assert(test("a", "ab*") == false);
    assert(test("a", "*a*") == true);
    assert(test("a", "*b*") == false);
    assert(test("a", "*?*") == true);
    assert(test("a", "*x*") == false);
    assert(test("aab", "c*a*b") == false);
    assert(test("aaa", "aa") == false);
    assert(test("aaa", "aaa") == true);
    assert(test("aac", "*") == true);
    assert(test("ab", "?*") == true);
    assert(test("babbaaaaabbbbbaaaabaaaaaabbbbbaabaabaaaabbbbbbbbaaaabaabababaaaaaabaabbababaaabaababaaaababbbbbbbbaaababbbbbbbbbabbabbaaaaabbbabaabbbbabaaabbaabbbabbabbabaababbbaaabbbbbaaaaabbababaaaabaaaababbaaabbba",
                "**********bbb*a**a*b**b*a*aba*aa***ba**a*ab**b*****aba*b***a**a*b******a***bb***a**b*******bb*****aaa*a"));
    return 0;
}
