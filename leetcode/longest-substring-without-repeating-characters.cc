#include <iostream>
#include <stdio.h>
#include <vector>
#include <set>
using namespace std;

#define max(a, b) ((a) > (b) ? (a) : (b))

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int ans = 0;
        for(int i=0; i<s.size(); i++) {
            set<char> have;
            have.insert(s[i]);
            int k=i+1;
            while(k < s.size()) {
                if(have.find(s[k]) != have.end()) {
                    break;
                }
                have.insert(s[k]);
                k++;
            }
            ans = max(ans, have.size());
        }

        return ans;
    }
};


int main() {
    Solution p;
    int ans = p.lengthOfLongestSubstring("bbbbbbbb");
    std::cout << "ans: " << ans << std::endl;

    ans = p.lengthOfLongestSubstring("abcabcbb");
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
