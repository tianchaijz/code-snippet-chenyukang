#include <vector>
#include <set>
#include <iostream>
using namespace std;

class Solution {
public:
    void setZeroes(vector<vector<int> > &matrix) {
        set<int> rows;
        set<int> cols;
        for(int i=0; i<matrix.size(); i++) {
            for(int j=0; j<matrix[0].size(); j++) {
                if(matrix[i][j] == 0) {
                    rows.insert(i);
                    cols.insert(j);
                }
            }
        }
        for(set<int>::iterator it = rows.begin(); it != rows.end(); ++it) {
            for(int k=0; k<matrix[0].size(); k++)
                matrix[*it][k] = 0;
        }
        for(set<int>::iterator it = cols.begin(); it != cols.end(); ++it) {
            for(int k=0; k<matrix.size(); k++)
                matrix[k][*it] = 0;
        }
    }
};

int main() {
    return 0;
}
