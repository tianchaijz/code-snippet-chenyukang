#include <stdio.h>
#include <iostream>
#include <queue>
#include <assert.h>
#include <vector>
#include <stack>
#include <string>
using namespace std;

class Solution {
private:
    string reverse(string a) {
        return "";
    }
    int val(char c) {
        if(c == '1') return 1;
        return 0;
    }

    char tochar(int v) {
        if(v == 1) return '1';
        return '0';
    }

public:
    string addBinary(string a, string b) {
        int len1 = a.size();
        int len2 = b.size();
        string res;
        int i, j, carry;
        if(len1 > len2)
            res = a;
        else
            res = b;
        carry = 0;
        for(i = len1-1, j = len2-1; i>=0 || j>=0; i--, j--) {
            int av = (i>=0 ? (val(a[i])) : 0);
            int bv = (j>=0 ? (val(b[j])) : 0);
            int v = av + bv + carry;
            if( v >= 2) {
                v %= 2;
                carry = 1;
            } else carry = 0;
            int idx = max(i, j);
            res[idx] = tochar(v);
        }
        if(carry)
            res = "1" + res;
        return res;
    }
};

int main() {
    Solution p;
    string res = p.addBinary("1010", "1011");
    std::cout << res << std::endl;
    return 0;
}
