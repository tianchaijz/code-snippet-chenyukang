#include <stdio.h>
#include <string.h>


void moveZeroes(int* nums, int numsSize) {
  int i, k = 0;
  for(i = 0; i<numsSize; i++) {
    if(nums[i] != 0) {
      nums[k++] = nums[i];
    }
  }
  while(k < numsSize) {
    nums[k++] = 0;
  }
}

int main() {
  int nums[5] = {0, 1, 0, 3, 12};
  int i;
  for(i=0; i<5; i++) {
    printf("%d ", nums[i]);
  }
  printf("\n");
  moveZeroes(nums, 5);
  for(i=0; i<5; i++) {
    printf("%d ", nums[i]);
  }
  printf("\n");
  return 0;
}

