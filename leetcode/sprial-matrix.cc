#include <stdio.h>
#include <vector>
using namespace std;

class Solution {
public:
    void iter(vector<vector<int> >& matrix, vector<int>& res, int row, int col) {
        for(int j=col; j<matrix[0].size() - col; j++) {
            res.push_back(matrix[row][j]);
        }
        for(int i=row+1;  i<matrix.size() - row; i++) {
            res.push_back(matrix[i][matrix[0].size() - col - 1]);
        }
        if(matrix.size() - row - 1 > row) {
            for(int j=matrix[0].size() - col - 2; j>=col; j--) {
                res.push_back(matrix[matrix.size() - row - 1][j]);
            }
        }
        if(col < matrix[0].size() - col - 1) {
            for(int i=matrix.size() - row - 2; i>row; i--) {
                res.push_back(matrix[i][col]);
            }
        }
    }

    vector<int> spiralOrder(vector<vector<int> > &matrix) {
        vector<int> res;
        int row_index = 0;
        int col_index = 0;

        int i, j;
        bool col, row;
        i = j = 0;
        if(matrix.size() == 0) return res;
        do {
            iter(matrix, res, i, j);
            row = col = false;
            if((i+1) * 2 < matrix.size()){
                i++;
                row = true;
            }
            if((j+1) * 2 < matrix[0].size()) {
                j++;
                col = true;
            }
        }while(row && col);

        return res;
    }
};


int test(int r, int c) {
    vector<vector<int> > matrix;
    int row, col;
    row = r;
    col = c;
    matrix.resize(row);
    for(int i=0; i<row; i++)
        matrix[i].resize(col);

    int step = 1;
    for(int i=0; i<row; i++) {
        for(int j=0; j<col; j++) {
            matrix[i][j] = step++;
        }
    }

    Solution p;
    vector<int> ans = p.spiralOrder(matrix);
    for(int i=0; i<ans.size(); i++) {
        printf("%d ", ans[i]);
    }
    printf("\n");
    return 0;
}

int main() {
    test(1, 1);
    test(1, 2);
    test(2, 1);
    test(2, 2);
    test(2, 10);
    test(5, 5);
    test(3, 3);
    return 0;
}
