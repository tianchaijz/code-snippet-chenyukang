#include <iostream>
#include <cassert>
using namespace std;

class Solution {
public:
    void sortColors(int A[], int n) {
    	int red, white;
    	red = white = 0;
        for(int i=0; i<n; i++)  {
        	if(A[i] == 0) red++;
        	if(A[i] == 1) white++;
        }
        for(int i=0; i<red; i++)
        	A[i] = 0;
        for(int i=red; i<red+white; i++)
        	A[i] = 1;
        for(int i=red+white; i<n; i++)
        	A[i] = 2;
    }
};

int main() {
    return 0;
}
