#include <vector>
#include <stdio.h>
#include <iostream>
using namespace std;

class Solution {
private:
    int getNum(string str, char key) {
        int num = 0;
        for(int i=0; i<str.size(); i++) {
            if(str[i] == key)
                num++;
        }
        return num;
    }

    void getValid(vector<string>& res, string state, int leftNum, int rightNum) {
        if(leftNum == 0 && rightNum == 0) {
            res.push_back(state);
            return;
        }
        int pastLeft = getNum(state, '(');
        int pastRight = getNum(state, ')');
        if(leftNum >= 1) {
            getValid(res, state + "(", leftNum-1, rightNum);
        }
        if(rightNum >=1 && pastLeft >= pastRight + 1)
            getValid(res, state + ")", leftNum, rightNum-1);
    }

public:
    vector<string> generateParenthesis(int n) {
        vector<string> res;
        getValid(res, "", n, n);
        return res;
    }
};

int main() {
    Solution p;
    vector<string> res = p.generateParenthesis(1);
    for(int i=0; i<res.size(); i++) {
        std::cout << res[i] << std::endl;
    }
    return 0;
}
