#include <stdio.h>
#include <stdlib.h>

int hammingWeight(uint32_t n) {
  int res = 0;
  while(n > 0) {
    if(n & 1)
      res++;
    n = n >> 1;
  }
  return res;
}

int main() {
  printf("res: %d", hammingWeight(11));
  return 0;
}
