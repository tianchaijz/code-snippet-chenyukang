#include <iostream>
using namespace std;

class Empty{
public:
    virtual void isEmpty() {};
    virtual void isEmpty2() {};

private:
    void test() {}
};

int main() {
    std::cout << "size: " << sizeof(Empty) << std::endl;
    Empty a;
    std::cout << "size: " << sizeof(a) << std::endl;
    return 0;
}
