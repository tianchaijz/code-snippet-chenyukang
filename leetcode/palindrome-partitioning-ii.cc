#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <climits>
#include <cassert>
using  namespace std;

class Solution {
    vector<int> ans;
    vector<vector<int> > dp;
    string str;

public:

    //for time performance cache it
    bool isPalindrome(int s ,int e) {
        int i = s;
        int j = e;
        if(dp[i][j] != -1) return dp[i][j] == 1;
        while(i < j) {
            if(str[i] != str[j]) {
                dp[s][e] = 0;
                return false;
            }
            i++, j--;
        }
        dp[s][e] = 1;
        return true;
    }

    int iter(int start) {
        if(isPalindrome(start, str.size() - 1)) {
            ans[start] = 0;
            return 0;
        }
        int needStep = INT_MAX;
        int leftStep;
        for(int i=str.size() - 2; i>=start; i--) {
            int end = i;
            int begin = start;
            if(isPalindrome(begin, end)) {
                if(ans[end+1] != INT_MAX)
                    leftStep = ans[end+1];
                else
                    leftStep = iter(end+1);
                if(1 + leftStep < needStep)
                    needStep = 1 + leftStep;
                if(needStep == 1 || needStep > ans[start]) {
                    break;
                }
            }
        }
        ans[start] = needStep;
        return ans[start];
    }

    int minCut(string s) {
        str = s;
        ans = vector<int>(s.size()+1, INT_MAX);
        dp.clear();
        dp.resize(s.size());
        for(int i=0; i<s.size(); i++)
            dp[i] = vector<int>(s.size(), -1);
        int start = 0;
        iter(start);
        return ans[0];
    }
};


void test0() {
    Solution p;
    string s = "cbbbcc";
    std::cout << "ans1: " << p.minCut(s) << std::endl;

    s = "dde";
    std::cout << "ans1: " << p.minCut(s) << std::endl;
}


void test1() {
    Solution p;
    string s = "fifgbeajcacehiicccfecbfhhgfiiecdcjjffbghdidbhbdbfbfjccgbbdcjheccfbhafehieabbdfeigbiaggchaeghaijfbjhi";
    std::cout << "ans1: " << p.minCut(s) << std::endl;
}

void test2() {
    Solution p;
    string s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    std::cout << "ans1: " << p.minCut(s) << std::endl;
}

int main() {
    test0();
    test1();
    test2();
    return 0;
}
