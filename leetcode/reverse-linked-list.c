

//Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode *next;
};

#define NULL 0
struct ListNode* reverseList(struct ListNode* head) {
  if(head == NULL || head->next == NULL)
    return head;
  struct ListNode* next = head->next;
  struct ListNode* cur = head;
  head->next = NULL;
  while(next) {
    struct ListNode* t = next->next;
    next->next = cur;
    cur = next;
    next = t;
  }
  return cur;
}


int main() {
  return 0;
}
