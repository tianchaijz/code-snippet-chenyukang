

def max_product(words)
  table = {}
  words.each{ |word|
    v = 0
    word.each_byte{ |c|
      v = v | (1 << (c - 97).to_i)
    }
    table[word] = v
  }

  ans = 0
  (0 .. words.size - 1).each{ |i|
    (i+1 .. words.size - 1).each { |j|
      next if table[words[i]] & table[words[j]] > 0
      product = words[i].size * words[j].size
      ans = product if product > ans
    }
  }
  ans
end

# @param {String[]} words
# @return {Integer}
def max_product(words)
  table = {}
  words.each{ |word|
    table[word] = word.bytes.map{ |x| 1 << (x - 97) }.reduce(&:|).to_i
  }
  ans = -1
  words = words.sort{|a, b| a.size <=> b.size}.reverse
  (0 .. words.size - 1).each{ |i|
    (i+1 .. words.size - 1).each { |j|
      next if table[words[i]] & table[words[j]] > 0
      product = words[i].size * words[j].size
      ans = product if product > ans
      ## since we have sorted words, the first will be the final answer
      break
    }
  }
  ans < 0 ? 0 : ans
end


puts max_product(["abcw", "baz", "foo", "bar", "xtfn", "abcdef"])
puts max_product(["a", "ab", "abc", "d", "cd", "bcd", "abcd"])
puts max_product(["a", "aa", "aaa", "aaaa"])
puts max_product(["beae","dff","caaceddbdeedfbed","feacbcecbbffdcaa","baafcceffecbbbbdad","dcbfa","bafcbddeadfde","ddfecabecc","aacffacaecbfadacfbf","bdeafcfdaabeebbbdfcb","fcbdaafebeccf","fdbdbafddbffbcbfeb","ba","ffcfd","fdfbfebdcbaebe","ddfabbcadd","aecbdaeddddc","fd","facddeedceceaf","bffb","cfaafebfdadeaaadfcbf","beedbacfabeebcdcabaa","ceebbefebeba","eeb","cbdccadcee","dbcbb","fbdfdca","fdbfde","ebafbdcc","deffcbbacedaeadcfbddd","bfcbebc","fddfecfeab","bbfcefacadf","ceeeafbecca","ecdeeb","cdfcbac","cebde","bcdcfabceefefcde","cdceffabdacaafffe","fccacabceccbfabdeeb","dddbd","fafbddfaeaaabdafef","fccbccfcdddfbeecbe","fbeccbbecbeffdcdcebcb","aeeefbbca","beebeedbccace","eeac","cccaedbaddf","dfabfb","fcdcaecbbbdcaffdafb","febdefbfdbaacfadce","bfb","eeaaadccd","eecbcb","dafebddfeeefaebfdb","fedcd","eaadefefdeddead","cabfcecdfd","eccdcce","fd","edbbbccafd","bdddebacfecdaabde","ffefbcaabacaadadafde","bf","bfcedcdcacbcddfcde","ebfbeaad","bdbdcadcfdeaccdabdbbb","adbdfcaabebefdefedf","badcfdabcfdbeeaceff","ee","ddeb","bbeabececbccbedcd","ddaecacbcdfdcfaec","caccaea","deaeaeccdcfedaebedbeb","feeeeeeeefcfdadbacac","eafcceadafdfbbabeccae","fdacbbe","daefcdbcbeedc","fdcedcbbdbdffaa","bcdddee","cbefcfcfeaa","dbbffcbfbfeeedaebaad","deaefdcabcdabcdccf","cbeaddfaaabfebcfcce","edadadaafaccdb","fdffea","ebff","fb","effdcbbad","ddfdaafbecdb","eafeaffbaebbddc","eadddaddabceaeaabcb","decafefebdbdf","dccceedaedbccdf","ddded","cafedeadbaebdcff","cd","eaccbafddcdba","acabcfdaad","deccfdfcddd","bfcbcfcadaddba","ddbeeeffdcc","ebcec","bfdebaabfffabbfffd","dfbdddcaf","adbdfbbb","eefcdfbcffabfeacb","cfacabddfecca","bfdbadeeffddeacc","eedfacbfdddcedeaede","fdffdfebc","df","efbedefe","facfb","ded","effcaaaabbfae","df","dcfcedbaaeefdca","dacedefacbccdeabfbbf","cddbcbccebbcfbfebcba","bacbefb","fbcccbfbfcfdfaeeca","ad","bbdcbeecafd","bcdfdffceaaffbbdf","cfbfcecacedacfff","abafafeceee","cebc","faebddbed","efdcceeafaeacfcdbedea","deccaaebda","ecc","aacbafcbdcf","fc","eb","ecf","ddbacacafdf","eedc","efaadddaaee","aeacabbddfbeccdedad","dadebac","afeedbcae","fdecfacbdcacbeecddaf","fadcedcefebbaebeaad","baffaaebfa","adcacdacccadccaacfdb","bdcdbeffaab","cbcececbbdedcacdbffeb","eceafaaef","bffb","bfcbaedcefcfbafebdb","cdfccafdddfadbd","fabdbfcbfbf","fbeffafabefccdcfbad","eddccebbddefc","ebcedccaef","bfdbaeeeaaedbabfcf","ccfadecbdedcebdcdc","fabcdeed","aea","defbbbfbaaebdecdbfb","efa","cfdadfcefdcc","dbbafcadf","fcefdddcbffecdbdfe","eebdbae","ddaeaddfaddbfffedcf","efdbdcccdda","adfecc","ccaaeadbfeddecad","ddeeefceacdacaebda","fefaadbeccffbacfaeef","ddaddadcdd","dceabafcabdfe","ffbacfedbaffcfae","befbf","ddeecacbdbabae","ccdabfadaeeabcaafcfd","aeeeb","efdbffeabebdbabfcfa","fbbed","aafaceacbc","fbcdaafca","abaceacabfefccbbdfd","bcddcfeed","abcafffedbefeecabcadd","bcfffedacdebe","bafac","abeaebfccceeadbcbefb","fdabfa","ceffcfabebbcaeefe","bcdada","edbebbb","edfbfbccbfaffa","dadcbeaceaaebbbcb","eaa","bfa","cdaffddfcceaeadddaf","edededaccffcacebaaafd","eeefedccb","babbfaffce"])
