/* Reference for: An Efficient Representation for Sparse Sets */
#include <iostream>
#include <set>
#include <vector>
#include <assert.h>
using namespace std;

class SparseSet {
private:
    int         maxId;
    int         Num;
    int*        dense;
    int*        sparse;

public:
    SparseSet(int maxid) : maxId(maxid), Num(0) {
        dense = (int*)malloc(sizeof(int) * (maxid+ 1));
        sparse  = (int*)malloc(sizeof(int) * (maxid + 1));
    }

    ~SparseSet() {
        free(dense);
        free(sparse);
    }

    void clear() {
        Num = 0;
    }

    void AddMember(int id) {
        dense[Num] = id;
        sparse[id] = Num;
        Num++;
    }

    bool isMember(int id) {
        return sparse[id] < Num && dense[sparse[id]] == id;
    }
};

int main() {
    int Num =  1000000;
    int time = 10000000;
    int mem[Num];
    printf("sizeof(mem) : %lu\n", sizeof(mem));
    SparseSet s(Num);
    for(int i=0; i<time; i++) {
        //s.AddMember(i);
        //assert(s.isMember(i));
        //s.clear();
        mem[i] = i;
        int v = rand() % 1000;
        //printf("value: %d\n", v);
        memset(mem, v, sizeof(mem));
    }
#if 0
    for(int i=101; i<200; i++) {
        assert(!s.isMember(i));
    }
    s.clear();
    for(int i=0; i<100; i++) {
        assert(!s.isMember(i));
    }
#endif
    return 0;
}
