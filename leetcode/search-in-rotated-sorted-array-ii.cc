#include <iostream>
#include <vector>
#include <cassert>
#include <stdio.h>
using namespace std;


class Solution {
public:
    bool search(int A[], int n, int target) {
        int l = 0;
        int r = n - 1;
        while(l < r) {
            int mid = l + ((r-l)>>1);
            if(A[mid] == target) {
                return true;
            }
            if(A[mid] >= A[l]) {
                if(target >= A[l] && target <= A[mid])
                    r = mid;
                else
                    l++;
            } else {
                if(target >= A[mid] && target <= A[r])
                    l = mid;
                else
                    r--;
            }
        }
        if(A[l] == target || A[r] == target)
            return true;
        return false;
    }
};

int main() {
    return 0;
}
