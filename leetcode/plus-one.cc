#include <iostream>
#include <assert.h>
#include <vector>
using namespace std;


class Solution {
public:
	vector<int> plusOne(vector<int>& digits) {
        vector<int> res(digits.size(), 0);
        if(digits.size() == 0) return res;
        int carry = 1;
        int sum = 0;
        for(int i=digits.size()-1; i>=0; i--) {
            sum = digits[i] + carry;
            if(sum >= 10) {
                carry = 1;
                sum = sum % 10;
            } else {
                carry = 0;
            }
            res[i] = sum;
        }
        if(carry) {
            res.insert(res.begin(), carry);
        }
        return res;
	}
};

void print(const vector<int>& vec) {
    for(int i=0; i<vec.size(); i++) {
        std::cout<<vec[i];
    }
    std::cout << std::endl;
}

void test1() {
    Solution p;
    vector<int> x;
    x.push_back(1);
    x.push_back(9);
    vector<int> res = p.plusOne(x);
    print(res);
}

void test2() {
    Solution p;
    vector<int> x;
    x.push_back(1);
    x.push_back(9);
    x.push_back(9);
    vector<int> res = p.plusOne(x);
    print(res);
}

int main() {
    test1();
    test2();
    return 0;
}
