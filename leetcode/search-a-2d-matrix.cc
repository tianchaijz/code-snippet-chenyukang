#include <vector>
#include <stdio.h>
#include <iostream>
using namespace std;

class Solution {
public:
    int searchRow(vector<vector<int> > &matrix, int target) {
        int col = matrix[0].size() - 1;
        for(int i=0; i<matrix.size(); i++) {
            int first = matrix[i][0];
            int last  = matrix[i][col];
            if(first <= target && target <= last) {
                return i;
            }
        }
        return -1;
    }

    int searchCol(vector<int>& row, int target) {
        int l = 0;
        int r = row.size() -1;
        while(l <= r) {
            int mid = (l + r) / 2;
            if(row[l] == target) return l;
            if(row[r] == target) return r;
            if(row[mid] == target) return mid;
            else if(row[mid] > target) r = mid - 1;
            else l = mid + 1;
        }
        return -1;
    }

    bool searchMatrix(vector<vector<int> > &matrix, int target) {
        int row = searchRow(matrix, target);
        //std::cout << "row: " << row << std::endl;
        if(row == -1) return false;
        int col = searchCol(matrix[row], target);
        if(col == -1) return false;
        return true;
    }
};


int main() {
    //[1,   3,  5,  7],
    //[10, 11, 16, 20],
    //[23, 30, 34, 50]
    vector<vector<int> > matrix(3);
    for(int i=0; i<matrix.size(); i++) {
        matrix[i].resize(4);
    }
    matrix[0][0] = 1;
    matrix[0][1] = 3;
    matrix[0][2] = 5;
    matrix[0][3] = 7;

    matrix[1][0] = 10;
    matrix[1][1] = 11;
    matrix[1][2] = 16;
    matrix[1][3] = 20;

    matrix[2][0] = 23;
    matrix[2][1] = 30;
    matrix[2][2] = 34;
    matrix[2][3] = 50;

    Solution p;
    bool res = p.searchMatrix(matrix, 3);
    std::cout << res << std::endl;

    res = p.searchMatrix(matrix, 100);
    std::cout << res << std::endl;

    res = p.searchMatrix(matrix, 16);
    std::cout << res << std::endl;
    return 0;
}
