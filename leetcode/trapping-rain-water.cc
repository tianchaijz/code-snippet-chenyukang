#include <iostream>
#include <stack>
#include <vector>
#include <cassert>
#include <stdio.h>
using namespace std;

class Solution {
public:
    int trap(int A[], int n) {
        stack<int> st;
        int i, k;
        int ans = 0;
        if(n == 0) return 0;

        //find the first decreasing point
        for(i=0; i<n-1; i++) {
            if(A[i] > A[i+1])
                break;
        }
        //if no such one return now
        if(i == n-1) return 0;

        st.push(i);
        for(k=i+1; k<n; k++) {
            if(A[k] <= A[st.top()]) {
                st.push(k);
            } else {
                int r = A[k];
                while(!st.empty() && r > A[st.top()]) {
                    int n = st.top();
                    st.pop();
                    int h = A[n];
                    if(!st.empty()) {
                        int l = A[st.top()];
                        int add = (min(l, r) - h) * (k - st.top() - 1);
                        ans += add;
                    } else
                        break;
                }
                st.push(k);
            }
        }
        return ans;
    }
};


int main() {
    int A[] = {0,1,0,2,1,0,1,3,2,1,2,1};
    Solution p;
    int ans = p.trap(A, sizeof(A)/sizeof(int));
    printf("ans: %d\n", ans);
    return 0;
}
