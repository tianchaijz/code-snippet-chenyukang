#include <iostream>
#include <stdio.h>
#include <cassert>
using namespace std;

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

#define max(a, b) ((a) >= (b) ? (a) : (b))

class Solution {
public:
    int maxDepth_iter(TreeNode* node) {
        if(node == NULL) return 0;
        int left = maxDepth_iter(node->left);
        int right = maxDepth_iter(node->right);
        return max(left, right) + 1;
    }

    int maxDepth(TreeNode *root) {
        return maxDepth_iter(root);
    }
};


int main() {
    return 0;
}
