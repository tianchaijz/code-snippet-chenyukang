#include <string>
#include <iostream>
#include <vector>
#include <unordered_set>
using namespace std;

class Solution {
public:
    void generate(vector<string>& res, vector<string> now,
                  string str, unordered_set<string>& dict) {
        if(str.size() <= 0) {
            string s = "";
            for(int i=0; i<now.size(); i++) {
                if(i != 0 && i != now.size())
                    s = s + " ";
                s = s + now[i];
            }
            res.push_back(s);
        }
        for(unordered_set<string>::iterator it = dict.begin(); it != dict.end(); ++it) {
            if(str.find(*it) == 0) {
                vector<string> buf = now;
                buf.push_back(*it);
                string next = str.substr(it->size(), str.size() - it->size());
                generate(res, buf, next, dict);
            }
        }
    }

    vector<string> wordBreak(string s, unordered_set<string> &dict) {
        vector<string> res;
        string str = s;
        vector<string> now;
        generate(res, now, str, dict);
        return res;
    }
};

int main() {
    string s = "catsanddog";
    unordered_set<string> dict;
    dict.insert("cat");
    dict.insert("cats");
    dict.insert("and");
    dict.insert("sand");
    dict.insert("dog");
    Solution p;
    vector<string> res = p.wordBreak(s, dict);
    for(int i=0; i<res.size(); i++) {
        std::cout << res[i] << std::endl;
    }
    return 0;
}
