#include <iostream>
#include <vector>
#include <climits>
using namespace std;


class Solution {
public:
    int maxSubArray(int A[], int n) {
        int ans = INT_MIN;
        int cur = 0;
        for(int i=0; i<n; i++) {
            if(cur > 0) cur += A[i];
            else cur = A[i];
            ans = cur > ans? cur : ans;
        }
        return ans;
    }
};


int main() {
    Solution p;
    int A[] = {-2,1,-3,4,-1,2,1,-5,4};
    int n = sizeof(A)/sizeof(int);
    int res = p.maxSubArray(A, n);
    std::cout << "res: " << res << std::endl;
    int B[] = {-1};
    res = p.maxSubArray(B, 1);
    std::cout << "res: " << res << std::endl;

    int C[] = {-1, -2, 0};
    res = p.maxSubArray(C, 3);
    std::cout << "res: " << res << std::endl;
    return 0;
}
