#include <vector>
#include <iostream>
using namespace std;

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

#define max(a, b) ((a) > (b) ? (a) : (b))
#define abs(a) ((a) > 0 ? (a) : (-(a)))

class Solution {

    int isBalanced_iter(TreeNode* node) {
        if(node == NULL) return 0;
        int left = isBalanced_iter(node->left);
        int right = isBalanced_iter(node->right);
        if(left == -1 || right == -1) return -1;
        if(abs(left - right) > 1) return -1;
        int m = max(left, right);
        return m + 1;
    }

public:
    bool isBalanced(TreeNode *root) {
        return isBalanced_iter(root) >= 0;
    }
};

int main() {
    return 0;
}
