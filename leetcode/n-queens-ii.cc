#include <vector>
#include <set>
#include <iostream>
#include <assert.h>
using namespace std;


class Solution {
public:
    bool valid(const vector<string>& map, int row, int col) {
        for(int k = 1; row - k >=0 && col - k >=0; k++) {
            if(map[row-k][col-k] == 'Q') return false;
        }
        for(int k = 1; row - k >=0 && col + k < map[0].size(); k++) {
            if(map[row-k][col+k] == 'Q') return false;
        }
        return true;
    }

    void dfs(vector<string>& map, vector<int>& cols, int index, int& ans) {
        if(index >= map.size()) {
            ans++;
        } else {
            for(int i=0; i<map[index].size(); i++) {
                if(cols[i] != -1) continue;
                map[index][i] = 'Q';
                if(valid(map, index, i)) {
                    cols[i] = i;
                    dfs(map, cols, index+1, ans);
                    cols[i] = -1;
                }
                map[index][i] = '.';
            }
        }
    }

    int totalNQueens(int n) {
        int ans = 0;
        vector<string>  Map(n);
        vector<int> cols(n, -1);
        for(int i=0; i<Map.size(); i++) {
            Map[i] = string(n, '.');
        }

        dfs(Map, cols, 0, ans);
        return ans;
    }
};

int main() {
    Solution p;
    int ans = p.totalNQueens(12);
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
