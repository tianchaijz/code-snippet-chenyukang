#include <vector>
#include <iostream>
using namespace std;

int dir[4][2] = {{-1, 0}, {1, 0},
                 {0, -1}, {0, 1}};

class Solution {
private:
    vector<vector<bool> > visit;
    string word;

public:
    bool valid(vector<vector<char> >& board, int row, int col) {
        if(row >=0 && row < board.size() &&
           col >=0 && col < board[row].size())
            return true;
        return false;
    }

    bool dfs(vector<vector<char> >& board, int row, int col, int index) {
        if(board[row][col] != word[index]) return false;
        if(index == word.size() - 1) return true;
        for(int i=0; i<4; i++) {
            int nx = row + dir[i][0];
            int ny = col + dir[i][1];
            if(valid(board, nx, ny) && visit[nx][ny] == false) {
                visit[nx][ny] = true;
                if(dfs(board, nx, ny, index+1))
                    return true;
                visit[nx][ny] = false;
            }
        }
        return false;
    }

    bool exist(vector<vector<char> > &board, string word) {
        visit.clear();
        visit.resize(board.size());
        this->word = word;
        for(int i=0; i<board.size(); i++) {
            visit[i] = vector<bool>(board[0].size(), false);
        }
        for(int i=0; i<board.size(); i++) {
            for(int j=0; j<board[i].size(); j++) {
                visit[i][j] = true;
                if(dfs(board, i, j, 0)) return true;
                visit[i][j] = false;
            }
        }
        return false;
    }
};


int main() {
    //["ABCE"],
    //["SFCS"],
    //["ADEE"]
    vector<string> strs;
    strs.push_back("ABCE");
    strs.push_back("SFCS");
    strs.push_back("ADEE");
    vector<vector<char> > board;
    board.resize(3);
    for(int i=0; i<3; i++) {
        for(int j=0; j<strs[i].size(); j++) {
            board[i].push_back(strs[i][j]);
        }
    }
    Solution p;
    std::cout << p.exist(board, "ABCCED") << std::endl;
    std::cout << p.exist(board, "SEE") << std::endl;
    std::cout << p.exist(board, "ABCB") << std::endl;
    return 0;
}
