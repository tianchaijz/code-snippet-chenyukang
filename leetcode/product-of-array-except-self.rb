
# @param {Integer[]} nums
# @return {Integer[]}
def product_except_self(nums)
  res, arr_a, arr_b = [], [], []
  a, b = 1, 1
  nums.map{|x|
    arr_a << (a *= x)
  }
  nums.reverse.map{|x|
    arr_b << ( b *= x)
  }
  arr_b = arr_b.reverse
  size = nums.size
  (0..size-1).each do |i|
    if i == 0
      res << arr_b[i+1]
    elsif i >= 1 && i < size -1
      res << arr_a[i-1] * arr_b[i+1]
    else
      res << arr_a[i-1]
    end
  end
  res
end

a = [1, 2, 3, 100]

puts product_except_self a
