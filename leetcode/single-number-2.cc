#include <iostream>

using namespace std;

class Solution {
public:
    int singleNumber(int A[], int n) {
        int ones, twos, threes;
        ones = twos = threes = 0;
        for(int i=0; i<n; i++) {
            twos |= (ones & A[i]);
            ones ^= A[i];
            threes = ~ (ones & twos);
            ones &= threes;
            twos &= threes;
        }
        return ones;
    }
};


int main() {
    int arr[] = {1, 1, 1, 2, 2, 2, 3, 5, 5, 5, 100, 100, 100};
    Solution* p = new Solution();

    std::cout << p->singleNumber(arr, sizeof(arr)/sizeof(int)) << std::endl;

    delete p;
    return 0;
}
