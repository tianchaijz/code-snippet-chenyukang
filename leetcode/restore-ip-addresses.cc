#include <vector>
#include <string>
#include <iostream>
using namespace std;


class Solution {
public:
    void iter(string s, int start, vector<string> now, vector<string>& ans) {
        if(now.size() > 4)
            return;
        if(start >= s.size() && now.size() == 4) {
            string a;
            for(int i=0; i<now.size(); i++) {
                if( a != "")
                    a = a + "." + now[i];
                else
                    a = now[i];
            }
            ans.push_back(a);
            return;
        }

        for(int i=0; i < 3 && i + start < s.size() ; i++) {
            string n = s.substr(start, i+1);
            if(n.size() > 1 && n[0] == '0')
                continue;
            if(atoi(n.c_str()) <= 255) {
                vector<string> next = now;
                next.push_back(n);
                iter(s, start+i+1, next, ans);
            }
        }
    }

    vector<string> restoreIpAddresses(string s) {
        int start = 0;
        vector<string> now;
        vector<string> ans;
        iter(s, start, now, ans);
        return ans;
    }
};

void test(string input) {
    Solution p;
    vector<string> ans = p.restoreIpAddresses(input);
    for(int i=0; i<ans.size(); i++) {
        std::cout << ans[i] << std::endl;
    }
}

int main() {
    test("1111");
    test("25525511135");
    test("1921680129");
    return 0;
}
