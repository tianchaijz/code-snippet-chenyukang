#include <vector>
#include <iostream>
using namespace std;


/**
 * Definition for an interval.  */
struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {}
    Interval(int s, int e) : start(s), end(e) {}
};

class Solution {
public:
    vector<Interval> merge(vector<Interval> &intervals) {
        vector<Interval> res;
        for(int i=0; i<intervals.size(); i++) {
            insert(res, intervals[i]);
        }
        return res;
    }

private:
    void insert(vector<Interval>& res, Interval newOne) {
        vector<Interval>::iterator it = res.begin();
        while(it != res.end()) {
            if(newOne.end < it->start) {
                res.insert(it, newOne);
                return;
            } else if(newOne.start > it->end) {
                it++;
                continue;
            } else {
                newOne.start = min(it->start, newOne.start);
                newOne.end = max(it->end, newOne.end);
                it = res.erase(it);
            }
        }
        res.insert(res.end(), newOne);
    }
};

void test0() {
    vector<Interval> vec;
    vec.push_back(Interval(0, 0));
    vec.push_back(Interval(5, 5));
    vec.push_back(Interval(2, 3));
    vec.push_back(Interval(5, 7));
    vec.push_back(Interval(0, 0));
    Solution p;
    vector<Interval> res = p.merge(vec);
    for(int i=0; i<res.size(); i++) {
        printf("%d %d\n", res[i].start, res[i].end);
    }
}

void test1() {
    vector<Interval> vec;
    vec.push_back(Interval(1, 3));
    vec.push_back(Interval(2, 6));
    vec.push_back(Interval(8, 10));
    vec.push_back(Interval(15, 18));
    Solution p;
    vector<Interval> res = p.merge(vec);
    for(int i=0; i<res.size(); i++) {
        printf("%d %d\n", res[i].start, res[i].end);
    }
}

int main() {
    test0();
    return 0;
}
