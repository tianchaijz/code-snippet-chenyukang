#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <map>
using namespace std;


class Solution {

public:
    vector<int> findSubstring(string S, vector<string> &L) {
        map<string, int> strCount;
        map<string, int> count;
        vector<int> ans;
        for(int i=0; i<L.size(); i++) {
            strCount[L[i]]++;
        }
        int size = S.size();
        int len = L.size();
        int strLen = L[0].size();
        for(int i=0; i<= size - len * strLen; i++) {
            int k = 0;
            count.clear();
            for(; k < L.size(); k++) {
                string substr = S.substr(i + k*strLen, strLen);
                if(strCount.find(substr) != strCount.end()) {
                    count[substr]++;
                    if(count[substr] > strCount[substr])
                        break;
                } else {
                    break;
                }
            }
            if(k == L.size()) {
                ans.push_back(i);
            }
        }
        return ans;
    }
};

int main() {
    string S = "barfoothefoobarman";
    vector<string> vec;
    vec.push_back("foo");
    vec.push_back("bar");
    Solution p;
    vector<int> ans = p.findSubstring(S, vec);
    for(int i=0; i<ans.size(); i++) {
        printf("%d ", ans[i]);
    }
    printf("\n");
    return 0;
}
