#include <iostream>
#include <map>
#include <stdio.h>
#include <assert.h>
using namespace std;

struct LRUNode {
    int key;
    int val;
    LRUNode* prev;
    LRUNode* next;
    LRUNode(int k, int v) : key(k), val(v), prev(NULL), next(NULL) {}
};

class LRUCache{
private:
    int capaty;
    LRUNode* lruList;
    map<int, LRUNode*> table;

    int visit(LRUNode* node) {
        if(lruList != node) {
            node->prev->next = node->next;
            node->next->prev = node->prev;
            node->next = lruList;
            node->prev = lruList->prev;
            lruList->prev->next = node;
            lruList->prev = node;
            lruList = node;
        }
    }

    void add(LRUNode* node) {
        if(lruList == NULL) {
            lruList = node;
            node->next = node;
            node->prev = node;
        } else {
            node->next = lruList;
            node->prev = lruList->prev;
            lruList->prev->next = node;
            lruList->prev = node;
            lruList = node;
        }
    }

public:
    LRUCache(int capacity) : capaty(capacity), lruList(NULL) { }

    ~LRUCache() {
        LRUNode* p = lruList;
        LRUNode* t = p;
        LRUNode* n = NULL;
        while(t) {
            n = t->next;
            delete t;
            if(n == p)
                break;
            t = n;
        }
    }

    int get(int key) {
        int res = -1;
        if(table.find(key) != table.end()) {
            res = table[key]->val;
            visit(table[key]);
        }
        return res;
    }

    void set(int key, int value) {
        LRUNode* node = NULL;
        if(table.find(key) != table.end()) {
            node = table[key];
            visit(node);
            node->val = value;
        } else if(table.size() < capaty) {
            node = new LRUNode(key, value);
            table[key] = node;
            add(node);
        } else {
            int k = lruList->prev->key;
            node = table[k];
            table.erase(node->key);
            visit(node);
            lruList->key = key;
            lruList->val = value;
            table[key] = lruList;
        }
    }
};


void test0() {
    LRUCache cache(1);
    cache.set(1, 1);
    cache.set(2, 2);
    assert(cache.get(1) == -1);
    assert(cache.get(2) == 2);
}

void test1() {
    LRUCache cache(5);
    assert(cache.get(1) == -1);
    cache.set(1, 1);
    assert(cache.get(1) == 1);
    cache.set(2, 2);
    cache.set(3, 3);
    cache.set(4, 4);
    cache.set(5, 5);
    assert(cache.get(1) == 1);
    cache.set(6, 6);
    assert(cache.get(2) == -1);
}

void test2() {
    LRUCache cache(3);
    cache.set(1,1);
    cache.set(2,2);
    cache.set(3,3);
    cache.set(4,4);
    assert(cache.get(4) == 4);
    assert(cache.get(3) == 3);
    assert(cache.get(2) == 2);
    assert(cache.get(1) == -1);
    cache.set(5,5);
}

void test3() {
    //2,[set(2,1),set(1,1),set(2,3),set(4,1),get(1),get(2)]
    LRUCache cache(2);
    cache.set(2, 1);
    cache.set(1, 1);
    cache.set(2, 3);
    cache.set(4, 1);
    assert(cache.get(1) == -1);
    assert(cache.get(2) == 3);
}

void test4() {
    //2,[set(2,1),set(1,1),get(2),set(4,1),get(1),get(2)]
    LRUCache cache(2);
    cache.set(2, 1);
    cache.set(1, 1);
    assert(cache.get(2) == 1);
    cache.set(4, 1);
    assert(cache.get(1) == -1);
    assert(cache.get(2) == 1);
}


int main() {
    test0();
    test1();
    test2();
    test3();
    test4();
    return 0;
}
