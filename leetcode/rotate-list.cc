#include <vector>
#include <iostream>
using namespace std;

/**
 * Definition for singly-linked list. */
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *rotateRight(ListNode *head, int k) {
        if(head == NULL) return NULL;
        if(k == 0) return head;
        int len = 1;
        ListNode* t = head;
        while(t->next) {
            t = t->next;
            len++;
        }
        t->next = head;
        k %= len;
        int step = len - k;
        while(step) {
            t = t->next;
            step--;
        }
        ListNode* r = t->next;
        t->next = NULL;
        return r;
    }
};

int main() {

}
