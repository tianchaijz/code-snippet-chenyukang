#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;


class Solution {
public:
    string minWindow(string S, string T) {
        const int MAX = 256;
        int expected[MAX];
        int now[MAX];
        int time = 0;
        int ans_len = INT_MAX;
        int start, end;
        string ans;

        memset(expected, 0, sizeof(expected));
        memset(now, 0, sizeof(now));

        if(S.size() == 0 ||
           S.size() < T.size()) {
            return ans;
        }

        for(int i=0; i<T.size(); i++) {
            expected[T[i]]++;
        }

        start = 0;
        for(end=0; end < S.size(); end++) {
            char x = S[end];
            if(expected[x] > 0) {
                now[x]++;
                if(now[x] <= expected[x])
                    time++;
            }
            if(time == T.size()) {
                for(; start <= end; start++) {
                    char x = S[start];
                    if(expected[x] == 0)
                        continue;
                    if(now[x] <= expected[x])
                        break;
                    now[x]--;
                }

                int len = end + 1 - start;
                if(ans.size() == 0 || len < ans.size() ) {
                    ans = S.substr(start, len);
                }
            }
        }
        return ans;
    }
};

int main() {
    Solution p;
    string ans = p.minWindow("AXXXBXXCABDC", "ABC");
    std::cout << "ans: " << ans << std::endl;

    ans = p.minWindow("ADOBECODEBANC", "ABC");
    std::cout << "ans: " << ans << std::endl;
    return 0;
}
