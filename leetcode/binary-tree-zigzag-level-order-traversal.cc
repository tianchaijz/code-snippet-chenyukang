#include <iostream>
#include <stdio.h>
#include <queue>
using namespace std;

/**
 * Definition for binary tree */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};


class Solution {
    vector<int> reverse(vector<int>& vec) {
        vector<int> res;
        for(int i=vec.size() - 1; i>=0; i--)
            res.push_back(vec[i]);
        return res;
    }
public:
    vector<vector<int> > zigzagLevelOrder(TreeNode *root) {
        vector<vector<int> > res;
        vector<int> now;
        queue<TreeNode*> Q;
        if(root == NULL) return res;
        Q.push(root);
        Q.push(NULL);
        while(!Q.empty()) {
            TreeNode* node = Q.front();
            Q.pop();
            if(node == NULL) {
                res.push_back(now);
                now.clear();
                if(Q.empty()) break;
                else Q.push(NULL);
            } else {
                now.push_back(node->val);
                if(node->left) Q.push(node->left);
                if(node->right) Q.push(node->right);
            }
        }
        //reverse
        vector<vector<int> > r;
        for(int i=0; i<res.size(); i++) {
            if(i%2) r.push_back(reverse(res[i]));
            else r.push_back(res[i]);
        }
        return r;
    }
};


int main() {
}
