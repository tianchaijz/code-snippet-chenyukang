


# @param {Integer} n
# @return {Integer}
def climb_stairs(n)
  return n if n <= 2
  p, q = 1, 2
  while n - 2 > 0
    x = p + q
    p, q = q, x
    n -= 1
  end
  q
end

