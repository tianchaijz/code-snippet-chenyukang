#include <stdio.h>
#include <stdlib.h>

int intcmp(const void *v1, const void *v2) {
  return (*(int *)v1 - *(int *)v2);
}

bool containsDuplicate(int* nums, int numsSize) {
  int i;
  if(numsSize <= 1) return false;
  qsort(nums, numsSize, sizeof(int), intcmp);
  for(i=0; i<numsSize-1; i++) {
    if(nums[i] == nums[i+1])
      return true;
  }
  return false;
}

int main() {
  int a[4] = {1, 2, 2, 3};
  if(containsDuplicate(a, 4))
    printf("yes\n");
  else
    printf("no\n");
  return 0;
}
