# Definition for singly-linked list.

class ListNode
    attr_accessor :val, :next
    def initialize(val)
        @val = val
        @next = nil
    end
end

# @param {ListNode} head
# @return {ListNode}
def reverse_list(head)
  return head if head.nil? or head.next.nil?
  cur = head
  nxt = head.next
  head.next = nil
  while nxt
    t, nxt.next = nxt.next, cur
    cur = nxt
    nxt = t
  end
  return cur
end
