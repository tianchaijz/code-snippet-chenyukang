#include <iostream>
#include <stdio.h>
#include <cassert>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode(int val): val(val), next(NULL) {}
};

class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if(head == NULL) return NULL;
        ListNode* prev = NULL;
        ListNode* last = head;
        ListNode* x = head;
        while(n>1) {
            last = last->next;
            n--;
        }
        while(last->next) {
            if(prev == NULL) prev = x;
            else prev = prev->next;
            x = x->next;
            last = last->next;
        }
        if(prev == NULL)
            return x->next;
        prev->next = x->next;
        return head;
    }
};

void print(ListNode* head) {
    ListNode* t = head;
    while(t) {
        std::cout<<t->val << " ";
        t = t->next;
    }
    std::cout << std::endl;
}

int main() {
    ListNode* head = new ListNode(1);
    Solution p;
    ListNode* res = p.removeNthFromEnd(head, 1);
    print(res);
    return 0;
}
