#include <iostream>
#include <vector>
using namespace std;


class Solution {
public:
    void rotate(vector<vector<int> > &matrix) {
        int N = matrix.size();
        for(int i=0; i<N; i++) {
            for(int j=0; j<N-i; j++) {
                int t = matrix[i][j];
                swap(matrix[i][j], matrix[N-j-1][N-i-1]);
            }
        }

        for(int i=0; i<N/2; i++) {
            for(int j=0; j<N; j++)
                swap(matrix[i][j], matrix[N-i-1][j]);
        }
    }
};


void test(int N) {
    vector<vector<int> > matrix;
    matrix.resize(N);
    for(int i=0; i<N; i++)
        matrix[i].resize(N);

    int Num = 1;
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            matrix[i][j] = Num++;
    Solution p;
    p.rotate(matrix);

    for(int i=0; i<N; i++) {
        for(int j=0; j<N; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main() {
    test(4);
    return 0;
}
