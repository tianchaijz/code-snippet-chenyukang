#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode* invertTree(struct TreeNode* root) {
  if(root == NULL) return NULL;
  struct TreeNode* l = root->left;
  struct TreeNode* r = root->right;
  struct TreeNode* t = 0;
  if ((l == NULL && r == NULL) ||
      (l != NULL && r != NULL && l->val == r->val)) {
    // pass
  } else {
    t = r, r = l, l = t;
  }
  root->left = invertTree(l);
  root->right = invertTree(r);
  return root;
}

int main() {
  struct TreeNode* root = (struct TreeNode*)malloc(sizeof(struct TreeNode));
  root->val = 1;
  root->left = (struct TreeNode*)malloc(sizeof(struct TreeNode));
  root->right = (struct TreeNode*)malloc(sizeof(struct TreeNode));
  root->left->val = 2;
  root->left->left = root->left->right = NULL;
  root->right->left = root->right->right = NULL;
  root = invertTree(root);
  return 0;
}
