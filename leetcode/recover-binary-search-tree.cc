//this alg using recursive, still use stack memory
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
using namespace std;


/**
 * Definition for binary tree */
struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
private:
    TreeNode* s1;
    TreeNode* s2;
    TreeNode* pre;
public:

    void recover_iter(TreeNode* node) {
        if(node == NULL) return;
        if(s1 != NULL && s2 != NULL) return;
        recover_iter(node->left);
        if(pre != NULL && pre->val > node->val) {
            if(s1 == NULL) {
                s1 = pre;
                s2 = node;
            }
            else {
                s2 = node;
                return;
            }
        }
        pre = node;
        recover_iter(node->right);
    }

    void recoverTree(TreeNode *root) {
        s1 = s2 = NULL;
        pre = NULL;
        recover_iter(root);
        swap(s1->val, s2->val);
    }
};


int main() {
    TreeNode* root = new TreeNode(1);
    root->right = new TreeNode(0);
    Solution p;
    p.recoverTree(root);
    printf("%d\n", root->val);
    return 0;
}
