-module(records).
-compile(export_all).

-record(robot, {name,
                type = industrial,
                hobbies,
                details=[]}).


first_robot() ->
    #robot{name = "Chen Yukang", type = human, details = ["this is a Human"]}.

repairman(Rob) ->
    Details = Rob#robot.details,
    NewRob = Rob#robot{details=["Repaired by repairman"|Details]},
    {repaired, NewRob}.
