#include <iostream>
using namespace std;

// 1 -> 2
// f(2) -> 2 + 2 + 1
// f(3) -> 3 + 3 + 2 + 1
// f(4) -> 4 + 4 + 3 + 2 + 1
// f(5) -> 5 + 5 + 4 + 3 + 2 + 1
// f(6) -> 6 + 6 + 5 + 4 + 3 + 2 + 1

typedef long long LL;

LL func(LL n) {
    return n + ((n + 1)*n)/2;
}


int main() {
    LL Num;
    LL n;
    cin >> Num;
    while(Num--) {
        cin >> n;
        std::cout << func(n) << std::endl;
    }
    return 0;
}
