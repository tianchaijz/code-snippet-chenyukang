#include <iostream>
#include <stdio.h>
using namespace std;

int main() {
    float sum;
    int   request;
    cin >> request >> sum;
    if((sum >= (float)request + 0.5) && (request % 5) == 0) {
        printf("%.2f\n", (sum - request - 0.5));
    } else {
        printf("%.2f\n", sum);
    }
    return 0;
}
