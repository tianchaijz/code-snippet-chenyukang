#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
using namespace std;

#define SIZE 2048

char buffer[SIZE];

int main() {
    unsigned long n, k;
    unsigned long ans, cnt, num, value;
    int j;
    ans = cnt = num = value = 0;
    scanf("%lu %lu\n", &n, &k);
    while((num = fread(buffer, 1, SIZE, stdin)) > 0 ) {
        for(int i=0; i<num; i++) {
            if(buffer[i] == '\n') {
                if(value % k == 0) {
                    ans++;
                }
                value = 0;
            } else {
                value = value * 10 + (buffer[i] - '0');
            }
        }
    }
    std::cout << ans << std::endl;
    return 0;
}
