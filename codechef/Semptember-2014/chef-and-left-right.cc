#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

void process(const string& str) {
    unsigned long long cur = 1;
    for(int i=0; i<str.size(); i++)  {
        if ((i%2) == 0) { //even
            if (str[i] == 'l') {
                cur *= 2;
            } else if (str[i] == 'r') {
                cur = cur * 2 + 2;
            }
        } else { //odd
            if (str[i] == 'l') {
                cur = cur * 2 - 1;
            } else if (str[i] == 'r') {
                cur = cur * 2 + 1;
            }
        }
        cur = cur % 1000000007;
    }
    std::cout << cur << std::endl;
}

int main() {
    int N;
    string str;
    cin >> N;
    for(int i=0; i<N; i++) {
        cin >> str;
        process(str);
    }
    return 0;
}
