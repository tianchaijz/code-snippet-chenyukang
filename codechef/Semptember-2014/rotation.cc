#include <iostream>
#include <string>
#include <stdio.h>
#include <vector>
using namespace std;

int main() {
    int N, M;
    int t;
    cin >> N >> M;
    vector<int> vec;
    for(int i=0; i<N; i++) {
        cin >> t;
        vec.push_back(t);
    }

    int cur = 0;
    char d;
    for(int i=0; i<M; i++) {
        cin >> d >> t;
        if (d == 'R') { //query
            std::cout << vec[(cur + t - 1)%vec.size()] << std::endl;
        } else if (d == 'A') { //anti-clock
            t = t % vec.size();
            cur = (cur - t >= 0) ? cur - t : (cur - t + vec.size());
        } else if (d == 'C') { //clock
            cur = (cur + t) % vec.size();
        }
    }
    return 0;
}
