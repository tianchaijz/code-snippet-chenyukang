#include <iostream>
using namespace std;

typedef long long LL;

int main() {
    LL T, N;
    cin >> T;
    while(T--) {
        cin >> N;
        std::cout << ((N%2) ? (N-1) : N) << std::endl;
    }
    return 0;
}
