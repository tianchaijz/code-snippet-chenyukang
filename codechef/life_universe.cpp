#include <iostream>
using namespace std;

int main() {
    int num;
    while(cin>>num) {
        if (num == 42)
            break;
        else
            std::cout << num << std::endl;
    }
    return 0;
}
