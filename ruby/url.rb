#!/usr/bin/env ruby
# coding: utf-8

## Read users from RankList, and get details for users,
## mainly get the school etc.

require 'nokogiri'
require 'restclient'

USER_URL = "localhost:9999/xbox/"

def get_page(number)
  url = USER_URL + number.to_s
  page = Nokogiri::HTML(RestClient::get(url))
  puts page
end


prng = Random.new(1234)
for x in 1..1000
  rand = prng.rand(0..100)
  get_page(rand)
end
