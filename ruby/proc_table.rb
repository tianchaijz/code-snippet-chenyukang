require 'open3'
require 'parallel'
require 'sys/proctable'

class MemoryOut < StandardError
end

jobs = *(1..10000)

Parallel.map(jobs, :in_threads=>20) do
  mem_thr = Thread.new do
    100.times do|i|
      begin
        Sys::ProcTable.ps {|p|
          puts p.pid.to_s
          puts p.comm
        }
      rescue Errno::ENOENT, Errno::ESRCH
        # ProcTable bug
      end
      if i > 5
        raise MemoryOut
      end
      sleep 0.1
    end
  end

  open_thr = Thread.new do
    Open3.popen2e "sleep 2" do |i, o, w|
      w.value
    end
    mem_thr.kill
    puts "heloo"
  end

  begin
    mem_thr.join
  rescue MemoryOut
  end

  open_thr.join
end
