require "rubygems"
require "bunny"

conn = Bunny.new("amqp://guest:guest@localhost:5672")
conn.start

ch  = conn.create_channel
x   = ch.fanout("nba.scores")

x.publish("BOS 101, NYK 89").publish("ORL 85, ALT 88")

conn.close
