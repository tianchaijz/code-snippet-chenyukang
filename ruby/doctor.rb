#!/usr/bin/env ruby
# coding: utf-8
require 'nokogiri'
require 'open-uri'
require 'fileutils'
require 'json'

Output = "doctors/"

def getDoc(url)
  begin
    html = open(url).read
    html = html.encode("utf-8", "gbk")
    return Nokogiri::HTML.parse html
  rescue
    return getDoc(url)
  end
end

URL = "http://400.haodf.com/index/search?diseasename=&province=&facultyname=&hosfaculty=&hospitalname=&nowpage="
ResultPath = "html/body/div[4]/div[1]/div[2]/div[2]/div[4]/div"

def ProcessDetails(url)
  doc = getDoc(url)
  info = doc.css("p.fb")[0]
  hos = classroom = ""
  if info != nil
    hos = info.text.split()[0]
    classroom = info.text.split()[1]
  end
  res = doc.css("div.oh")[3]
  text = ""
  if res != nil
    text = res.text
  end
  return hos, classroom, text.gsub("查看详情>>", "")
end

def ProcessPage(page)
  url = "#{URL}#{page}"
  #puts url
  doc = getDoc(url)
  if doc == nil
    return
  end
  #puts doc
  
  res = doc.css("div.showResult-cell")
  
  for index in 0..9
    if res[index] == nil
      next
    end
    
    name = res[index].xpath("div/p/a").text.strip()
    puts name
    link = res[index].xpath("div/p/a")[0]
    if link == nil
      next
    end
    link = link['href']
    #puts link
    
    #pic
    pic = res[index].css("img")
    piclink = pic[0]["src"]
    if piclink != "" 
      %x[wget #{piclink} -O #{Output}#{name}.jpg 1>/dev/null 2>&1]
    end
    
    title = res[index].xpath("div/p")[0].text.strip().gsub(name, "")
    #puts title
    lines = res[index].xpath("div/div")
    special = lines[0].xpath("div/span").text
    time = lines[2].xpath("div/span").text
    hos, classroom, resume = ProcessDetails(link)
    desc = File.open(Output + name+".txt", "w+")
    result = { "Name" => name,
               "Title" => title,
               "Hospital" => hos,
               "ClassRoom" => classroom,
               "Special" => special,
               "Booking" => time,
               "Resume" => resume}
    
    desc.write(result.to_json)
    desc.close()
  end
end

def Main()
  for x in 686..812
    puts "page: #{x}"
    ProcessPage(x)
  end
end

Main()

