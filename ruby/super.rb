class Animal
  def move
    "I can move"
  end
end

class Bird < Animal
  def move
    @a = 1
    super + " by flying"
  end

  def initialize
    @a = 1
  end

  def fly
    puts "now: #{@a}"
  end
end

puts Animal.new.move
puts Bird.new.move
a = Bird.new
a.fly
