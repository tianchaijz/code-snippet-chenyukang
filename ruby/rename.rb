#!/usr/bin/env ruby
# coding: utf-8
require 'fileutils'


def run()
  Dir.foreach("./") { |file|
    if File.basename(file) =~ /.*\.scm$/
      elems = file.split(".")
      if elems.size() == 3
        name = elems[1]
        if name.size() <= 1
          name = "0" + name
        end
        new_name = "#{name}.scm"
        print file, " => ", new_name , "\n"
        `mv #{file} #{new_name}`
      end
      
    end
  }
end


run()
        
