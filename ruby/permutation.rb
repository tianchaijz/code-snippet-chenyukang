#!/usr/bin/env ruby


def print_arr(arr)
  print "array(#{arr.size()}): [ "
  arr.each {|x|
    print x, " "
  }
  print "]\n"
end

def iter(arr, depth, res)
  return res if depth >= arr.size()
  if res.size() == 0
    res << [arr[depth]]
    return iter(arr, depth+1, res.clone)
  else
    nxt = []
    for x in depth..arr.size()-1
      cur = arr[x]
      for i in 0..res.size()-1
        if res[i].index(cur) == nil
          for m in 0..res[i].size()
            nxt << res[i].clone.insert(m, cur)
          end
        end
      end
    end
    return iter(arr, depth+1, nxt.clone)
  end
end

## return the permutation of a arrary
def permutation(arr)
  return iter(arr, 0, [])
end

#print_arr([1,2,3])
res = permutation([1, 2, 3, 4, 5, 6])
print "now:"
print_arr res
