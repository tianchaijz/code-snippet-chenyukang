class SmartStack < Array
  attr_reader :sorted

  def initialize
    @sorted = []
  end

  def push elem
    super @sorted[@sorted.index { |n| n >= elem } || @sorted.size, 0] = elem
  end

  def pop
    @sorted.delete_at @sorted.index super
  end

  def remove_greater x
    @sorted.slice!(@sorted.index { |n| n > x }..-1).each do |rem|
      delete_at rindex rem
    end
  end
end

ss = SmartStack.new

display = -> msg do
  puts msg
  puts 'Stack'
  p ss.reverse
  puts 'Sorted'
  p ss.sorted
  puts
end

rand(1..40).times { ss.push rand -1000..1000 }
display['Initial state']

x = rand -1000..1000
ss.remove_greater x
display["Removed greater than #{x}"]

(ss.size / 2).times { ss.pop }
display["Popped half"]
