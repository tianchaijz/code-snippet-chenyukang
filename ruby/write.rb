#!/usr/bin/env ruby
# coding: utf-8

require 'writeexcel'
require 'json'

Doctors = []

Dir.foreach("./doctors/") { |file|
  if File.basename(file) =~ /.*\.txt$/
    puts file
    buf = File.read("./doctors/" + file)
    data_hash = JSON.parse(buf)
    Doctors << data_hash
  end
}

# Create a new workbook and add a worksheet
workbook  = WriteExcel.new('医生.xls')
worksheet = workbook.add_worksheet

# Set the column width for columns 1, 2, 3 and 4
worksheet.set_column(0, 3, 15)

# Create a format for the column headings
header = workbook.add_format
header.set_bold
header.set_size(12)
header.set_color('blue')

# Create a format for the stock price
f_price = workbook.add_format
f_price.set_align('left')
f_price.set_num_format('0.00')

# Create a format for the stock volume
f_volume = workbook.add_format
f_volume.set_align('left')
f_volume.set_num_format('#,##0')


# Create a format for the price change. This is an example of a
# conditional format. The number is formatted as a percentage. If it is
# positive it is formatted in green, if it is negative it is formatted
# in red and if it is zero it is formatted as the default font colour
# (in this case black). Note: the [Green] format produces an unappealing
# lime green. Try [Color 10] instead for a dark green.
#
f_change = workbook.add_format
f_change.set_align('left')
f_change.set_num_format('[Green]0.0%;[Red]-0.0%;0.0%')

# Write out the data
worksheet.write(0, 0, '姓名', header)
worksheet.write(0, 1, '职称', header)
worksheet.write(0, 2, '医院', header)
worksheet.write(0, 3, '科室', header)
worksheet.write(0, 4, '擅长', header)
worksheet.write(0, 5, '预约', header)
worksheet.write(0, 6, '简介', header)

index = 1
Doctors.each { |d|
  worksheet.write(index, 0, d["Name"])
  worksheet.write(index, 1, d["Title"])
  worksheet.write(index, 2, d["Hospital"])
  worksheet.write(index, 3, d["ClassRoom"])
  worksheet.write(index, 4, d["Special"])
  worksheet.write(index, 5, d["Booking"])
  worksheet.write(index, 6, d["Resume"])
  index = index + 1
}

workbook.close
