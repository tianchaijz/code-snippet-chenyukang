
Card = Struct.new(:name, :age, :other)

c = Card.new

c.other = Card.new

c.other.name = "chen"
c.other.age = 24

### pascal syntax
# with c.other do begin
#                   name = "chen"
#                   age = 24
#                 end

puts c
