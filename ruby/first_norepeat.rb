
def first_norepeat(str)
  norepeat = []
  repeat = []
  str.each_char do |c|
    if !norepeat.include?(c) && !repeat.include?(c)
      norepeat << c
    elsif norepeat.include?(c)
      norepeat.delete(c)
      repeat << c
    end
  end
  return !norepeat.empty? ? norepeat.first : ""
end


puts first_norepeat("hello")
puts first_norepeat("")
puts first_norepeat("xxxhelo")
puts first_norepeat("xxxxxx")
