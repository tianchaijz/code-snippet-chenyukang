
class Lawyer
  def method_missing(method, *args)
    puts "You called: #{method} (#{args.join(',')})"
    puts "You also passed it a block" if block_given?
  end
end

bob = Lawyer.new()
bob.talk_simple('a', 'b') do
  puts "block now"
end


require 'ruport'

table = Ruport::Data::Table.new :column_names => ["component", "branch"],
:data => [["yukang", "kang2013"],
          ["this is long", "this is also long"],
          ["hongzhi", "hongzhi2013"]]

puts table.to_text


          
