#!/usr/bin/env ruby
# coding: utf-8
require 'nokogiri'
require 'open-uri'
require 'fileutils'
require 'json'

Output = "hospital"
Detail = "details"

Cities = ["beijing", "tianjin", "hebei", "shanxi", "shaanxi", "neimenggu", "liaoning",
          "jilin", "heilongjiang", "shanghai", "jiangsu", "zhejiang", "anhui",
          "fujian", "jiangxi", "shandong", "henan", "hubei", "hunan", "guangdong",
          "guangxi", "hainan", "sichuan", "guizhou", "yunnan", "xizang", "gansu",
          "qinghai", "ningxia", "xinjiang", "taiwan", "xianggang", "aomen"]

def getDoc(url)
  return Nokogiri::HTML(open(url))
end

def analysis(url)
  res = ""
  doc = getDoc(url)
  num = 0
  doc.css("td").each do |line|
    res += line.to_s + "\n"
    num = num + 1
  end
  return res, num
end

def getHospitalFromCity(city)
  url = "http://ent.jobmd.cn/hospital/" + city + ".htm"
  res, num = analysis(url)
  out = File.open(Output + "/" + city + ".txt", "w+")
  out.write(res)
  out.close()
  printf "#{city} : #{num}\n"
end

def getList()
  Cities.each do |city|
   getHospitalFromCity(city)
  end
end


def processCity(dir, listfile)
  list = File.open(listfile, "r").readlines()
  list.each do |line|
    elems = line.split("\"")
    name = elems[5]
    name = name.gsub("/", "_")
    outdir = File.join(dir, name)
    back = Dir.pwd()
    if File.directory?(outdir)
      return
    end
    Dir.mkdir(outdir)
    Dir.chdir(outdir)
    
    url = elems[1].gsub(".htm", "/intro.htm")
    doc = getDoc(url)
    
    sidebar = "html/body/div/div[3]/div[1]"
    doc.xpath(sidebar + "/div[1]/div[1]/img").each do |div|
      pig = div['src']
      type = File.extname(pig)
      %x[wget #{pig} -O nva#{type} 1>/dev/null 2>&1]
    end
    
    level = ""
    doc.xpath(sidebar + "/div[2]/div[2]/p/span[2]/a").each do |div|
      level =  div.text
    end
    
    website = ""
    doc.xpath(sidebar + "/div[2]/div[2]/p/a[1]").each do |div|
      website =  div.text
    end
    
    phonenumber = ""
    doc.xpath(sidebar + "/div[2]/div[2]/p/span[3]").each do |div|
      phonenumber =  div.text
    end
    
    addr = ""
    doc.xpath(sidebar + "/div[2]/div[2]/p/span[4]/a").each do |div|
      addr =  div.text
    end
    
    postnumber = ""
    doc.xpath(sidebar + "/div[2]/div[2]/p/span[5]/a").each do |div|
      postnumber =  div.text
    end
    
    intro = ""
    doc.xpath("html/body/div/div[3]/div[2]/div[2]/p").each do |div|
      intro  += div.text + "\n"
    end
    
    doc.xpath("html/body/div/div[3]/div[2]/div[2]/div/div/img").each do |div|
      pig = div["src"]
      type = File.extname(pig)
      %x[wget #{pig} -O desc#{type} 1>/dev/null 2>&1 ]
    end
    
    result = {"Name" => name,
              "Level" => level,
              "Website" => website,
              "PhoneCall" => phonenumber,
              "Address" => addr,
              "PostNumber" => postnumber,
              "Intro" => intro}
    desc = File.open("desc.txt", "w+")
    desc.write(result.to_json)
    desc.close()
    print "finished: #{elems[1]}, #{elems[5]}\n"
    Dir.chdir(back)
  end
end

def getDetails()
  cnt = 0
  Dir.foreach(Output) { |file|
    if File.basename(file) =~ /.*\.txt$/
      city = file.gsub(".txt", "")
      puts "\n" + city
      cnt = cnt + 1
      dir = File.join(Detail, city)
      FileUtils.remove_dir(dir, :force => true)
      Dir.mkdir(dir)
      processCity(dir, File.join(Output, file))
    end
  }
end

#getList()
getDetails()



