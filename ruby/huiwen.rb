
def solve str
  r = str.size - 1
  l = 0
  while l < r
    return false if str[l] != str[r]
    l += 1
    r -= 1
  end
  return true
end

# puts solve ""
# puts solve "a"
# puts solve "aba"
# puts solve "aaa"
# puts solve "abb"
# puts solve "ab"

def merge a, b
  res = []
  idx_a = idx_b = 0
  while idx_a < a.size && idx_b < b.size
    if a[idx_a] < b[idx_b]
      res << a[idx_a]
      idx_a += 1
    elsif a[idx_a] > b[idx_b]
      res << b[idx_b]
      idx_b += 1
    else
      idx_a += 1
      idx_b += 1
      ## skip the same elems consitive
      while idx_a < a.size && a[idx_a] == a[idx_a-1]
        idx_a += 1
      end
      while idx_b < b.size && b[idx_b] == b[idx_b-1]
        idx_b += 1
      end
    end
  end
  res += (a[idx_a..a.size]) if idx_a < a.size
  res += (b[idx_b..b.size]) if idx_b < b.size
  res
end

puts "fuck"
puts merge [1, 2, 3], [2, 4, 5]
puts "--"
puts merge [1, 2, 3, 4], [3, 3, 4, 4, 5]
puts "--"
puts merge [1, 2, 3], [3, 3, 4, 4, 5]

puts "--"
puts merge [1], []

def test_sum arr, sum
  hash = {}
  arr.each{|x|
    return true if !(hash[sum - x].nil?)
    hash[x] = 1
  }
  false
end

puts test_sum [1, 2, 3], 1
puts test_sum [1, 2, 3], 6

def sort_file file_name
  len = 2000001
  arr = Array.new(len, 0)
  fp = File.open(file_name, "r")
  while idx = fp.read_next_int()
    arr[idx+1000000] += 1
  end
  for idx in 0..len-1 do
    while arr[idx] > 0
      fp.write_int(idx-1000000)
    end
  end
end
