module Debug
  def whoAmI?
    "I am #{self.to_s}"
  end
end

class Photo
  include Debug
end

ph = Photo.new

puts ph.whoAmI?
