#!/usr/bin/env ruby
# coding: utf-8
require 'nokogiri'
require 'open-uri'
require 'fileutils'
require 'json'


Hospitals = "hospitals"
Regions = "regions"

Cities = ["beijing", "tianjin", "hebei", "shanxi",
          "shaanxi", "neimenggu", "liaoning",
          "jilin", "heilongjiang", "shanghai",
          "jiangsu", "zhejiang", "anhui",
          "fujian", "jiangxi", "shandong",
          "henan","hubei", "hunan", "guangdong",
          "guangxi", "hainan", "sichuan", "guizhou",
          "yunnan", "xizang", "gansu",
          "qinghai", "ningxia", "xinjiang",
          "taiwan", "xianggang", "aomen"]

ProvinceMap = {"beijing" => "北京", "tianjin" => "天津",
               "hebei" => "河北", "neimenggu" => "内蒙古",
               "shanxi" => "山西","shanghai" => "上海",
               "anhui" => "安徽", "jiangsu" => "江苏",
               "zhejiang" => "浙江", "shandong" => "山东",
               "fujian" => "福建", "jiangxi" => "江西",
               "guangdong" => "广东", "guangxi" => "广西",
               "hainan" => "海南", "henan" => "河南",
               "hubei" => "湖北", "hunan" => "湖南",
               "heilongjiang" => "黑龙江", "jilin" => "吉林",
               "liaoning" => "辽宁", "shaanxi" => "陕西",
               "gansu" => "甘肃", "ningxia" => "宁夏",
               "qinghai" => "青海", "xinjiang" => "新疆",
               "chongqing" => "重庆", "sichuan" => "四川",
               "yunnan" => "云南", "guizhou" => "贵州",
               "xizang" => "西藏", "xianggang" => "香港",
               "aomen" => "澳门", "taiwan" => "台湾"}

def getDoc(url)
  return Nokogiri::HTML(open(url))
end

Res = []

class Unit
  attr_accessor :province, :district, :link, :hospitals
  def initialize(province, district, link)
    @province = province
    @district = district
    @link = link
    @hospitals = []
  end
  
  def addHospital(hospital)
    @hospitals << hospital
  end
end

def analysis(doc)
  res = []
  table = doc.css("table.msg_tab")
  items = table.css("a")
  items.each do |item|
    res << [item["href"], item.text]
  end
  return res
end

def ProcessHos(hos, province, district)
  name = hos[1]
  link = hos[0]
  dir = "#{Hospitals}/#{name}"
  if !File.directory?(dir)
    Dir.mkdir(dir)
  end
  back = Dir.pwd()
  Dir.chdir(dir)
  
  url = link.gsub(".htm", "/intro.htm")
  doc = getDoc(url)
  
  sidebar = "html/body/div/div[3]/div[1]"
  doc.xpath(sidebar + "/div[1]/div[1]/img").each do |div|
    pig = div['src']
    type = File.extname(pig)
    %x[wget #{pig} -O nva#{type} 1>/dev/null 2>&1]
  end
  
  level = ""
  doc.xpath(sidebar + "/div[2]/div[2]/p/span[2]/a").each do |div|
    level =  div.text
  end
  
  website = ""
  doc.xpath(sidebar + "/div[2]/div[2]/p/a[1]").each do |div|
    website =  div.text
  end
  
  phonenumber = ""
  doc.xpath(sidebar + "/div[2]/div[2]/p/span[3]").each do |div|
    phonenumber =  div.text
  end
  
  addr = ""
  doc.xpath(sidebar + "/div[2]/div[2]/p/span[4]/a").each do |div|
    addr =  div.text
  end
  
  postnumber = ""
  doc.xpath(sidebar + "/div[2]/div[2]/p/span[5]/a").each do |div|
    postnumber =  div.text
  end
  
  intro = ""
  doc.xpath("html/body/div/div[3]/div[2]/div[2]/p").each do |div|
    intro  += div.text + "\n"
  end
  
  doc.xpath("html/body/div/div[3]/div[2]/div[2]/div/div/img").each do |div|
    pig = div["src"]
    type = File.extname(pig)
    %x[wget #{pig} -O desc#{type} 1>/dev/null 2>&1 ]
  end
  
  result = {"Name" => name,
            "Level" => level,
            "Province" => province,
            "District" => district,
            "Website" => website,
            "PhoneCall" => phonenumber,
            "Address" => addr,
            "PostNumber" => postnumber,
            "Intro" => intro}
  desc = File.open("desc.txt", "w+")
  desc.write(result.to_json)
  desc.close()
  print "finished: #{province}, #{district}, #{name}\n"
  Dir.chdir(back)
end

def getDetails(unit)
  doc = getDoc(unit.link)
  res = analysis(doc)
  res.each do |h|
    ProcessHos(h, unit.province, unit.district)
    unit.addHospital(h[1])
  end
  desc = File.open("#{Regions}/#{unit.province}-#{unit.district}.txt", "w+")
  hospitals = unit.hospitals.join("||")
  result = { "Province" => unit.province,
             "District" => unit.district,
             "Hospitals" => hospitals }
  desc.write(result.to_json)
  desc.close()
  puts "finish:#{unit.province}-#{unit.district}"
end


def getList()
  num = 0
  Cities.each do |city|
    num = num + 1
    if num >= 6
      puts "now processing: #{num}"
      url = "http://ent.jobmd.cn/hospital/#{city}"
      doc = getDoc(url)
      list = doc.css("div.l")[0]
      list = list.xpath("dl[3]/dd")
      items = list.css("a")
      for x in 1..items.size()-1
        link = items[x]["href"]
        district =  items[x].text
        province = ProvinceMap[city]
        unit = Unit.new(province, district, link)
        getDetails(unit)
      end
    end
  end
end

getList()
getDetails()
