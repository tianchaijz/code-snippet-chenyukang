

class Demo
end


def add_checked_attribute1(clazz, attr)
  val
  eval "
        class #{clazz}
            def #{attribute}=(value)
                raise 'Invalid attribute' unless value
                @{attribute} = value
            end

            def #{attribute}()
                @{attribute}
            end
         end
         "
end

def add_checked_attribute2(clazz, attr)
  clazz.class_eval do
    define_method "#{attr}=" do |value|
      raise "Invalid attribute" unless value
      instance_variable_set("@#{attr}", value)
    end

    define_method attr do
      instance_variable_get("@#{attr}")
    end
  end
end


# add_checked_attribute(Demo, "yukang")
# demo = Demo.new()
# demo.yukang=1
# puts demo.yukang


def add_checked_attribute3(clazz, attr, &validation)
  clazz.class_eval do
    define_method "#{attr}=" do |value|
      raise "Invalid attribute" unless validation.call(value)
      instance_variable_set("@#{attr}", value)
    end

    define_method attr do
      instance_variable_get("@#{attr}")
    end
  end
end

# add_checked_attribute3(Demo, "yukang") { |v| v >= 18 }
# demo = Demo.new
# demo.yukang= 18
# puts demo.yukang

class Class
  def attr_checked(attr, &validation)
    define_method "#{attr}=" do |value|
      raise "Invalid attribute #{value}" unless validation.call(value)
      instance_variable_set("@#{attr}", value)
    end

    define_method attr do
      instance_variable_get("@#{attr}")
    end
  end
end


class Man
  attr_checked("age") { |v| v >= 18 }
end

# man = Man.new
# man.age = 17


class String
  def self.inherited(subclass)
    puts "#{self} was inherited by #{subclass}"
  end
end

class MyString  < String
end


module CheckedAttribute
  def self.include(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def attr_checked(attr, &validation)
      define_method "#{attr}=" do |value|
        raise "Invalid attribute #{value}" unless validation.call(value)
        instance_variable_set("@#{attr}", value)
      end

      define_method attr do
        instance_variable_get("@#{attr}")
      end
    end
  end
end


class Man
  include CheckedAttribute
  attr_checked(:age) do |v|
    v >= 18
  end
end

man = Man.new
man.age = 17
