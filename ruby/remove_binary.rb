#!/usr/bin/env ruby

def walk(dir)
  Dir.foreach(dir) do |item|
    next if item == '.' or item == '..'
    item = File.join(dir, item)
    if File.directory?(item)
      walk(item)
    else
      if File.extname(item).empty? && File.executable?(item)
        puts "remove: #{item.inspect}\n"
        `rm #{item}`
      end
    end
  end
end


puts Dir.pwd
walk(Dir.pwd)
