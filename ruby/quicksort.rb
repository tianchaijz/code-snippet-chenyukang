
def qsort(lst)
  return [] if lst.size() == 0
  first, *left = *lst
  less, more = left.partition {|x| x < first}
  return qsort(less) + [first] + qsort(more)
end

l = [10, 3, 4, 13]
res = qsort(l)
p res

l = [1]
res = qsort(l)
p res

l = [2, 1]
res = qsort(l)
p res

l = [2, 1, 100]
res = qsort(l)
p res
