class Demo
  private
  def meaning_of_life
    puts "now : #{self.class.to_s} meaning_of_life"
  end
end

class Init
  def initialize(secrect_mock)
    @secrect_mock = secrect_mock
  end

  def meaning_of_life
    @secrect_mock.send(:meaning_of_life)
  end
end

Init.new(Demo.new).meaning_of_life


def relay(array, data_type)
  array.map { |x| x.send("to_#{data_type}") }
end

puts relay([1, 2, 3], 's')


class Monk
  ["life", "the_universe", "everything"].each do |name|
    define_method("meditate_on_#{name}") do
      "I konw the meaning of #{name}"
      end
  end
end


puts Monk.new.meditate_on_life
puts Monk.new.meditate_on_the_universe
puts Monk.new.meditate_on_everything
