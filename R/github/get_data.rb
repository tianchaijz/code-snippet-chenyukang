#!/usr/bin/env ruby
require 'octokit'

#Provide authentication credentials
Octokit.configure do |c|
 c.login = 'chenyukang'
 c.password = '*******'
end

# Fetch the current user
def all_users()
  all_users = []
  while true
    users =  Octokit.all_users(:since => all_users.size())
    puts users.size()
    if all_users.size() >= 500
      break
    end
    all_users += users
  end
  all_users
end


def all_followers(user_name)
  puts "getting #{user_name}"
  followers = []
  Octokit.auto_paginate = true
  while true
    f = Octokit.followers(user_name, :since => followers.size())
    puts followers.size()
    if f.size() == 0
      break
    end
    followers += f
  end
  followers
end

res = all_users()
puts res.size()

res.each { |u|
  follows = all_followers(u.login)
  print "#{u.login}  #{follows.size()}\n"
}


#puts user.name
#puts user.fields
#puts user.email
