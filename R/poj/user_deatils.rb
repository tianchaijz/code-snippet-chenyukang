#!/usr/bin/env ruby
# coding: utf-8

## Read users from RankList, and get details for users,
## mainly get the school etc.

require 'nokogiri'
require 'restclient'

USER_URL = "http://poj.org/userstatus?user_id="

rank = IO.readlines("RankList")

def get_school_from_user(user_name)
  url = USER_URL + user_name
  page = Nokogiri::HTML(RestClient::get(url))
  res = page.css("table tr")[11].css("td").text.split(":")[1].strip()
  res
end

#puts get_school_from_user("chenyukang")
#puts get_school_from_user("bnucollect")
#puts get_school_from_user("Sempr")
first = rank.slice!(0)

first =  first.gsub("\n", "") + " \"school\"\n"
output = File.new("table", "w")
puts first
output.write(first)
output.close()
puts rank.size()
rank = rank.slice(0, 100)
puts  rank.size()
rank.each { |line|
  name = line.split(" ")[2]
  name = name[1, name.size() - 2]
  school = get_school_from_user(name)
  puts name, school
}
