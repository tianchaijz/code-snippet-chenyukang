if(!require(XML)) install.packages('XML')
library(XML)


## NOTE: remove the first row
extract <- function(afactor) {
  res = sapply(afactor, function(x) as.numeric(gsub('[^0-9]','', x)))
  res = res[-1]
  res
}

## fetch from start -> start + 500
fetch <- function(start,
                  warning=function(w) {
                    print(paste('warning:',w));
                    browser()},
                  error=function(e) {
                    print(paste('e:',e));
                    browser()}
                  ) {
  url = sprintf("http://poj.org/userlist?start=%d&size=500&of1=solved&od1=desc&of2=submit&od2=asc", start)
  tables = readHTMLTable(url)
  ## table 6 is rank list
  users <- tables[[6]]
  name <- sapply(users$V2, as.character)
  name <- name[-1]
  rank <- extract(users$V1)
  solved <- extract(users$V4)
  submit <- extract(users$V5)
  rate <- extract(users$V6)
  data.frame(rank, name, solved, submit, rate)
}

main <- function() {
  rank = name = solved = submit = rate = NULL
  for (i in 0:20) {
    print(sprintf("processing: %d", i))
    res = fetch(500*i)
    rank = c(rank, res$rank)
    name = c(name, as.vector(res$name))
    solved = c(solved, res$solved)
    submit = c(submit, res$submit)
    rate = c(rate, res$rate)
  }
  final <- data.frame(rank, name, solved, submit, rate)
  write.table(final, "RankList")
  #final
}

#main()
#rank = read.table("RankList", header = TRUE)
