#include <stdio.h>
#include <string.h>
#define MAX 300002

int visit[MAX];

int dr[4] = {-1, 0, 1,  0};
int dc[4] = {0, -1, 0, 1};

int idx(int r, int c, int N) {
    return r * N + c;
}

int inmap(int x, int y, int N, int M) {
    if(x >= 0 && x < N && y >= 0 && y < M)
        return 1;
    return 0;
}

void dfs(int **A, int x, int y, int N, int M) {
    for(int i=0; i<4; i++) {
        int r = x + dr[i];
        int c = y + dc[i];
        if(inmap(r, c, N, M) && !visit[idx(r, c, N)] && A[r][c] == A[x][y]) {
            visit[idx(r, c, N)] = 1;
            dfs(A, r, c, N, M);
        }
    }
}

int solution(int **A, int N, int M) {
    int res = 0;
    memset(visit, 0, sizeof(visit));
    for(int i=0; i<N; i++) {
        for(int j=0; j<M; j++) {
            if(!visit[idx(i, j, N)]) {
                visit[idx(i, j, N)] = 1;
                res++;
                dfs(A, i, j, N, M);
            }
        }
    }
    return res;
}

int main() {
    int A[7][3] = {{5, 4, 4}, {4, 3, 4}, {3, 2, 4}, {2, 2, 2}, {3, 3, 4}, {1, 4, 4}, {4, 1, 1}};
    int res = solution(A, 7, 3);
    printf("res: %d", res);
    
}
