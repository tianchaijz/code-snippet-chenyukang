#include <stdio.h>

struct object {
  int type;
  int content;
};

struct blob {
  struct object obj;
};


int main() {
  printf("object size: %lu\n", sizeof(struct object));
  printf("blob size: %lu\n", sizeof(struct blob));
  return 0;
}



