/*******************************************************************************
 *
 *      3.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2015-04-14 23:09:49
 *
 *******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int cmp(const void * a, const void * b){
    return ( *(int*)a - *(int*)b );
}

int solution(int A[], int N) {
    if(N < 3) return 0;
    qsort(A, N, sizeof(int), cmp);
    for(int i=2; i<N; i++) {
        int sum = A[i-2] + A[i-1];
        if(sum > A[i]) {
            return sum + A[i];
        }
    }
    return -1;
}


int main() {
    return 0;
}
