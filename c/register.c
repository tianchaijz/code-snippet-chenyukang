/*******************************************************************************
 *
 *      register.c
 *
 *      @brief
 *
 *      @author   Yukang Chen  @date  2014-09-04 11:19:19
 *
 *******************************************************************************/

#include <stdio.h>
#include <string.h>

#if 0
/* error: address of register variable requested */
int test_register() {
    register int a = 0;
    int* b = &a;
    printf("b value is : %x", (unsigned int)b);
    return 0;
}
#endif

/* register is used as a hint for compiler,
   but some compliers just ignore this hint, and it's valid,
   also some compliers will have some auto optimaztion for register allocations,
   so it's better to leave this kind of task for compilers */
int main() {
  int SIZE = 100000;
  int array[SIZE];
  register unsigned int a, b, c;
  int n;
  for (n = 0; n < SIZE; ++n){
        c = a + b;
        b = a;
        a = c;
        array[n] = c;
    }
    return 0;
}
