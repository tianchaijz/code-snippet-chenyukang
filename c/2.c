#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#define INV 1<<2

int trans(int v) {
    if(v > 0) return 1;
    if(v < 0) return -1;
    return 0;
}

int valid(int b, int dir) {
    if (dir == 1 && b <= 0)
        return -1;
    if (dir == -1 && b >= 0)
        return 1;
    if (dir == 0)
        return trans(b);
    return INV;
}

int solution(int A[], int N) {
    if(N == 0) {
        return 0;
    }
    int res = 1;
    int cnt = 1;
    int dir = trans(A[0]);
    for(int i=1; i< N; i++) {
        dir = valid(A[i], dir);
        if(dir != INV) {
            cnt++;
            if(cnt > res) {
                res = cnt;
            }
        } else {
            cnt = 1;
            dir = trans(A[i]);
        }
    }
    return res;
}

int main() {
    int v[12] =  {5, 4, -3, 2, 0, 1, -1, 0, 2, -3, 4, -5};
    int res = solution(v, 12);
    printf("res: %d\n", res);

    int v2[5] =  {0, 1, 0, -1, 1};
    int res2 = solution(v2, 5);
    printf("res: %d\n", res2);
    return 0;
}

    
