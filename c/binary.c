#include <stdio.h>
#include <stdlib.h>

/* return index if found key, else return -1 */
int iter(int *data, int left, int right, int key) {
    int mid = left + ((right - left) >> 1);
    if (data[mid] == key ) return mid;
    if (data[mid] > key) return iter(data, left, mid - 1, key);
    if (data[mid] < key) return iter(data, mid + 1, right, key);
    return -1;
}

int bi_search(int *data, int len, int key) {
    return iter(data, 0, len-1, key);
}

int main() {
    int arr[1] = {0};
    int idx = bi_search(arr, 1, 0);
    printf("idx: %d\n", idx);
}
