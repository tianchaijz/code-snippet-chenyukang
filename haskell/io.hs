import Control.Monad

-- main = do
--   putStrLn "Hello, What's your name:"
--   line <- getLine
--   if null line
--      then return ()
--      else do
--        putStrLn $ reverseWords line
--        main

reverseWords :: String -> String
reverseWords  = unwords . map reverse . words


myPutStr :: String -> IO()
myPutStr [] = return ()
myPutStr (x:xs) = do
  putChar x
  myPutStr xs


main = do
  c <- getChar
  when (c /= ' ') $ do
       putChar c
       main
