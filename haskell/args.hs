import System.Environment
import Data.List

main = do
  args <- getArgs
  progName <- getProgName
  putStrLn "progName is: "
  putStrLn progName
  putStrLn "args is: "
  mapM putStrLn args
