
import System.IO

main = do
  handle <- openFile "cat.hs" ReadMode
  contents <- hGetContents handle
  putStrLn contents
  hClose handle
