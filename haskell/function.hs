
lucky :: (Integral a) => a -> String
lucky 7 = "Lucky Number"
lucky x = "Sorry, not lucky"

fact :: (Integral a) => a -> a
fact 0 = 1
fact n = n * fact (n - 1)


bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
        | bmi <= 18.5 = "first"
        | bmi <= 25.0 = "second"
        | bmi <= 30.0 = "third"
        | otherwise  = "you are wired"

max' :: (Ord a) => a -> a -> a
max' a b
     | a > b = a
     | otherwise = b


myCompare :: (Ord a) => a -> a -> Ordering
a `myCompare` b
  | a > b = GT
  | a == b  = EQ
  | otherwise = LT

removeNonUpperCase :: [Char] -> [Char]
removeNonUpperCase st = [ c | c <- st, c `elem` ['A' .. 'Z']]

addThree:: Int -> Int -> Int -> Int
addThree x y z = x + y + z

my_fact :: Integer -> Integer
my_fact n = product [1..n]

length' :: (Num a) => [a] -> Integer
length' [] = 0
length' (_ : xs) = 1 + length' xs

sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

capital' :: String -> String
capital' "" = "Empty string, Fuck"
capital' all@(x:xs) = "The first of " ++ all ++ " is " ++ [x]


bmiTell' :: (RealFloat a) => a -> a -> String
bmiTell' weight height
    | weight / height ^ 2 <= 18.5 = "You're underweight, you emo, you!"
    | weight / height ^ 2 <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | weight / height ^ 2 <= 30.0 = "You're fat! Lose some weight, fatty!"
    | otherwise                 = "You're a whale, congratulations!"


head' :: [a] -> a
head' xs = case xs of [] -> error "No head for empty list!"
                      (x: _) -> x
